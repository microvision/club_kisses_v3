<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_model extends MY_Model {
	protected $table_name = 'vision_products';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_date';

    public function get_search_products($product)
    {
        return $this->db->query("SELECT bf_vision_products.*,unit_name FROM bf_vision_products  
									LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id									
									WHERE (product_name LIKE '%".$product."%') and bf_vision_products.status=1 ORDER BY product_name")->result();
    }
    public function stock_check($productId)
    {
        return $this->db->query("SELECT SUM(current_qty) as active_stock, product_id,buying_price FROM  bf_vision_stock_shop WHERE  (status = 1 or status=0)  and product_id ='".$productId."' ")->row();
    }
    public function get_active_stock($itemId)
    {
        return $this->db->query("SELECT * FROM bf_vision_stock_shop WHERE product_id='".$itemId."' and status=1 LIMIT 1")->row();
    }
    public function update_stock($stockId, $currentquantity,$status)
    {
        $this->db->query("UPDATE bf_vision_stock_shop set current_qty='".$currentquantity."', status='".$status."'  WHERE id='".$stockId."' ");
    }
    public function get_latest_inactivestockdate($productId){
        return $this->db->query("select purchased_date as purchasedate from bf_vision_stock_shop where product_id='".$productId."' and status=0 ORDER BY purchased_date LIMIT 1")->row();
    }
    public function activate_stock($productid, $purchasedate)
    {
        $this->db->query("UPDATE bf_vision_stock_shop set status= 1  WHERE status = 0 and  product_id='".$productid."' and purchased_date = '".$purchasedate."' ");
    }
    public function get_stock_value($itemID)
    {
        return $this->db->query("SELECT * FROM bf_vision_stock_shop WHERE product_id='".$itemID."' and status = 1 LIMIT 1")->row();
    }
    public function get_last_inactive_stock($itemID)
    {
        return $this->db->query("SELECT max(id) as id FROM bf_vision_stock_shop WHERE product_id='".$itemID."'")->row();

    }
    public function update_last_inactive_stock($stockID)
    {
        $this->db->query("UPDATE bf_vision_stock_shop set status=1 where id='".$stockID."'");
    }
    public function update_stock_value($stockId, $new_stock_value)
    {
        $this->db->query("UPDATE bf_vision_stock_shop SET current_qty='".$new_stock_value."' WHERE id='".$stockId."' ");
    }
}