<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_accounts_model extends MY_Model {
	protected $table_name = 'vision_corporate_client_transactions';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


	public function get_sale_transaction($sale_id, $client_id){
	    return $this->db->query("SELECT * FROM bf_vision_corporate_client_transactions WHERE transaction_type=2 and transaction_no='".$sale_id."' and client_id='".$client_id."' LIMIT 1")->row();
    }
}