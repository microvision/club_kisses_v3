<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends MY_Model {
	protected $table_name = 'vision_corporate_clients';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_search_products($product)
    {
        return $this->db->query("SELECT bf_vision_products.*,unit_name FROM bf_vision_products  
									LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id									
									WHERE (product_name LIKE '%".$product."%') and bf_vision_products.status=1 ORDER BY product_name")->result();
    }
	
}