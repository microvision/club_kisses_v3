<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" required="required" name="start_time" value="06:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" name="end_time" required="required" value="20:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($sales_summary)): ?>
                                <?php echo "<h3>&nbsp;Business Summary Report</h3><h6><i>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")</i></h6>"; ?>
                                <button  class="btn btn-success btn-sm btn-rect pull-right" onclick="return popitup('<?php echo base_url(); ?>reports/business_summary/print_report?start=<?php echo $startdate; ?>&end=<?php echo $enddate; ?>')"><i class="fa fa-print"></i> Print Summary</button>
                                <hr>
                                <div id="table_wrapper">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="box">
                                            <header>
                                                <h4>Sales Summary</h4>

                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <td>Total Sales</td>
                                                        <td>Ksh <?php echo number_format($sales_summary->Sales,2); ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Credit Payments </td>
                                                        <td>Ksh <?php echo number_format($total_client_payments->payments,2); ?></td>
                                                    </tr>


                                                    <tr>
                                                        <td><b>TOTAL SALES</b></td>
                                                        <td><b>Ksh <?php echo number_format($sales_summary->Sales+$total_client_payments->payments,2); ?></b></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box">
                                            <header>
                                                <h4>Sales BreakDown</h4>
                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed responsive-table">

                                                    <tbody>
                                                    <?php
                                                    foreach($sales_breakdown as $row):
                                                        $p_option = "";
                                                        if($row->payment_option==1){
                                                            $p_option = "Cash Sales";
                                                        }elseif($row->payment_option==2){
                                                            $p_option = "Mpesa Sales";
                                                        }elseif($row->payment_option==3){
                                                            $p_option = "Credit Sales";
                                                        }elseif($row->payment_option==4){
                                                            $p_option = "Cheque Sales";
                                                        }elseif($row->payment_option==5){
                                                            $p_option = "Debit Card Sales";
                                                        }
                                                        ?>
                                                        <tr>

                                                            <td><?php echo $p_option;?></td>
                                                            <td align="right"><?php echo number_format($row->Sales,2);?></td>

                                                        </tr>
                                                    <?php endforeach; ?>
													<tr>
														<td colspan="2">
														<table width="100%"><tr>
															<?php foreach($client_breakdown as $row){
																$p_option = "";
																if($row->bill_type==1){
																	$p_option = "Shop Sales";
																}elseif($row->bill_type==2){
																	$p_option = "Corporate Client";
																}elseif($row->bill_type==3){
																	$p_option = "Distributors";
																}
																echo "<td align='center'><small><b>".ucfirst($p_option)."</b><br>Ksh. ".number_format($row->Sales)." </small></td>";
															}
															?></tr>
														</table>
														</td>
													</tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row"><hr>
                                    <div class="col-lg-6">
                                        <div class="box">
                                            <header>
                                                <h4>Distributors Sales Summary</h4>

                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed responsive-table">
                                                   <thead>
                                                    <tr>
                                                        <th>Distributor</th>
                                                        <th># Sales</th>
                                                        <th>Total Sales</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $num=0; foreach($distributor_summary as $row): $num++;?>
                                                        <tr>

                                                            <td><?php echo $row->distributor;?></td>
                                                            <td><?php echo $row->no_sales; ?></td>
                                                            <td>Ksh <?php echo number_format($row->TSales);?></td>

                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box">
                                            <header>
                                                <h4>Current Stock Summary</h4>
                                        <br>
                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed table-striped responsive-table">
                                                    <tbody>
                                                    <tr>
                                                        <td>Purchase Stock Value</td>
                                                        <td align="right">Ksh <?php echo number_format($stock_summary->PValue,2); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Retail Sale Stock Value</td>
                                                        <td align="right">Ksh <?php echo number_format($stock_summary->RValue,2); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Distribution Sale Stock Value</td>
                                                        <td align="right">Ksh <?php echo number_format($stock_summary->DValue,2); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Corporate Sale Value</td>
                                                        <td align="right">Ksh <?php echo number_format($stock_summary->WValue,2); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Products</td>
                                                        <td align="right"><?php echo number_format($stock_summary->products); ?> Products</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12"><hr>
                                        <div class="box">
                                            <header>
                                                <h4>Corporate Clients Summary</h4>

                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed responsive-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Client</th>
                                                        <th><span class="pull-right">Total Sales(Ksh)</span></th>
                                                        <th><span class="pull-right">Current Bal(Ksh)</span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $num=0; foreach($corporate_summary as $row): $num++;?>
                                                        <tr>
                                                            <td width="50%"><?php echo $row->client;?></td>
                                                            <td align="right"><?php echo number_format($row->TSales,2);?></td>
                                                            <td align="right"><?php echo number_format($row->account_bal,2); ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12"><hr>
                                        <div class="box">
                                            <header>
                                                <h4>Products Summary Summary</h4>

                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed responsive-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th><span class="pull-right">Current Stock</span></th>
                                                        <th><span class="pull-right">Total Sales(Ksh)</span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $num=0; foreach($top_profits as $row): $num++;?>
                                                        <tr>
                                                            <td width="50%"><?php echo $row->product_name;?></td>
                                                            <td align="right"><?php echo number_format($row->current_stock); ?> Pieces</td>
                                                            <td align="right"><?php echo number_format($row->sales,2);?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12"><hr>
                                        <div class="box">
                                            <header>
                                                <h4>Expenses</h4>

                                            </header>
                                            <div id="condensedTable" class="body collapse in">
                                                <table class="table table-condensed responsive-table">
                                                    <thead>
                                                    <tr>
                                                        <th>Received By</th>
                                                        <th>Description</th>
                                                        <th><span class="pull-right">Amount(Ksh)</span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $t=0; $num=0; foreach($expenses as $row): $num++; $t=$t+$row->amount; ?>
                                                        <tr>
                                                            <td><?php echo $row->received_by;?></td>
                                                            <td width="60%"><?php echo $row->comments; ?></td>
                                                            <td align="right" width="10%"><?php echo number_format($row->amount,2);?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    <tr>
                                                        <td colspan="2"><b>TOTAL</b></td>
                                                        <td align="right"><b><?php echo number_format($t,2);?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><b>CLOSING BAL</b></td>
                                                        <td align="right"><b><?php echo number_format($expenses_closing_bal->amount,2);?></b></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
