<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Signal POS - Business Report</title>
<script>
    //window.onunload = refreshParent;
    function refreshParent() {
        
		
    }
	window.onload= function () {   
	refreshParent();  
	//printreceipt();
	var count = 0;
	while (count<3){
	window.print();
	count=count+1;
	}
	
	
   }
	
	window.onfocus=function () {   print ();   }
	
	//setTimeout(funtion(){window.close();},9);
</script>
<style type="text/css">
        .style1 {
            font-family: cambria,Agency FB;
            font-size: 22px
        }
		.style9 {
            font-family: cambria;
            font-size: 16px;
        }		
        .style2 {
            font-family: calibri;
            font-size: 17px;
        }

        .style5 {
			font-family: calibri;
            font-size: 18px;
        }

        
        .style3 {
            font-family: calibri;
            font-size: 13px;
        }

        .style10 {
            font-family: calibri;
            font-size: 20px;
        }

        .style15 {
            font-family: calibri;
            font-size: 17px;
        }

    </style>
</head>

<body  onFocus= "window.close()">
<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
<tr>
    <td colspan="4" align="center" valign="middle"><span class="style1">
	<?php


		echo "Business Summary Report ";
        echo "<br><span class=\"style9\"> (".$startdate."  ~  ".$enddate.") </span>";
	?>
	</span>
	</td>	
</tr>
  <tr><td colspan="4" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
</table>
<h4>Sales Summary</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <tr>
        <td>Total Sales</td>
        <td>Ksh <?php echo number_format($sales_summary->Sales,2); ?></td>
    </tr>

    <tr>
        <td>Credit Payments </td>
        <td>Ksh <?php echo number_format($total_client_payments->payments,2); ?></td>
    </tr>


    <tr>
        <td><b>TOTAL SALES</b></td>
        <td><b>Ksh <?php echo number_format($sales_summary->Sales+$total_client_payments->payments,2); ?></b></td>
    </tr>
</table>
<h4>Sales BreakDown</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <tbody>
    <?php
    foreach($sales_breakdown as $row):
        $p_option = "";
        if($row->payment_option==1){
            $p_option = "Cash Sales";
        }elseif($row->payment_option==2){
            $p_option = "Mpesa Sales";
        }elseif($row->payment_option==3){
            $p_option = "Credit Sales";
        }elseif($row->payment_option==4){
            $p_option = "Cheque Sales";
        }elseif($row->payment_option==5){
            $p_option = "Debit Card Sales";
        }
        ?>
        <tr>

            <td><?php echo $p_option;?></td>
            <td align="right"><?php echo number_format($row->Sales,2);?></td>

        </tr>
    <?php endforeach; ?>
	<tr>
														<td colspan="2">
														<table width="100%"><tr>
															<?php foreach($client_breakdown as $row){
																$p_option = "";
																if($row->bill_type==1){
																	$p_option = "Shop Sales";
																}elseif($row->bill_type==2){
																	$p_option = "Corporate Client";
																}elseif($row->bill_type==3){
																	$p_option = "Distributors";
																}
																echo "<td align='center'><small><b>".ucfirst($p_option)."</b><br>Ksh. ".number_format($row->Sales)." </small></td>";
															}
															?></tr>
														</table>
														</td>
													</tr>
    </tbody>
</table>
<h4>Distributors Sales Summary</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <thead>
    <tr>
        <th>Distributor</th>
        <th># Sales</th>
        <th>Total Sales</th>
    </tr>
    </thead>
    <tbody>
    <?php $num=0; foreach($distributor_summary as $row): $num++;?>
        <tr>

            <td><?php echo $row->distributor;?></td>
            <td><?php echo $row->no_sales; ?></td>
            <td>Ksh <?php echo number_format($row->TSales);?></td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<h4>Stocks Summary</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <tr>
        <td>Purchase Stock Value</td>
        <td align="right">Ksh <?php echo number_format($stock_summary->PValue,2); ?></td>
    </tr>
    <tr>
        <td>Retail Sale Stock Value</td>
        <td align="right">Ksh <?php echo number_format($stock_summary->RValue,2); ?></td>
    </tr>
    <tr>
        <td>Distribution Sale Stock Value</td>
        <td align="right">Ksh <?php echo number_format($stock_summary->DValue,2); ?></td>
    </tr>
    <tr>
        <td>Corporate Sale Value</td>
        <td align="right">Ksh <?php echo number_format($stock_summary->WValue,2); ?></td>
    </tr>
    <tr>
        <td>Number of Products</td>
        <td align="right"><?php echo number_format($stock_summary->products); ?> Products</td>
    </tr>
</table>
<h4>Corporate Clients Summary</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <thead>
    <tr>
        <th>Client</th>
        <th><span class="pull-right">Sales(Ksh)</span></th>
        <th><span class="pull-right">Dues(Ksh)</span></th>
    </tr>
    </thead>
    <tbody>
    <?php $num=0; foreach($corporate_summary as $row): $num++;?>
        <tr>
            <td width="50%"><?php echo $row->client;?></td>
            <td align="right"><?php echo number_format($row->TSales,2);?></td>
            <td align="right"><?php echo number_format($row->account_bal,2); ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<h4>Products Summary Summary</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <thead>
    <tr>
        <th>Product</th>
        <th><span class="pull-right">Stocks</span></th>
        <th><span class="pull-right">Sales(Ksh)</span></th>
    </tr>
    </thead>
    <tbody>
    <?php $num=0; foreach($top_profits as $row): $num++;?>
        <tr>
            <td width="50%"><?php echo $row->product_name;?></td>
            <td align="right"><?php echo number_format($row->current_stock); ?> Pieces</td>
            <td align="right"><?php echo number_format($row->sales,2);?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<h4>Expenses</h4>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <thead>
    <tr>
        <th>Received By</th>
        <th>Description</th>
        <th><span class="pull-right">Amount(Ksh)</span></th>
    </tr>
    </thead>
    <tbody>
    <?php $t=0; $num=0; foreach($expenses as $row): $num++; $t=$t+$row->amount; ?>
        <tr>
            <td><?php echo $row->received_by;?></td>
            <td><?php echo $row->category; ?></td>
            <td align="right" width="10%"><?php echo number_format($row->amount,2);?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2"><b>TOTAL</b></td>
        <td align="right"><b><?php echo number_format($t,2);?></b></td>
    </tr>
    <tr>
        <td colspan="2"><b>CLOSING BAL</b></td>
        <td align="right"><b><?php echo number_format($expenses_closing_bal->amount,2);?></b></td>
    </tr>
</table>
<p align="center">-------- END -------</p>
</body>
</html>

