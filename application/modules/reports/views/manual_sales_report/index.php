<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" required="required" name="start_time" value="06:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" name="end_time" required="required" value="20:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($sales_summary)):?>
                                <span class="h4 text-danger"><?php echo "&nbsp;Manual Receipts Report</span><h6>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")"; ?></i></h6>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button><br><hr>
                                <div id="table_wrapper">
                                    <table id="dataTable" class="table table-bordered table-hover  dataTable">
                                        <thead>
                                        <tr>
                                            <th width="3%">#</th>
                                            <th>Sale Date</th>
                                            <th>System Receipt</th>
                                            <th>Manual Receipt</th>
                                            <th>Sales Amount</th>
                                            <th>Entered By</th>
                                            <th>Entered On</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $num=0; $total_sales=0; $total_profit=0; $VATS=0; foreach($sales_summary as $row): $num++; ?>
                                            <tr>
                                                <td width="2%"><?php echo $num; ?></td>
                                                <td><?php echo date('d-M-Y',strtotime($row->billing_time)); ?></td>
                                                <td><?php echo $row->id; ?></td>
                                                <td><?php echo $row->manual_receipt; ?></td>
                                                <td>Ksh. <?php echo number_format($row->sales,2); ?></td>
                                                <td><?php echo $row->display_name; ?></td>
                                                <td><?php echo date('d-M-Y h:i a',strtotime($row->created_on)); ?></td>

                                            </tr>
                                        <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
