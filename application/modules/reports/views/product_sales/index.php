<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                        <div class="col-lg-12">
                            <div class="col-lg-5">
                                <div class="form-group has-success">
                                    <label class="control-label ">Select Product</label><br>
                                    <div class="col-lg-11">
                                        <select  name="product" required="required"  class="select2 form-control">
                                            <option value="" disabled selected>Select Product</option>
                                            <?php foreach($products as $p): ?>
                                                <option value="<?php echo $p->id; ?>"><?php echo $p->product_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">


                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" required="required" name="start_time" value="06:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" name="end_time" required="required" value="20:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                        </div>
                        </form>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($product_details)):?>
                                <span class="h4 text-danger"><?php echo "&nbsp;".$product_details->product_name." Sales Report</span><h6>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")"; ?></i></h6>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button><br><hr>
                                <div id="table_wrapper">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Sale Date</th>
                                            <th>Receipt #</th>
                                            <th>Purchase Price</th>
                                            <th>Sell Price</th>
                                            <th>Qty Sold</th>
                                            <th>Total Sale</th>
                                            <th>Profit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $num=0; $total_sales=0; $total_profit=0; $qty = 0; foreach($product_report as $row): $num++;?>
                                            <tr>
                                                <td width="3%"><?php echo $num; ?></td>
                                                <td><?php echo date('d-M-Y h:i a',strtotime($row->billing_time)); ?></td>
                                                <td><?php echo $row->bill_id; ?></td>
                                                <td>Ksh. <?php echo number_format($row->buying_price,2); ?></td>
                                                <td>Ksh. <?php echo number_format($row->selling_price,2); ?></td>
                                                <td><?php echo $row->qty; $qty = $qty+$row->qty;?> </td>
                                                <td>Ksh. <?php echo number_format($row->total,2);  $total_sales = $total_sales + $row->total; ?></td>
                                                <td>Ksh. <?php echo number_format($row->profit,2); $total_profit = $total_profit + $row->profit; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table><h4>
                                        <table class="table table-striped">
                                            <thead>
                                            <tr >
                                                <td align="center"><b>Quantity Sold</b></td>
                                                <td align="center"> <b>Total Sales</b> </td>
                                                <td align="center" ><b>Total Profits </b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="active">
                                                <td align="center"><?php if(ISSET($product_details->unit_name)){echo number_format($qty)." ".$product_details->unit_name;} ?></td>
                                                <td align="center">Ksh. <?php echo number_format($total_sales,2); ?></td>
                                                <td align="center">Ksh. <?php echo number_format($total_profit,2); ?></td>
                                            </tr>
                                            </tbody>
                                        </table></h4>


                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
