<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-4">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label><br>
                                        <div class="col-lg-7">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" required="required" name="start_time" value="06:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label><br>
                                        <div class="col-lg-7">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" name="end_time" required="required" value="20:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Show Sold Products" class="btn btn-warning">
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit2" value="Unsold Products" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($sales_summary)):?>
                                <span class="h4 text-danger"><?php echo "&nbsp;Sales Summary Report</span><h6>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")"; ?></i></h6></span>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button><br><hr>
                                <div id="table_wrapper">
                                    <table id="dataTable" class="table table-bordered table-hover  dataTable">
                                <thead>
                                <tr>
                                    <th width="2%">#</th>
                                    <th>Products</th>
                                    <th>Current Stock</th>
                                    <th>Qty Sold</th>
                                    <th>Total Sales</th>
                                    <th>Input VAT</th>
                                    <th>Output VAT</th>
                                    <th>Sales Revenue</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $num=0; $total_sales=0; $total_profit=0; $VATS=0; $iVAT=0; $oVAT=0; foreach($sales_summary as $row): $num++; $VATS=$VATS+$row->VATS;?>
                                    <tr>
                                        <td width="2%"><?php echo $num; ?></td>
                                        <td><?php echo $row->product_name; ?></td>
                                        <td><?php echo $row->current_stock; ?></td>
                                        <td><?php echo number_format($row->qty,1); ?></td>
                                        <td><?php echo number_format($row->sales,2); $total_sales = $total_sales + $row->sales; ?></td>
                                        <td><?php echo number_format($row->in_put_vat,1); $iVAT=$iVAT+$row->in_put_vat; ?></td>
                                        <td><?php echo number_format($row->out_put_vat,1); $oVAT= $oVAT+$row->out_put_vat; ?></td>
                                        <td><?php $p = $row->sales-$row->profits; echo number_format($p,2); $total_profit = $total_profit + ($row->sales-$row->profits-($row->out_put_vat-$row->in_put_vat)); ?></td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>

											<table class="table table-striped">
												<thead>
												  <tr >
													<td align="center"> <b>Total Sales</b> </td>
													<td align="center" ><b>Total Profits </b></td>
													<td align="center" ><b>Input VAT </b></td>
													<td align="center" ><b>Output VAT </b></td>
													<td align="center" ><b>Total VAT </b></td>
													<td align="center" ><b>After Tax Profits </b></td>
												  </tr>
												</thead>
												<tbody>
													<tr class="active">
														<td align="center">Ksh <?php echo number_format($total_sales,2); ?></td>
														<td align="center">Ksh <?php echo number_format($total_profit,2); ?></td>
														<td align="center">Ksh <?php echo number_format($iVAT,2); ?></td>
														<td align="center">Ksh <?php echo number_format($oVAT,2); ?></td>
														<td align="center">Ksh <?php echo number_format($oVAT-$iVAT,2); ?></td>
														<td align="center">Ksh <?php echo number_format($total_profit-($oVAT-$iVAT),2); ?></td>
													</tr>
												</tbody>
											</table>

                                        <table class="table table-striped">
                                            <thead>
                                            <tr >
                                                <td align="center"> <b>Total Sales</b><small>(Pay Options)</small> </td>
                                                <?php
                                                    foreach($sales_breakdown as $row):
                                                        $p_option = "";
                                                        if($row->payment_option==1){
                                                            $p_option = "Cash Sales";
                                                        }elseif($row->payment_option==2){
                                                            $p_option = "Mpesa Sales";
                                                        }elseif($row->payment_option==3){
                                                            $p_option = "Credit Sales";
                                                        }elseif($row->payment_option==4){
                                                            $p_option = "Cheque Sales";
                                                        }elseif($row->payment_option==5){
                                                            $p_option = "Debit Card Sales";
                                                        }
                                                ?>

                                                    <?php echo "<td align=\"center\" ><span class=\"text-info\"><b>".ucfirst($p_option)."</b></span></td>"; ?>

                                                <?php endforeach; ?>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="active">
                                                <td align="center">Ksh. <?php echo number_format($total_sales,2); ?></td>
                                                <?php foreach($sales_breakdown as $row): ?>
                                                    <?php echo "<td align=\"center\" ><span class=\"text-info\">Ksh. ".number_format($row->Sales)."</span></td>"; ?>
                                                <?php endforeach; ?>

                                            </tr>
                                            </tbody>
                                        </table>
                                    <hr>
                                    <span class="text-success">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr >
                                            <td align="center"> <b>Total Sales</b><small>(Client Type)</small> </td>
                                            <?php foreach($client_breakdown as $row):
                                                $p_option = "";
                                                if($row->bill_type==1){
                                                    $p_option = "Shop Sales";
                                                }elseif($row->bill_type==2){
                                                    $p_option = "Corporate Client Sales";
                                                }elseif($row->bill_type==3){
                                                    $p_option = "Distributors";
                                                }

                                                ?>

                                                <?php echo "<td align=\"center\" ><span class=\"text-success\"><b>".ucfirst($p_option)."</b></span></td>"; ?>

                                            <?php endforeach; ?>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="active">
                                            <td align="center">Ksh. <?php echo number_format($total_sales,2); ?></td>
                                            <?php foreach($client_breakdown as $row): ?>
                                                <?php echo "<td align=\"center\" ><span class=\"text-success\">Ksh. ".number_format($row->Sales)."</span></td>"; ?>
                                            <?php endforeach; ?>

                                        </tr>
                                        </tbody>
                                    </table>
                                </span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
