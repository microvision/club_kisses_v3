<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group has-success">
                                            <label class="control-label ">Select User</label>
                                            <div class="col-lg-11">
                                               <select name="user" class="form-control chzn-select" required="">
                                                   <option selected disabled value="">Select User</option>
                                                   <?php foreach($users as $row): ?>
                                                   <option value="<?php echo $row->id ?>"><?php echo $row->display_name; ?></option>
                                                   <?php endforeach; ?>
                                               </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group has-success">
                                            <label class="control-label">Start Date</label><br>
                                            <div class="col-lg-6">
                                                <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="time" id="time2" required="required" name="start_time" value="06:00" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group has-success">
                                            <label class="control-label">End Date</label><br>
                                            <div class="col-lg-6">
                                                <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            </div>
                                            <div class="col-lg-5">
                                                <input type="time" id="time2" name="end_time" required="required" value="20:00" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($user_details)):?>
                            <h4>&nbsp;Sales Report For <?php echo $user_details->display_name; ?> </h4><h6><i>&nbsp; &nbsp; Sales period <?php echo "(".$startdate."  ~  ".$enddate.")"; ?></i></h6> <hr>
                                <div id="tabs" class="tabs">
                                    <ul class="nav nav-tabs" id="iconsTab">
                                        <li class="active">
                                            <a href="#summary" data-toggle="tab">Sales Summary</a>
                                        </li>
                                        <li> <a href="#details" data-toggle="tab">Detailed Sales</a> </li>
                                    </ul>

                                    <div class="tab-content">
                                    <div class="tab-pane active" id="summary">
                                        <div class="box">
                                            <header>
                                                <h4>Sales Summary </h4>
                                                <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button>
                                            </header>
                                            <div class="body">
                                                <div id="table_wrapper">
                                                    <div class="row">
                                                        <div class="col-lg-12" id="table_wrapper">

                                                            <table class="table table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Product</th>
                                                                    <th>Selling Price</th>
                                                                    <th>Quantity Sold</th>
                                                                    <th><span class="pull-right">Total Sales(Ksh)</span></th>
                                                                    <th><span class="pull-right">Sales Profits(Ksh)</span></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $num=0; $total_sales=0; $total_profit=0; $VATS=0; foreach($sales_summary as $row): $num++; ?>
                                                                    <tr>
                                                                        <td width="2%"><?php echo $num; ?></td>
                                                                        <td><?php echo $row->product_name; ?></td>
                                                                        <td>Ksh.<?php echo number_format($row->selling_price,2); ?></td>
                                                                        <td><?php echo number_format($row->qty,1)." ".$row->unit_name; ?></td>
                                                                        <td align="right"><?php echo number_format($row->sales,2); $total_sales = $total_sales + $row->sales;?></td>
                                                                        <td align="right"><?php echo number_format($row->profits,2); $total_profit = $total_profit + $row->profits; ?></td>

                                                                    </tr>
                                                                <?php endforeach; ?>
                                                                </tbody>
                                                            </table><h4>
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                    <tr >
                                                                        <td align="center"> <b>Total Sales</b> </td>
                                                                        <td align="center" ><b>Total Profits </b></td>

                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr class="active">
                                                                        <td align="center">Ksh. <?php echo number_format($total_sales,2); ?></td>
                                                                        <td align="center">Ksh. <?php echo number_format($total_profit,2); ?></td>

                                                                    </tr>
                                                                    </tbody>
                                                                </table></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="details">
                                            <br>
                                            <?php foreach($bills as $rows):
                                                if($rows->status==0){$status="Deleted";}elseif($rows->status==1){$status="Active";}elseif($rows->status==2){$status="Paid";}elseif($rows->status==3){$status="Credit";}elseif($rows->status==4){$status="Reactivated";}
                                                ?>
                                                <div class="row">
                                                    <div class="col-lg-2"><h5>Receipt Number: <?php echo $rows->id;?></h5> </div>
                                                    <div class="col-lg-4"> <h5>Sale Ref: <?php echo $rows->bill_alias;?></h5></div>
                                                    <div class="col-lg-4"> <h5>Sale Date: <?php echo date('d-M-Y H:i a',strtotime($rows->created_on));?></h5></div>
                                                    <div class="col-lg-2"><h5>Status: <?php echo $status;?></h5> </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-2"> </div>
                                                    <div class="col-lg-8"><h5>
                                                            <table class="table table-condensed responsive-table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Product</th>
                                                                    <th>Quantity</th>
                                                                    <th><span class="pull-right">Unit Price(Ksh)</span></th>
                                                                    <th><span class="pull-right">Sale(Ksh)</span></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                <?php $total=0; foreach($detailed_sales as $item):
                                                                    if($item->bill_id == $rows->id):

                                                                        ?>
                                                                        <tr>
                                                                            <td width="45%"><?php echo $item->product_name; ?></td>
                                                                            <td><?php echo $item->qty; ?></td>
                                                                            <td align='right'><?php echo number_format($item->selling_price,2); ?></td>
                                                                            <td align='right'><?php $sale=$item->selling_price*$item->qty; echo number_format($sale,2); $total=$total+$sale;?></td>
                                                                        </tr>
                                                                    <?php endif; endforeach; ?>

                                                                </tbody>
                                                            </table></h5>
                                                        <h5 class="pull-right"><b>Total Sale: Ksh <?php echo number_format($total,2);?></b></h5>

                                                    </div>
                                                    <div class="col-lg-2"> </div>
                                                </div>
                                                <hr>
                                            <?php endforeach; ?>
                                        </div>

                                </div>
                                </div>

                                <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
