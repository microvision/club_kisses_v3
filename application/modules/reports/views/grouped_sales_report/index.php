<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-4">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Select grouping</label>
                                        <div class="col-lg-12">
                                            <select id="option" name="option" required="required"  class="form-control">
                                                <option value="">Select Group Option</option>
                                                <option value="1">Group by Day</option>
                                                <option value="2">Group by Month</option>
                                                <option value="3">Group by Year</option>
                                            </select>
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label>
                                        <div class="col-lg-12">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label>
                                        <div class="col-lg-12">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($sales_summary)): ?>
                            <span class="h4 text-danger"><?php echo "Grouped Sales Report</span><h6>(".$startdate."  ~  ".$enddate.")"; ?></i></h6></span>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button>
                            <div id="table_wrapper">
                                <table  id="dataTable" class="table table-bordered table-hover ">
												<thead>
													<tr>
														<th >id</th>
														<th>Date</th>
                                                        <th><span class="pull-right">Purchase Cost(Ksh)</span></th>
                                                        <th><span class="pull-right">Total Sales(Ksh)</span></th>
                                                        <th><span class="pull-right">Input VAT(Ksh)</span></th>
                                                        <th><span class="pull-right">Output VAT(Ksh)</span></th>
                                                        <th><span class="pull-right">Revenue(Ksh)</span></th>
<!--                                                        <th><span class="pull-right">VAT(Ksh)</span></th>-->
													</tr>
												</thead>
												<tbody>
													<?php $num=0; $total_sales=0;$total_purchases=0; $total_profit=0; $VATS=0; foreach($sales_summary as $row): $num++; $VATS=$VATS+$row->VATS;

                                                        ?>
                                                        <tr>

															<td width="3%"><?php echo $num; ?></td>
															<td><?php echo date('d-M-Y', strtotime($row->sales_date)); ?></td>
															<td align="right"><?php echo number_format($row->purchases,2);$total_purchases = $total_purchases + $row->purchases; ?></td>
															<td align="right"><?php echo number_format($row->sales,2); $total_sales = $total_sales + $row->sales; ?></td>
                                                            <td align="right"><?php echo number_format(0.14*$row->purchases,2); ?></td>
                                                            <td align="right"><?php echo number_format(0.14* $row->sales,2); $actual_vat = (0.14* $row->sales) - (0.14*$row->purchases); $VATS=$VATS+$actual_vat; ?></td>
															<td align="right"><?php   echo number_format(($row->sales-$row->purchases) - $actual_vat,2); $total_profit = $total_profit + ($row->sales-$row->purchases); ?></td>
<!--															<td align="right">--><?php //  echo number_format($row->profit,2); $total_profit = $total_profit + $row->profit; ?><!--</td>-->
<!--															<td align="right">--><?php //  echo number_format($row->VATS,2);  ?><!--</td>-->

														</tr>
                                                    <?php   endforeach; ?>

												</tbody>
											</table><h4>
											<table class="table table-striped">
												<thead>
												  <tr >
													<td align="center"> <b>Total Purchase Cost</b> </td>
													<td align="center"> <b>Total Sales</b> </td>
													<td align="center" ><b>Total Revenue </b></td>
													<td align="center" ><b>Total VAT </b></td>
													<td align="center" ><b>After Tax Revenue </b></td>
												  </tr>
												</thead>
												<tbody>
													<tr class="active">
														<td align="center">Ksh. <?php echo number_format($total_purchases,2); ?></td>
														<td align="center">Ksh. <?php echo number_format($total_sales,2); ?></td>
														<td align="center">Ksh. <?php echo number_format($total_profit,2); ?></td>
														<td align="center">Ksh. <?php echo number_format($VATS,2); ?></td>
														<td align="center">Ksh. <?php echo number_format($total_profit-$VATS,2); ?></td>
													</tr>
												</tbody>
											</table>
                                </h4>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
