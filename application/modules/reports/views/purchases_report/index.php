<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" required="required" name="start_time" value="06:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label><br>
                                        <div class="col-lg-6">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>
                                        <div class="col-lg-5">
                                            <input type="time" id="time2" name="end_time" required="required" value="20:00" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($sales_summary)):?>
                                <span class="h4 text-danger"><?php echo "&nbsp;Purchases Report</span><h6>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")"; ?></i></h6>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button><br><hr>
                                <div id="table_wrapper">
                                    <table id="dataTable" class="table table-bordered table-hover  dataTable">
                                        <thead>
													<tr>
														<th width="3%">id</th>
														<th>Product</th>
														<th>Qty Purchased</th>
														<th>Purchase value</th>
														<th>Qty Sold</th>
														<th>Total Sales</th>
														<th>Current Stock</th>
													</tr>
												</thead>
												<tbody>
													<?php $num=0; $total_purchases=0; $total_sales=0;  foreach($sales_summary as $row): $num++;
                                                        if($row->id==0){$currentstock = 0; }else{$currentstock = $row->current_stock;}
                                                        ?>
                                                        <tr>
															<td width="3%"><?php echo $num; ?></td>
															<td><?php echo $row->product_name; ?></td>
															<td><?php echo $row->p_qty; ?></td>
															<td><?php echo number_format($row->purchase_price,2);
                                                                $total_purchases= $total_purchases+$row->purchase_price; ?></td>
															<td><?php echo $row->s_qty; ?></td>
															<td><?php echo number_format($row->sales,2);
                                                                $total_sales= $total_sales + $row->sales; ?></td>
															<td><?php echo $row->current_stock; ?></td>
														</tr>
                                                        <?php
                                                    endforeach; ?>

												</tbody>
											</table>
											<h4>
											<table class="table table-striped">
												<thead>
												  <tr >
													<td align="center"> <b>Total Purchases</b> </td>
													<td align="center" ><b>Total Sales </b></td>
													<td align="center" ><b>Total (Sales - Purchases) </b></td>
												  </tr>
												</thead>
												<tbody>
													<tr class="active">
														<td align="center">Ksh <?php echo number_format($total_purchases,2); ?></td>
														<td align="center">Ksh <?php echo number_format($total_sales,2); ?></td>
														<td align="center">Ksh <?php echo number_format($total_sales-$total_purchases,2); ?></td>

													</tr>
												</tbody>
											</table></h4>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
