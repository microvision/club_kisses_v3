<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Signal POS - Expenses Statement Of Account</title>
<script>
    //window.onunload = refreshParent;
    function refreshParent() {
        
		
    }
	window.onload= function () {   
	refreshParent();  
	//printreceipt();
	var count = 0;
	while (count<3){
	window.print();
	count=count+1;
	}
	
	
   }
	
	window.onfocus=function () {   print ();   }
	
	//setTimeout(funtion(){window.close();},9);
</script>
<style type="text/css">
        .style1 {
            font-family: cambria,Agency FB;
            font-size: 22px
        }
		.style9 {
            font-family: cambria;
            font-size: 16px;
        }		
        .style2 {
            font-family: calibri;
            font-size: 17px;
        }

        .style5 {
			font-family: calibri;
            font-size: 18px;
        }

        
        .style3 {
            font-family: calibri;
            font-size: 13px;
        }

        .style10 {
            font-family: calibri;
            font-size: 20px;
        }

        .style15 {
            font-family: calibri;
            font-size: 17px;
        }

    </style>
</head>

<body  onFocus= "window.close()">
<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
<tr>
    <td colspan="4" align="center" valign="middle"><span class="style1">
	<?php

		echo $company->name;
		echo "<br><span class=\"style9\"> ".$company->address." <BR> ".$company->phone." </span>";
		echo "<br><span class=\"style9\">EXPENSE STATEMENT OF ACCOUNT </span>";
	?>
	</span>
	</td>	
</tr>
    <tr>
        <td colspan="4" align="left" bordercolor="#000000"><span class="style15"><b><u> ACCOUNT: <?php echo $account->name; ?> </u></b> </span></td>
    </tr>
  <tr>
        <td colspan="4" align="left" bordercolor="#000000"><span class="style15">Date Generated:
                <?php $currenttime=date("Y-m-d H:i:s"); echo $currenttime; ?>
    </span></td>
    </tr>
    <tr>
        <td colspan="4" align="left" bordercolor="#000000"><span class="style15">Statement Period:
                <?php echo $startdate."  ~  ".$enddate; ?>
    </span></td>
    </tr>
  <tr><td colspan="4" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
</table>
<table align="center" width="338" border="1" cellpadding="2" cellspacing="0">
    <thead>

    </thead>
    <tbody>
    <tr>
        <td colspan="3">OPENING BALANCE</td>
        <td align="right">
            <?php $opening_bal=(ISSET($opening_balance->amount))?$opening_balance->amount:0; echo number_format($opening_bal,2); $closing_bal =$opening_bal; ?>
        </td>
    </tr>
    <tr>
        <th>Date</th>
        <th><span class="pull-right">Dr</span></th>
        <th><span class="pull-right">Cr</span></th>
        <th><span class="pull-right">Bal</span></th>
    </tr>
    <?php $num=0; $total_debits=0; $total_credits=0;  foreach($transactions as $row): $num++; ?>
        <tr>
           <td width="30%"><?php echo date('d-M-Y', strtotime($row->created_on)); ?></td>

            <td align="right"><?php echo ($row->type==2)?number_format($row->amount,2):"0.00"; ?></td>
            <td align="right"><?php echo ($row->type==1)?number_format($row->amount,2):"0.00"; ?></td>
            <td align="right">
                <?php
                $closing_bal=($row->type==2)?$closing_bal+$row->amount:$closing_bal;
                $closing_bal=($row->type==1)?$closing_bal-$row->amount:$closing_bal;
                echo number_format($closing_bal,2);
                ?>
            </td>

        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="3">CLOSING BALANCE</td>
        <td align="right"><b><?php echo number_format($closing_bal,2);  ?></b></td>
    </tr>
    </tbody>
</table>
<h4 align="center"><b>CLOSING BALANCE : KSH. <?php echo number_format($closing_bal,2); ?></b></h4>

</body>
</html>

