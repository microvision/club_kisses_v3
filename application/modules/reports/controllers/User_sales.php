<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_sales extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

		$this->load->model('user_sales_model');

	}

    public function index(){
        if(ISSET($_POST['submit'])){
            $startdate = $_POST['start']." ".date("H:i",strtotime($_POST['start_time']));
            $enddate= $_POST['end']." ".date("H:i",strtotime($_POST['end_time']));
            $userId = $_POST['user'];
            Template::set('user_details', $this->user_sales_model->get_user_details($userId));
            Template::set('sales_summary', $this->user_sales_model->get_user_sale_summary($userId,$startdate,$enddate));
            Template::set('detailed_sales', $this->user_sales_model->get_user_detailed_summary($userId,$startdate,$enddate));
            Template::set('bills', $this->user_sales_model->get_user_detailed_bills($userId,$startdate,$enddate));
            Template::set('startdate', $startdate);
            Template::set('enddate', $enddate);
        }
        Template::set('users', $this->user_sales_model->get_users());
        Template::set_theme('default');
        Template::set('page_title', 'User Sales Report');
        Template::render('');

    }

}