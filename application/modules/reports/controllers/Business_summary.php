<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Business_summary extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		$this->load->model('business_model');
        $this->load->model('sales_report_model');

	}

    public function index($id=null){
        if ($this->input->post('submit')){
            if ($this->input->post('start') and $this->input->post('end')){
                $startdate = $this->input->post('start')." ".date("H:i",strtotime($_POST['start_time']));
                $enddate= $this->input->post('end')." ".date("H:i",strtotime($_POST['end_time']));
                Template::set('sales_summary', $this->business_model->get_total_sales_summary($startdate,$enddate));
                Template::set('buying_price', $this->business_model->get_total_buying_price($startdate,$enddate));
                Template::set('stock_summary', $this->business_model->get_stock_summary());
                Template::set('distributor_summary', $this->business_model->get_distributor_sales($startdate,$enddate));
                Template::set('corporate_summary', $this->business_model->get_corporate_sales($startdate,$enddate));
                Template::set('client_payments', $this->business_model->get_credit_payments($startdate,$enddate));
                Template::set('total_client_payments', $this->business_model->get_total_credit_payments($startdate,$enddate));
                Template::set('top_profits', $this->business_model->get_top_profits($startdate,$enddate));
                Template::set('sales_breakdown', $this->sales_report_model->get_total_sales_breakdown($startdate,$enddate));
                Template::set('client_breakdown', $this->sales_report_model->get_total_client_sales_breakdown($startdate,$enddate));
                Template::set('expenses', $this->business_model->get_expenses($startdate,$enddate));
                Template::set('expenses_closing_bal', $this->business_model->get_expense_closing_bal($enddate));
                Template::set('startdate', $startdate);
                Template::set('enddate', $enddate);
            }
        }
        Template::set_theme('default');
        Template::set('page_title', 'Business Summary Report');
        Template::render('');

    }
    public function print_report()
    {
        $startdate = $_GET['start'];
        $enddate= $_GET['end'];
        $data['sales_summary']= $this->business_model->get_total_sales_summary($startdate,$enddate);
        $data['buying_price']= $this->business_model->get_total_buying_price($startdate,$enddate);
        $data['stock_summary']= $this->business_model->get_stock_summary();
        $data['distributor_summary']= $this->business_model->get_distributor_sales($startdate,$enddate);
        $data['corporate_summary']= $this->business_model->get_corporate_sales($startdate,$enddate);
        $data['client_payments']= $this->business_model->get_credit_payments($startdate,$enddate);
        $data['total_client_payments']= $this->business_model->get_total_credit_payments($startdate,$enddate);
        $data['top_profits']= $this->business_model->get_top_profits($startdate,$enddate);
        $data['sales_breakdown']= $this->sales_report_model->get_total_sales_breakdown($startdate,$enddate);
        $data['client_breakdown']= $this->sales_report_model->get_total_client_sales_breakdown($startdate,$enddate);
        $data['expenses']= $this->business_model->get_expenses($startdate,$enddate);
        $data['expenses_closing_bal']= $this->business_model->get_expense_closing_bal($enddate);
        $data['startdate']= $startdate;
        $data['enddate']= $enddate;
        $this->load->view("reports/business_summary/print_report", $data);
        
    }
    
   
}