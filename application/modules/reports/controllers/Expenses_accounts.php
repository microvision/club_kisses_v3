<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Expenses_accounts extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

        $this->load->model('expenses_accounts_model');

		
	}
	
	public function index(){
	    if(ISSET($_POST['submit'])){
            $start_date  = date("d-M-Y",strtotime($_POST['start']));
            $end_date  = date("d-M-Y",strtotime($_POST['end']));
            Template::set('opening_balance', $this->expenses_accounts_model->get_opening_bal($_POST['start'],$_POST['account']));
            Template::set('transactions', $this->expenses_accounts_model->get_account_transactions($_POST['start'],$_POST['end'],$_POST['account']));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
            Template::set('account', $this->expenses_accounts_model->as_object()->find($_POST['account']));
        }
        Template::set('accounts', $this->expenses_accounts_model->get_accounts());
        Template::set_theme('default');
        Template::set('page_title', 'Expenses Accounts');
        Template::render('');
    }
    public function print_statement()
    {
        $start_date  = date("Y-m-d",strtotime($_GET['start']));
        $end_date  = date("Y-m-d",strtotime($_GET['end']));
        $data['opening_balance'] = $this->expenses_accounts_model->get_opening_bal($start_date,$_GET['account']);
        $data['transactions'] = $this->expenses_accounts_model->get_account_transactions($start_date,$end_date,$_GET['account']);
        $data['account'] = $this->expenses_accounts_model->as_object()->find($_GET['account']);
        $data['company'] = $this->expenses_accounts_model->get_company();
        $data['startdate'] =$start_date;
        $data['enddate'] =$end_date;
        $this->load->view('reports/expenses_accounts/print_statement',$data);
    }

}