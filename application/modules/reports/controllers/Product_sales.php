<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_sales extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

        $this->load->model('sales_report_model');

		
	}
	
	public function index(){
	    if(ISSET($_POST['submit'])){
            $productid = $this->input->post('product');
            $start_date = $_POST['start']." ".date("H:i",strtotime($_POST['start_time'])).":00";
            $start_date = date("Y-m-d H:i:s",strtotime($start_date));
            $end_date= $_POST['end']." ".date("H:i",strtotime($_POST['end_time'])).":00";
            $end_date = date("Y-m-d H:i:s",strtotime($end_date));
            Template::set('product_details', $this->sales_report_model->get_product_details($this->input->post('product')));
            Template::set('product_report', $this->sales_report_model->get_product_report($productid,$start_date,$end_date));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
        }
        Template::set('products',$this->sales_report_model->get_products());
        Template::set_theme('default');
        Template::set('page_title', 'Product Sales Report');
        Template::render('');
    }


}