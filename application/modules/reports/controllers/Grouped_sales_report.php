<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Grouped_sales_report extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

        $this->load->model('sales_report_model');

		
	}
	
	public function index(){
	    if(ISSET($_POST['submit'])){
            $start_date  = date("Y-m-d",strtotime($_POST['start']));
            $end_date  = date("Y-m-d",strtotime($_POST['end']));
            $option = $_POST['option'];
            Template::set('sales_summary', $this->sales_report_model->get_grouped_sales_summary($start_date,$end_date,$option));

            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
        }

        Template::set_theme('default');
        Template::set('page_title', 'Grouped Sales Report');
        Template::render('');
    }


}