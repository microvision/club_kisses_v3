<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Detailed_sales extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

		$this->load->model('user_sales_model');

	}

    public function index(){
        if(ISSET($_POST['submit'])){
            $startdate = $_POST['start']." ".date("H:i",strtotime($_POST['start_time']));
            $enddate= $_POST['end']." ".date("H:i",strtotime($_POST['end_time']));
            Template::set('detailed_sales', $this->user_sales_model->get_detailed_summary($startdate,$enddate));
            Template::set('bills', $this->user_sales_model->get_detailed_bills($startdate,$enddate));
            Template::set('startdate', $startdate);
            Template::set('enddate', $enddate);
        }

        Template::set_theme('default');
        Template::set('page_title', 'Detailed Sales Report');
        Template::render('');

    }

}