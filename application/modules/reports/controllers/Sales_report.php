<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales_report extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

        $this->load->model('sales_report_model');

		
	}
	
	public function index(){
	    if(ISSET($_POST['submit']) and $_POST['submit']){
            $start_date = $_POST['start']." ".date("H:i",strtotime($_POST['start_time'])).":00";
            $start_date = date("Y-m-d H:i:s",strtotime($start_date));
            $end_date= $_POST['end']." ".date("H:i",strtotime($_POST['end_time'])).":00";
            $end_date = date("Y-m-d H:i:s",strtotime($end_date));
            Template::set('sales_summary', $this->sales_report_model->get_sales_report($start_date,$end_date));
            Template::set('sales_breakdown', $this->sales_report_model->get_total_sales_breakdown($start_date,$end_date));
            Template::set('client_breakdown', $this->sales_report_model->get_total_client_sales_breakdown($start_date,$end_date));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
        }elseif(ISSET($_POST['submit2']) and $_POST['submit2']){
            $start_date = $_POST['start']." ".date("H:i",strtotime($_POST['start_time'])).":00";
            $start_date = date("Y-m-d H:i:s",strtotime($start_date));
            $end_date= $_POST['end']." ".date("H:i",strtotime($_POST['end_time'])).":00";
            $end_date = date("Y-m-d H:i:s",strtotime($end_date));
            Template::set('sales_summary', $this->sales_report_model->get_sales_report_all($start_date,$end_date));
            Template::set('sales_breakdown', $this->sales_report_model->get_total_sales_breakdown($start_date,$end_date));
            Template::set('client_breakdown', $this->sales_report_model->get_total_client_sales_breakdown($start_date,$end_date));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
        }

        Template::set_theme('default');
        Template::set('page_title', 'Sales Report');
        Template::render('');
    }


}