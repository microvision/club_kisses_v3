<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_sales_model extends MY_Model {


    public function get_users()
    {
        return $this->db->query("SELECT * FROM bf_users WHERE role_id>=7 ORDEr BY display_name ASC")->result();
    }
    //------------------------------------------------------
    public function get_user_details($userId)
    {
        return $this->db->query("SELECT * FROM bf_users WHERE id='".$userId."'")->row();
    }
    //------------------------------------------------------
    public function get_user_sale_summary($userId,$startdate,$enddate)
    {
        return $this->db->query("SELECT product_name,unit_name, user_id,sum(qty) as qty, sum(selling_price*qty) as sales,sum((bf_vision_sales.selling_price*qty)-(bf_vision_stock_shop.buying_price*qty))as profits,selling_price FROM bf_vision_bills 
                                  LEFT JOIN bf_vision_sales ON bf_vision_bills.id=bf_vision_sales.bill_id 
                                  LEFT JOIN bf_vision_products ON bf_vision_sales.product_id=bf_vision_products.id
                                   LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=bf_vision_products.unit_id
                                  LEFT JOIN bf_vision_stock_shop ON bf_vision_stock_shop.id=bf_vision_sales.stock_shop_id 
                                  WHERE bf_vision_bills.created_on <= '".$enddate."' and bf_vision_bills.created_on >= '".$startdate."'  and bf_vision_bills.user_id='".$userId."' 
                                  GROUP BY bf_vision_sales.product_id")->result();
    }
    //------------------------------------------------------
    public function get_user_detailed_bills($userId,$startdate,$enddate)
    {
        return $this->db->query("SELECT  bf_vision_bills.* FROM bf_vision_bills WHERE created_on <= '".$enddate."' and created_on >= '".$startdate."'  and user_id='".$userId."' ")->result();
    }
    //------------------------------------------------------
    public function get_user_detailed_summary($userId,$startdate,$enddate)
    {
        return $this->db->query("SELECT bf_vision_sales.*, bf_vision_bills.*,product_name FROM bf_vision_bills
                                  LEFT JOIN bf_vision_sales on bf_vision_bills.id=bill_id 
                                  LEFT JOIN bf_vision_products on bf_vision_products.id=product_id 
                                WHERE bf_vision_bills.created_on <= '".$enddate."' and bf_vision_bills.created_on >= '".$startdate."'  and user_id='".$userId."' ")->result();
    }
    //------------------------------------------------------
    public function get_detailed_bills($startdate,$enddate)
    {
        return $this->db->query("SELECT  bf_vision_bills.* FROM bf_vision_bills WHERE created_on <= '".$enddate."' and created_on >= '".$startdate."'  ")->result();
    }
    //------------------------------------------------------
    public function get_detailed_summary($startdate,$enddate)
    {
        return $this->db->query("SELECT bf_vision_sales.*, bf_vision_bills.*,product_name FROM bf_vision_bills
                                  LEFT JOIN bf_vision_sales on bf_vision_bills.id=bill_id 
                                  LEFT JOIN bf_vision_products on bf_vision_products.id=product_id 
                                WHERE bf_vision_bills.created_on <= '".$enddate."' and bf_vision_bills.created_on >= '".$startdate."'   ")->result();
    }
}