<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Business_model extends MY_Model {



    public function get_sales_report($startdate,$enddate)
    {
        return $this->db->query("Select product_name,sum(qty) as qty,sum(total) as sales,sum(profit)as profits,sum(vat) as VATS, current_stock,sales.created_on as sale_date FROM bf_vision_products 
                                      LEFT JOIN (SELECT bf_vision_sales.product_id,bf_vision_sales.created_on, qty, bill_id,bf_vision_sales.vat as vat, stock_shop_id, buying_price, selling_price, (selling_price * qty) as total, ((buying_price) * qty) as profit from bf_vision_sales left join bf_vision_stock_shop on  bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id) as sales on bf_vision_products.id = sales.product_id 
                                      LEFT JOIN (SELECT product_id,sum(current_qty) as current_stock from bf_vision_stock_shop  group by product_id)as stocks on bf_vision_products.id = stocks.product_id where sales.created_on <= '".$enddate."' and sales.created_on >= '".$startdate."'  group by sales.product_id")->result();
    }

    public function get_total_sales_breakdown($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(selling_price*qty)  as Sales, payment_option FROM bf_vision_sales 
                                    RIGHT JOIN bf_vision_bills ON bf_vision_bills.id=bill_id 
                                    WHERE bf_vision_sales.created_on <= '".$enddate."' and bf_vision_sales.created_on >= '".$startdate."' GROUP BY payment_option")->result();
    }
    public function get_total_client_sales_breakdown($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(selling_price*qty)  as Sales, bill_type FROM bf_vision_sales 
                                    RIGHT JOIN bf_vision_bills ON bf_vision_bills.id=bill_id 
                                    WHERE bf_vision_sales.created_on <= '".$enddate."' and bf_vision_sales.created_on >= '".$startdate."' GROUP BY bill_type")->result();
    }
    //---------------------------------------------------------
    public function get_total_sales_summary($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(selling_price*qty)  as Sales, SUM(vat) as VAT FROM bf_vision_sales where created_on <= '".$enddate."' and created_on >= '".$startdate."'")->row();
    }
    public function get_credit_payments($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(amount)  as payments, client_id, name FROM bf_vision_corporate_client_transactions
                                        LEFT JOIN bf_vision_corporate_clients ON bf_vision_corporate_clients.id=client_id
                                      WHERE bf_vision_corporate_client_transactions.created_on <= '".$enddate."' and bf_vision_corporate_client_transactions.created_on >= '".$startdate."' and transaction_type=1
                                      GROUP BY client_id")->result();
    }
    public function get_total_credit_payments($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(amount)  as payments FROM bf_vision_corporate_client_transactions
                                        WHERE bf_vision_corporate_client_transactions.created_on <= '".$enddate."' and bf_vision_corporate_client_transactions.created_on >= '".$startdate."' and transaction_type=1
                                      ")->row();
    }
    public function get_total_buying_price($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(bf_vision_stock_shop.buying_price*qty) AS PPrice FROM bf_vision_sales 
                                  LEFT JOIN bf_vision_stock_shop ON stock_shop_id = bf_vision_stock_shop.id WHERE bf_vision_sales.created_on <= '".$enddate."' AND bf_vision_sales.created_on >= '".$startdate."'")->row();
    }
    public function get_stock_summary()
    {
        return $this->db->query("SELECT SUM(bf_vision_stock_shop.buying_price*current_qty) as PValue, SUM(bf_vision_products.retail_price*current_qty)as RValue, SUM(wholesale_price*current_qty)as WValue ,SUM(distribution_price*current_qty)as DValue , count(product_name) as products FROM bf_vision_stock_shop 
                                  LEFT JOIN bf_vision_products on product_id=bf_vision_products.id where bf_vision_stock_shop.status=0 or bf_vision_stock_shop.status=1 LIMIT 1")->row();
    }

    public function get_distributor_sales($startdate,$enddate)
    {
        return $this->db->query("SELECT distributors.id as distributor_id,distributors.name as distributor,SUM(bf_vision_sales.selling_price*bf_vision_sales.qty) as TSales, COUNT(bf_vision_bills.id) as no_sales FROM bf_vision_bills
                                  LEFT JOIN bf_vision_sales ON bf_vision_sales.bill_id = bf_vision_bills.id
                                  LEFT JOIN (SELECT bf_vision_distributor_orders.id as order_id, bf_vision_distributors.* FROM bf_vision_distributor_orders LEFT JOIN bf_vision_distributors ON bf_vision_distributors.id=distributor_id )as distributors ON distributors.order_id = client_id
                                  WHERE bf_vision_bills.created_on <= '".$enddate."' and bf_vision_bills.created_on >= '".$startdate."' and bill_type=3 GROUP BY distributor_id ")->result();
    }

    public function get_corporate_sales($startdate,$enddate)
    {
        return $this->db->query("SELECT clients.id as clients_id,clients.name as client,clients.ac_bal as account_bal,SUM(bf_vision_sales.selling_price*bf_vision_sales.qty) as TSales FROM bf_vision_bills
                                  LEFT JOIN bf_vision_sales ON bf_vision_sales.bill_id = bf_vision_bills.id
                                  LEFT JOIN (SELECT bf_vision_corporate_clients.*, ac_bal FROM bf_vision_corporate_clients 
                                  LEFT JOIN (SELECT client_id, SUM(if(transaction_type='1',amount,-amount)) as ac_bal FROM bf_vision_corporate_client_transactions GROUP BY client_id)as account_bal ON account_bal.client_id=bf_vision_corporate_clients.id)as clients ON clients.id = client_id
                                  WHERE bf_vision_bills.created_on <= '".$enddate."' and bf_vision_bills.created_on >= '".$startdate."' and bill_type=2 GROUP BY client_id ")->result();
    }

    public function get_top_profits($startdate,$enddate)
    {
        return $this->db->query("SELECT product_name,SUM(selling_price*qty)as sales,current_stock FROM bf_vision_sales                                    
                                    LEFT JOIN bf_vision_products on bf_vision_products.id=bf_vision_sales.product_id
                                    LEFT JOIN (SELECT product_id,sum(current_qty) as current_stock from bf_vision_stock_shop  group by product_id)as stocks ON stocks.product_id=bf_vision_products.id
                                  WHERE bf_vision_sales.created_on <= '".$enddate."' and bf_vision_sales.created_on >= '".$startdate."' group by bf_vision_sales.product_id")->result();
    }

    public function get_expenses($startdate,$enddate)
    {
        return $this->db->query("SELECT bf_vision_expenses.*,bf_vision_expense_category.name as category FROM bf_vision_expenses 
                                  LEFT JOIN bf_vision_expense_category ON category_id=bf_vision_expense_category.id
                                  WHERE expense_date <= '".$enddate."' and expense_date >= '".$startdate."' and type=1")->result();
    }
    public function get_expense_closing_bal($enddate)
    {
        return $this->db->query("SELECT SUM(if(type='1',-amount,amount)) as amount FROM bf_vision_expenses WHERE date(expense_date)<='".$enddate."' ")->row();
    }
}