<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expenses_accounts_model extends MY_Model {
	protected $table_name = 'vision_expense_accounts';
    protected $key = 'id';


    public function get_accounts()
    {
        return $this->db->query("SELECT * FROM bf_vision_expense_accounts WHERE status=1")->result();
    }
    public function get_opening_bal($start,$account_id)
    {
        return $this->db->query("SELECT SUM(if(type='1',-amount,amount)) as amount, account_id FROM bf_vision_expenses WHERE date(expense_date)<'".$start."' and account_id='".$account_id."' GROUP BY account_id")->row();
    }
    public function get_account_transactions($start,$end,$account_id)
    {
        return $this->db->query("SELECT bf_vision_expenses.* FROM bf_vision_expenses WHERE date(expense_date)>='".$start."' and date(expense_date)<='".$end."' and account_id='".$account_id."'")->result();
    }
    public function get_company()
    {
        return $this->db->query("SELECT * FROM bf_vision_business_profile LIMIT 1")->row();
    }
}