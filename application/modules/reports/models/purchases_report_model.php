<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchases_report_model extends MY_Model {
	protected $table_name = 'vision_retail_units';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_purchases_summary($startdate,$enddate)
    {
        return $this->db->query("Select bf_vision_products.id,product_name,sum(purchased_qty) as p_qty,sum(p_total) as purchase_price,SUM(quantity) as s_qty,SUM(s_total) as sales, purchased_date,current_stock FROM bf_vision_products
								        LEFT JOIN (SELECT bf_vision_stock_shop.product_id, purchased_date, purchased_qty,bf_vision_stock_shop.id,buying_price,(purchased_qty * buying_price) as p_total,(selling_price * quantity) as s_total,quantity FROM bf_vision_stock_shop
									        LEFT JOIN (SELECT stock_shop_id, SUM(qty)as quantity,selling_price FROM bf_vision_sales GROUP BY stock_shop_id) as sales ON sales.stock_shop_id = bf_vision_stock_shop.id) as purchases on purchases.product_id=bf_vision_products.id
								        LEFT JOIN (select product_id,sum(current_qty) as current_stock from bf_vision_stock_shop  group by product_id)as stocks on bf_vision_products.id = stocks.product_id 
								        WHERE date(purchased_date) <= '".$enddate."' and date(purchased_date) >= '".$startdate."'     group by purchases.product_id")->result();
    }

}