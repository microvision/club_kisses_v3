<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_report_model extends MY_Model {
	protected $table_name = 'vision_retail_units';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


    public function get_sales_report_old($startdate,$enddate)
    {
        return $this->db->query("Select product_name,sum(qty) as qty,sum(total) as sales,sum(profit)as profits,sum(vat) as VATS, current_stock,sales.created_on as sale_date FROM bf_vision_products 
                                      LEFT JOIN (SELECT bf_vision_sales.product_id,bf_vision_sales.created_on, qty, bill_id,bf_vision_sales.vat as vat, stock_shop_id, buying_price, selling_price, (selling_price * qty) as total, ((buying_price) * qty) as profit from bf_vision_sales left join bf_vision_stock_shop on  bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id) as sales on bf_vision_products.id = sales.product_id 
                                      LEFT JOIN (SELECT product_id,sum(current_qty) as current_stock from bf_vision_stock_shop  group by product_id)as stocks on bf_vision_products.id = stocks.product_id 
                                      where sales.created_on <= '".$enddate."' and sales.created_on >= '".$startdate."'  group by sales.product_id")->result();
    }
    public function get_sales_report_all_old($startdate,$enddate)
    {
        return $this->db->query("Select product_name,sum(qty) as qty,sum(total) as sales,sum(profit)as profits,sum(vat) as VATS, current_stock,sales.created_on as sale_date FROM bf_vision_products 
                                      LEFT JOIN (SELECT bf_vision_sales.product_id,bf_vision_sales.created_on, qty, bill_id,bf_vision_sales.vat as vat, stock_shop_id, buying_price, selling_price, (selling_price * qty) as total, ((buying_price) * qty) as profit from bf_vision_sales 
                                                    LEFT JOIN bf_vision_stock_shop on  bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id) as sales on bf_vision_products.id = sales.product_id 
                                      LEFT JOIN (SELECT product_id,sum(current_qty) AS current_stock FROM bf_vision_stock_shop  GROUP BY product_id)as stocks on bf_vision_products.id = stocks.product_id where sales.created_on <= '".$enddate."' and sales.created_on >= '".$startdate."'  group by sales.product_id HAVING qty IS NULL")->result();
    }
    public function get_sales_report($startdate,$enddate)
    {
        return $this->db->query("Select bf_vision_products.id,product_name,sum(qty) as qty,sum(invat) as in_put_vat,sum(outvat) as out_put_vat,sum(total) as sales,sum(profit)as profits,sum(VATS) as VATS, current_stock,sales.created_on as sale_date FROM bf_vision_products  
									LEFT JOIN (select bf_vision_sales.product_id,bf_vision_sales.created_on, qty, bill_id,bf_vision_sales.vat as VATS, stock_shop_id, buying_price, selling_price,((bf_vision_stock_shop.buying_price-(bf_vision_stock_shop.buying_price/1.16))*qty) as invat,((selling_price-(selling_price/1.16))*qty) as outvat, (selling_price * qty) as total, ((buying_price) * qty) as profit from bf_vision_sales 
													LEFT JOIN bf_vision_stock_shop on  bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id) as sales on bf_vision_products.id = sales.product_id 
									LEFT JOIN (select product_id,sum(current_qty) as current_stock from bf_vision_stock_shop  group by product_id)as stocks on bf_vision_products.id = stocks.product_id 
									WHERE sales.created_on <= '".$enddate."' and sales.created_on >= '".$startdate."'  group by sales.product_id")->result();


    }
    public function get_sales_report_all($startdate,$enddate)
    {
        return $this->db->query("Select bf_vision_products.id,product_name,sum(qty) as qty,sum(invat) as in_put_vat,sum(outvat) as out_put_vat,sum(total) as sales,sum(profit)as profits,sum(VATS) as VATS, current_stock,sales.created_on as sale_date FROM bf_vision_products  
									LEFT JOIN (select bf_vision_sales.product_id,bf_vision_sales.created_on, qty, bill_id,bf_vision_sales.vat as VATS, stock_shop_id, buying_price, selling_price,((bf_vision_stock_shop.buying_price-(bf_vision_stock_shop.buying_price/1.16))*qty) as invat,((selling_price-(selling_price/1.16))*qty) as outvat, (selling_price * qty) as total, ((buying_price) * qty) as profit from bf_vision_sales 
													LEFT JOIN bf_vision_stock_shop on  bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id WHERE bf_vision_sales.created_on <= '".$enddate."' and bf_vision_sales.created_on >= '".$startdate."') as sales on bf_vision_products.id = sales.product_id 
									LEFT JOIN (select product_id,sum(current_qty) as current_stock from bf_vision_stock_shop  group by product_id)as stocks on bf_vision_products.id = stocks.product_id 
									WHERE bf_vision_products.status=1  group by bf_vision_products.id HAVING qty IS NULL")->result();


    }

    public function get_sales_categories_report($startdate,$enddate)
    {
        return $this->db->query("SELECT `bill_type`,sum(qty) as qty,product_id,product_name,sum(selling_price*qty)as sales,unit_name FROM `bf_vision_bills` 
                                      LEFT JOIN (SELECT qty,selling_price, bill_id,product_id,product_name,unit_name FROM bf_vision_sales 
                                                    LEFT JOIN bf_vision_products ON bf_vision_products.id=product_id
                                                    LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=bf_vision_products.unit_id)as sales ON sales.bill_id =`bf_vision_bills`.id
                                      
                                  WHERE bf_vision_bills.created_on <= '".$enddate."' and bf_vision_bills.created_on >= '".$startdate."'  group by product_id,bill_type")->result();
    }

    public function get_total_sales_breakdown($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(selling_price*qty)  as Sales, payment_option FROM bf_vision_sales 
                                    RIGHT JOIN bf_vision_bills ON bf_vision_bills.id=bill_id 
                                    WHERE bf_vision_sales.created_on <= '".$enddate."' and bf_vision_sales.created_on >= '".$startdate."' GROUP BY payment_option")->result();
    }
    public function get_total_client_sales_breakdown($startdate,$enddate)
    {
        return $this->db->query("SELECT SUM(selling_price*qty)  as Sales, bill_type FROM bf_vision_sales 
                                    RIGHT JOIN bf_vision_bills ON bf_vision_bills.id=bill_id 
                                    WHERE bf_vision_sales.created_on <= '".$enddate."' and bf_vision_sales.created_on >= '".$startdate."' GROUP BY bill_type")->result();
    }
    public function get_grouped_sales_summary($startdate, $enddate, $group_option)
    {
        if ($group_option == 1) {
            //day
            $groupby = 'day(sales.created_on),month(sales.created_on)';
        } else if ($group_option == 2) {
            $groupby = 'month(sales.created_on)';
        } else {
            $groupby = 'year(sales.created_on)';
        }
        return $this->db->query("Select sum(purchases) as purchases, sum(total) as sales ,sum(VATS) as VATS,sum(profit) as profit, current_stock,date(sales.created_on) as sales_date FROM bf_vision_products 
                                    left join 
                                      (select bf_vision_sales.product_id,bf_vision_sales.created_on, qty, bill_id,bf_vision_sales.vat as VATS, stock_shop_id, buying_price, selling_price, (selling_price * qty) as total,((bf_vision_sales.selling_price-bf_vision_stock_shop.buying_price)*qty) as profit,(bf_vision_stock_shop.buying_price*qty) as purchases from bf_vision_sales 
                                          left join bf_vision_stock_shop on bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id) as sales on bf_vision_products.id = sales.product_id 
                                    left join 
                                      (select product_id,sum(current_qty) as current_stock from bf_vision_stock_shop 
                                          group by product_id)as stocks on bf_vision_products.id = stocks.product_id 
                                    WHERE date(sales.created_on) <= '" . $enddate . "' and date(sales.created_on) >= '" . $startdate . "' GROUP BY ".$groupby." ORDER BY sales.created_on")->result();
    }
	public function get_manual_sales($startdate,$enddate){
        return $this->db->query("Select bf_vision_bills.*,sales,display_name FROM bf_vision_bills 
                                      LEFT JOIN (SELECT SUM(selling_price*qty) sales,bill_id FROM bf_vision_sales GROUP BY  bill_id) as s on bf_vision_bills.id = s.bill_id 
                                      LEFT JOIN bf_users on bf_users.id = bf_vision_bills.created_by where bf_vision_bills.billing_time <= '".$enddate."' and bf_vision_bills.billing_time >= '".$startdate."' and sale_type=1")->result();
    }
    public function get_products(){
        return $this->db->query("SELECT * FROM bf_vision_products WHERE status=1 ORDER BY product_name")->result();
    }
    public function get_product_report($productid, $startdate, $enddate)
    {
        return $this->db->query("select bf_vision_sales.created_on as billing_time, qty, bill_id, stock_shop_id, buying_price, selling_price, (selling_price * qty) as total, ((selling_price - buying_price) * qty) as profit from bf_vision_sales 
                                        join bf_vision_stock_shop on  bf_vision_sales.stock_shop_id = bf_vision_stock_shop.id 
                                        where bf_vision_sales.product_id = '" . $productid . "' and bf_vision_sales.created_on <= '" . $enddate . "' and bf_vision_sales.created_on >= '" . $startdate . "'")->result();
    }
    public function get_product_details($id){
        return $this->db->query("SELECT bf_vision_products.*,bf_vision_retail_units.unit_name FROM bf_vision_products LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=bf_vision_products.unit_id WHERE bf_vision_products.id='".$id."'")->row();
    }


    public function get_rooms_report($startdate,$enddate){
return $this->db->query("select r.room_no,c.category,l.level,bvra.*,datediff(bvra.check_out,check_in) as days from bf_vision_rooms r
    inner join bf_vision_room_categories c on r.category = c.id
    inner join bf_vision_room_levels l on l.id=r.level
inner join  bf_vision_room_allocation bvra on r.id = bvra.room_id WHERE date(bvra.created_on) <= '" . $enddate . "' and date(bvra.created_on) >= '" . $startdate . "'")->result();
    }
}