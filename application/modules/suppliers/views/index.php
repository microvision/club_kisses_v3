<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <?php if(has_permission('Vision.Suppliers.Create_supplier')):?>
                <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".new_supplier">Create New Supplier</button><br><hr>
            <?php endif; ?>
            <table id="example" class="table table-bordered table-hover  table-condensed dataTable">
                <thead>
                <tr>
                    <th >#</th>
                    <th>Supplier</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>A/C Bal(Ksh)</th>
                    <th>Status</th>
                    <th width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(($suppliers)): $num=0; foreach($suppliers as $row): $num++;?>
                <tr class="<?php echo ($row->status==0)?"bg-warning":"";?>">
                    <td width="3%"><?php e($num); ?></td>
                    <td><?php e($row->name); ?></td>
                    <td><?php e($row->address);?></td>
                    <td><?php e($row->phone);?></td>
                    <td><?php e($row->email);?></td>
                    <td><?php  echo ($row->amount>0)?"<span class='text-danger'>(".number_format($row->amount,2).")</span>":abs(number_format($row->amount,2));?></td>
                    <td><?php echo($row->status==1)?"Active":"<span class='text-danger'>Disabled</span>";?></td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <?php if($row->status==1): ?>
                                <?php if(has_permission('Vision.Suppliers.Edit_supplier')):?>
                                    <button type="button" class="btn btn-default btn-default" title="Edit supplier" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>suppliers/edit_supplier', 'ch2=<?php e($row->id);?>&amp;ch=1')"><i class="fa fa-edit"></i></button>
                                <?php endif;?>
                                <?php if(has_permission('Vision.Suppliers.Delete_supplier')):?>
                                    <button type="button" class="btn btn-default btn-danger" title="Disable Supplier" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>suppliers/disable_supplier', 'ch=<?php e($row->id);?>&amp;bt=Disable')"><i class="fa fa-trash-o"></i></button>
                                <?php endif;?>
                            <?php else: ?>
                                <?php if(has_permission('Vision.Suppliers.Delete_supplier')):?>
                                    <button type="button" class="btn btn-default btn-primary" title="Activate Supplier" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>suppliers/enable_supplier', 'ch=<?php e($row->id);?>&amp;bt=Activate')"><i class="fa fa-unlock"></i></button>
                                <?php endif;?>
                            <?php endif;?>
                        </div>
                        <button type="button" class="btn btn-success btn-primary btn-xs" title="Supplier Payment" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>suppliers/pay_supplier', 'ch=<?php e($row->id);?>&amp;bt=Activate')"><i class="fa fa-stack-overflow"></i></button>
                    </td>
                </tr>
                <?php endforeach; endif;?>
                </tbody>

            </table>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->
        <div class="modal fade new_supplier" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form method="post" id="wizard" class="form-horizontal" role="form" action="<?php echo base_url(); ?>suppliers/new_supplier" accept-charset="utf-8" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">New Supplier Account</h4>
                        </div>
                        <div class="modal-body">


                            <div class="wizard-steps clearfix"></div>
                            <div class="step" id="account-details"><br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Company Name</label>
                                    <div class="col-lg-6">
                                        <input id="name" name="name" required="required" type="text" class="form-control col-md-7 col-xs-12" />
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Phone</label>
                                    <div class="col-lg-6">
                                        <input id="phone" required="required" name="phone" type="text" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Email</label>
                                    <div class="col-lg-6">
                                        <input id="email"  name="email" type="email" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Address</label>
                                    <div class="col-lg-6">
                                        <textarea id="autosize" name="address" class="form-control col-md-7 col-xs-12"></textarea>
                                    </div>
                                </div><!-- End .form-group  -->

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" name="submit"  class="btn btn-primary" value="Create Supplier">
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>