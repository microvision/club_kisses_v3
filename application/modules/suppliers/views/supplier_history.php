<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-4">
                                    <div class="form-group has-success">
                                        <label class="control-label ">suppliers</label>
                                        <div class="col-lg-12">
                                            <select id="supplier" name="supplier" required="required"  class="form-control">
                                                <option selected disabled value="">Select supplier</option>
                                                <?php foreach($suppliers as $row): ?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label>
                                        <div class="col-lg-12">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label>
                                        <div class="col-lg-12">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label><br>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($transactions)): ?>
                            <span class="h4 text-danger"><?php echo "&nbsp;Supplier Statement Of Account</span><h6>".$supplier->name."<br>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")"; ?></i></h6>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button>

                            <div id="table_wrapper">
                                <table id="dataTable" class="table table-bordered table-hover  dataTable">
                                <thead>
                                    <tr>
                                        <th width="2%">#</th>
                                        <th>Deliver Date</th>
                                        <th>Invoice#</th>
                                        <th>Product Name</th>
                                        <th>Quantity </th>
                                        <th><span class="pull-right">Unit Price</span></th>
                                        <th><span class="pull-right">Total </span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $total=0; $num=0; foreach($transactions as $row): $num++; ?>
                                    <tr>
                                        <td width="2%"><?php echo $num; ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($row->delivery_date)); ?></td>
                                        <td><?php echo $row->invoice_number; ?></td>
                                        <td><?php echo $row->product_name; ?></td>
                                        <td><?php echo $row->purchased_qty." ".$row->unit_name; ?></td>
                                        <td align="right"><?php echo number_format($row->buying_price,2); ?></td>
                                        <td align="right"><?php echo number_format($row->buying_price*$row->purchased_qty,2); $total=$total+($row->buying_price*$row->purchased_qty); ?></td>

                                    </tr>
                                <?php endforeach; ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="5">TOTAL</td>
                                        <td align="right"><?php echo number_format($total,2);  ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
