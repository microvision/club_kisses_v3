<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"> Pay Supplier </b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-9">
                        <h2><?php e($supplier->name); ?></h2>
                        <span class="h3">
                            <?php echo $supplier->address; ?> | <?php echo $supplier->phone; ?> | <?php echo $supplier->email; ?>
                        </span>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <br>
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Payment Date</label>
                    <div class="col-lg-5 col-sm-5">
                        <input id="pay_date"  name="pay_date" type="date" class="form-control col-md-7 col-xs-12" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Payment Mode</label>
                    <div class="col-lg-5 col-sm-5">
                        <select id="pay_mode"  name="payment_mode"  required class="form-control col-md-7 col-xs-12" >
                            <option selected disabled value="">Select Payment Mode</option>
                            <option value="Cash">Cash</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Bank Transfer">Bank Transfer</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Description</label>
                    <div class="col-lg-5 col-sm-5">
                        <input id="details"  name="details" type="text" class="form-control col-md-7 col-xs-12" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Amount Paid (Ksh)</label>
                    <div class="col-lg-5 col-sm-5">
                        <input id="credit"  name="amount" type="number" required class="form-control col-md-7 col-xs-12" />
                        <input type="hidden" class="form-control" name="supplier_id" value ="<?php e($supplier->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div><!-- End .form-group  -->
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Pay Supplier</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
