<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Supplier Details </b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <br>

                <input type="hidden" name="productId" value="$itemId">
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Company Name</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="name" value ="<?php e($supplier->name); ?>" required="">
                        <input type="hidden" class="form-control" name="supplier_id" value ="<?php e($supplier->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Phone</label>
                    <div class="col-lg-6 col-sm-6">
                        <input id="phone" required="required" class="form-control" name="phone" type="text" value ="<?php e($supplier->phone); ?>" />
                    </div>
                </div><!-- End .form-group  -->
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Email</label>
                    <div class="col-lg-6 col-sm-6">
                        <input id="email"  name="email" class="form-control" type="email" value ="<?php e($supplier->email); ?>" />
                    </div>
                </div><!-- End .form-group  -->
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Address</label>
                    <div class="col-lg-6 col-sm-6">
                        <textarea id="autosize" name="address" class="form-control"><?php e($supplier->address); ?></textarea>
                    </div>
                </div><!-- End .form-group  -->
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
