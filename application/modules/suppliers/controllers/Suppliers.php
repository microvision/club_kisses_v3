<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		$this->load->model('suppliers_model');
		$this->load->model('supplier_payments_model');
		$this->load->model('supplier_transactions_model');
	}
	
	public function index($id=null){
		$this->auth->restrict('Vision.Suppliers.View');
		
		Template::set('suppliers', $this->suppliers_model->get_suppliers());
		Template::set_theme('default');
		Template::set('page_title', 'Suppliers');
		Template::render('');

    }
    public function new_supplier(){
        $this->auth->restrict('Vision.Suppliers.Create_supplier');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'address' => $_POST['address'],
                'email' => $_POST['email'],
                'status' => 1
            );
            $this->suppliers_model->insert($data);
        }
        log_activity($this->auth->user_id(),"Created new supplier ".$_POST['name'], 'suppliers');
        Template::set_message('The supplier account was successfully created', 'alert fresh-color alert-success');
        redirect("suppliers/index",true);
    }

    public function edit_supplier(){
        $this->auth->restrict('Vision.Suppliers.Edit_supplier');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'address' => $_POST['address'],
                'email' => $_POST['email']
            );
            $this->suppliers_model->update($_POST['supplier_id'],$data);
            log_activity($this->auth->user_id(),"Edited the supplier details for ".$_POST['name'], 'catalog');
            redirect("suppliers/index",true);
        }else{
            $supplier_id = $_GET['ch2'];
            $data['supplier'] = $this->suppliers_model->as_object()->find($supplier_id);
            $data['url']=base_url()."suppliers/edit_supplier";
            $this->load->view('suppliers/edit_supplier',$data);
        }

    }
    public function disable_supplier(){
        $this->auth->restrict('Vision.Suppliers.Delete_supplier');
        if (ISSET($_POST['submit'])){
            $supplier = $this->suppliers_model->as_object()->find($_POST['supplier_id']);

            $data = array('status'=> 0);
            if ($this->suppliers_model->update($_POST['supplier_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the supplier ".$supplier->name, 'catalog');
                Template::set_message('The supplier '.$supplier->name.' was successfully deactivated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the supplier account. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('suppliers/index',true);
        }else{
            $data['supplier_id'] = $this->input->get("ch");
            $data['supplier'] = $this->suppliers_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."suppliers/disable_supplier";
            $data['action']=$_GET['bt'];
            $this->load->view('suppliers/disable_supplier',$data);
        }
    }
    public function enable_supplier(){
        $this->auth->restrict('Vision.Suppliers.Delete_supplier');
        if (ISSET($_POST['submit'])){
            $supplier = $this->suppliers_model->as_object()->find($_POST['supplier_id']);

            $data = array('status'=> 1);
            if ($this->suppliers_model->update($_POST['supplier_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Activated the supplier ".$supplier->name, 'catalog');
                Template::set_message('The supplier '.$supplier->name.' was successfully activated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered activatingd the supplier account. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('suppliers/index',true);
        }else{
            $data['supplier_id'] = $this->input->get("ch");
            $data['supplier'] = $this->suppliers_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."suppliers/enable_supplier";
            $data['action']=$_GET['bt'];
            $this->load->view('suppliers/disable_supplier',$data);


        }
    }
    public function pay_supplier(){
        if(ISSET($_POST['submit'])){
            $supplier = $this->suppliers_model->as_object()->find($_POST['supplier_id']);
            $data = array(
                'supplier_id' => $supplier->id,
                'description' => $_POST['payment_mode']." Payment ".$_POST['details'],
                'transaction_date' => $_POST['pay_date'],
                'amount' => $_POST['amount'],
                'transaction_type' => 1
            );
            $this->supplier_transactions_model->insert($data);
            log_activity($this->auth->user_id(),"Made supplier Payment for ".$supplier->name, 'Supplier');
            Template::set_message('Payments of <b>Ksh '.number_format($_POST['amount'],2).'</b> for '.$supplier->name.' was successfully received.', 'alert fresh-color alert-success');
            redirect("suppliers/index",true);
        }else{
            $supplier_id = $_GET['ch'];
            $data['supplier'] = $this->suppliers_model->as_object()->find($supplier_id);
            $data['url']=base_url()."suppliers/pay_supplier";
            $this->load->view('pay_supplier',$data);
        }

    }
    public function supplier_statements(){
        if(ISSET($_POST['submit'])){
            $start_date  = date("d-M-Y",strtotime($_POST['start']));
            $end_date  = date("d-M-Y",strtotime($_POST['end']));
            Template::set('opening_balance', $this->supplier_transactions_model->get_opening_bal($_POST['start'],$_POST['supplier']));
            Template::set('transactions', $this->supplier_transactions_model->get_supplier_transactions($_POST['start'],$_POST['end'],$_POST['supplier']));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
            Template::set('supplier', $this->suppliers_model->as_object()->find($_POST['supplier']));
        }
        Template::set('suppliers', $this->suppliers_model->order_by('name','asc')
                                                    ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'suppliers Statements');
        Template::render('');
    }
    public function supplier_history(){
        if(ISSET($_POST['submit'])){
            $start_date  = date("d-M-Y",strtotime($_POST['start']));
            $end_date  = date("d-M-Y",strtotime($_POST['end']));
            //Template::set('opening_balance', $this->supplier_transactions_model->get_opening_bal($_POST['start'],$_POST['supplier']));
            Template::set('transactions', $this->supplier_transactions_model->get_supplier_history($_POST['start'],$_POST['end'],$_POST['supplier']));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
            Template::set('supplier', $this->suppliers_model->as_object()->find($_POST['supplier']));
        }
        Template::set('suppliers', $this->suppliers_model->order_by('name','asc')
            ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Suppliers History');
        Template::render('');
    }
}