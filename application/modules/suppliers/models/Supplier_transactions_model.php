<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier_transactions_model extends MY_Model {
	protected $table_name = 'vision_supplier_transactions';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


    public function get_opening_bal($start,$supplier_id)
    {
        return $this->db->query("SELECT SUM(if(transaction_type='1',-amount,amount)) as amount, supplier_id FROM bf_vision_supplier_transactions WHERE date(created_on)<'".$start."' and supplier_id='".$supplier_id."' GROUP BY supplier_id")->row();
    }
    public function get_supplier_transactions($start,$end,$supplier_id)
    {
        return $this->db->query("SELECT bf_vision_supplier_transactions.* FROM bf_vision_supplier_transactions WHERE date(created_on)>='".$start."' and date(created_on)<='".$end."' and supplier_id='".$supplier_id."'")->result();
    }
    public function get_supplier_history($start,$end,$supplier_id)
    {
        return $this->db->query("SELECT bf_vision_supply_orders.*,bf_vision_stock_store.*,product_name,unit_name FROM bf_vision_supply_orders
                                    LEFT JOIN bf_vision_stock_store ON bf_vision_stock_store.supply_order_id=bf_vision_supply_orders.id
                                    LEFT JOIN bf_vision_products ON bf_vision_products.id=product_id
                                    LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id
                                    WHERE date(delivery_date)>='".$start."' and date(delivery_date)<='".$end."' and bf_vision_supply_orders.supplier_id='".$supplier_id."'")->result();
    }
	
}