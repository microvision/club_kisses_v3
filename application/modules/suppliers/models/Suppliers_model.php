<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers_model extends MY_Model {
	protected $table_name = 'vision_suppliers';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_suppliers()
    {
        return $this->db->query("SELECT bf_vision_suppliers.*,amount FROM bf_vision_suppliers
                                      LEFT JOIN (SELECT SUM(if(transaction_type='1',-amount,amount)) as amount,supplier_id FROM bf_vision_supplier_transactions GROUP BY supplier_id)as trans ON trans.supplier_id=bf_vision_suppliers.id
                                      ORDER BY name")->result();
    }

	
}