<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Manage Stocks Suppliers',
    'name'          => 'Suppliers',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);