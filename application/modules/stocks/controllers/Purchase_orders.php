<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_orders extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		

		$this->load->model('store_stocks_model');
		$this->load->model('measurement_units_model');
		$this->load->model('inventory_model');
		$this->load->model('shop_stocks_model');
		$this->load->model('requisitions_model');
		$this->load->model('suppliers_model');
        $this->load->model('supplier_transactions_model');
	}

    public function index($id=null)
    {
        if($id){
            Template::set('order_details', $this->requisitions_model->get_order($id));
            Template::set('received_items', $this->store_stocks_model->get_received_items($id));
        }
        Template::set('orders', $this->requisitions_model->get_orders());
        Template::set('suppliers', $this->suppliers_model->order_by('name','asc')
            ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Purchase to shop');
        Template::render('');
    }
    public function print_order(){
	    $id = $_GET['order_id'];
        $data['order_details'] = $this->requisitions_model->get_order($id);
        $data['received_items'] = $this->store_stocks_model->get_received_items($id);
        $data['company'] = $this->inventory_model->get_company();
        $this->load->view("purchase_orders/print_order", $data);
    }
    public function create_purchase_order()
    {
        $today = date('Y-m-d');
        $data = array(
//            'supplier_id'=> $_POST['supplier_id'],
//            'invoice_number'=> $_POST['invoice'],
            'delivery_date'=> $today,
            'status'=> 0
        );
        $order_id = $this->requisitions_model->insert($data);
        redirect('stocks/purchase_orders/index/'.$order_id,true);
    }
    public function receive_purchase_item()
    {
        $itemIds = $_POST['items'];
        $today = date('Y-m-d');
        foreach($itemIds as $itemId){
            $buying_price = $_POST['buying_price']/$_POST['qty'];
            $data = array(
                'supply_order_id'=> $_POST['order_id'],
                'product_id'=> $itemId,
//                'batch_no'=> $_POST['batch'],
//                'expiry_date'=> $_POST['expiry'],
                'purchased_qty'=> $_POST['qty'],
                'current_qty'=> 0,
                'purchased_date'=> $today,
                'buying_price'=> $buying_price,
                'status'=> 0
            );
            $store_id=$this->store_stocks_model->insert($data);

        }
        redirect('stocks/purchase_orders/index/'.$_POST['order_id'],true);
    }
    public function submit_requisition($id){

        $order_details= $this->requisitions_model->get_order($id);
        if($order_details->supplier_id==""){
            Template::set_message('The Requisition was not submitted for approval. Please select a supplier for the order', 'alert fresh-color alert-danger');
        }else{
            //update order status
            $data = array('status'=>1);
            $this->requisitions_model->update($order_details->id,$data);
            Template::set_message('The Requisition was successfully submitted for approval. Please await approval before the stocks can be activated for use in the store', 'alert fresh-color alert-success');
        }

        redirect('stocks/purchase_orders/index/',true);
    }
    public function approve_requisition($id){
        //order_details
        $order_details = $this->requisitions_model->get_order($id);
        //get ordered items
        //todo: get only items that have not been received
        $order_items=$this->store_stocks_model->where(array("supply_order_id" => $id))
                                        ->find_all();
        //print_r($order_items); exit;
        foreach($order_items as $row){
            //receive stocks to shop
            $data2 = array(
                'store_stock_id'=> $row->id,
                'product_id'=> $row->product_id,
//                'batch_no'=> $row->batch_no,
//                'expiry_date'=> $row->expiry_date,
                'purchased_qty'=> $row->purchased_qty,
                'current_qty'=> $row->purchased_qty,
                'purchased_date'=> $row->purchased_date,
                'buying_price'=> $row->buying_price,
                'store_order_id'=> $id,
                'status'=> 0
            );
            $this->shop_stocks_model->insert($data2);
        }
        //get order total and debit the suppliers account with the amount
        //todo: get sum only for items not received
        $order_sum = $this->store_stocks_model->get_order_sum($id);
        $data = array(
            'supplier_id' => $order_details->supplier_id,
            'description' => "Invoice ".$order_sum->invoice_number,
            'transaction_date' => $order_sum->delivery_date,
            'amount' => $order_sum->totals,
            'transaction_type' => 2
        );
        $this->supplier_transactions_model->insert($data);
        //update order status
        $data = array('status'=>2);
        $this->requisitions_model->update($id,$data);
        Template::set_message('The Requisition has been successfully received, approved and stocks added to the store', 'alert fresh-color alert-success');
        redirect('stocks/purchase_orders/index/',true);
    }
    public function revert_requisition($id){

        //update order status
        $data = array('status'=>0);
        $this->requisitions_model->update($id,$data);
        Template::set_message('The Requisition has been successfully reverted to the store manager', 'alert fresh-color alert-success');
        redirect('stocks/purchase_orders/index/',true);
    }
    public function update_order_fields(){
        $field = $_GET['val'];
        if($field == "supplier"){
            $data = array('supplier_id'=>$_GET['supplier_id']);
            $this->requisitions_model->update($_GET['order_id'],$data);
            $order_details = $this->requisitions_model->get_order($_GET['order_id']);
            echo <<<eod
             <br>
             <span class="h4 text-danger">$order_details->supplier_name<br></span>
              <span class="h6"><i class="fa fa-phone"></i> $order_details->phone | <i class="fa fa-envelope"></i> $order_details->email  | <i class="fa fa-home"></i> $order_details->address</span>

eod;

        }elseif($field == "invoice"){
            $data = array('invoice_number'=>$_GET['invoice']);
            $this->requisitions_model->update($_GET['order_id'],$data);
        }elseif($field == "delivery_date"){
            $data = array('delivery_date'=>$_GET['delivery_date']);
            $this->requisitions_model->update($_GET['order_id'],$data);
        }
    }
    public function new_supplier(){
        $this->auth->restrict('Vision.Suppliers.Create_supplier');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'address' => $_POST['address'],
                'email' => $_POST['email'],
                'status' => 1
            );
            $supplier_id = $this->suppliers_model->insert($data);
            $data = array('supplier_id'=>$supplier_id);
            $this->requisitions_model->update($_POST['order_id'],$data);
            log_activity($this->auth->user_id(),"Created new supplier ".$_POST['name'], 'stores');
            Template::set_message('The supplier account was successfully created', 'alert fresh-color alert-success');
        }

        redirect("stocks/purchase_orders/index/".$_POST['order_id'],true);
    }
    public function get_store_search_items()
    {
        $product = $this->input->get('ch');
        $data['allitems'] = $this->inventory_model->get_search_products($product);
        $this->load->view("stocks/purchase_orders/get_inventory_search_results", $data);
    }
    public function show_qty_fields(){
        $id=$_GET['ch'];
        $details = $this->inventory_model->as_object()->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,unit_name')
            ->find_by('bf_vision_products.id',$id);
        echo <<<eod
            <div class="block">
            
			<div class="form-group">
				<label class="col-md-3 control-label">Quantity Received</label>
				<div class="col-md-6">
					<input type="number" name="qty" placeholder="Order Quantity" required="required" class="form-control" />																		
				</div>
                <div class="col-md-1"><h4>$details->unit_name</h4></div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label">Total Cost</label>
				<div class="col-md-6">
					<input type="number" name="buying_price" step="any" placeholder="Total Cost" required="required" class="form-control" />																		
				</div>
                <div class="col-md-1"><h4>Ksh</h4></div>
			</div>
            <div class="form-group">
                <label class="col-md-3 control-label">Comments</label>
                <div class="col-md-6">
                    <textarea name="comments" placeholder="Comments/Description" class="form-control"></textarea>
                </div>
            </div>
		</div>
eod;

    }
    public function delete_stock_order_item(){
        $this->store_stocks_model->delete($_GET['order_item_id']);
        $this->shop_stocks_model->delete_puchase_stocks($_GET['order_item_id']);
        redirect("stocks/purchase_orders/index/".$_GET['order_id'],true);

    }
//    public function receive_requisition($id){
//        $order_details= $this->requisitions_model->as_object()->find_by('id',$id);
//        $received_items= $this->requisition_items_model->get_ordered_items($id);
//        //receive items to stock
//        foreach($received_items as $row){
//            //update store stock
//            $data = array(
//                'store_id'=> $row->store_id,
//                'order_id'=> $row->purchase_order_id,
//                'product_id'=> $row->item_id,
//                'purchased_qty'=> $row->received_qty,
//                'current_qty'=> $row->received_qty,
//                'status'=> 1
//            );
//            $this->stocks_model->insert($data);
//        }
//        //update order status
//        $data3 = array('status'=>3);
//        $this->requisitions_model->update($order_details->id,$data3);
//        Template::set_message('The purchase requisition was successfully received.', 'alert fresh-color alert-success');
//        redirect('stocks/requisitions/index/',true);
//    }

}