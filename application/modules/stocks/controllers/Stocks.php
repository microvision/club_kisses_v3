<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stocks extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
        Assets::add_module_js('stores', 'jquery.dataTables.min');
        $this->load->model('shop_stocks_model');
        $this->load->model('inventory_model');
		
	}
	

    public function shop_stock()
    {

        Template::set('items', $this->shop_stocks_model->get_shop_stocks());
        Template::set_theme('default');
        Template::set('page_title', 'Stock in Store');
        Template::render('');
    }
    public function print_store_stock(){

        $data['items']=$this->shop_stocks_model->get_shop_stocks();
        $this->load->view('stocks/print_store_stock',$data);
    }
    public function edit_shop_stock()
    {
        if(ISSET($_POST['submit'])){
            $itemId = $_POST['productId'];
            $product = $this->inventory_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                ->find_by('bf_vision_products.id',$itemId);
            $active_stocks = $this->shop_stocks_model->get_product_shop_stocks($itemId);
            //we check for active fields using the stock ids
            foreach ($active_stocks as $rows){
                if($_POST['p_'.$rows->id]){
                    $data = array('purchased_qty'=> $_POST['p_'.$rows->id]);
                    $this->shop_stocks_model->update($rows->id,$data);
                    log_activity($this->auth->user_id(),"Conducted changes to stocks for ".$product->product_name." Purchased qty From : ".$rows->purchased_qty." to ".$_POST['p_'.$rows->id], 'stocks');
                }
                if($_POST['c_'.$rows->id]){
                    $data = array( 'current_qty'=> $_POST['c_'.$rows->id]);
                    $this->shop_stocks_model->update($rows->id,$data);
                    log_activity($this->auth->user_id(),"Conducted changes to stocks for ".$product->product_name." From : ".$rows->current_qty." to ".$_POST['c_'.$rows->id], 'stocks');
                }
                if($_POST['b_'.$rows->id]){
                    $data = array( 'buying_price'=> $_POST['b_'.$rows->id]);
                    $this->shop_stocks_model->update($rows->id,$data);
                    log_activity($this->auth->user_id(),"Conducted changes to stocks for ".$product->product_name." Buying Price From : ".$rows->buying_price." to ".$_POST['b_'.$rows->id], 'stocks');
                }
                if(($rows->status==2) and ($_POST['c_'.$rows->id]>0)){
                    $data = array( 'status'=> 0);
                    $this->shop_stocks_model->update($rows->id,$data);
                }
            }

            Template::set_message('Stock changes for the '.$product->product_name.' were successfully updated', 'alert alert-success');
            redirect ('stocks/shop_stock',true);
        }

        $item_id = $this->input->get("ch2");
        $product = $this->inventory_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
            ->find_by('bf_vision_products.id',$item_id);
        $stocks = $this->shop_stocks_model->get_product_shop_stocks($item_id);
        $url=base_url()."stocks/edit_shop_stock";
        $security_name = $this->security->get_csrf_token_name();
        $security_code = $this->security->get_csrf_hash();
        echo <<<eod
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Requisition History for <b>$product->product_name </b></h4>
				  </div>
				  <form method="post" class="form-horizontal" action="$url">
					  <div class="modal-body">					  
					  <h6>
					    <div class="row">
					        <div class="col-md-1"></div>
					        <div class="col-md-9">
					            <div class="alert alert-warning"> <b>Important <i class="fa fa-bell"></i></b> :  Altering stock values has direct implications on the reports and available stocks.</div>
                            </div>
                        </div>
						<table class="table responsive-table">
						  <thead>
							<tr>
							  
							  <th>Delivery Date</th>
							  <th>Buying Price</th>
							  <th>Received Stock</th>
							  <th>Current Stock</th>
							  <th>Status</th>
							</tr>
						  </thead>
eod;
        foreach ($stocks as $rows){
            if($rows->status == 0){
                $label= "<p class=\"text-danger\">Pending</p>";
            }elseif($rows->status == 1) {
                $label= "<p class=\"text-warning\">Active</p>";
            }else{
                $label= "<p class=\"text-muted\">Closed</p>";
            }
            echo "<tr>
													
													<td>".date('d-M-Y',strtotime($rows->purchased_date))."</td>
													<td width=\"20%\">".number_format($rows->buying_price,2)."</td>
													<td width=\"20%\">".$rows->purchased_qty." ".$product->unit_name."</td>
													<td width=\"20%\">".$rows->current_qty." ".$product->unit_name."</td>
													<td>".$label."</td>
													<td>";
													if((has_permission('Vision.Stocks.Edit_stocks')) and ($rows->status !=2) ){
													echo "<a href=\"#\" class=\"btn btn-danger btn-xs btn-grad\" data-original-title=\"\" title=\"\" onClick=\"mover(".$rows->id.")\"><span class=\"fa fa-edit\"></span></a>";
													}
											echo		"</td>
											 </tr>
											 <tr  class=\"row even\" id = \"w".$rows->id."\"  style=\"background-color:rgb(255, 255, 255); display: none;\">
											     <td><input type=\"number\" placeholder=\"".number_format($rows->buying_price,2)."\" name=\"b_".$rows->id."\" class=\"form-control\"></td>
												 <td><input type=\"number\" placeholder=\"".$rows->purchased_qty."\" name=\"p_".$rows->id."\" class=\"form-control\"></td>
												 <td><input type=\"number\" placeholder=\"".$rows->current_qty."\" name=\"c_".$rows->id."\" class=\"form-control\"></td>
												 
											 </tr>
											 ";

        }

        echo <<<eod
					  </table></h6>
					  
					  </div>
					  <input type="hidden" name="productId" value="$item_id">
				      <input type="hidden" name="$security_name" value="$security_code" >
				  <div class="modal-footer">
					<button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				  </form>
				</div><!-- /.modal-content -->
		  </div>
eod;
    }
}