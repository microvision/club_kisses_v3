<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Manage shop stocks',
    'name'          => 'Stocks',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);