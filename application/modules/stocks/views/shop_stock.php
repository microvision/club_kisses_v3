
<div class="header_bg">

    <div class="header">
        <div class="head-t">
            <div class="logo">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>
            <!-- start header_right -->
            <div class="header_right">
                <h3>Shop Stocks List </h3>
                <div class="search">

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button><br><hr>
        <!-- start content -->
        <div class="catalog"  id="table_wrapper">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Current Stock</th>
                    <th><span class="pull-right">Selling Price(KES)</span></th>

                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $num=0; foreach($items as $row): $num++;?>
                    <tr>
                        <td width="3%"><?php echo $num; ?></td>
                        <td><?php echo $row->product_name; ?></td>
                        <td><?php echo $row->description; ?></td>
                        <td align="left"><?php echo $row->category; ?></td>
                        <td align="center"><b><?php echo number_format($row->current_qty)." ".$row->unit_name; ?> </b></td>
                        <td align="right"><?php echo number_format($row->retail_price,2); ?></td>

                        <td>
                            <button class="btn btn-primary btn-xs" data-toggle="modal" href="#txtResult" title="edit_stock" onclick="htmlData('<?php echo base_url();?>stocks/edit_shop_stock', 'ch2=<?php echo $row->product_id; ?>')"><i class="fa fa-list"></i> stocks</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->

    </div>
