<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Signal POS - Receipt</title>
<script>
    function popitup(url) {
        newwindow=window.open(url,'name','height=540,width=340,top=50,left=500');
        if (window.focus) {newwindow.focus()}
        return false;
    }
    //window.onunload = refreshParent;
   	window.onfocus=function () {
        window.print();
        window.close();
    }
	
	//setTimeout(funtion(){window.close();},9);
</script>
<style type="text/css">
        .style1 {
            font-family: cambria,Agency FB;
            font-size: 22px
        }
		.style9 {
            font-family: cambria;
            font-size: 16px;
        }		
        .style2 {
            font-family: calibri;
            font-size: 15px;
        }

        .style5 {
			font-family: calibri;
            font-size: 18px;
        }

        
        .style3 {
            font-family: calibri;
            font-size: 13px;
        }

        .style10 {
            font-family: calibri;
            font-size: 20px;
        }

        .style15 {
            font-family: calibri;
            font-size: 17px;
        }

    </style>
</head>

<body  onFocus= "window.close()">
<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
<tr>
    <td colspan="4" align="center" valign="middle"><span class="style1">
	<?php

		echo $company->name;
		echo "<br><span class=\"style9\"> ".$company->address." <BR> ".$company->phone." </span>";
		echo "<br><span class=\"style9\">Stock Requisition</span>";
	?>
	</span>
	</td>	
</tr>

  <tr>
    <td colspan="4" align="left" bordercolor="#000000"><span class="style15">Supplier:
            <?php echo $order_details->supplier_name; ?>
    </span></td>
  </tr>
    <tr>
    <td colspan="3" align="left" bordercolor="#000000"><span class="style15">Delivery Date:
        <?php  echo date('d/M/Y',strtotime($order_details->delivery_date)); ?>
    </span></td>

    <td colspan="1" align="right" bordercolor="#000000"><span class="style15">
        <?php echo " #:" .$order_details->invoice_number; ?>
    </span></td>
  </tr>
  <tr><td colspan="4" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
</table>
<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
    <thead>
    <tr>
        <th class="style9">#</th>
        <th class="style9" align="left">Product</th>
        <th class="style9" align="left">Qty</th>
        <th class="style9" align="right"><span class="pull-right">Cost (Ksh)</span></th>

    </tr>
    </thead>
    <tbody>
    <?php $total=0; if($received_items): $num=0; foreach($received_items as $row): $num++; ?>
        <tr>
            <td width="2%" class="style2">
                <?php echo $num;  ?>
            </td>
            <td class="style2"><?php echo $row->product_name;?></td>
            <td class="style2"><?php echo $row->purchased_qty." ".$row->unit_name; ?></td>
            <td align="right" class="style2"> <?php echo number_format($row->buying_price*$row->purchased_qty,2); ?></td>

        </tr>
        <?php $total=$total+($row->buying_price*$row->purchased_qty); endforeach; endif; ?>
    <tr>
        <td></td>
        <td class="style2" colspan="2"><b>TOTAL</b></td>
        <td class="style2" align="right"><b> <?php echo number_format($total,2); ?></b></td>
    </tr>
    </tbody>
</table>


</body>
</html>
