<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends MY_Model {
	protected $table_name = 'vision_shop_orders';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_ordered_items($id)
    {
        return $this->db->query("SELECT bf_stores_order_items.*,measurement_unit,item,description,current_stock,store_name FROM bf_stores_order_items
                                    LEFT JOIN (SELECT SUM(current_qty)as current_stock,product_id FROM bf_stores_stocks WHERE status=1 GROUP BY product_id)as s ON s.product_id=bf_stores_order_items.item_id
									LEFT JOIN (SELECT bf_stores_inventory.*,measurement_unit,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_stores_measurement_units ON bf_stores_measurement_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=store_id) as items ON items.id=bf_stores_order_items.item_id
									WHERE order_id='".$id."'")->result();
    }
    public function get_store_ordered_items($id,$store_id)
    {
        return $this->db->query("SELECT bf_stores_order_items.*,measurement_unit,item,description,current_stock,store_name,store_id FROM bf_stores_order_items
                                    LEFT JOIN (SELECT SUM(current_qty)as current_stock,product_id FROM bf_stores_stocks WHERE status=1 GROUP BY product_id)as s ON s.product_id=bf_stores_order_items.id
									LEFT JOIN (SELECT bf_stores_inventory.*,measurement_unit,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_stores_measurement_units ON bf_stores_measurement_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=store_id) as items ON items.id=bf_stores_order_items.item_id
									WHERE order_id='".$id."' and store_id='".$store_id."'")->result();
    }
    public function get_user_orders($user_id)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name FROM bf_stores_orders 
									LEFT JOIN bf_users ON bf_users.id = order_by
									WHERE order_by='".$user_id."' ORDER BY id DESC LIMIT 50")->result();
    }
    public function get_order($id)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									WHERE bf_stores_orders.id='".$id."'")->row();
    }
    public function get_orders2()
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									WHERE status=-1 or status=2 or status=0 ORDER BY id DESC LIMIT 40")->result();
    }
    public function get_orders2_filtered($status)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									WHERE status=".$status." ORDER BY id DESC LIMIT 40")->result();
    }

    public function get_orders3()
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									WHERE status=2 or status=3 ORDER BY id DESC LIMIT 40")->result();
    }
    public function get_orders3_filtered($status)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									WHERE status=".$status." ORDER BY id DESC LIMIT 40")->result();
    }

    public function get_orders4($stores)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									RIGHT JOIN (SELECT order_id,store_id FROM bf_stores_order_items 
									                LEFT JOIN bf_stores_inventory ON bf_stores_inventory.id=item_id WHERE store_id in $stores GROUP BY order_id) as s ON s.order_id=bf_stores_orders.id
									WHERE status=2 or status=3 ORDER BY id DESC LIMIT 40")->result();
    }
    public function get_orders4_filtered($stores,$status)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name, department FROM bf_stores_orders 
									LEFT JOIN (SELECT bf_users.*,department FROm bf_users LEFT JOIN bf_departments ON bf_departments.id=department_id) as bf_users ON bf_users.id = order_by
									RIGHT JOIN (SELECT order_id,store_id FROM bf_stores_order_items 
									                LEFT JOIN bf_stores_inventory ON bf_stores_inventory.id=item_id WHERE store_id in $stores GROUP BY order_id) as s ON s.order_id=bf_stores_orders.id
									WHERE status=".$status." ORDER BY id DESC LIMIT 40")->result();
    }
	
    public function get_manager_store_id($user_id)
    {
        return $this->db->query("SELECT * FROM bf_stores_managers WHERE user_id='".$user_id."' LIMIT 1")->row();
    }
	public function get_manager_stores($user_id)
    {
        return $this->db->query("SELECT store_id FROM bf_stores_managers WHERE user_id='".$user_id."'")->result_array();
    }
    public function delete_order_items($order_id){
        $this->db->query("DELETE FROM bf_stores_order_items WHERE order_id='".$order_id."'");
    }
    public function cancel_order_items($order_id){
        $this->db->query("UPDATE bf_stores_order_items SET status=0 WHERE order_id='".$order_id."'");
    }
    public function activate_order_items($order_id){
        $this->db->query("UPDATE bf_stores_order_items SET status=1 WHERE order_id='".$order_id."'");
    }
    public function get_order_search_items($item,$user_id){
        return $this->db->query("SELECT s.*,order_id FROM bf_stores_order_items
                                    LEFT JOIN(SELECT bf_stores_orders.*,display_name FROM bf_stores_orders 
                                        LEFT JOIN bf_users ON bf_users.id = order_by
                                        WHERE order_by='".$user_id."' ORDER BY id) as s ON s.id=bf_stores_order_items.order_id
                                    WHERE item_id='".$item."' GROUP BY order_id DESC")->result();
    }
    public function get_order_search_items_all($item){
        return $this->db->query("SELECT s.*,order_id FROM bf_stores_order_items
                                    LEFT JOIN(SELECT bf_stores_orders.*,display_name FROM bf_stores_orders 
                                        LEFT JOIN bf_users ON bf_users.id = order_by
                                        ORDER BY id DESC) as s ON s.id=bf_stores_order_items.order_id
                                    WHERE item_id='".$item."' GROUP BY order_id DESC")->result();
    }
    public function get_order_search_period($date1,$date2,$user_id){
        return $this->db->query("SELECT bf_stores_orders.*,display_name FROM bf_stores_orders 
                                        LEFT JOIN bf_users ON bf_users.id = order_by
                                        WHERE order_by='".$user_id."' and order_date>='".$date1."' and order_date<='".$date2."' ORDER BY id DESC")->result();
    }
    public function get_order_search_period_all($date1,$date2){
        return $this->db->query("SELECT bf_stores_orders.*,display_name FROM bf_stores_orders 
                                        LEFT JOIN bf_users ON bf_users.id = order_by
                                        WHERE order_date>='".$date1."' and order_date<='".$date2."' ORDER BY id DESC")->result();
    }
}