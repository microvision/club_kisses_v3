<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_stocks_model extends MY_Model {
	protected $table_name = 'vision_stock_store';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


	public function get_received_items($id)
    {
        return $this->db->query("SELECT bf_vision_stock_store.*,unit_name,product_name,description,bf_vision_stock_store.id store_stock_id FROM bf_vision_stock_store
									LEFT JOIN (SELECT bf_vision_products.*,unit_name FROM bf_vision_products
												LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id) as products ON products.id=bf_vision_stock_store.product_id
									WHERE supply_order_id='".$id."'")->result();
    }
    public function get_received_items_old($id)
    {
        return $this->db->query("SELECT bf_vision_stock_store.*,unit_name,product_name,description,store_stock_id FROM bf_vision_stock_store
                                    LEFT JOIN bf_vision_stock_shop s ON s.store_stock_id = bf_vision_stock_store.id
									LEFT JOIN (SELECT bf_vision_products.*,unit_name FROM bf_vision_products
												LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id) as products ON products.id=bf_vision_stock_store.product_id
									WHERE supply_order_id='".$id."'")->result();
    }
    public function get_store_stocks()
    {
        return $this->db->query("SELECT SUM(current_qty) as current_qty,product_id,product_name,description,unit_name,dose,category,retail_price,wholesale_price FROM  bf_vision_stock_store
									LEFT JOIN (SELECT bf_vision_products.*,unit_name,bf_vision_product_categories.name as category FROM bf_vision_products 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id
													LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id=category_id) as products ON products.id=bf_vision_stock_store.product_id
									GROUP BY  bf_vision_stock_store.product_id")->result();
    }
    public function get_product_store_stocks($item_id)
    {
        return $this->db->query("SELECT * FROM  bf_vision_stock_store WHERE product_id='".$item_id."' and status=0")->result();
    }
    public function get_order_sum($id)
    {
        return $this->db->query("SELECT SUM(purchased_qty*buying_price)as totals,invoice_number,delivery_date FROM bf_vision_stock_store
								    LEFT JOIN bf_vision_supply_orders ON bf_vision_supply_orders.id=supply_order_id
									WHERE supply_order_id='".$id."'
									GROUP BY supply_order_id")->row();
    }
}