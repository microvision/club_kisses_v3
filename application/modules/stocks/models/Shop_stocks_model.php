<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop_stocks_model extends MY_Model {
	protected $table_name = 'vision_stock_shop';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function delete_puchase_stocks($store_item_id){
        $this->db->query("delete from bf_vision_stock_shop WHERE store_stock_id='".$store_item_id."'");
    }
    public function get_shop_stocks()
    {
        return $this->db->query("SELECT SUM(current_qty) as current_qty,product_id,product_name,description,unit_name,dose,category,retail_price,wholesale_price FROM  bf_vision_stock_shop
									LEFT JOIN (SELECT bf_vision_products.*,unit_name,bf_vision_product_categories.name as category FROM bf_vision_products 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id
													LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id=category_id) as products ON products.id=bf_vision_stock_shop.product_id
									GROUP BY  bf_vision_stock_shop.product_id")->result();
    }
    public function get_product_shop_stocks($item_id)
    {
        return $this->db->query("SELECT * FROM  bf_vision_stock_shop WHERE product_id='".$item_id."' ORDER BY id DESC LIMIT 10")->result();
    }
	
}