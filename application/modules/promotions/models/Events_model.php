<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events_model extends MY_Model {
	protected $table_name = 'vision_events';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_events()
    {
        return $this->db->query("SELECT * FROM bf_vision_events WHERE status != 0 order by start_date desc,id desc")->result();
    }
    //------------------------------------------------------
    public function get_event($id)
    {
        return $this->db->query("SELECT * FROM bf_vision_events WHERE id='".$id."'")->row();
    }
    //------------------------------------------------------
    public function get_event_products($event_id)
    {
        return $this->db->query("SELECT bf_vision_event_products.*,product_name item_name,unit_name unit_measure,qty FROM bf_vision_event_products LEFT JOIN 
										(SELECT p.*,u.unit_name FROM bf_vision_products p LEFT JOIN bf_vision_retail_units u ON p.unit_id=u.id) as items ON items.id=product_id
										LEFT JOIN bf_vision_events ON bf_vision_events.id=event_id
										LEFT JOIN(SELECT SUM(qty) as qty, product_id FROM bf_vision_sales s,bf_vision_events e 
										              WHERE e.start_date<=s.created_on and e.end_date>=s.created_on and  e.id='".$event_id."' 
										              GROUP BY product_id)as sales ON sales.product_id=bf_vision_event_products.product_id
									WHERE event_id='".$event_id."'")->result();
    }
    //------------------------------------------------------
    public function create_event(){
        $this->db->query("INSERT INTO bf_vision_events (status) VALUES (0)");
        return $this->db->query("SELECT MAX(id)as id FROM bf_vision_events")->row();
    }
    //------------------------------------------------------
    public function submit_event($event_id,$event,$start,$end,$type){
        $this->db->query("UPDATE bf_vision_events SET event='".$event."',start_date='".$start."',end_date='".$end."',type='".$type."', status=1 WHERE id='".$event_id."'");
    }
    public function save_event_product($event,$product,$oprice,$price){
        $this->db->query("INSERT INTO bf_vision_event_products (event_id,product_id,original_price,promotion_price) VALUES ('".$event."','".$product."','".$oprice."','".$price."')");
    }
    public function get_event_product($product_id)
    {
        return $this->db->query("SELECT bf_vision_event_products.*,event_id FROM bf_vision_event_products LEFT JOIN bf_vision_events ON bf_vision_events.id=event_id WHERE bf_vision_event_products.id='".$product_id."'")->row();
    }
    public function delete_event_product($product_id)
    {
        $this->db->query("DELETE FROM bf_vision_event_products WHERE id='".$product_id."'");
    }
	
}