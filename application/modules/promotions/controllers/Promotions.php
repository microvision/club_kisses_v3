<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotions extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		$this->load->model('events_model');
		$this->load->model('inventory_model');
	}
	


    public function index($id=null) {
       
        if($id){
            Template::set('event_details',  $this->events_model->get_event($id));
            Template::set('event_products', $this->events_model->get_event_products($id));
        }
        Template::set('products', $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                        ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                        ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                        ->where(array("bf_vision_products.id>" => 0))
                                        ->find_all());
        Template::set('events', $this->events_model->get_events());
        Template::set_theme('default');
        Template::set('page_title', 'Events & Promotions');
        Template::render('');
    }
    public function new_event() {
//        $event = $this->events_model->create_event();
        $data = array('status' => 0 );
        $event = $this->events_model->insert($data);
        redirect('promotions/index/'.$event,true);
    }
    public function submit_event(){
        if(ISSET($_POST['submit'])){
            $startdate = $_POST['start_date']." ".date("H:i",strtotime($_POST['start_time'])).":00";
            $startdate = date("Y-m-d H:i:s",strtotime($startdate));
            $enddate= $_POST['end_date']." ".date("H:i",strtotime($_POST['end_time'])).":00";
            $enddate = date("Y-m-d H:i:s",strtotime($enddate));
            $this->events_model->submit_event($_POST['event_id'],$_POST['event'],$startdate,$enddate,$_POST['occurence']);

            redirect('promotions/index/'.$_POST['event_id'],true);
        }
    }
    public function add_event_product()
    {
        $this->events_model->save_event_product($_GET['event'],$_GET['product'],$_GET['oprice'],$_GET['pprice']);
        $products = $this->events_model->get_event_products($_GET['event']);

        echo <<<eod
		<br>
										<table class="table">
											<thead>
												<tr>
													<th>Product</th>
													<th><span class="pull-right">Original Price(Ksh)</span></th>
													<th><span class="pull-right">Promotion Price(Ksh)</span></th>
													<th>Qty Sold</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
											<tbody>
eod;
        foreach($products as $row){
            $original_price = number_format($row->original_price,2);
            $promotion_price = number_format($row->promotion_price,2);
            echo"
												<tr>
													<td>".ucwords(strtolower($row->item_name))."</td>
													<td align=\"right\">".$original_price."</td>
													<td align=\"right\">".$promotion_price."</td>
													<td>".number_format($row->qty)." ".ucfirst(strtolower($row->unit_measure))."</td>
													<td>
													<a href=\"".base_url()."promotions/remove_event_product/".$row->id."\" class=\"btn btn-metis-5 btn-xs btn-grad\" data-original-title=\"\" title=\"\"><i class=\"glyphicon glyphicon-remove\"></i></a>
													</td>
												</tr>";
        }
        echo <<<eod
											</tbody>
										</table>
eod;
        //redirect('promotions/index/'.$_GET[event],true);
    }
    public function remove_event_product($product_id){
        $event_product=$this->events_model->get_event_product($product_id);
        $this->events_model->delete_event_product($product_id);
        redirect('promotions/index/'.$event_product->event_id,true);
    }

    public function get_product_details(){
        $product = $this->inventory_model->get_product($_GET['ch']);
        $url = base_url()."promotions/add_event_product";
        echo <<<eod
			<div class="col-md-3">
											<label class="control-label">Current Price (Ksh)</label>
											<input type="number" name="numbe2r" required="required" id="current_price" disabled placeholder="Current price" value="$product->retail_price" class="form-control">
											<input type="hidden" name="item_id" value="$product->id" id="item_id">
										</div>
										<div class="col-md-3">
											<label class="control-label">Promotional Price (Ksh)</label>
											<input type="number" name="numbe2r" required="required" id="promotion_price" placeholder="Promotional price" class="form-control">
										</div>
										<div class="col-md-1">
											<label class="control-label">&nbsp;</label><br>
											<button type="button" class="btn btn-danger pull-left" name="submit_product" id="submit_product" onclick="htmlData('$url','product='+item_id.value+'&event='+event_id.value+'&oprice='+current_price.value+'&pprice='+promotion_price.value)">Add </button>
										</div>
eod;
    }
    public function occurence_dates(){
        $data['occurence'] = $_GET['ch'];
        $this->load->view("promotions/occurence_dates", $data);
    }

}