<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>
            <div class="header_top">
                <h3 class="pull-left">Promotions & Events</h3>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="row">

            <div class="col-lg-3">
                <h4><u>Promotional Events</u></h4>
                <a href="<?php echo base_url();?>promotions/new_event" class="btn btn-danger btn-block"><i class="fa fa-plus"></i> Create Event</a>
                <table class="table table-hover">
                    <?php if(ISSET($events)): foreach ($events as $row): ?>
                        <tr onclick="location.href='<?php echo base_url()."promotions/index/".$row->id; ?>'">
                            <td><?php echo date('d/M/Y',strtotime($row->start_date)); ?></td>
                            <td><?php echo $row->event; ?></td>
                        </tr>
                    <?php endforeach; else: ?>
                        <tr>
                            <td colspan="2">No Events Created</td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
            <div class="col-lg-9">
                <?php if(ISSET($event_details)): ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="h5"><?php if($event_details->event){ echo $event_details->event; }else{echo "Event ".$event_details->id; } ?></span>
                        </div>
                        <div class="panel-body mail">
                            <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>promotions/submit_event" name="event_form" id="event_form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3">Event</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="event" required="" <?php echo ($event_details->start_date)?'value="'.$event_details->event.'"':''; ?> placeholder="Name of Event" class="form-control">
                                                <input type="hidden" name="event_id" id="event_id" value="<?php echo $event_details->id; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3">Occurence</label>
                                            <div class="col-lg-5">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="uniform" type="radio" name="occurence" value="1" onclick="htmlData4('<?php echo base_url(); ?>promotions/occurence_dates','ch='+this.value)" checked <?php echo ($event_details->type==1)?'checked':''; ?> >One Time Event
                                                    </label>
                                                </div><!-- /.checkbox -->
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input class="uniform" type="radio" name="occurence" value="2" <?php echo ($event_details->type==2)?'checked':''; ?> onclick="htmlData4('<?php echo base_url(); ?>promotions/occurence_dates','ch='+this.value)">Daily Event
                                                    </label>
                                                </div><!-- /.checkbox -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="txtResult4">
                                    <!-- Check for event type for dates display-->
                                    <?php if(($event_details->type)and ($event_details->type==2)): ?>
                                        <div class="col-lg-7">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Dates </label>
                                                <div class="col-lg-5">
                                                    Start Date
                                                    <input type="date" class="form-control" required="" name="start_date" id="date" <?php echo ($event_details->start_date)?'value="'.date('Y-m-d',strtotime($event_details->start_date)).'"':''; ?>>
                                                </div>

                                                <div class="col-lg-5">
                                                    End Date
                                                    <input type="date" class="form-control" required="" name="end_date" id="date" <?php echo ($event_details->end_date)?'value="'.date('Y-m-d',strtotime($event_details->end_date)).'"':''; ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Time</label>
                                                <div class="col-lg-5">
                                                    Daily Start
                                                    <input type="time" class="form-control" required=""  name="start_time" id="time" <?php echo ($event_details->start_date)?'value="'.date('H:i',strtotime($event_details->start_date)).'"':''; ?>>
                                                </div>
                                                <div class="col-lg-5">
                                                    Daily End
                                                    <input type="time" class="form-control" required="" name="end_time" id="time" <?php echo ($event_details->end_date)?'value="'.date('H:i',strtotime($event_details->end_date)).'"':''; ?>>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Starting </label>
                                                <div class="col-lg-5">
                                                    <input type="date" class="form-control" <?php echo ($event_details->start_date)?'value="'.date('Y-m-d',strtotime($event_details->start_date)).'"':''; ?> required="" name="start_date" id="date">
                                                </div>
                                                <div class="col-lg-4">
                                                    <input type="time" class="form-control" required="" <?php echo ($event_details->start_date)?'value="'.date('H:i',strtotime($event_details->start_date)).'"':''; ?> name="start_time" id="time">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Ending</label>
                                                <div class="col-lg-5">
                                                    <input type="date" class="form-control" <?php echo ($event_details->end_date)?'value="'.date('Y-m-d',strtotime($event_details->end_date)).'"':''; ?> required="" name="end_date" id="date">
                                                </div>
                                                <div class="col-lg-4">
                                                    <input type="time" class="form-control" required="" <?php echo ($event_details->end_date)?'value="'.date('H:i',strtotime($event_details->end_date)).'"':''; ?> name="end_time" id="time">
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div  class="row">
                                    <div class="col-lg-12">
                                        <u>Promotional Products</u>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <label class="control-label">Select Product</label>
                                        <select class="form-control chzn-select" name="store" id="store" tabindex="1" onchange="htmlData3('<?php echo base_url();?>promotions/get_product_details', 'ch='+this.value)" >
                                            <option value="" selected disabled>Select Product</option>
                                            <?php foreach($products as $row): ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo ucwords(strtolower($row->product_name)); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input type="hidden" name="eventid" id="eventid" value="<?php echo $event_details->id; ?>">
                                    </div>
                                    <div id="txtResult3">
                                        <div class="col-md-3">
                                            <label class="control-label">Current Price (Ksh)</label>
                                            <input type="number" name="numbe2r"  id="current_price" disabled placeholder="Current price" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Promotional Price (Ksh)</label>
                                            <input type="number" name="numbe2r"  id="promotion_price" placeholder="Promotional price" class="form-control">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="control-label">&nbsp;</label><br>
                                            <button type="button" class="btn btn-danger pull-left" >Add </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-lg-10" id="txtResult"><br>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th><span class="pull-right">Original Price</span></th>
                                                <th><span class="pull-right">Promotion Price</span></th>
                                                <th>Qty Sold</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($event_products as $row): ?>
                                                <tr>
                                                    <td><?php echo ucwords(strtolower($row->item_name)); ?></td>
                                                    <td align="right"><?php echo number_format($row->original_price,2); ?></td>
                                                    <td align="right"><?php echo number_format($row->promotion_price,2); ?></td>
                                                    <td><?php echo number_format($row->qty)." ".ucfirst(strtolower($row->unit_measure)); ?></td>
                                                    <td>
                                                        <?php echo "<a href=\"".base_url()."promotions/remove_event_product/".$row->id."\" class=\"btn btn-metis-5 btn-xs btn-grad\" data-original-title=\"\" title=\"\"><i class=\"glyphicon glyphicon-remove\"></i></a>"; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <hr>
<!--                                        <input type="submit" class="btn btn-danger btn-grad btn-lg pull-right" name="submit" value="Save Event Details">-->
                                        <button type="submit" class="btn btn-danger btn-grad btn-lg pull-right" name="submit" value="submit">Save Event Details</button
                                    </div>
                                </div>
                        </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <div id="txtResult2" class="modal fade"> </div>
        <div id="txtResult3" class="modal fade"> </div>
    </div>
</div>