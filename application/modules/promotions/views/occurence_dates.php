<?php if($occurence==1):?>								
								<div class="col-lg-6">
									<div class="form-group">
										<label class="control-label col-lg-3">Starting </label>
										<div class="col-lg-5">
										  <input type="date" class="form-control" required="" name="start_date" id="date">
										</div>
										<div class="col-lg-4">
										  <input type="time" class="form-control" required=""  name="start_time" id="time">
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="control-label col-lg-3">Ending</label>
										<div class="col-lg-5">
										  <input type="date" class="form-control"  required="" name="end_date" id="date">
										</div>
										<div class="col-lg-4">
										  <input type="time" class="form-control" required=""  name="end_time" id="time">
										</div>
									</div>
								</div>
<?php elseif($occurence==2):?>
								<div class="col-lg-7">
									<div class="form-group">
										<label class="control-label col-lg-2">Dates </label>
										<div class="col-lg-5">
										Start Date
										  <input type="date" class="form-control" required="" name="start_date" id="date">
										</div>
										
										<div class="col-lg-5">
										End Date
										  <input type="date" class="form-control" required="" name="end_date" id="date">
										</div>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label class="control-label col-lg-2">Time</label>
										<div class="col-lg-5">
										  Daily Start
										  <input type="time" class="form-control" required=""  name="start_time" id="time">
										</div>
										<div class="col-lg-5">
										Daily End
										  <input type="time" class="form-control" required="" name="end_time" id="time">
										</div>
									</div>
								</div>

<?php endif;?>								