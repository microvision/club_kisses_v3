<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expenses_model extends MY_Model {
	protected $table_name = 'vision_expenses';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_date';
	protected $modified_by_field = 'modified_by';

    public function get_accounts()
	{
		return $this->db->query("SELECT * FROM bf_vision_expense_accounts WHERE status=1")->result();
	}
	//------------------------------------------------------
	public function get_categories()
	{
		return $this->db->query("SELECT * FROM bf_vision_expense_category WHERE status=1")->result();
	}
	//------------------------------------------------------
	//public function get_users()
	//{
		//return $this->db->query("SELECT * FROM users where accountstatus=1")->result();
	//}
	//------------------------------------------------------
	public function get_today_expenses()
	{
		$dates=date('Y-m-d');
		return $this->db->query("SELECT bf_vision_expenses.*, bf_vision_expense_accounts.name as account_name, bf_vision_expense_category.name as category_name FROM bf_vision_expenses left join bf_vision_expense_accounts on bf_vision_expense_accounts.id=account_id left join bf_vision_expense_category on bf_vision_expense_category.id=category_id  where date(expense_date)='".$dates."'")->result();
	}
    public function get_all_expenses()
    {
        //$dates=date('Y-m-d');
        return $this->db->query("SELECT bf_vision_expenses.*, bf_vision_expense_accounts.name as account_name, bf_vision_expense_category.name as category_name FROM bf_vision_expenses left join bf_vision_expense_accounts on bf_vision_expense_accounts.id=account_id left join bf_vision_expense_category on bf_vision_expense_category.id=category_id ORDER BY bf_vision_expenses.id DESC")->result();
    }
	//------------------------------------------------------
	public function save_expense($category,$account,$amount,$user,$dates)
	{
		$this->db->query("INSERT INTO expenses (accountid_fk,categoryid_fk,userid_fk,amount,expense_date) VALUES ('".$account."','".$category."','".$user."','".$amount."','".$dates."')");
	}
	//------------------------------------------------------
	public function get_month_expenses($years,$months)
	{
		return $this->db->query("SELECT bf_vision_expenses.account_id,sum(bf_vision_expenses.amount) as amounts,bf_vision_expense_accounts.name as account_name FROM bf_vision_expenses left join bf_vision_expense_accounts on bf_vision_expense_accounts.id=account_id where YEAR(expense_date)=".$years." and MONTH(expense_date)=".$months." and type=1 GROUP BY account_id")->result();
	}
	//------------------------------------------------------
	public function get_daily_expenses()
	{
		$dates=date('Y-m-d');
		return $this->db->query("SELECT bf_vision_expenses.account_id,sum(bf_vision_expenses.amount) as amounts,bf_vision_expense_accounts.name as account_name FROM bf_vision_expenses left join bf_vision_expense_accounts on bf_vision_expense_accounts.id=account_id where date(expense_date)='".$dates."' and type=1 GROUP BY account_id")->result();
	}
    public function get_account_balances()
    {
        return $this->db->query("SELECT bf_vision_expense_accounts.*, bal FROM bf_vision_expense_accounts 
                                  LEFT JOIN (SELECT account_id, SUM(if(type='1',-amount,amount)) as bal FROM bf_vision_expenses GROUP BY account_id)as account_bal ON account_bal.account_id=bf_vision_expense_accounts.id
                                  WHERE bf_vision_expense_accounts.status=1")->result();
    }
}