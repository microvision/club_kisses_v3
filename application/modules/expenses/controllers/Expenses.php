<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Expenses extends Front_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		//Assets::add_css('jquery-ui-timepicker.css');
		$this->load->model('expenses_model');

//        Assets::add_module_js('billables', 'jquery.dataTables.min');
		
	}
	

    public function new_expense() {
        $this->auth->restrict('Vision.Expenses.View');
		if($this->input->post("submit")){
			//check that a value has been submitted
			if(!$this->input->post("expense_date")){
				$dates = date('Y-m-d');
			}else{
				$dates = $this->input->post("expense_date");
			}

                $data = array(
                    'category_id'=> $this->input->post("category"),
                    'account_id'=> $this->input->post("account"),
                    'received_by'=> $this->input->post("received_by"),
                    'expense_date'=> $dates,
                    'comments'=> $this->input->post("comments"),
                    'amount'=> $this->input->post("amount")
                );
                $this->expenses_model->insert($data);
                Template::set_message('The expense was successfully added', 'alert fresh-color alert-success');
				redirect("expenses/new_expense",true);
			
			
		}else{
			$data['page'] = 'Expenses';
			$years=date('Y');
			$months=date('m');
			Template::set('accounts_bal', $this->expenses_model->get_account_balances());
			Template::set('monthly_expenses', $this->expenses_model->get_month_expenses($years, $months));
			Template::set('daily_expenses', $this->expenses_model->get_daily_expenses());
			//$data['users'] = $this->expenses_model->get_users();
			Template::set('accounts', $this->expenses_model->get_accounts());
            Template::set('categories', $this->expenses_model->get_categories());
            if(ISSET($_GET['show'])){
                Template::set('expenses', $this->expenses_model->get_all_expenses());
            }else{
                Template::set('expenses', $this->expenses_model->get_today_expenses());
            }

			Template::set_theme('default');
			Template::set('page_title', 'Petty Cash Expenses');
			Template::render('');
		}
	}

	public function receive_deposit(){
        if($this->input->post("submit")){

            $data = array(
                'category_id'=> 0,
                'account_id'=> $this->input->post("deposit_account"),
                'type'=> 2,
                'received_by'=> $this->input->post("deposited_by"),
                'expense_date'=> $this->input->post("deposit_date"),
                'comments'=> $this->input->post("deposit_comments"),
                'amount'=> $this->input->post("deposit_amount")
            );
            $this->expenses_model->insert($data);
            Template::set_message('The cash deposit was successfully received', 'alert fresh-color alert-success');
            redirect("expenses/new_expense",true);


        }
	    redirect('expenses/new_expense',true);
    }

    public function delete_transaction(){
//        $this->auth->restrict('Vision.Clients.Delete_client');
        if (ISSET($_POST['submit'])){
            $transaction = $this->expenses_model->as_object()->find($_POST['transaction_id']);
            $this->expenses_model->delete($_POST['transaction_id']);
            log_activity($this->auth->user_id(),"Deleted Expense transaction ".$transaction->id." of amount ".$transaction->amount, 'catalog');
            Template::set_message('The expense transaction  was successfully deleted.', 'alert fresh-color alert-success');

            redirect('expenses/new_expense',true);
        }else{
            $data['transaction_id'] = $this->input->get("ch");
            $data['transaction'] = $this->expenses_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."expenses/delete_transaction";
            $data['action']="Delete";
            $this->load->view('expenses/delete_transaction',$data);
        }
    }
}