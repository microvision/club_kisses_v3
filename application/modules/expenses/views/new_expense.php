
    <?php

    /* Simple way to get current month name */

    $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");

    $date = getdate();
    $month = $date['mon'];
    //Assigning the current month to a variable for display
    $month_name = $mons[$month];

    // Getting todays date
    $dates=date('d');
    //variable for date and month
    $today = $dates."-".$month_name;


    ?>
    <div class="header_bg">
        <div class="header">
            <div class="head-t">
                <div class="logo pull-right">
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>

    </div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="row">
            <!--Start of orders list -->
            <div class="col-lg-3 col-sm-3">
                <h3>Expenses Summary</h3>
                <h5><u> Current Account Balances</u></h5>
                <h5>
                <table cellpadding="2" width="80%">
                    <?php $net=0; foreach($accounts_bal as $rows): ?>
                        <tr><td><?php echo $rows->name; ?></td><td>Ksh. <?php echo number_format($rows->bal);?></td></tr>
                        <?php $net=$net+$rows->bal; endforeach; ?>

<!--                    <tr><td><b>NET </b></td><td><b>Ksh. --><?php //echo number_format($net);?><!--</b></td></tr>-->
                </table></h5>

                <hr>
                <h5><u> Expenses for the Month (<?php $total=0; echo $month_name; ?>)</u></h5>
                <h5>
                <table cellpadding="2" width="80%">
                    <?php  foreach($monthly_expenses as $rows): ?>
                        <tr><td><?php echo $rows->account_name; ?></td><td>Ksh. <?php echo number_format($rows->amounts);?></td></tr>
                        <?php $total=$total+$rows->amounts; endforeach; ?>

                    <tr><td><b>Total</b></td><td><b>Ksh. <?php echo number_format($total);?></b></td></tr>
                </table></h5>

                <hr>
                <h5><u> Expenses for Today (<?php $total=0; echo $today; ?>)</u>
                <table cellpadding="2" width="80%">
                    <?php  foreach($daily_expenses as $rows): ?>
                        <tr><td><?php echo $rows->account_name; ?></td><td>Ksh. <?php echo number_format($rows->amounts);?></td></tr>
                        <?php $total=$total+$rows->amounts; endforeach; ?>

                    <tr><td><b>Total</b></td><td><b>Ksh. <?php echo number_format($total);?></b></td></tr>
                </table></h5>
			</div>
			<div class="col-lg-9 col-sm-9">
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target=".cash_deposit"><i class="fa fa-level-up"></i> Receive Petty Cash Deposit</button>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="box dark">
                            <header>
                                <h5>Add New Expense</h5>

                            </header>
                            <div id="stripedTable" class="body collapse in">
                                <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>expenses/new_expense" id="popup-validation">

                                    <div class="form-group">
                                        <label for="expense_date" class="control-label col-lg-4">Expense Date</label>
                                        <div class="col-lg-4">
                                            <input type="date"  required="" name="expense_date" id="expense_date" value="<?php echo date('Y-m-d');?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text1" class="control-label col-lg-4">Account</label>
                                        <div class="col-lg-4">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                            <select name="account" required="" class="form-control">
                                                <option>Select Account</option>
                                                <?php foreach ($accounts as $row):?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                <?php endforeach; ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text1" class="control-label col-lg-4">Category</label>
                                        <div class="col-lg-4">
                                            <select name="category" required="" class="form-control">
                                                <option>Select Category</option>
                                                <?php foreach ($categories as $row):?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text1" class="control-label col-lg-4">Received by</label>
                                        <div class="col-lg-4">
                                            <input type="text" required="" name="received_by" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text1" class="control-label col-lg-4">Amount</label>
                                        <div class="col-lg-4">
                                            <input type="number" step="0.01" required="" name="amount" placeholder="Expense amount" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text1" class="control-label col-lg-4">Description</label>
                                        <div class="col-lg-4">
                                            <textarea  name="comments" placeholder="Comments/Expense Description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-actions no-margin-bottom">
                                        <label class="control-label col-lg-4"></label>
                                        <div class="col-lg-12">
                                            <input type="submit" value="Add Expense" name="submit" class="btn btn-danger">
                                            <a type="button" value="Cancel" class="btn btn-default pull-right" href="<?php echo base_url(); ?>expenses/new_expense">Cancel </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><br>

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"><hr>
                <div class="box inverse">
                    <header>
                        <h5>Expenses For Today</h5><a href="<?php echo base_url();?>/expenses/new_expense?show=all" class="btn btn-primary btn-xs" >View All</a>

                    </header>
                    <div id="stripedTable" class="body collapse in">
                        <table class="table table-striped responsive-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Account</th>
                                <th>Category</th>
                                <th>Desc</th>
                                <th>Amount</th>
                                <th>Received By</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $x=0; foreach($expenses as $row): $x++?>
                                <tr>
                                    <td width="2%"><?php echo $x; ?></td>
                                    <td><?php echo date('d-M-Y',strtotime($row->expense_date));?> </td>
                                    <td><?php echo $row->account_name;?> </td>
                                    <td><?php echo ($row->type==1)?$row->category_name:"<span class='text-danger'>Cash Deposit</span>"; ?> </td>
                                    <td><?php echo $row->comments;?> </td>
                                    <td><?php echo number_format($row->amount,2);?> </td>
                                    <td width="3%"><?php echo $row->received_by;?> </td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                            <button type="button" class="btn btn-danger btn-grad" title="Delete Expense" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php echo base_url();?>expenses/delete_transaction', 'ch=<?php echo $row->id?>')">&nbsp;<i class="fa fa-shield"></i>&nbsp;</button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- end content -->
        <!-- #edit bills -->
        <div id="txtResult" class="modal fade"> </div>

        <!-- #transfer bills -->
        <div id="txtResult2" class="modal fade"> </div>

        <!-- #clear bills -->
        <div id="txtResult3" class="modal fade"> </div>

        <!-- CASH DEPOSIT MODAL-->
        <div class="modal fade cash_deposit" tabindex="-1" data-backdrop="false" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="deposit_form" name="deposit_form" class="form-horizontal" role="form" action="<?php echo base_url(); ?>expenses/receive_deposit" accept-charset="utf-8" autocomplete="off">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Receive Petty Cash Deposit</h4>
                    </div>
                    <div class="modal-body">


                        <div class="wizard-steps clearfix"></div>
                        <div class="step" id="account-details"><br>
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="username">Deposit Date</label>
                                <div class="col-lg-6">
                                    <input id="deposit_date" name="deposit_date" required="required" type="date" value="<?php echo date('Y-m-d');?>" class="form-control" />
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                </div>
                            </div><!-- End .form-group  -->
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Receiving A/C</label>
                                <div class="col-lg-6">
                                    <select name="deposit_account" required="" class="form-control">
                                        <option>Select Account</option>
                                        <?php foreach ($accounts as $row):?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="username">Received By</label>
                                <div class="col-lg-6">
                                    <input id="deposited_by" required="required" name="deposited_by" type="text" class="form-control" />
                                </div>
                            </div><!-- End .form-group  -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="username">Amount(Ksh)</label>
                                <div class="col-lg-6">
                                    <input id="deposit_amount" required="required" name="deposit_amount" type="number" class="form-control" />
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                </div>
                            </div><!-- End .form-group  -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="username">Comments</label>
                                <div class="col-lg-6">
                                    <textarea id="deposit_comments" name="deposit_comments" class="form-control"></textarea>
                                </div>
                            </div><!-- End .form-group  -->
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" name="submit"  class="btn btn-primary" value="Deposit Cash">
                        </div>
                </form>
            </div>
        </div>
    </div>
    </div>
