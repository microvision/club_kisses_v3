<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/20/2019
 * Time: 3:10 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Delete Expense Transaction</h4>
        </div>
        <form method="post" action="<?php echo $url; ?>">
            <div class="modal-body">
                <h4>
                    Are you Sure you want to delete this <b> <?php echo ($transaction->type==1)?"expense":"deposit"; ?> </b> Transaction?
                    <br>Transaction Date: <?php echo date('d-M-Y',strtotime($transaction->amount)) ?> | Amount KES <?php echo number_format($transaction->amount,2); ?>
                    <input type="hidden" name="transaction_id" value="<?php echo $transaction->id; ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                </h4>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-danger" >Delete Transaction</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
