<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Business expenses and pettycash',
    'name'          => 'Expenses',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);