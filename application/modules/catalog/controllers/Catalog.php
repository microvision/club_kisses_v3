<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Catalog extends Front_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		//Assets::add_css('jquery-ui-timepicker.css');
		$this->load->model('catalog_model');
		$this->load->model('stock_model');

//        Assets::add_module_js('billables', 'jquery.dataTables.min');
		
	}
	
	public function index($id=null){
		$this->auth->restrict('Vision.Catalog.View');
		
		Template::set('catalog', $this->catalog_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                                        ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                                        ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
														->where(array("bf_vision_products.id>" => 0))
                                                        ->find_all());


		Template::set_theme('default');
		Template::set('page_title', 'Catalog');
		Template::render('');

    }
    public function new_product(){
        $this->auth->restrict('Vision.Catalog.Create_product');
        if(ISSET($_POST['submit'])) {
            //we check if the product name has already been enterd in the system
            $product = $this->catalog_model->get_product_by_name($_POST['product_name']);
            if ((isset($product->counts)) and ($product->counts > 0)) {
                Template::set_message('Error Saving Product!! A product with the name '.$product_name.' has already been created', 'alert fresh-color alert-danger');
                redirect("catalog/new_product");
            } else {
                //we upload the product image and get a name
                if (!empty($_FILES['product_image']['name'])) {
                    $this->time = time();
                    $name = $_FILES["product_image"]["name"];
                    $ext = end((explode(".", $name)));
                    $config['upload_path'] = './themes/default/uploads/products';
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $config['max_size'] = '2000';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';
                    $config['file_name'] = $this->time . "." . $ext;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('product_image')) {
                        $x = $this->form_validation->set_message('uploadfile', $this->upload->display_errors());
                        Template::set_message("A problem was encountered while uploading your file!!" . $this->upload->display_errors(), 'alert fresh-color alert-danger');
                        $file_location ="";
                    } else {
                        $file_location = "uploads/products/" . $config['file_name'];
                    }
                } else {
                    $file_location = "";
                }
                //we save the product
                $wholesale_price=(ISSET($_POST['wholesale_price']))?$_POST['wholesale_price']:'';
                $data = array(
                    'product_name' => $_POST['product_name'],
                    'dose' => $_POST['dose'],
                    'category_id' => $_POST['product_category'],
                    'description' => $_POST['description'],
                    'status' => 1,
                    'reorder_level' => $_POST['reorder_level'],
                    'retail_price' => $_POST['retail_price'],
                    'unit_id' => $_POST['retail_measure'],
//                    'distribution_price' => $_POST['distribution_price'],
                    'wholesale_price' => $wholesale_price,
                    'trade_price' => $_POST['trade_price'],
                    'image_url' => $file_location,
                    'isvatable' => $_POST['VAT']
                );
                $product_id = $this->catalog_model->insert($data);
                //we start saving initial stock
                if($_POST['opening_stock'] and $_POST['purchase_price']){
                    $data_stock = array(
                        'product_id' => $product_id,
                        'store_stock_id' => 0,
                        'purchased_qty' => $_POST['opening_stock'],
                        'current_qty' => $_POST['opening_stock'],
                        'purchased_date' => date('Y-m-d'),
                        'buying_price' => $_POST['purchase_price'],
                        'store_order_id' => 0,
                        'status' => 1
                    );
                    $this->stock_model->insert($data_stock);
                }
                log_activity($this->auth->user_id(),"Created new product ".$_POST['product_name'], 'catalog');
                Template::set_message('The product was successfully created', 'alert fresh-color alert-success');
                redirect("catalog/new_product",true);
            }
        }

        Template::set('units', $this->catalog_model->get_measurement_units());
        Template::set('categories', $this->catalog_model->get_product_categories());
        Template::set_theme('default');
        Template::set('page_title', 'New Product');
        Template::render('');
    }
    public function add_wholesale_units(){
        $id = $this->input->get("ch2");
//        $wholesale_units = $this->inventory_model->get_wholesale_units($id);
        $wholesale_units = $this->catalog_model->get_measurement_units();

        echo <<<eod
                            <div class="form-group">
			                    <label class="control-label col-lg-4">Wholesale</label>
									<div class="col-lg-2">
										  <table><tr><td><input type="number" step="0.01" required="" class="form-control" name="wholesale_price" placeholder="Wholesale Price" id="req"></td><td><h5>Ksh</h5></td></tr></table>
										</div>
										<div class="col-lg-1"></div>
									<div class="col-lg-2">
									  <select name="wholesale_measure" id="sport" required="" class="form-control">
										<option value="">Wholesale Measure</option>
							
                            
eod;
                            foreach ($wholesale_units as $rows){
                                echo "<option value=\"".$rows->id."\">".$rows->unit_name."</option>";
                            }
        echo <<<eod
							</select>
									</div>
                        </div>
                            										
							
eod;
    }
    public function edit_product(){
        $this->auth->restrict('Vision.Catalog.Edit_products');
        if(ISSET($_POST['submit'])){
            $wholesale_price=(ISSET($_POST['wholesale_price']))?$_POST['wholesale_price']:'';
            $data = array(
                'product_name' => $_POST['product_name'],
                'dose' => $_POST['dose'],
                'category_id' => $_POST['product_category'],
                'description' => $_POST['description'],
                'reorder_level' => $_POST['reorder_level'],
                'retail_price' => $_POST['retail_price'],
//                'distribution_price' => $_POST['distribution_price'],
                'unit_id' => $_POST['retail_measure'],
                'wholesale_price' => $wholesale_price,
                'trade_price' => $_POST['trade_price'],
                'isvatable' => $_POST['VAT']
            );
            $this->catalog_model->update($_POST['product_id'],$data);
            log_activity($this->auth->user_id(),"Edited the product ".$_POST['product_name'], 'catalog');
            redirect("catalog/index",true);
        }else{
            $product_id = $_GET['ch2'];
            $data['product'] = $this->catalog_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                            ->find($product_id);
            $data['units'] = $this->catalog_model->get_measurement_units();
            $data['categories'] = $this->catalog_model->get_product_categories();
            $data['url']=base_url()."catalog/edit_product";
            $this->load->view('catalog/edit_product',$data);
        }

    }
    public function disable_product(){
        $this->auth->restrict('Vision.Catalog.Delete_products');
        if (ISSET($_POST['submit'])){
            $product = $this->catalog_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                                    ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                                    ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                                    ->find($_POST['product_id']);

            $data = array('status'=> 0);
            if ($this->catalog_model->update($_POST['product_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the product ".$product->product_name, 'catalog');
                Template::set_message('The product '.$product->product_name.' was successfully disabled.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the product. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('catalog/index',true);
        }else{
            $data['product_id'] = $this->input->get("ch");
            $data['product'] = $this->catalog_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                                            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                                            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                                            ->find($_GET['ch']);
            $data['url']=base_url()."catalog/disable_product";
            $data['action']=$_GET['bt'];
            $this->load->view('catalog/disable_product',$data);


        }
    }
    public function enable_product(){
        $this->auth->restrict('Vision.Catalog.Delete_products');
        if (ISSET($_POST['submit'])){
            $product = $this->catalog_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                ->find($_POST['product_id']);

            $data = array('status'=> 1);
            if ($this->catalog_model->update($_POST['product_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Activated the product ".$product->product_name, 'catalog');
                Template::set_message('The product '.$product->product_name.' was successfully activated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the product. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('catalog/index',true);
        }else{
            $data['product_id'] = $this->input->get("ch");
            $data['product'] = $this->catalog_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                                            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                                            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                                            ->find($_GET['ch']);
            $data['url']=base_url()."catalog/enable_product";
            $data['action']=$_GET['bt'];
            $this->load->view('catalog/disable_product',$data);


        }
    }
    public function receive_history(){
        $product_id = $_GET['ch'];
        $data['receive_history']= $this->catalog_model->get_receive_history($product_id);
        $data['product'] = $this->catalog_model->as_object()->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
            ->find($product_id);

        $this->load->view('catalog/receive_history',$data);
    }

    public function product_name_search(){
        $search = $_GET['ch'];
        $search_result = $this->catalog_model->get_name_search_options($search);
        $url=base_url()."catalog/change_name_field";
        foreach($search_result as $s) {
            echo "<a href=\"#\" class=\"text-danger\" onclick=\"htmlData3('".$url."','ch=".$s->product_name."')\">".$s->product_name."</a><br>";
        }
    }
    public function change_name_field(){
        $url = base_url()."catalog/product_name_search";
        $values = $_GET['ch'];
        echo <<<eod
        <input type="text" class="form-control" required="" name="product_name" value="$values" id="req" onkeyup="htmlData2('$url','ch='+this.value)">
eod;

    }
}