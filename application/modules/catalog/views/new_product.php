
<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <span class="h3 text-danger">New Product</span><hr>
            <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-lg-4">Product Name</label>
                    <div class="col-lg-5" id="txtResult3">
                        <input type="text" class="form-control" required="" name="product_name" id="req" onkeyup="htmlData2('<?php echo base_url(); ?>catalog/product_name_search','ch='+this.value)">

                    </div>
                    <div class="col-lg-3" id="txtResult2">

                    </div>
                </div>
<!--                <div class="form-group">-->
<!--                    <label class="control-label col-lg-4">Dosage</label>-->
<!--                    <div class="col-lg-5">-->
                        <input type="hidden" class="form-control"  name="dose" >
<!--                    </div>-->
<!--                </div>-->
                <div class="form-group">
                    <label class="control-label col-lg-4">Product Category</label>
                    <div class="col-lg-5">
                        <select name="product_category" id="sport" required="" class="form-control">
                            <option value="" selected disabled>Select a product category</option>
                            <?php foreach ($categories as $rows): ?>
                                <option value="<?php echo $rows->id; ?>"><?php echo $rows->name; ?></option>
                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Description</label>
                    <div class="col-lg-5">
                        <textarea id="limiter" class="form-control" name="description" maxlength="140"></textarea>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Product Image</label>
                    <div class="col-lg-5">
                        <input type="file" name="product_image" id="fileUpload" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Product Status</label>
                    <div class="col-lg-5">
                        <select name="product_status" id="sport" required="" class="form-control">
                            <option value="">Select product status</option>
                            <option value="0">Active</option>
                            <option value="1">In-active</option>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Reorder level </label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control" name="reorder_level" placeholder="Stocks reorder level alert option" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">VAT (16%)</label>
                    <div class="col-lg-5">
                        <select name="VAT" id="VAT" class="form-control" required="">
                            <option value="">Select VAT Inclusion options</option>
                            <option value="1">Charge VAT</option>
                            <option value="0">VAT Exempt</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Trade Price </label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control" name="trade_price" placeholder="Trade buying Price" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Retail Price</label>
                    <div class="col-lg-2 col-sm-5">
                        <table width="100%"><tr><td><input type="number" step="0.01" required="" class="form-control" name="retail_price" placeholder="Retail Price" id="req"></td><td><h5> Ksh</h5></td></tr></table>
                    </div>
                    <div class="col-lg-1 col-sm-2"></div>
                    <div class="col-lg-2 col-sm-5">
                        <select name="retail_measure" id="sport" required="" class="validate[required] form-control" onchange="htmlData('<?php echo base_url(); ?>catalog/add_wholesale_units', 'ch=0&ch2='+this.value)">
                            <option value="">Retail Measure</option>
                            <?php foreach ($units as $rows): ?>
                                <option value="<?php echo $rows->id; ?>"><?php echo $rows->unit_name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div id="txtResult">

                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2"></label>
                    <div class="col-lg-8"><hr></div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4">Opening Stock</label>
                    <div class="col-lg-2 col-sm-5">
                        <table width="100%"><tr><td><input type="number" step="0.01" class="form-control" name="purchase_price" placeholder="Purchase Price" id="req"></td><td><h5> Ksh</h5></td></tr></table>
                    </div>
                    <div class="col-lg-1 col-sm-2"></div>
                    <div class="col-lg-2 col-sm-5">
                        <table width="100%"><tr><td><input type="number" step="0.01" class="form-control" name="opening_stock" placeholder="Current Stock" id="req"></td></tr></table>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-lg-4"></label>
                    <div class="col-lg-8">
                        <input type="submit" value="Create Product" name="submit" class="btn btn-primary">
                        <a type="button" value="Cancel" class="btn btn-default pull-right" href="<?php echo base_url();?>catalog/new_product">Cancel </a>
                    </div>
                </div>
            </form>
        </div>

        <!-- end content -->

    </div>
