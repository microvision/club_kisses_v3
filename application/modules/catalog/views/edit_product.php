<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Details for <b><?php echo $product->product_name; ?></b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <br>

                <input type="hidden" name="productId" value="$itemId">
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Product Name</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="product_name" value ="<?php e($product->product_name); ?>" required="">
                        <input type="hidden" class="form-control" name="product_id" value ="<?php e($product->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Dosage</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control"  value ="<?php e($product->dose); ?>" name="dose" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Product Category</label>
                    <div class="col-lg-6 col-sm-6">
                        <select name="product_category" id="sport" class="form-control" required="">

                            <?php

                                foreach ($categories as $rows){
                                    $selected = ($rows->id == $product->category_id)?'selected=""selected':"";
                                    echo "<option ".$selected." value=\"".$rows->id."\">".$rows->name."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Description</label>
                    <div class="col-lg-6 col-sm-6">
                        <textarea id="limiter" class="form-control" name="description" maxlength="140"><?php e($product->description); ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Reorder level [Stock alert]</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="reorder_level" value ="<?php e($product->reorder_level); ?>" required="">
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">VAT(16%)</label>
                    <div class="col-lg-6 col-sm-6">
                        <select name="VAT" id="VAT" class="form-control">
                            <option value="">Select product VAT Status</option>
                            <option <?php echo($product->isvatable==1)?'selected="selected"':''; ?> value="1">Charge VAT</option>
                            <option <?php echo($product->isvatable==0)?'selected="selected"':''; ?> value="0">VAT Exempt</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Trade Price </label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control" name="trade_price" value="<?php echo $product->trade_price; ?>" placeholder="Trade buying Price" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4"></label>
                    <div class="col-lg-8 col-sm-8">
                        <hr>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Selling Price</label>
                    <div class="col-lg-4 col-sm-4">
                        <table width="100%"><tr><td><input type="text" class="form-control" name="retail_price" placeholder="Retail Price" value ="<?php e($product->retail_price); ?>" id="req"></td><td><h5>Ksh</h5></td></tr></table>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <select name="retail_measure" required="" class="form-control">

                        <?php

                            foreach ($units as $rows){
                                $selected = ($rows->id == $product->unit_id)?'selected=""selected':"";
                                echo "<option ".$selected." value=\"".$rows->id."\">".$rows->unit_name."</option>";
                            }
                            ?>
                        </select>
                    </div>


                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Wholesale Selling</label>
                    <div class="col-lg-4 col-sm-4">
                        <table width="100%"><tr><td><input type="text" class="form-control" name="wholesale_price" placeholder="Wholesale Price" value ="<?php e($product->wholesale_price); ?>" required=""></td><td><h5>Ksh</h5></td></tr></table>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <select name="wholesale_measure" required="" class="form-control">

                        <?php
                            foreach ($units as $rows){
                                $selected = ($rows->id == $product->unit_id)?'selected=""selected':"";
                                echo "<option ".$selected." value=\"".$rows->id."\">". $rows->unit_name."</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
<!--                <div class="form-group">-->
<!--                    <label class="control-label col-lg-4 col-sm-4">Distribution Selling</label>-->
<!--                    <div class="col-lg-4 col-sm-4">-->
<!--                        <table width="100%"><tr><td><input type="text" class="form-control" name="distribution_price" placeholder="Distribution Price" value ="--><?php //e($product->distribution_price); ?><!--" required=""></td><td><h5>Ksh</h5></td></tr></table>-->
<!--                    </div>-->
<!--                    <div class="col-lg-4 col-sm-4">-->
<!--                        <select name="distribution_measure" required="" class="form-control">-->
<!--                            --><?php
//                            foreach ($units as $rows){
//                                $selected = ($rows->id == $product->unit_id)?'selected=""selected':"";
//                                echo "<option ".$selected." value=\"".$rows->id."\">". $rows->unit_name."</option>";
//                            }
//                            ?>
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
