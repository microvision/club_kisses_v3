<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog_model extends MY_Model {
	protected $table_name = 'vision_products';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_date';
	protected $modified_by_field = 'modified_by';

    public function get_name_search_options($search)
    {
        return $this->db->query("SELECT * FROM bf_vision_products WHERE product_name LIKE '%".$search."%' ORDER BY product_name ASC LIMIT 5")->result();
    }
    public function get_measurement_units()
    {
        return $this->db->query("SELECT * FROM bf_vision_retail_units WHERE status=1")->result();
    }
    public function get_product_categories()
    {
        return $this->db->query("SELECT * FROM bf_vision_product_categories WHERE status=1")->result();
    }
    public function get_product_by_name($product_name)
    {
        return $this->db->query("SELECT count(*) as counts FROM  bf_vision_products  WHERE product_name='".$product_name."'")->row();

    }
    public function get_receive_history($product_id)
    {
        return $this->db->query("SELECT bf_vision_stock_shop.*,bf_vision_suppliers.name,phone,invoice_number,delivery_date FROM bf_vision_stock_shop 
                                  LEFT JOIN bf_vision_supply_orders ON bf_vision_supply_orders.id=bf_vision_stock_shop.store_order_id
                                  LEFT JOIN bf_vision_suppliers ON bf_vision_suppliers.id=bf_vision_supply_orders.supplier_id
                                  WHERE product_id='".$product_id."' ORDER BY delivery_date desc LIMIT 10")->result();
    }
	
}