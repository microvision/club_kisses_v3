<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Inventory Management and Product creation',
    'name'          => 'Catalog',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);