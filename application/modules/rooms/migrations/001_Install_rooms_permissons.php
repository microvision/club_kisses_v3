<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_rooms_permissons extends Migration
{

	/**
	 * Migrate Permissions for the finance module
	 *
	 * @var Array
	 */
	private $permission_values = array(
		array(
			'name'          => 'Vision.Rooms.Create_room',
			'description'   => 'Can create new hotel rooms.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Rooms.Edit_rooms',
			'description'   => 'Can edit hotel rooms details and prices.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Rooms.Delete_rooms',
			'description'   => 'Can disable and enable hotel rooms for allocation.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Rooms.Manage_rooms',
			'description'   => 'Overall Management of rooms and cancel allocations or reservations.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Rooms.Allocate_rooms',
			'description'   => 'Can allocate rooms to new guests and make reservations.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Rooms.View',
			'description'   => 'Can view the rooms module and links.',
			'status'        => 'active',
		),
		
		
		
	);

    /**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $table_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->table_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->table_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->table_name, array('name' => $permission_value['name']));
		}
	}

	//--------------------------------------------------------------------

}