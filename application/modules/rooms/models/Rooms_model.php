<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rooms_model extends MY_Model {
	protected $table_name = 'vision_rooms';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_name_search_options($search)
    {
        return $this->db->query("SELECT * FROM bf_vision_rooms WHERE room_no LIKE '%".$search."%' ORDER BY room_no ASC LIMIT 5")->result();
    }
    public function get_room_levels()
    {
        return $this->db->query("SELECT * FROM bf_vision_room_levels WHERE status=1")->result();
    }
    public function get_room_categories()
    {
        return $this->db->query("SELECT * FROM bf_vision_room_categories WHERE status=1")->result();
    }
    public function get_room_by_name($room_name)
    {
        return $this->db->query("SELECT count(*) as counts FROM  bf_vision_rooms  WHERE room_no='".$room_name."'")->row();

    }
    public function get_allocation_history($room_id)
    {
        return $this->db->query("SELECT * FROM bf_vision_room_allocation 
                                  WHERE room_id='".$room_id."' ORDER BY created_on desc LIMIT 200")->result();
    }
	
}