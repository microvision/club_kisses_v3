<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rooms_allocation_model extends MY_Model {
	protected $table_name = 'vision_room_allocation';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


	public function allocation_check($room_id){
	    return $this->db->query("SELECT * FROM bf_vision_room_allocation WHERE room_id='".$room_id."' AND status > 0 ORDER BY id DESC LIMIT 1")->row();
    }
    public function allocated_rooms(){
        return $this->db->query("SELECT bf_vision_room_allocation.*,bf_vision_rooms.room_no, bf_vision_room_categories.category, bf_vision_room_levels.level   FROM bf_vision_room_allocation 
                                              LEFT JOIN bf_vision_rooms ON bf_vision_rooms.id = bf_vision_room_allocation.room_id
                                              LEFT JOIN bf_vision_room_categories On bf_vision_room_categories.id=bf_vision_rooms.category
                                              LEFT JOIN bf_vision_room_levels ON bf_vision_room_levels.id = bf_vision_rooms.level
                                              WHERE bf_vision_room_allocation.status > 0 ORDER BY id")->result();
    }
    public function checkout_rooms(){
        $this->db->query("UPDATE bf_vision_room_allocation,bf_vision_rooms SET bf_vision_room_allocation.status=0,bf_vision_rooms.occupancy=0 
                                              WHERE bf_vision_rooms.id=bf_vision_room_allocation.room_id AND bf_vision_room_allocation.status=1 AND CURDATE()>check_out");
    }
    public function get_company()
    {
        return $this->db->query("SELECT * FROM bf_vision_business_profile LIMIT 1")->row();
    }


}