<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rooms extends Front_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		$this->load->model('rooms_model');
		$this->load->model('rooms_allocation_model');
		$this->load->model('room_categories_model');
		$this->load->model('room_levels_model');
	}
	
	public function index($id=null){
		$this->auth->restrict('Vision.Rooms.View');
		Template::set('rooms', $this->rooms_model->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                               ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                               ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                               ->find_all());
		Template::set_theme('default');
		Template::set('page_title', 'Catalog');
		Template::render('');
    }
    public function new_room(){
        $this->auth->restrict('Vision.Rooms.Create_room');
        if(ISSET($_POST['submit'])) {
            //we check if the product name has already been enterd in the system
            $room = $this->rooms_model->get_room_by_name($_POST['room_name']);
            if ((isset($room->counts)) and ($room->counts > 0)) {
                Template::set_message('Error Saving Hotel Room!! A room with the name/number '.$_POST['room_name'].' has already been created', 'alert fresh-color alert-danger');
                redirect("rooms/new_room");
            } else {
                //we save the room
                $data = array(
                    'room_no' => $_POST['room_name'],
                    'category' => $_POST['room_category'],
                    'description' => $_POST['description'],
                    'status' => 1,
                    'price' => $_POST['price'],
                    'price' => $_POST['price'],
                    'level' => $_POST['room_level']
                );
                $room_id = $this->rooms_model->insert($data);

                log_activity($this->auth->user_id(),"Created new hotel room ".$room_id, 'rooms');
                Template::set_message('The Hotel room was successfully created', 'alert fresh-color alert-success');
                redirect("rooms/new_room",true);
            }
        }

        Template::set('levels', $this->rooms_model->get_room_levels());
        Template::set('categories', $this->rooms_model->get_room_categories());
        Template::set_theme('default');
        Template::set('page_title', 'Hotel Rooms');
        Template::render('');
    }
    public function edit_room(){
        $this->auth->restrict('Vision.Rooms.Edit_rooms');
        if(ISSET($_POST['submit'])){
            $data = array(
                'room_no' => $_POST['room_name'],
                'category' => $_POST['room_category'],
                'description' => $_POST['description'],
                'price' => $_POST['price'],
                'level' => $_POST['room_level']
            );
            $this->rooms_model->update($_POST['room_id'],$data);
            log_activity($this->auth->user_id(),"Edited the room ".$_POST['room_name'], 'rooms');
            redirect("rooms/index",true);
        }else{
            $room_id = $_GET['ch2'];
            $data['rooms'] = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                        ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                        ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                        ->find($room_id);
            $data['levels'] = $this->rooms_model->get_room_levels();
            $data['categories'] = $this->rooms_model->get_room_categories();
            $data['url']=base_url()."rooms/edit_room";
            $this->load->view('rooms/edit_room',$data);
        }

    }
    public function disable_room(){
        $this->auth->restrict('Vision.Rooms.Delete_rooms');
        if (ISSET($_POST['submit'])){
            $room = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                            ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                            ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                            ->find($_POST['room_id']);

            $data = array('status'=> 0);
            if ($this->rooms_model->update($_POST['room_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the hotel room ".$room->room_no, 'rooms');
                Template::set_message('The hotel room '.$room->room_no.' was successfully disabled.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the hotel room. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/index',true);
        }else{
            $data['room_id'] = $this->input->get("ch");
            $data['room'] = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                            ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                            ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                            ->find($_GET['ch']);
            $data['url']=base_url()."rooms/disable_room";
            $data['action']=$_GET['bt'];
            $this->load->view('rooms/disable_room',$data);


        }
    }
    public function enable_room(){
        $this->auth->restrict('Vision.Rooms.Delete_rooms');
        if (ISSET($_POST['submit'])){
            $room = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                        ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                        ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                        ->find($_POST['room_id']);

            $data = array('status'=> 1);
            if ($this->rooms_model->update($_POST['room_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Activated the hotel room ".$room->room_no, 'rooms');
                Template::set_message('The hotel room '.$room->room_no.' was successfully activated.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the hotel room. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/index',true);
        }else{
            $data['room_id'] = $this->input->get("ch");
            $data['room'] = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                            ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                            ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                            ->find($_GET['ch']);
            $data['url']=base_url()."rooms/enable_room";
            $data['action']=$_GET['bt'];
            $this->load->view('rooms/disable_room',$data);
        }
    }
    public function allocation_history(){
        $room_id = $_GET['ch'];
        $data['allocation_history']= $this->rooms_model->get_allocatin_history($room_id);
        $data['product'] = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                        ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                        ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                        ->find($room_id);

        $this->load->view('rooms/allocation_history',$data);
    }

    public function room_name_search(){
        $search = $_GET['ch'];
        $search_result = $this->rooms_model->get_name_search_options($search);
        $url=base_url()."rooms/change_name_field";
        foreach($search_result as $s) {
            echo "<a href=\"#\" class=\"text-danger\" onclick=\"htmlData3('".$url."','ch=".$s->room_no."')\">".$s->room_no."</a><br>";
        }
    }
    public function change_name_field(){
        $url = base_url()."rooms/product_name_search";
        $values = $_GET['ch'];
        echo <<<eod
        <input type="text" class="form-control" required="" name="product_name" value="$values" id="req" onkeyup="htmlData2('$url','ch='+this.value)">
eod;

    }
}