<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		$this->load->model('room_categories_model');
	}

    public function index($id=null){
        $this->auth->restrict('Vision.Rooms.Manage_rooms');
        Template::set('categories', $this->room_categories_model->order_by('category','asc')
            ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Hotel Room Categories');
        Template::render('');
    }
    public function create_category(){
        if(ISSET($_POST['category_name'])){
            $data = array(
                'category'=> $_POST['category_name'],
                'status'=> 1
            );
            if ($this->room_categories_model->insert($data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Created new hotel room category ".$_POST['category_name'], 'rooms');
                Template::set_message('The hotel room category was successfully created.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered creating the hotel room category. Please check the values submitted.', 'alert fresh-color alert-danger');
            }
        }
        redirect('rooms/categories/index',true);
    }
    public function disable_category(){
        if (ISSET($_POST['submit'])){
            $cat = $this->room_categories_model->as_object()->find($_POST['category_id']);

            $data = array('status'=> 0);
            if ($this->room_categories_model->update($_POST['category_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the hotel room category ".$cat->category, 'rooms');
                Template::set_message('The hotel room category was successfully disabled.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the hotel room category. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/categories/index',true);
        }else{
            $data['category_id'] = $this->input->get("ch");
            $data['cat'] = $this->room_categories_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."rooms/categories/disable_category";
            $data['action']=$_GET['bt'];
            $this->load->view('rooms/categories/disable_category',$data);
        }
    }
    public function enable_category(){
        if (ISSET($_POST['submit'])){
            $cat = $this->room_categories_model->as_object()->find($_POST['category_id']);
            $data = array('status'=> 1);
            if ($this->room_categories_model->update($_POST['category_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Enabled the hotel room category ".$cat->category, 'rooms');
                Template::set_message('The hotel room category was successfully activated.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the hotel room category. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/categories/index',true);
        }else{
            $data['category_id'] = $this->input->get("ch");
            $data['cat'] = $this->room_categories_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."rooms/categories/enable_category";
            $data['action']=$_GET['bt'];
            $this->load->view('rooms/categories/disable_category',$data);
        }
    }
    public function edit_category()
    {
        if (ISSET($_POST['submit'])) {
            $cat = $this->room_categories_model->as_object()->find($_POST['category_id']);
            $data = array('category' => $_POST['category_name']);
            if ($this->room_categories_model->update($_POST['category_id'], $data)) {
                // Log the Activity
                log_activity($this->auth->user_id(), "Edited the hotel room category " . $cat->category, 'rooms');
                Template::set_message('The hotel room category was successfully changed.', 'alert fresh-color alert-success');
            } else {
                Template::set_message('Error Saving!! A problem was encountered editing the hotel room category. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/categories/index', true);
        } else {
            $data['category_id'] = $this->input->get("ch");
            $data['cat'] = $this->room_categories_model->as_object()->find($_GET['ch']);
            $data['url'] = base_url() . "rooms/categories/edit_category";
            $this->load->view('rooms/categories/edit_category', $data);
        }
    }
}