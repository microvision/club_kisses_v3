<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Levels extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		$this->load->model('room_levels_model');
	}

    public function index($id=null){
        $this->auth->restrict('Vision.Rooms.Manage_rooms');
        Template::set('levels', $this->room_levels_model->order_by('level','asc')
            ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Hotel Room Levels');
        Template::render('');
    }
    public function create_level(){
        if(ISSET($_POST['level_name'])){
            $data = array(
                'level'=> $_POST['level_name'],
                'status'=> 1
            );
            if ($this->room_levels_model->insert($data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Created new hotel room level ".$_POST['level_name'], 'rooms');
                Template::set_message('The hotel room level was successfully created.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered creating the hotel room level. Please check the values submitted.', 'alert fresh-color alert-danger');
            }
        }
        redirect('rooms/levels/index',true);
    }
    public function disable_level(){
        if (ISSET($_POST['submit'])){
            $cat = $this->room_levels_model->as_object()->find($_POST['level_id']);

            $data = array('status'=> 0);
            if ($this->room_levels_model->update($_POST['level_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the hotel room level ".$cat->level, 'rooms');
                Template::set_message('The hotel room level was successfully disabled.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the hotel room level. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/levels/index',true);
        }else{
            $data['level_id'] = $this->input->get("ch");
            $data['cat'] = $this->room_levels_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."rooms/levels/disable_level";
            $data['action']=$_GET['bt'];
            $this->load->view('rooms/levels/disable_level',$data);
        }
    }
    public function enable_level(){
        if (ISSET($_POST['submit'])){
            $cat = $this->room_levels_model->as_object()->find($_POST['level_id']);
            $data = array('status'=> 1);
            if ($this->room_levels_model->update($_POST['level_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Enabled the hotel room level ".$cat->level, 'rooms');
                Template::set_message('The hotel room level was successfully activated.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the hotel room level. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/levels/index',true);
        }else{
            $data['level_id'] = $this->input->get("ch");
            $data['cat'] = $this->room_levels_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."rooms/levels/enable_level";
            $data['action']=$_GET['bt'];
            $this->load->view('rooms/levels/disable_level',$data);
        }
    }
    public function edit_level()
    {
        if (ISSET($_POST['submit'])) {
            $cat = $this->room_levels_model->as_object()->find($_POST['level_id']);
            $data = array('level' => $_POST['level_name']);
            if ($this->room_levels_model->update($_POST['level_id'], $data)) {
                // Log the Activity
                log_activity($this->auth->user_id(), "Edited the hotel room level " . $cat->level, 'rooms');
                Template::set_message('The hotel room level was successfully changed.', 'alert fresh-color alert-success');
            } else {
                Template::set_message('Error Saving!! A problem was encountered editing the hotel room level. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('rooms/levels/index', true);
        } else {
            $data['level_id'] = $this->input->get("ch");
            $data['cat'] = $this->room_levels_model->as_object()->find($_GET['ch']);
            $data['url'] = base_url() . "rooms/levels/edit_level";
            $this->load->view('rooms/levels/edit_level', $data);
        }
    }
}