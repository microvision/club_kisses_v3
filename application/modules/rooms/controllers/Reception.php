<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reception extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		$this->load->model('rooms_allocation_model');
		$this->load->model('rooms_model');
        $this->load->model('room_categories_model');
        $this->load->model('room_levels_model');
	}

    public function index($id=null){
        //checkout rooms beyond their checkout time
        $this->rooms_allocation_model->checkout_rooms();
        //Load the booking room
        $this->set_current_user();

        Template::set('rooms', $this->rooms_model->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
                                    ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
                                    ->where(array("bf_vision_rooms.status" => '1'))
                                    ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
                                    ->find_all());
        Template::set('categories', $this->room_categories_model->where(array("status" => '1'))->order_by('category','asc')->find_all());
        Template::set('levels', $this->room_levels_model->where(array("status" => '1'))->order_by('level','asc')->find_all());
        Template::set('allocated_rooms', $this->rooms_allocation_model->allocated_rooms());
        Template::set_theme('default');
        Template::set('page_title', 'Hotel Reception');
        Template::set('booking',$id);

        Template::render();
    }
    public function allocation_modal(){

        $this->set_current_user();
        $data['current_user'] = $this->current_user->id;
        $room_id = $this->input->get('ch1');
        $user_id = $this->session->userdata('user_id');
        //check if the current room allocation
        $data['allocation_check'] = $this->rooms_allocation_model->allocation_check($room_id);
        $data['room_details'] = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
            ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
            ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
            ->find($room_id);
        $this->load->view("rooms/reception/allocation_modal",$data);

    }
    public function allocate_room(){
	    switch ($_GET['submit']){
            case 'Reserve Room':
                $payment_mode = 0;
                break;
            case 'Receive Cash Payment':
                $payment_mode = 1;
                break;
            case 'Receive M-pesa Payment':
                $payment_mode = 2;
                break;
            default:
                $payment_mode = 1;
        }
        //allocated room
        $check_in = $_GET['checkin_date']." ".$_GET['checkin_time'];
        $check_out = $_GET['checkout_date']." ".$_GET['checkout_time'];
        $current = date('Y-m-d H:i:s');
        //update room status

        $booking = array(
            'room_id' => $_GET['room_id'],
            'national_id' => $_GET['national_id'],
            'occupant_name' => $_GET['guest_name'],
            'phone' => $_GET['phone'],
            'address' => $_GET['address'],
            'guests' => $_GET['guests'],
            'check_in' => $check_in,
            'check_out' => $check_out,
            'cost' => $_GET['price'],
            'payment_mode' => $payment_mode,
            'status' => 1
        );
        $id=$this->rooms_allocation_model->insert($booking);
        $booking=$this->rooms_allocation_model->find($id);
        //update room status if booking is within current time
        if(strtotime($check_in)<=strtotime($current) and strtotime($check_out)>=strtotime($current)){
            $data = array('occupancy'=> 1);
            $this->rooms_model->update($_GET['room_id'],$data);
        }
        //room details
        $room = $this->rooms_model->as_object()->join('bf_vision_room_categories','bf_vision_room_categories.id=bf_vision_rooms.category','left')
            ->join('bf_vision_room_levels','bf_vision_room_levels.id=bf_vision_rooms.level','left')
            ->select('bf_vision_rooms.*,bf_vision_room_categories.category category_name,bf_vision_room_levels.level level_name')
            ->find($_GET['room_id']);
        // Log the Activity
        log_activity($this->auth->user_id(),"Allocated hotel room ".$room->room_no, 'rooms');
        Template::set_message('The hotel room '.$room->room_no.' was successfully allocated.', 'alert fresh-color alert-success');

        $data['company']=$this->rooms_allocation_model->get_company();
        $data['booking']=$booking;
        $data['username']=$this->current_user->display_name;;
        $this->load->view("reception/print_receipt", $data);
//        Template::set('booking','dsafdadf'.$booking->id);
//        redirect('rooms/reception/index/'.$booking->id,false);
    }
    public function print_receipt($id){
        $booking=$this->rooms_allocation_model->find($id);
        $data['company']=$this->rooms_allocation_model->get_company();
        $data['booking']=$booking;
        $data['username']=$this->current_user->display_name;;
        $this->load->view("reception/print_receipt", $data);
    }
    public function cancel_allocation($allocation_id){
        $allocation_details = $this->rooms_allocation_model->as_object()
                                    ->join('bf_vision_rooms','bf_vision_rooms.id=room_id','left')
                                    ->select('bf_vision_room_allocation.*,room_no')
                                    ->find($allocation_id);
        //cancel allocation
        $data = array('status'=> -1);
        $this->rooms_allocation_model->update($allocation_id,$data);
        //declare the room vacant
        $data = array('occupancy'=> 0);
        $this->rooms_model->update($allocation_details->room_id,$data);
        //log activity
        log_activity($this->auth->user_id(),"Cancelled hotel room allocation ".$allocation_id." for room no ".$allocation_details->room_no, 'rooms');
        Template::set_message('The hotel room '.$allocation_details->room_no.' was successfully allocated.', 'alert fresh-color alert-success');
        //redirect to reception home
        redirect('rooms/reception',false);
    }
    public function vacate_room($allocation_id){
        $allocation_details = $this->rooms_allocation_model->as_object()
            ->join('bf_vision_rooms','bf_vision_rooms.id=room_id','left')
            ->select('bf_vision_room_allocation.*,room_no')
            ->find($allocation_id);
        $data = array('occupancy'=> 0);
        $this->rooms_model->update($allocation_details->room_id,$data);
        //log activity
        log_activity($this->auth->user_id(),"Declared hotel room ".$allocation_details->room_no." vacant for allocation ".$allocation_id, 'rooms');
        Template::set_message('The hotel room '.$allocation_details->room_no.' has been declared vacant. You can now reallocate the room', 'alert fresh-color alert-success');
        //redirect to reception home
        redirect('rooms/reception',false);
    }

}