<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Receive history for <b><?php echo $product->product_name; ?></b></h4>
        </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Supplier</th>
                                    <th>Invoice No</th>
                                    <th>Delivery Date</th>
                                    <th>Buying Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($receive_history as $row): ?>
                                <tr>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->invoice_number; ?></td>
                                    <td><?php echo date('d-M-Y',strtotime($row->delivery_date)); ?></td>
                                    <td>Ksh <?php echo number_format($row->buying_price,2); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

    </div><!-- /.modal-content -->
</div>
