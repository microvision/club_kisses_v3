<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 06/21/2021
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Details for Hotel Room <b><?php echo $rooms->room_no; ?></b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <br>

                <input type="hidden" name="productId" value="$itemId">
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Room Number/Name</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="room_name" value ="<?php e($rooms->room_no); ?>" required="">
                        <input type="hidden" class="form-control" name="room_id" value ="<?php e($rooms->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Room Category</label>
                    <div class="col-lg-6 col-sm-6">
                        <select name="room_category" id="sport" class="form-control" required="">

                            <?php

                                foreach ($categories as $rows){
                                    $selected = ($rows->id == $rooms->category)?'selected=""selected':"";
                                    echo "<option ".$selected." value=\"".$rows->id."\">".$rows->category."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Description</label>
                    <div class="col-lg-6 col-sm-6">
                        <textarea id="limiter" class="form-control" name="description" maxlength="140"><?php e($rooms->description); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Room Level</label>
                    <div class="col-lg-6 col-sm-6">
                        <select name="room_level" id="sport" class="form-control" required="">

                            <?php

                            foreach ($levels as $rows){
                                $selected = ($rows->id == $rooms->level)?'selected=""selected':"";
                                echo "<option ".$selected." value=\"".$rows->id."\">".$rows->level."</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Room Price</label>
                    <div class="col-lg-6">
                        <table width="100%"><tr><td><input type="text" class="form-control" name="price" placeholder="Cost per night" value ="<?php e($rooms->price); ?>" id="req"></td><td><h5>Ksh</h5></td></tr></table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
