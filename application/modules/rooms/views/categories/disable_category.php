<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 06/20/2021
 * Time: 01:54 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?php echo $action; ?> Hotel Room Category</h4>
        </div>
        <form method="post" action="<?php echo $url; ?>">
            <div class="modal-body">
                <h4>
                    Are you Sure you want to <?php echo $action; ?> the room category <b> <?php echo $cat->category; ?> </b> ?
                    <input type="hidden" name="category_id" value="<?php echo $cat->id; ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                </h4>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-danger" ><?php echo $action; ?> Hotel Room Category</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
