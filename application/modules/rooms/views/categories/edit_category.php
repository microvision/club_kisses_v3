<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 06/20/2021
 * Time: 01:10 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Hotel Room Category</h4>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo $url; ?>">
            <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-lg-4">Category Name</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" required="" name="category_name" value="<?php echo $cat->category; ?>">
                        </div>
                    </div>
                    <input type="hidden" name="category_id" value="<?php echo $cat->id; ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >

            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
