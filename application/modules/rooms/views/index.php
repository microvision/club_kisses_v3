
<div class="header_bg">

    <div class="header">
        <div class="head-t">

            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>
            <!-- start header_right -->
            <div class="header_top">
                <h3 class="pull-left">Hotel Rooms</h3>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>Rooms #</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Level</th>
                    <th><span class="pull-right">Cost</span></th>

                    <th>Status</th>
                    <th width="8%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if($rooms): foreach($rooms as $row):?>
                <tr class="<?php echo ($row->status==0)?"bg-warning":"";?>">
                    <td width='20%'>
                        <?php e($row->room_no); ?>
                    </td>

                    <td><?php e($row->description);?></td>
                    <td><?php e($row->category_name);?></td>
                    <td><?php e($row->level_name);?></td>
                    <td align='right'><?php e(number_format($row->price,2));?></td>

                    <td><?php echo($row->status==1)?"Active":"<span class='text-danger'>Disabled</span>";?></td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <?php if($row->status==1): ?>
                                <?php if(has_permission('Vision.Rooms.Edit_rooms')):?>
                                    <button type="button" class="btn btn-default btn-default" title="Edit Room" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>rooms/edit_room', 'ch2=<?php e($row->id);?>&amp;ch=1')"><i class="fa fa-edit"></i></button>
                                <?php endif;?>
                                <?php if(has_permission('Vision.Rooms.Delete_rooms')):?>
                                    <button type="button" class="btn btn-default btn-danger" title="Disable Room" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>rooms/disable_room', 'ch=<?php e($row->id);?>&amp;bt=Disable')"><i class="fa fa-lock"></i></button>
                                <?php endif;?>
                            <?php else: ?>
                                <?php if(has_permission('Vision.Rooms.Delete_rooms')):?>
                                    <button type="button" class="btn btn-default btn-primary" title="Enable Room" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>rooms/enable_room', 'ch=<?php e($row->id);?>&amp;bt=Activate')"><i class="fa fa-unlock"></i></button>
                                <?php endif;?>
                            <?php endif;?>
                        </div>
                        <button type="button" class="btn btn-primary btn-xs" title="Allocation History" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>rooms/allocation_history', 'ch=<?php e($row->id);?>&amp;bt=receive')"><i class="fa fa-stack-overflow"></i></button>
                    </td>
                </tr>
                <?php endforeach; endif;?>
                </tbody>

            </table>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->

    </div>
