<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">
                <div class="col-lg-4 col-sm-12">
                    <span class="h4 text-danger">New Hotel Room Level</span><hr>
                    <form class="form-horizontal" action="<?php echo base_url();?>rooms/levels/create_level" method="post">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Room Level </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" required="" placeholder="level Name" name="level_name" id="req" autofocus>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-4"></label>
                            <div class="col-lg-8">
                                <input type="submit" value="Create Room Level " name="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8 col-sm-12">
                    <span class="h4 text-danger">Hotel Room Levels</span><hr>
                    <table id="dataTable" class="table table-bordered table-hover  dataTable">
                        <thead>
                        <tr>
                            <th width="3%">#</th>
                            <th width="60%">Room level</th>
                            <th width="27%">Status</th>
                            <th width="10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($levels): $num=0; foreach($levels as $row): $num++; ?>
                            <tr class="<?php echo ($row->status==0)?"bg-warning":"";?>">
                                <td><?php echo $num; ?></td>
                                <td><?php echo $row->level; ?></td>
                                <td><?php echo ($row->status==1)?"Active":"<span class='text-danger'>Disabled</span>";?></td>
                                <td>
                                    <?php if($row->status==1): ?>
                                        <?php if(has_permission('Vision.Setting.Edit_categories')): ?>
                                            <button type="button" class="btn btn-default btn-xs" title="Edit level" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php echo base_url(); ?>rooms/levels/edit_level', 'ch=<?php echo $row->id; ?>')"><i class="fa fa-edit"></i></button>
                                        <?php endif; ?>
                                        <?php if(has_permission('Vision.Setting.Delete_categories')): ?>
                                            <button type="button" class="btn btn-danger btn-xs" title="Disable level" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php echo base_url(); ?>rooms/levels/disable_level', 'ch=<?php echo $row->id; ?>&bt=Disable')"><i class="fa fa-lock"></i></button>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if(has_permission('Vision.Setting.Delete_categories')): ?>
                                            <button type="button" class="btn btn-primary btn-xs" title="Enable level" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php echo base_url(); ?>rooms/levels/enable_level', 'ch=<?php echo $row->id; ?>&bt=Activate')"><i class="fa fa-unlock"></i></button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
