<script src='<?php echo Template::theme_url('js/jquery-1.10.2.min.js'); ?>' type='text/javascript'></script>
<?php

if(isset($booking)): ?>
<script type="text/javascript">
    let print_url='<?php echo base_url()."/rooms/reception/print_receipt/".$booking ?>';
    popitup2(print_url)
</script>

<?php endif?>




<div class="header_bg">

    <div class="header">

        <div class="head-t">
            <div class="row">
                <div class="col-lg-7">
                    <div id="tabs" class="tabs pull-left">
                        <nav class="pull-left"><?php $receipt_url = base_url()."rooms/reception"; ?>
                            <ul>
                                <li class="active"> <a href="#new_bill" class="icon-shop" data-toggle="tab" onClick="htmlData2('<?php echo $receipt_url; ?>', 'ch=0')"">Rooms Allocation</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="pull-right"><br>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <table cellpadding="3" cellspacing="2">
                                    <tr>
                                        <td width="50%">
                                            <div class="input-group">
                                                <input type="text" id="productsearch2" class="form-control pull-right" placeholder="Rooms Search ..." >
                                                <span class="input-group-btn">
                                                    <button id="box1Clear" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td width="50%">
                                            <select id="productsearch" class="form-control pull-right" placeholder="Rooms Search ...">
                                                <option value="all">Filter Rooms by Level</option>
                                                <?php
                                                foreach($levels as $rows){
                                                    echo "<option value=\"".$rows->id."\">".$rows->level."</options>";
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->

<div class="content">
    <div class="row">
        <div class="col-md-9 col-sm-9">

            <div class="women_main">
                <!-- start content -->

                <div class="w_content">
                    <div class="tab-content">
                        <!--START OF FIRST TAB CREATING BILLS -->
                        <div class="tab-pane active" id="new_bill">
                            <div class="row">
                                <?php
                                $row_num=0;
                                $new_row="<div class=\"clearfix\"></div>";
                                $keypad_url = base_url()."home/keypad_modal";
                                $keypad_url = base_url()."rooms/reception/allocation_modal";
                                ?>
                                <ul class="nav nav-tabs">
                                    <?php $n=0; foreach($categories as $rows): $n++; ?>
                                        <li role="presentation"  <?php if($n==1){ echo "class=\"active\"";} ?>><a href="#<?php echo "category_".$rows->id; ?>" data-toggle="tab"><?php echo ucfirst(strtolower($rows->category)); ?></a></li>
                                    <?php endforeach; ?>

                                </ul>
                                <div class="tab-content">
                                    <?php $n=0; foreach($categories as $cat): $n++; ?>
                                        <div class="tab-pane <?php if($n==1){ echo "active"; }?>" id="<?php echo "category_".$cat->id; ?>" style="height:600px; overflow:auto">
                                            <table class="table table-hover">
                                                <tr>
                                                    <?php
                                                    $row_num=0; foreach($rooms as $rows): if($cat->id == $rows->category):
                                                        if($row_num==3){
                                                            $new_row="</tr><tr>"; $row_num=0;
                                                        }else{
                                                            $new_row=""; $row_num++;
                                                        }
                                                        ?>
                                                        <td align="center" style="width: 150px;">
                                                            <div class="content_box"><a href="details.html"></a>
                                                                <span class="h5">
                                                                    <?php echo $rows->room_no; ?>
                                                                    <span class="h6 text-muted"><br><?php echo $rows->level_name; ?></span>
                                                                </span>
                                                                <div class="grid_1 simpleCart_shelfItem">
                                                                    <div class="item_add"><span class="item_price h6 text-muted">Ksh <?php echo number_format($rows->price,2); ?></span></div>
                                                                    <div class="item_add">
                                                                        <span class="item_price">
                                                                            <?php if($rows->occupancy==0): ?>
                                                                            <a href="#txtResult" data-toggle="modal" title="Allocate Room" onClick="htmlData('<?php echo $keypad_url; ?>', 'ch1=<?php echo $rows->id; ?>')">Allocate Room</a>
<!--                                                                            <a href="#" data-toggle="modal" title="Allocate Room" onClick="return popitup('<?php //echo $keypad_url."?ch1=".$rows->id; ?>')">Allocate Room</a> -->
                                                                            <?php else: ?>
                                                                                <button href="#txtResult" class="btn btn-primary btn-sm" data-toggle="modal" title="Allocate Room" onClick="htmlData('<?php echo $keypad_url; ?>', 'ch1=<?php echo $rows->id; ?>')">View Allocation</button>
                                                                            <?php endif; ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                        <?php echo $new_row; endif;endforeach; if($row_num!=5){echo "</tr>";}?>
                                            </table>
                                        </div>
                                    <?php  endforeach; ?>
                                </div>

                            </div>
                        </div>
                        <!--END OF FIRST TAB CREATING BILLS -->

                    </div>

                </div>

                <div class="clearfix"></div>
                <!-- end content -->


            </div>

        </div>
        <?php $url = base_url()."home/add_quantity"; ?>

        <div class="col-md-3 col-sm-3" id="txtResult2">
            Occupied Rooms
            <hr>
            <table class="table table-responsive ">
                <thead>
                <tr>
                    <th>Room</th>
                    <th>Checkout</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($allocated_rooms as $r):?>
                <tr>
                    <td><?php echo $r->room_no."-".$r->level; ?></td>
                    <td width="50%"><?php echo date('d/M/Y H:i',strtotime($r->check_out)); ?></td>
                    <td>
                        <button type="button" class="btn btn-default btn-xs" title="Edit product" data-toggle="modal" href="#txtResult" onClick="htmlData('<?php echo $keypad_url; ?>', 'ch1=<?php echo $r->room_id; ?>')"><i class="fa fa-bars"></i></button>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>

            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<!--content-->
</div>
</div>
<!--//content-inner-->
<div id="txtResult" class="modal fade in"> </div>

<script type="text/javascript">

    function date_change(){
       let checkin_date=$("#checkin_date").val()
       let checkout_date=$("#checkout_date").val()

        let start_date=new Date(checkin_date)
        let end_date=new Date(checkout_date)
        let milli=end_date.getTime()-start_date.getTime()
        let days=milli/(1000*3600*24)
        let room_price=$("#room_price").val()
         $("#total_room_price").val(room_price*days)

    }


</script>