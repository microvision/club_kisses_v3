
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
$submit_url = base_url()."rooms/reception/allocate_room";
$url5 = base_url()."rooms/reception/rbuttons";
//$bill_id = $_GET['ch'];
$room_id = $_GET['ch1'];
$user_id = $current_user;
if($room_details->occupancy == 1):
    switch ($allocation_check->status){
        case 0:
            $status = "Vacant";
            break;
        case 1:
            $status = "Occupied";
            break;
        case 2:
            $status = "Reserved";
            break;
        case -1:
            $status = "Vacant";
            break;
        default:
            $status="";
    }
?>
		
			
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-thumbs-down"></i>The room <b><?php echo $room_details->room_no; ?></b> is not Available</h4>
          </div>
          <div class="modal-body">
			<div class="alert alert-danger ">
				
				<h3><?php echo "The room ".$room_details->room_no." is currently ".$status; ?>. </h3><br>
                <table class="table table-stripped">
                <tr>
                    <td>Guest Name</td>
                    <td><?php echo $allocation_check->occupant_name; ?></td>
                </tr>
                <tr>
                    <td>National ID/Passport</td>
                    <td><?php echo $allocation_check->national_id; ?> </td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td><?php echo $allocation_check->phone; ?> </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td> <?php echo $allocation_check->address; ?></td>
                </tr>
                <tr>
                    <td>Check-in</td>
                    <td> <?php echo date('d-M-Y h:i a',strtotime($allocation_check->check_in)); ?></td>
                </tr>
                <tr>
                    <td>Check-out</td>
                    <td> <?php echo date('d-M-Y h:i a',strtotime($allocation_check->check_out)); ?></td>
                </tr>
                <tr>
                    <td>Amount Paid</td>
                    <td> Ksh. <?php echo number_format($allocation_check->cost,0); ?></td>
                </tr>
                </table>
			</div>
		  </div>
          <div class="modal-footer">
              <a href="<?php echo base_url()."rooms/reception/vacate_room/".$allocation_check->id; ?>" type="submit"  class="btn btn-warning pull-left" >Set Room Vacant</a>
              <?php if(has_permission('Vision.Rooms.Manage_rooms')):?>
              <a href="<?php echo base_url()."rooms/reception/cancel_allocation/".$allocation_check->id; ?>" type="submit"  class="btn btn-success pull-left" >Cancel Allocation</a>
              <?php endif; ?>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>			
          </div>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    
	


<?php else: ?>
	
	

		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Allocate Room <b> <?php echo $room_details->room_no;; ?></b></h4>
			</div>
			<form method='GET' class="form-horizontal" name="allocate_room" id="allocate_room" action="<?php echo $submit_url; ?>">
			<div class="modal-body">
				<div class="row">
                    <table border="0" align="center" width="80%" cellspacing="5" cellpadding="5">
                        <tr>
                            <td colspan="2"><h4><u>Room Details</u></h4></td>
                        </tr>
                        <tr>
                            <td>Room number/Name</td>
                            <td><b><?php echo $room_details->room_no; ?></b></td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td><?php echo $room_details->category_name; ?></td>
                        </tr>
                        <tr>
                            <td>Level/Class</td>
                            <td><?php echo $room_details->level_name; ?></td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td><?php echo $room_details->description; ?></td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>Ksh. <?php echo number_format($room_details->price,0); ?></td>
                        </tr>
                    </table>
                    <hr>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Guest Name</label>
                        <div class="col-lg-7 col-sm-7">
                            <input type="text" class="form-control" name="guest_name"  required="">
                            <input type="hidden" class="form-control" name="room_id" value ="<?php e($room_details->id); ?>" >
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">National ID/Passport</label>
                        <div class="col-lg-7 col-sm-7">
                            <input type="text" class="form-control" name="national_id"  required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Phone Contact</label>
                        <div class="col-lg-7 col-sm-7">
                            <input type="text" class="form-control" name="phone" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Address</label>
                        <div class="col-lg-7 col-sm-7">
                            <input type="text" class="form-control" name="address" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Number of Guests</label>
                        <div class="col-lg-7 col-sm-7">
                            <input type="number" class="form-control" name="guests" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Check in </label>
                        <div class="col-lg-4 col-sm-4">
                            <input type="date" class="form-control" id="checkin_date" onchange="date_change()" name="checkin_date" placeholder="Check-in date" value ="<?php echo date('Y-m-d'); ?>" required>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <input type="time" class="form-control" id="checkin_time"  name="checkin_time"  placeholder="Check-in time" value ="<?php echo date('H:i'); ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Check out</label>
                        <div class="col-lg-4 col-sm-4">
                            <input type="date" class="form-control" id="checkout_date" name="checkout_date" onchange="date_change()"  placeholder="Check-in date" value ="<?php echo date('Y-m-d', strtotime(" +1 days")); ?>" required>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <input type="time" class="form-control" name="checkout_time" placeholder="Check-in time" value ="10:00" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-sm-4">Amount to Pay</label>
                        <div class="col-lg-6">
                            <table width="73%"><tr><td>
                                        <input type="hidden" id="room_price" value ="<?php e($room_details->price); ?>" />
                                        <input type="text" class="form-control" id="total_room_price" readonly name="price" placeholder="Cost per night" value ="<?php e($room_details->price); ?>" id="req"></td><td><h5> &nbsp; Ksh</h5></td></tr></table>
                        </div>
                    </div>
				</div>
            </div>
            <div class="modal-footer">
<!--                    <button type="submit" name="submit" value="submit" class="btn btn-success" >Allocate Room</button>-->
                    <input type="submit" name="submit" value="Reserve Room" class="btn btn-warning" >
                    <input type="submit" name="submit" value="Receive Cash Payment" class="btn btn-info" >
                    <input type="submit" name="submit" value="Receive M-pesa Payment" class="btn btn-success" >
<!--                    <button type="submit" name="submit" value="submit" class="btn btn-success" onClick="return popitup('<?php //echo $submit_url?checkin; ?>')">Allocate Room</button>-->
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
			</form>
		</div><!-- /.modal-content -->
		</div>

	
<?php endif; ?>



