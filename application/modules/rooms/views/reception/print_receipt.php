<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Signal POS - Receipt</title>
    <script>
        function popitup(url) {
            newwindow = window.open(url, 'name', 'height=540,width=340,top=50,left=500');
            if (window.focus) {
                newwindow.focus()
            }
            return false;
        }

        //window.onunload = refreshParent;
        function refreshParent() {
            //window.opener.location.reload();
            window.opener.location.href = "<?php echo base_url();?>/rooms/reception";
        }

        window.onload = function () {


            //printreceipt();
            var count = 0;
            while (count < 3) {
                window.print();
                count = count + 1;
            }
            refreshParent();

        }

        window.onfocus = function () {
            refreshParent();
            window.print();
            window.close();
        }

        //setTimeout(funtion(){window.close();},9);
    </script>
    <style type="text/css">
        .style1 {
            font-family: cambria, Agency FB;
            font-size: 22px
        }

        .style9 {
            font-family: cambria;
            font-size: 16px;
        }

        .style2 {
            font-family: calibri;
            font-size: 17px;
        }

        .style5 {
            font-family: calibri;
            font-size: 18px;
        }


        .style3 {
            font-family: calibri;
            font-size: 13px;
        }

        .style10 {
            font-family: calibri;
            font-size: 20px;
        }

        .style15 {
            font-family: calibri;
            font-size: 17px;
        }

    </style>
</head>

<body onFocus="window.close()">
<table align="center" width="338" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
    <tr>
        <td colspan="4" align="center" valign="middle"><span class="style1">
	<?php
    $button = "Room Reservation";
    echo $company->name;
    echo "<br><span class=\"style9\"> " . $company->address . " <BR> " . $company->phone . " </span>";
    echo "<br><span class=\"style9\">" . $button . " </span>";
    ?>
	</span>
        </td>
    </tr>

    <tr>
        <td colspan="1" align="left" bordercolor="#000000"><span class="style15">Date:
        <?php $currenttime = date("Y-m-d H:i:s");
        echo $booking->created_on; ?>
    </span></td>

        <td colspan="1" align="right" bordercolor="#000000"><span class="style15">
        <?php echo " #:" . $booking->id; ?>
    </span></td>
    </tr>
    <tr>
        <td colspan="2" align="center" class="style3">
            ------------------------------------------------------------------------------
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center" class="style2">
            <strong> Customer Details</strong>
        </td>
    </tr>

    <tr>
        <td>Names</td>
        <td><?php echo $booking->occupant_name; ?></td>
    </tr>
    <tr>
        <td colspan="2"><p></p></td>
    </tr>
    <tr>
        <td>National ID</td>
        <td><?php echo $booking->national_id; ?></td>
    </tr>
    <tr>
        <td>Phone</td>
        <td><?php echo $booking->phone; ?></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><?php echo $booking->address; ?></td>
    </tr>
    <tr>
        <td>Check In</td>
        <td><?php echo $booking->check_in; ?></td>
    </tr>
    <tr>
        <td>Check Out</td>
        <td><?php echo $booking->check_out; ?></td>
    </tr>
    <tr>
        <td>Days</td>
        <td><?php
            $earlier = new DateTime(Date("d-m-Y", strtotime($booking->check_in)));
            $later = new DateTime(Date("d-m-Y", strtotime($booking->check_out)));
            $abs_diff = $later->diff($earlier); //3

            echo $abs_diff->d; ?></td>
    </tr>
    <tr>
        <td>Cost</td>
        <td align="right"><strong><?php echo number_format($booking->cost, 2); ?></strong></td>
    </tr>
    <tr>
        <td>Payment mode</td>
        <td align="right">
            <strong>
                <?php
                    switch ($booking->payment_mode){
                        case 1:
                            echo "Cash payment";
                            break;
                        case 2:
                            echo "M-Pesa payment";
                            break;
                        case 0:
                            echo "Room Reservation";
                            break;
                        default:
                            echo "Check Payment";
                    }
                ?>
            </strong>
        </td>
    </tr>

    <tr>
        <td colspan="2" align="center" class="style3">
            ------------------------------------------------------------------------------
        </td>
    </tr>

    <tr>
        <td><?php echo $company->mpesa_payment_type ;?></td>
        <td align="right"><strong><?php echo  $company->mpesa_payment_number; ?></strong></td>
    </tr>    <tr align="center">
        <td colspan="2"> Served By:<?php echo $username;?></td>

    </tr>  <tr align="center">
        <td colspan="2"> Thank you & Welcome again  <br><?php echo $company->slogan ;?></td>

    </tr>  <tr align="center">
        <td colspan="2"> <?php echo  Date('Y');   ?>  &copy; SignalPOS <br>info@microvision.co.ke  </td>

    </tr>
</table>

</body>
</html>

<!-- **removed VAT from receipt line 143
					  <tr><td colspan="2"></td><td colspan="2"><hr></td></tr>
                        </tr>
						<tr class="text-primary">
                          <td colspan=3 align="right" class="style9">VAT &nbsp;</td>
                          <td class="style9"> Ksh $vat </td>
					  </tr> -->
