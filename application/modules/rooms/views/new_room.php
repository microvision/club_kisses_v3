
<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <span class="h3 text-danger">New Hotel Room</span><hr>
            <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-lg-4">Room number/name</label>
                    <div class="col-lg-5" id="txtResult3">
                        <input type="text" class="form-control" required="" name="room_name" id="req" onkeyup="htmlData2('<?php echo base_url(); ?>rooms/room_name_search','ch='+this.value)">

                    </div>
                    <div class="col-lg-3" id="txtResult2">

                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Room Category</label>
                    <div class="col-lg-5">
                        <select name="room_category" id="room_category" required="" class="form-control">
                            <option value="" selected disabled>Select a room category</option>
                            <?php foreach ($categories as $rows): ?>
                                <option value="<?php echo $rows->id; ?>"><?php echo $rows->category; ?></option>
                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Description</label>
                    <div class="col-lg-5">
                        <textarea id="description" class="form-control" name="description" maxlength="140"></textarea>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4">Room Level</label>
                    <div class="col-lg-5">
                        <select name="room_level" id="room_level" required="" class="form-control">
                            <option value="" selected disabled>Select room level</option>
                            <?php foreach ($levels as $rows): ?>
                                <option value="<?php echo $rows->id; ?>"><?php echo $rows->level; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-4">Room Cost</label>
                    <div class="col-lg-5">
                        <table width="100%"><tr><td><input type="number" step="0.01" required="" class="form-control" name="price" placeholder="Cost per night" id="req"></td><td><h5> Ksh</h5></td></tr></table>
                    </div>
                    <div class="col-lg-1 col-sm-2"></div>

                </div>
                <div id="txtResult"> </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-lg-4"></label>
                    <div class="col-lg-8">
                        <input type="submit" value="Create Hotel Room" name="submit" class="btn btn-primary">
                        <a type="button" value="Cancel" class="btn btn-default pull-right" href="<?php echo base_url();?>rooms/new_room">Cancel </a>
                    </div>
                </div>
            </form>
        </div>

        <!-- end content -->

    </div>
