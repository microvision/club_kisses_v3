<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/20/2019
 * Time: 3:10 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><?php echo $action; ?> Hotel Room</h4>
        </div>
        <form method="post" action="<?php echo $url; ?>">
            <div class="modal-body">
                <h4>
                    Are you sure you want to <?php echo $action; ?> hotel room <b> <?php echo $room->room_no; ?> </b>?
                    <input type="hidden" name="room_id" value="<?php echo $room->id; ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                </h4>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-danger" ><?php echo $action; ?> Hotel Room</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
