<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Hotel rooms management and reservations',
    'name'          => 'Rooms',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);