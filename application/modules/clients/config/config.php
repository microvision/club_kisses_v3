<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Manage clients and client payments',
    'name'          => 'Clients',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);