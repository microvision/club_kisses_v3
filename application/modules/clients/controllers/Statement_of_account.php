<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Statement_of_account extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		$this->load->model('clients_model');
		$this->load->model('client_accounts_model');
	}
	
	public function index($id=null){
		$this->auth->restrict('Vision.Clients.View');
        if(ISSET($_POST['submit'])){
//            $start_date = $_POST['start']." ".date("H:i",strtotime($_POST['start_time'])).":00";
//            $start_date = date("Y-m-d H:i:s",strtotime($start_date));
//            $end_date= $_POST['end']." ".date("H:i",strtotime($_POST['end_time'])).":00";
//            $end_date = date("Y-m-d H:i:s",strtotime($end_date));
            $start_date  = date("d-M-Y",strtotime($_POST['start']));
            $end_date  = date("d-M-Y",strtotime($_POST['end']));
            Template::set('opening_balance', $this->client_accounts_model->get_opening_bal($_POST['start'],$_POST['client']));
            Template::set('transactions', $this->client_accounts_model->get_client_transactions($_POST['start'],$_POST['end'],$_POST['client']));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
            Template::set('client', $this->clients_model->as_object()->find($_POST['client']));
        }

		Template::set('clients', $this->clients_model->get_clients());
		Template::set_theme('default');
		Template::set('page_title', 'Statement Of Account');
		Template::render('');

    }
    public function print_statement()
    {
        $start_date  = date("Y-m-d",strtotime($_GET['start']));
        $end_date  = date("Y-m-d",strtotime($_GET['end']));
        $data['opening_balance'] = $this->client_accounts_model->get_opening_bal($start_date,$_GET['client']);
        $data['transactions'] = $this->client_accounts_model->get_client_transactions($start_date,$end_date,$_GET['client']);
        $data['client'] = $this->clients_model->as_object()->find($_GET['client']);
        $data['company'] = $this->clients_model->get_company();
        $data['startdate'] =$start_date;
        $data['enddate'] =$end_date;
        $this->load->view('clients/statement_of_account/print_statement',$data);
    }

}