<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		
		$this->load->model('clients_model');
		$this->load->model('client_accounts_model');
	}
	
	public function index($id=null){
		$this->auth->restrict('Vision.Clients.View');
		
		Template::set('clients', $this->clients_model->get_clients());
//		Template::set('clients', $this->clients_model->order_by('name','asc')
//                                                        ->find_all());
		Template::set_theme('default');
		Template::set('page_title', 'Cooperate Clients');
		Template::render('');

    }
    public function new_client(){
        $this->auth->restrict('Vision.Clients.Create_client');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'contact_person' => $_POST['contact_person'],
                'phone' => $_POST['phone'],
                'address' => $_POST['address'],
                'credit_limit' => $_POST['credit_limit'],
                'opening_balance' => $_POST['opening_balance'],
                'client_type' => 1,
                'status' => 1
            );
            $client_id = $this->clients_model->insert($data);
            //we insert the first transaction for the opening balance
            if($client_id){
                $data_balance = array(
                    'client_id' => $client_id,
                    'transaction_no' => "Opening Bal",
                    'amount' => $_POST['opening_balance'],
                    'transaction_type' => 1
                );
                $this->client_accounts_model->insert($data_balance);
            }
        }
        log_activity($this->auth->user_id(),"Created new client ".$_POST['name'], 'clients');
        Template::set_message('The client account was successfully created', 'alert fresh-color alert-success');
        redirect("clients/index",true);
    }

    public function edit_client(){
        $this->auth->restrict('Vision.clients.Edit_client');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'contact_person' => $_POST['contact_person'],
                'phone' => $_POST['phone'],
                'address' => $_POST['address'],
                'credit_limit' => $_POST['credit_limit']
            );
            $this->clients_model->update($_POST['client_id'],$data);
            log_activity($this->auth->user_id(),"Edited the client details for ".$_POST['name'], 'clients');
            redirect("clients/index",true);
        }else{
            $client_id = $_GET['ch2'];
            $data['client'] = $this->clients_model->as_object()->find($client_id);
            $data['url']=base_url()."clients/edit_client";
            $this->load->view('clients/edit_client',$data);
        }

    }
    public function disable_client(){
        $this->auth->restrict('Vision.Clients.Delete_client');
        if (ISSET($_POST['submit'])){
            $client = $this->client_model->as_object()->find($_POST['client_id']);

            $data = array('status'=> 0);
            if ($this->client_model->update($_POST['client_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the client ".$client->name, 'catalog');
                Template::set_message('The client '.$client->name.' was successfully deactivated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the client account. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('clients/index',true);
        }else{
            $data['client_id'] = $this->input->get("ch");
            $data['client'] = $this->client_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."clients/disable_client";
            $data['action']=$_GET['bt'];
            $this->load->view('clients/disable_client',$data);
        }
    }
    public function enable_client(){
        $this->auth->restrict('Vision.clients.Delete_client');
        if (ISSET($_POST['submit'])){
            $client = $this->client_model->as_object()->find($_POST['client_id']);

            $data = array('status'=> 1);
            if ($this->client_model->update($_POST['client_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Activated the client ".$client->name, 'clients');
                Template::set_message('The client '.$client->name.' was successfully activated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the client account. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('clients/index',true);
        }else{
            $data['client_id'] = $this->input->get("ch");
            $data['client'] = $this->client_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."clients/enable_client";
            $data['action']=$_GET['bt'];
            $this->load->view('clients/disable_client',$data);


        }
    }
    public function receive_payment(){
        if(ISSET($_POST['submit'])){
            $client = $this->clients_model->as_object()->find($_POST['client_id']);
            $data = array(
                'client_id' => $client->id,
                'transaction_no' => "Payment",
                'amount' => $_POST['amount'],
                'transaction_type' => 1
            );
            $this->client_accounts_model->insert($data);
            log_activity($this->auth->user_id(),"Received Client Payments for ".$client->name, 'clients');
            Template::set_message('Payments of <b>Ksh '.number_format($_POST['amount'],2).'</b> for '.$client->name.' was successfully received.', 'alert fresh-color alert-success');
            redirect("clients/index",true);
        }else{
            $client_id = $_GET['ch2'];
            $data['client'] = $this->clients_model->as_object()->find($client_id);
            $data['url']=base_url()."clients/receive_payment";
            $this->load->view('clients/received_payment',$data);
        }

    }
}