<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client_accounts_model extends MY_Model {
	protected $table_name = 'vision_corporate_client_transactions';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_opening_bal($start,$client_id)
    {
        return $this->db->query("SELECT SUM(if(transaction_type='1',-amount,amount)) as amount, client_id FROM bf_vision_corporate_client_transactions WHERE date(created_on)<'".$start."' and client_id='".$client_id."' GROUP BY client_id")->row();
    }
    public function get_client_transactions($start,$end,$client_id)
    {
        return $this->db->query("SELECT bf_vision_corporate_client_transactions.* FROM bf_vision_corporate_client_transactions WHERE date(created_on)>='".$start."' and date(created_on)<='".$end."' and client_id='".$client_id."'")->result();
    }
	
}