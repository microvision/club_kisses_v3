<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends MY_Model {
	protected $table_name = 'vision_corporate_clients';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_clients()
    {
        return $this->db->query("SELECT bf_vision_corporate_clients.*, ac_bal FROM bf_vision_corporate_clients 
                                  LEFT JOIN (SELECT client_id, SUM(if(transaction_type='1',amount,-amount)) as ac_bal FROM bf_vision_corporate_client_transactions GROUP BY client_id)as account_bal ON account_bal.client_id=bf_vision_corporate_clients.id")->result();
    }
    public function get_company()
    {
        return $this->db->query("SELECT * FROM bf_vision_business_profile LIMIT 1")->row();
    }
	
}