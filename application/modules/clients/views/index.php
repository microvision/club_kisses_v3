<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <?php if(has_permission('Vision.Clients.Create_client')):?>
                <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".new_client">Create New Client</button><br><hr>
            <?php endif; ?>
            <table id="example" class="table table-bordered table-hover  table-condensed dataTable">
                <thead>
                <tr>
                    <th >#</th>
                    <th>Client</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th><span class="pull-right">Credit Limit(Ksh)</span></th>
                    <th><span class="pull-right">A/C Bal(Ksh)</span></th>
                    <th>Status</th>
                    <th width="7%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(($clients)): $num=0; foreach($clients as $row): $num++;?>
                <tr class="<?php echo ($row->status==0)?"bg-warning":"";?>">
                    <td width="3%"><?php e($num); ?></td>
                    <td><?php e($row->name); ?><br><small class="muted"><?php e($row->contact_person);?></small></td>
                    <td><?php e($row->address);?></td>
                    <td><?php e($row->phone);?></td>
                    <td align="right"><?php e(number_format($row->credit_limit,2));?></td>
                    <td align="right"><?php e(number_format($row->ac_bal,2));?></td>
                    <td><?php echo($row->status==1)?"Active":"<span class='text-danger'>Disabled</span>";?></td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <?php if($row->status==1): ?>
                                <?php if(has_permission('Vision.Clients.Edit_client')):?>
                                    <button type="button" class="btn btn-default btn-default" title="Edit client" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>clients/edit_client', 'ch2=<?php e($row->id);?>&amp;ch=1')"><i class="fa fa-edit"></i></button>
                                <?php endif;?>
                                <?php if(has_permission('Vision.Clients.Delete_client')):?>
                                    <button type="button" class="btn btn-default btn-danger" title="Disable Client" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>clients/disable_client', 'ch=<?php e($row->id);?>&amp;bt=Disable')"><i class="fa fa-trash-o"></i></button>
                                <?php endif;?>
                                <button type="button" class="btn btn-default btn-success" title="Receive Payment" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>clients/receive_payment', 'ch2=<?php e($row->id);?>&amp;ch=1')"><i class="fa fa-stack-overflow"></i></button>
                            <?php else: ?>
                                <?php if(has_permission('Vision.Clients.Delete_client')):?>
                                    <button type="button" class="btn btn-default btn-primary" title="Activate Client" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>clients/enable_client', 'ch=<?php e($row->id);?>&amp;bt=Activate')"><i class="fa fa-unlock"></i></button>
                                <?php endif;?>
                            <?php endif;?>
                    </td>
                </tr>
                <?php endforeach; endif;?>
                </tbody>

            </table>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->
        <div class="modal fade new_client" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form method="post" id="wizard" class="form-horizontal" role="form" action="<?php echo base_url(); ?>clients/new_client" accept-charset="utf-8" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">New Client Account</h4>
                        </div>
                        <div class="modal-body">


                            <div class="wizard-steps clearfix"></div>
                            <div class="step" id="account-details"><br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Business Name</label>
                                    <div class="col-lg-6">
                                        <input id="name" name="name" required="required" type="text" class="form-control col-md-7 col-xs-12" />
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Contact Person</label>
                                    <div class="col-lg-6">
                                        <input id="name" name="contact_person" required="required" type="text" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Phone</label>
                                    <div class="col-lg-6">
                                        <input id="phone" required="required" name="phone" type="text" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Address</label>
                                    <div class="col-lg-6">
                                        <textarea id="autosize" name="address" class="form-control col-md-7 col-xs-12"></textarea>
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Credit Limit</label>
                                    <div class="col-lg-6">
                                        <input id="credit"  name="credit_limit" type="number" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Opening Balance</label>
                                    <div class="col-lg-6">
                                        <input id="opening_balance"  name="opening_balance" type="number" value="0" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" name="submit"  class="btn btn-primary" value="Create Client">
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>