<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-vertical" action="<?php echo current_url();?>" method="post">
                                <div class="col-lg-4">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Client</label>
                                        <div class="col-lg-12">
                                            <select id="client" name="client" required="required"  class="form-control chzn-select">
                                                <option selected disabled value="">Select Client</option>
                                                <?php foreach($clients as $row): ?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group has-success">
                                        <label class="control-label ">Start Date</label>
                                        <div class="col-lg-12">
                                            <input type="date" id="date2" name="start" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group has-success">
                                        <label class="control-label">End Date</label>
                                        <div class="col-lg-12">
                                            <input type="date" id="date2" name="end" required="required" value="<?php echo date('Y-m-d'); ?>" class="form-control">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label class="control-label">&nbsp;</label>
                                    <input type="submit" name="submit" value="Display Report" class="btn btn-danger">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12"><hr>
                            <?php if(ISSET($transactions)): ?>
                            <span class="h4 text-danger"><?php echo "&nbsp;Statement Of Accounts</span><h6>".$client->name."<br>&nbsp;&nbsp;(".$startdate."  ~  ".$enddate.")"; ?></i></h6>
                                    <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button>
                                    <button  class="btn btn-primary btn-sm pull-right" onclick ="return popitup2('<?php echo base_url()."clients/statement_of_account/print_statement" ?>?client=<?php echo $client->id; ?>&start=<?php echo $startdate; ?>&end=<?php echo $enddate; ?>')"><i class="fa fa-print"></i> Print Report</button><br><hr>
                            <table id="dataTable" class="table table-bordered table-hover  dataTable">
                                <thead>

                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td colspan="4">OPENING BALANCE</td>
                                        <td align="right">
                                            <?php $opening_bal=(ISSET($opening_balance->amount))?$opening_balance->amount:0; echo number_format($opening_bal,2); $closing_bal =$opening_bal; ?>
                                        </td>
                                    </tr>
                                     <tr>
                                        <th width="3%">#</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th><span class="pull-right">Debit(KSH)</span></th>
                                        <th><span class="pull-right">Credit(KSH)</span></th>
                                        <th><span class="pull-right">Balance(KSH)</span></th>
                                    </tr>
                                <?php $num=0; $total_debits=0; $total_credits=0;  foreach($transactions as $row): $num++; ?>
                                    <tr>
                                        <td width="2%"><?php echo $num; ?></td>
                                        <td><?php echo date('d-M-Y', strtotime($row->created_on)); ?></td>
                                        <td>
                                           <?php
                                           echo ($row->transaction_type==2)?"Receipt # ":"";
                                                echo $row->transaction_no;
                                            ?>
                                        </td>
                                        <td align="right"><?php echo ($row->transaction_type==2)?number_format($row->amount,2):"0.00"; ?></td>
                                        <td align="right"><?php echo ($row->transaction_type==1)?number_format($row->amount,2):"0.00"; ?></td>
                                        <td align="right">
                                            <?php
                                                $closing_bal=($row->transaction_type==2)?$closing_bal-$row->amount:$closing_bal;
                                                $closing_bal=($row->transaction_type==1)?$closing_bal+$row->amount:$closing_bal;
                                                echo number_format($closing_bal,2);
                                            ?>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="4">CLOSING BALANCE</td>
                                        <td align="right"><?php echo number_format($closing_bal,2);  ?></td>
                                    </tr>
                                </tbody>
                            </table>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
