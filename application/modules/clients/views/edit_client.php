<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Client Details </b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <br>

                <input type="hidden" name="productId" value="$itemId">
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Business Name</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="name" value ="<?php e($client->name); ?>" required="">
                        <input type="hidden" class="form-control" name="client_id" value ="<?php e($client->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Contact Person</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="contact_person" value ="<?php e($client->contact_person); ?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Phone</label>
                    <div class="col-lg-6 col-sm-6">
                        <input id="phone" required="required" class="form-control" name="phone" type="text" value ="<?php e($client->phone); ?>" />
                    </div>
                </div><!-- End .form-group  -->
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Address</label>
                    <div class="col-lg-6 col-sm-6">
                        <textarea id="autosize" name="address" class="form-control"><?php e($client->address); ?></textarea>
                    </div>
                </div><!-- End .form-group  -->
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Credit Limit</label>
                    <div class="col-lg-6 col-sm-6">
                        <input id="credit"  name="credit_limit" type="number" value ="<?php e($client->credit_limit); ?>" class="form-control col-md-7 col-xs-12" />
                    </div>
                </div><!-- End .form-group  -->
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
