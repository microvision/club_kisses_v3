<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <div class="row">
            <!--Start of orders list -->
            <div class="col-lg-3 col-sm-3">
                <div class="block">
                    <?php if(has_permission('Vision.Distribution.Issue_stocks')): ?>
                    <a href="<?php echo base_url()."distribution/create_stock_issue_order"; ?>" class="btn btn-danger btn-block "><span class="fa fa-upload"></span> Issue Sales Stock</a>
                    <?php endif; ?>
                </div>
                <div class="block">
                    <div class="list-group border-bottom">
                        <h5> Issued Stocks </h5>
                        <table class="table table-condensed">
                        <?php foreach($stock_issue as $row): ?>
                            <tr onclick="window.location='<?php echo base_url()."distribution/index/".$row->id;?>';" <?php echo((ISSET($stock_issue_details)) and ($row->id==$stock_issue_details->id))?'class="active"':''; ?>>
                                <td><span class="h6"><span <?php echo((ISSET($stock_issue_details))and($row->id==$stock_issue_details->id))?'class="fa fa-caret-right"':'class="fa fa-angle-right"'; ?> >&nbsp;<?php echo $row->id; ?></span></td>
                                <td><span class="h6"><?php echo ($row->distributor)?$row->distributor:'<span class="text-danger">Currently Issuing</span>'; ?></span></td>
                                <td width="35%"><span class="h6"><?php echo date('d-M-Y', strtotime($row->created_on)); ?></span></td>

                            </tr>
                        <?php endforeach; ?>
                        </table>
                    </div>
                </div>

            </div>
            <!-- End of orders list -->
            <!-- START CONTENT FRAME BODY -->
            <div class="col-lg-9 col-sm-9">
                <?php if(ISSET($stock_issue_details)): ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="h5 text-white">Stock Issuing # <?php echo $stock_issue_details->id; ?></span>
                            <div class="pull-right" style="width: 250px;">
                                <?php if($stock_issue_details->status==1 or $stock_issue_details->status==2): ?>
                                    <?php if($stock_issue_details->status==2): ?> <span class="h4 text-warning pull-right">Awaiting Reconciliation</span> <?php endif; ?>
                                    <?php if(has_permission('Vision.Distribution.Issue_stocks')):?>
                                        <div class="input-group pull-right">
                                            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".new_items"> <span class="fa fa-plus"></span>Add Product</button>
                                        </div>
                                    <?php endif; ?>
                                <?php elseif($stock_issue_details->status==3): ?>
                                    <span class="h5 text-danger pull-right">Reconciled on <?php echo date('d/M/Y', strtotime($stock_issue_details->modified_on))." | <i class='fa fa-user'></i> ".$stock_issue_details->display_name; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="panel-body mail">
                            <form class="form-vertical" id="receive_stock" name="receive_stock" method="post" action="<?php echo base_url(); ?>distribution/submit_reconciliation" >
                                <div class="row">
                                    <div class="col-lg-5 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>

                                                <div class="input-group">
                                                    <select class="form-control chzn-select" id="supplier" required="required" name="supplier" onchange="htmlData4('<?php echo base_url(); ?>distribution/update_order_fields', 'val=distributor&order_id=<?php e($stock_issue_details->id);?>&distributor_id='+this.value)">
                                                        <option value="" selected disabled>Select Distributor</option>
                                                        <?php foreach($distributors as $distributor):?>
                                                        <option <?php echo(($stock_issue_details->distributor_id)and($stock_issue_details->distributor_id==$distributor->id))?"selected":""; ?> value="<?php e($distributor->id); ?>"><?php e($distributor->name); ?> </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="input-group-btn">
												        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".new_distributor">New Distributor</button>
                                                    </span>
                                                </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-sm-6" id="txtResult4">
                                        <br>
                                        <span class="h4 text-danger"><?php echo($stock_issue_details->distributor)?$stock_issue_details->distributor:""; ?><br></span>
                                        <span class="h6"><i class="fa fa-phone"></i> <?php echo($stock_issue_details->phone)?$stock_issue_details->phone:""; ?> | <i class="fa fa-truck"></i> <?php echo($stock_issue_details->type_dist and $stock_issue_details->type_dist==2)?"Commission Sales":""; ?>  </span>

                                    </div>
                                </div><br><br>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Comments </label>
                                            <input type="text" class="form-control"  placeholder="Comments" value="<?php echo($stock_issue_details->comments)?$stock_issue_details->comments:""; ?>" name="comments" id="req" onfocusout="htmlData5('<?php echo base_url(); ?>distribution/update_order_fields', 'val=comments&order_id=<?php e($stock_issue_details->id);?>&comments='+this.value)">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-2">

                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Issuing Date</label>
                                            <input type="date" class="form-control" required="" value="<?php echo($stock_issue_details->issuing_date)?date('Y-m-d',strtotime($stock_issue_details->issuing_date)):""; ?>" name="issuing_date" id="req" onfocusout="htmlData5('<?php echo base_url(); ?>distribution/update_order_fields', 'val=issuing_date&order_id=<?php e($stock_issue_details->id);?>&issuing_date='+this.value)">
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <table class="table table-responsive ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product</th>
                                            <th>Issued Qty</th>
                                            <th>Returned Qty</th>

                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php   if($issued_items): $num=0; foreach($issued_items as $row): $num++;
                                        $price=($stock_issue_details->type_dist==1)?$row->retail_price:$row->distribution_price;
                                    ?>
                                        <tr>
                                            <td width="2%"><?php echo $num;?></td>
                                            <td width="35%"><?php echo $row->product_name; ?></td>
                                            <td width="18%"><input type="number"  name="is_<?php echo $row->id; ?>" disabled class="form-control" required="" value="<?php echo($row->issued_qty)?$row->issued_qty:""; ?>" placeholder="Issued Quantity"></td>
                                            <td width="18%"><input type="number" max="<?php echo($row->issued_qty)?$row->issued_qty:""; ?>" id="<?php echo $row->id; ?>"  name="re_<?php echo $row->id; ?>" <?php echo ($stock_issue_details->status==1 or $stock_issue_details->status==3)?'disabled':''; ?> step="0.01" class="form-control returned-items" selling-price="<?php echo $row->selling_price; ?>" issued-qty="<?php echo($row->issued_qty)?$row->issued_qty:""; ?>" required="" value="<?php echo($row->returned_qty)?$row->returned_qty:""; ?>" placeholder="Returned Quantity"></td>

                                            <td width="7%" align="right">
                                                <span id="span_<?php echo trim($row->id);?>"></span>
                                                <?php if($stock_issue_details->status==1): ?>
                                                <a  class="btn btn-danger btn-xs" title="Delete Stock" data-toggle="modal" href="<?php echo base_url(); ?>distribution/delete_stock_order_item?order_item_id=<?php echo $row->id; ?>&order_id=<?php echo $stock_issue_details->id; ?>" ><i class="fa fa-times"></i></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; endif; ?>
                                        <tr>
                                            <td></td>
                                            <td colspan="2" align="right"><b>Amount Received</b></td>
                                            <td><input type="number" step="0.01" class="form-control" name="amount_reconciled" required placeholder="Amount Received"></td>
                                            <td> <span id="total" class="pull-right"></span></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2" align="right"><b>Payment Mode</b></td>
                                            <td>
                                                <Select  class="form-control" name="payment_mode" required>
                                                    <option value="" selected disabled>Select Payment Mode</option>
                                                    <option value="1">Cash</option>
                                                    <option value="2">Mpesa</option>
                                                </Select>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>



                            </div>
                            <div class="panel-footer">
                                <?php if($stock_issue_details->status==1): ?>
                                    <a href="<?php echo base_url();?>distribution" class="btn btn-default" title="Close Order">Close</a>
                                    <?php if(has_permission('Vision.Distribution.Issue_stocks')): ?>
                                     <a href="#" class="btn btn-primary pull-right" onclick ="return popitup('<?php echo base_url()."distribution/submit_requisition/".$stock_issue_details->id; ?>?step=issue')">Submit</a>
                                    <?php endif; ?>
                                <?php elseif($stock_issue_details->status==2): ?>
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                    <a href="<?php echo base_url();?>distribution" class="btn btn-default" title="Close Order">Close</a>
                                    <input type="hidden" name="order_id" value="<?php echo $stock_issue_details->id; ?>" />
                                    <?php if(has_permission('Vision.Distribution.Reconcile_Stocks')): ?>
                                     <input type="submit" name="submit" class="btn btn-danger pull-right" value="Submit Stock Reconciliation" />
                                    <?php endif; ?>
                                    <a href="#" class="btn btn-primary pull-right" onclick ="return popitup('<?php echo base_url()."distribution/submit_requisition/".$stock_issue_details->id; ?>?step=issue')"><i class="fa fa-print"></i> Issuing Receipt</a>
                                <?php elseif($stock_issue_details->status==3): ?>
                                    <a href="<?php echo base_url();?>distribution" class="btn btn-default" title="Close Order">Close</a>
                                    <a href="#" class="btn btn-primary pull-right" onclick ="return popitup('<?php echo base_url()."distribution/submit_requisition/".$stock_issue_details->id; ?>?step=issue')"><i class="fa fa-print"></i> Reconciliation Receipt</a>
                                <?php endif;?>

                            </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
            <!-- END CONTENT FRAME BODY -->
        <!-- MODAL FOR RECEIVING PRODUCT STOCK-->
                <div class="modal fade new_items" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form method="post" id="stock_issue_form" name="stock_issue_form" class="form-horizontal" role="form" action="<?php echo base_url(); ?>distribution/issue_order_item" accept-charset="utf-8" autocomplete="off">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Issue Product Stock</h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                    <input type="hidden" value="<?php echo $stock_issue_details->id;?>" name="order_id">
                                    <input type="hidden" value="1" name="payment_method">
                                    <div class="row">
                                        <div class="block">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Search Product</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                        <input type="text" class="form-control" id="box1Filter" name="search" placeholder="Search.." required="required" onkeypress="htmlData2('<?php echo base_url();?>distribution/get_search_items', 'ch='+this.value)">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="txtResult2"> </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <input type="submit" name="submit"  class="btn btn-danger" value="Issue Product">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        <!-- END MODAL FOR RECEIVING PRODUCT STOCK-->
        <!-- MODAL FOR CREATING SUPPLIER-->
                <div class="modal fade new_distributor" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="wizard" class="form-horizontal" role="form" action="<?php echo base_url(); ?>distribution/new_distributor" accept-charset="utf-8" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">New Distributor Account</h4>
                        </div>
                        <div class="modal-body">


                            <div class="wizard-steps clearfix"></div>
                            <div class="step" id="account-details"><br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Sales Person</label>
                                    <div class="col-lg-7">
                                        <input id="name" name="name" required="required" type="text" class="form-control col-md-7 col-xs-12" />
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        <input type="hidden" value="<?php echo $stock_issue_details->id;?>" name="order_id">
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Phone</label>
                                    <div class="col-lg-7">
                                        <input id="phone" required="required" name="phone" type="text" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Contract Type</label>
                                    <div class="col-lg-7">
                                        <select  name="type"  required="" class="form-control col-md-7 col-xs-12" >
                                            <option value="" disabled selected>Select Contract Type</option>
                                            <option value="1">Salaried Sales</option>
                                            <option value="2">Commission Sales</option>
                                        </select>
                                    </div>
                                </div><!-- End .form-group  -->


                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" name="submit"  class="btn btn-primary" value="Create Distributor">
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL FOR CREATING SUPPLIER-->
        </div>
    </div>
</div>
<script>
    var total=0;
    $(document).ready(function () {
        $(".returned-items").focusout(function(){
             var id=$(this).attr("id");
             var price=$(this).attr("selling-price");
             var qty=$(this).attr("issued-qty");
             var returned=$(this).val();
if(returned!="") {
    var r=(qty - returned) * price
    $("#span_" + id).html(r.toLocaleString())
    total += (qty - returned) * price
    $("#total").html(total.toLocaleString())
}
        })

    })

//
//    $(document).on('click', 'input.returned-items', function () {
//        alert("tesdddddt");
//    })
</script>