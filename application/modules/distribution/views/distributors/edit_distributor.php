<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit distributor Details </b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <br>

                <input type="hidden" name="productId" value="$itemId">
                <div class="form-group">
                    <label class="control-label col-lg-4 col-sm-4">Distributor Name</label>
                    <div class="col-lg-6 col-sm-6">
                        <input type="text" class="form-control" name="name" value ="<?php e($distributor->name); ?>" required="">
                        <input type="hidden" class="form-control" name="distributor_id" value ="<?php e($distributor->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Phone</label>
                    <div class="col-lg-6 col-sm-6">
                        <input id="phone" required="required" class="form-control" name="phone" type="text" value ="<?php e($distributor->phone); ?>" />
                    </div>
                </div><!-- End .form-group  -->
                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Contract Type</label>
                    <div class="col-lg-6 col-sm-6">
                        <select  name="type"  required="" class="form-control" >
                            <option value="" disabled selected>Select Contract Type</option>
                            <option <?php echo ($distributor->type==1)?"Selected":""; ?> value="1">Salaried Sales</option>
                            <option <?php echo ($distributor->type==2)?"Selected":""; ?> value="2">Commission Sales</option>
                        </select>
                    </div>
                </div><!-- End .form-group  -->

            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Save Changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
