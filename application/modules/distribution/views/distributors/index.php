<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <?php if(has_permission('Vision.Clients.Create_client')):?>
                <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".new_distributor">Create Sales Person</button>
            <?php endif; ?>
            <table id="dataTable" class="table table-bordered table-hover  table-condensed dataTable">
                <thead>
                <tr>
                    <th >#</th>
                    <th>Sales Person</th>
                    <th>Contract Type</th>
                    <th>Phone</th>
                    <th><span class="pull-right">A/C Bal(Ksh)</span></th>
                    <th>Status</th>
                    <th width="7%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(($distributors)): $num=0; foreach($distributors as $row): $num++;?>
                <tr class="<?php echo ($row->status==0)?"bg-warning":"";?>">
                    <td width="3%"><?php e($num); ?></td>
                    <td><?php e($row->name); ?></td>
                    <td><?php echo($row->type and $row->type==2)?"Commission Sales":"Salaried Staff"; ?></td>
                    <td><?php e($row->phone);?></td>
                    <td align="right"><?php e(number_format($row->ac_bal,2));?></td>
                    <td><?php echo($row->status==1)?"Active":"<span class='text-danger'>Disabled</span>";?></td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <?php if($row->status==1): ?>
                                <?php if(has_permission('Vision.Clients.Edit_client')):?>
                                    <button type="button" class="btn btn-default btn-default" title="Edit Distributor" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>distribution/distributors/edit_distributor', 'ch2=<?php e($row->id);?>&amp;ch=1')"><i class="fa fa-edit"></i></button>
                                <?php endif;?>
                                <?php if(has_permission('Vision.Clients.Delete_client')):?>
                                    <button type="button" class="btn btn-default btn-default" title="Disable Distributor" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>distribution/distributors/disable_distributor', 'ch=<?php e($row->id);?>&amp;bt=Disable')"><i class="fa fa-trash-o"></i></button>
                                <?php endif;?>
                                <button type="button" class="btn btn-default btn-default" title="Receive Payment" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>distribution/distributors/receive_payment', 'ch2=<?php e($row->id);?>&amp;ch=1')"><i class="fa fa-stack-overflow"></i></button>

                            <?php else: ?>
                                <?php if(has_permission('Vision.Clients.Delete_client')):?>
                                    <button type="button" class="btn btn-default btn-primary" title="Activate Distributor" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php e(base_url());?>distribution/distributors/enable_distributor', 'ch=<?php e($row->id);?>&amp;bt=Activate')"><i class="fa fa-unlock"></i></button>
                                <?php endif;?>
                            <?php endif;?>
                    </td>
                </tr>
                <?php endforeach; endif;?>
                </tbody>

            </table>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->
        <!-- MODAL FOR CREATING DISTRIBUTOR-->
        <div class="modal fade new_distributor" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="wizard" class="form-horizontal" role="form" action="<?php echo base_url(); ?>distribution/distributors/new_distributor" accept-charset="utf-8" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">New Distributor Account</h4>
                        </div>
                        <div class="modal-body">


                            <div class="wizard-steps clearfix"></div>
                            <div class="step" id="account-details"><br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Sales Person</label>
                                    <div class="col-lg-7">
                                        <input id="name" name="name" required="required" type="text" class="form-control col-md-7 col-xs-12" />
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >

                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Phone</label>
                                    <div class="col-lg-7">
                                        <input id="phone" required="required" name="phone" type="text" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Contract Type</label>
                                    <div class="col-lg-7">
                                        <select  name="type"  required="" class="form-control col-md-7 col-xs-12" >
                                            <option value="" disabled selected>Select Contract Type</option>
                                            <option value="1">Salaried Sales</option>
                                            <option value="2">Commission Sales</option>
                                        </select>
                                    </div>
                                </div><!-- End .form-group  -->


                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" name="submit"  class="btn btn-primary" value="Create Distributor">
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL FOR CREATING DISTRIBUTOR-->
    </div>
</div>