<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 1/21/2019
 * Time: 8:16 AM
 */
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Receive Distributor Payments </b></h4>
        </div>
        <form method="post" class="form-horizontal" action="<?php e($url); ?>">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-9">
                        <h2><?php e($distributor->name); ?></h2>
                        <span class="h3">
                            <?php echo($distributor->type and $distributor->type==2)?"Commission Sales":"Salaried Staff"; ?>
                        </span>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <br>


                <div class="form-group">
                    <label class="col-lg-4 col-sm-4 control-label" for="username">Amount Received</label>
                    <div class="col-lg-6 col-sm-6">
                        <input id="credit"  name="amount" type="number" class="form-control col-md-7 col-xs-12" />
                        <input type="hidden" class="form-control" name="distributor_id" value ="<?php e($distributor->id); ?>" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                    </div>
                </div><!-- End .form-group  -->
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary" >Receive Payment</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
