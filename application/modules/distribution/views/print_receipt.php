<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Signal POS - Stock Sheet</title>
<script>
    //window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload();
    }
	window.onload= function () {   
	refreshParent();  
	//printreceipt();
	var count = 0;
	while (count<3){
	window.print();
	count=count+1;
	}
	
	
   }
	
	window.onfocus=function () {   print ();  window.close(); }
	
	//setTimeout(funtion(){window.close();},9);
</script>
<style type="text/css">
        .style1 {
            font-family: cambria,Agency FB;
            font-size: 22px
        }
		.style9 {
            font-family: cambria;
            font-size: 16px;
        }		
        .style2 {
            font-family: calibri;
            font-size: 17px;
        }

        .style5 {
			font-family: calibri;
            font-size: 18px;
        }

        
        .style3 {
            font-family: calibri;
            font-size: 13px;
        }

        .style10 {
            font-family: calibri;
            font-size: 20px;
        }

        .style15 {
            font-family: calibri;
            font-size: 17px;
        }

    </style>
</head>

<body >
<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
<tr>
    <td colspan="4" align="center" valign="middle"><span class="style1">
	<?php
		$button=($button=="CASH SALE")? "FISCAL RECEIPT": $button;
		echo $company->name;
		echo "<br><span class=\"style9\"> ".$company->address." <BR> ".$company->phone." </span>";
		echo "<br><span class=\"style9\">".$button." </span>";
	?>
	</span>
	</td>	
</tr>

  <tr>
    <td colspan="3" align="left" bordercolor="#000000"><span class="style15">Date:
        <?php $currenttime=date("Y-m-d H:i:s"); echo $order_details->created_on; ?>
    </span></td>
  
    <td colspan="1" align="right" bordercolor="#000000"><span class="style15">
        <?php echo " #:" .$order_details->id; ?>
    </span></td>
  </tr>
  <tr><td colspan="4" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
</table>
<?php
$total=0;
$vat=0;
echo <<<eod

			<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                      
					  <thead>
                        <tr>
                          
                          <td align="left" width="63%" class="style9" colspan="2">ITEM</td>
						  <td align="right" width="11%" class="style9">COST</td>
                          <td align="center" width="11%" class="style9">QTY</td>						  
                          <td align="center" width="11%" class="style9">SOLD</td>						  
                          <td align="Right" width="15%" class="style9">SALE</td>
						  
                        </tr>
                      </thead>
                      <tbody>
					  <tr><td colspan="6" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
eod;
$Discount_amount = 0;
foreach($receipt as $rows){

	if($rows->returned_qty){
        $pricesold = $rows->selling_price*($rows->issued_qty-$rows->returned_qty);
    }else{
        $pricesold = $rows->selling_price*$rows->issued_qty;
    }
	//$vat = $rows->VATS + $vat;
	$total = $total + $pricesold;
//    $q = ($rows->returned_qty and $rows->returned_qty>=0)?number_format($rows->issued_qty-$rows->returned_qty):0;
    $q = number_format($rows->issued_qty-$rows->returned_qty);
			echo "
                        <tr>
                          
                          <td class=\"style2\" colspan=\"2\">".ucwords(strtolower($rows->product_name))."</td>
                          <td class=\"style2\" align=\"right\" >".number_format($rows->selling_price,2)."</td>
						  <td class=\"style2\" align=\"center\" >".$rows->issued_qty."</td>
						  <td class=\"style2\" align=\"center\" >".$q."</td>
                          <td class=\"style2\" align=\"right\">".number_format($pricesold,2)."</td>
						  
                        </tr>";


}

 $label=($order_details->status==1 or $order_details->status==2)?"Due":"Sold";
	echo <<<eod
           </tbody>
					  
					   <tr><td colspan="6" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
					  <tr class="text-primary">
                          <td colspan=5 align="right" class="style2">Sale &nbsp;</td>
                          <td class="style2" align="right" width="30%"> $total </td>
					  </tr>					  
					  
					  <tr class="text-primary">	
                          <td colspan=5 align="right" class="style2">Total $label &nbsp;</td>
                          <td class="style2" align="right"> 	
eod;
	
		
		//$balance = $total - $amount_paid;
		//$amount_paid = number_format($amount_paid);
		//$balance = number_format($balance);
		echo number_format($total,2);

		echo <<<eod
					</td>
						
					  <tr><td colspan="6" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
						<tr>
						<td colspan="6" align="center" valign="middle" bordercolor="#000000" class="style9"><br> Served by:&nbsp;$username
						| Issued To $order_details->distributor
						</td>
					  </tr>
					  <tr>
					    <td colspan="6" align="center" valign="middle" bordercolor="#000000" class="style3">
							Thank you for shopping with Us <br>  $company->slogan<br>
							
						</td>
						<tr>
					  <tr>
						<td colspan="6" align="center" valign="middle" bordercolor="#000000" class="style3">
						 <br> 2019 &copy; SignalPOS <br> info@microvision.co.ke
						</td>
					  </tr>
                    </table>
eod;

?>

</body>
</html>

<!-- **removed VAT from receipt line 143				  
					  <tr><td colspan="2"></td><td colspan="2"><hr></td></tr>
                        </tr>
						<tr class="text-primary">
                          <td colspan=3 align="right" class="style9">VAT &nbsp;</td>
                          <td class="style9"> Ksh $vat </td>
					  </tr> -->
					  