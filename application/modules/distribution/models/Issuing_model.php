<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Issuing_model extends MY_Model {
	protected $table_name = 'vision_distributor_orders';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


    public function get_order($id)
    {
        return $this->db->query("SELECT bf_vision_distributor_orders.*,display_name,bf_vision_distributors.name as distributor,bf_vision_distributors.phone as phone, bf_vision_distributors.type as type_dist  FROM bf_vision_distributor_orders 
                                    LEFT JOIN bf_vision_distributors ON bf_vision_distributors.id = bf_vision_distributor_orders.distributor_id
									LEFT JOIN bf_users ON bf_users.id = bf_vision_distributor_orders.created_by
									WHERE bf_vision_distributor_orders.id='".$id."'")->row();
    }
    public function get_orders()
    {
        return $this->db->query("SELECT bf_vision_distributor_orders.*,display_name,bf_vision_distributors.name as distributor  FROM bf_vision_distributor_orders 
                                    LEFT JOIN bf_vision_distributors ON bf_vision_distributors.id = bf_vision_distributor_orders.distributor_id
									LEFT JOIN bf_users ON bf_users.id = bf_vision_distributor_orders.created_by
									WHERE bf_vision_distributor_orders.status!=0 ORDER BY id DESC LIMIT 12")->result();
    }
    public function get_company()
    {
        return $this->db->query("SELECT * FROM bf_vision_business_profile LIMIT 1")->row();
    }

}