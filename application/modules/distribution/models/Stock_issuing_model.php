<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock_issuing_model extends MY_Model {
	protected $table_name = 'vision_distributor_order_items';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_issued_items($id)
    {
        return $this->db->query("SELECT bf_vision_distributor_order_items.*,unit_name,product_name,retail_price,distribution_price FROM bf_vision_distributor_order_items
									LEFT JOIN (SELECT bf_vision_products.*,unit_name FROM bf_vision_products 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id) as products ON products.id=bf_vision_distributor_order_items.product_id
									WHERE order_id='".$id."'")->result();
    }

    public function get_issued_item($id)
    {
        return $this->db->query("SELECT bf_vision_distributor_order_items.*,unit_name,product_name,retail_price,distribution_price FROM bf_vision_distributor_order_items
									LEFT JOIN (SELECT bf_vision_products.*,unit_name FROM bf_vision_products 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id) as products ON products.id=bf_vision_distributor_order_items.product_id
									WHERE bf_vision_distributor_order_items.id='".$id."'")->row();
    }
    public function delete_item($id)
    {
        $this->db->query("DELETE FROM bf_vision_distributor_order_items WHERE id='".$id."'");
    }
}