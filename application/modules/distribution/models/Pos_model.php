<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pos_model extends MY_Model {
	protected $table_name = 'vision_bills';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_user_bills($user_id)
    {
        //return $this->db->query("SELECT * FROM bills WHERE userId_fk='".$userId."' and billstatus!=1 Order by createdDate desc")->result();
        return $this->db->query("SELECT * FROM bf_vision_bills WHERE user_id='".$user_id."' and (status=1 or status=4) Order by created_on asc")->result();
    }
    public function get_last_bill()
    {
        return $this->db->query("SELECT RIGHT(bill_alias,LOCATE('ale ',bill_alias)) AS bill_alias,created_on from bf_vision_bills ORDER BY created_on DESC, RIGHT(bill_alias,LOCATE('ale ',bill_alias)) DESC LIMIT 1")->row();
    }
    public function get_products()
    {
        return $this->db->query("SELECT bf_vision_products.*,bf_vision_product_categories.name as category, unit_name,S FROM  bf_vision_products 
                                        LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id = category_id 
                                        LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id 
                                        LEFT JOIN(SELECT product_id, count(product_id)as S from bf_vision_sales group by product_id)as salesFreq on product_id=bf_vision_products.id 
                                  WHERE bf_vision_products.status = 1 ORDER BY S desc,product_name")->result();
    }
    public function get_cat_products($catid)
    {
        return $this->db->query("SELECT bf_vision_products.*,bf_vision_product_categories.name as category, unit_name FROM  bf_vision_products 
                                        LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id = category_id 
                                        LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id 
                                  WHERE bf_vision_products.status = 1 AND (product_name LIKE '%".$catid."%' OR  bf_vision_product_categories.name LIKE '%".$catid."%') ORDER BY product_name")->result();
    }
    public function get_cat_products_type($catid)
    {
        return $this->db->query("SELECT bf_vision_products.*,bf_vision_product_categories.name as category, unit_name FROM  bf_vision_products 
                                        LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id = category_id 
                                        LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id 
                                  WHERE bf_vision_products.status = 1 AND (product_name LIKE '%".$catid."%' OR  bf_vision_product_categories.name LIKE '%".$catid."%') ORDER BY product_name LIMIT 50")->result();
    }
    public function get_receipt($billId)
    {
        return $this->db->query("SELECT bf_vision_sales.*,bf_vision_products.*,unit_name,bf_vision_sales.vat as VATS, bf_vision_sales.id as sale_id FROM bf_vision_sales
                                        LEFT JOIN bf_vision_products ON bf_vision_sales.product_id = bf_vision_products.id 
                                        LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id WHERE bill_id='".$billId."' ")->result();
    }
    public function get_bill($bill_Id)
    {
        return $this->db->query("SELECT bf_vision_bills.*,bf_users.display_name, bf_users.username,total FROM bf_vision_bills
                                      LEFT JOIN (SELECT sum(qty*selling_price)as total,bill_id FROM bf_vision_sales WHERE bill_id='".$bill_Id."' GROUP BY bill_id)as sales ON bill_id=bf_vision_bills.id
                                      LEFT JOIN bf_users ON bf_vision_bills.created_by=bf_users.id WHERE bf_vision_bills.id='".$bill_Id."' LIMIT 1")->row();
    }
    public function get_company()
    {
        return $this->db->query("SELECT * FROM bf_vision_business_profile LIMIT 1")->row();
    }
    public function updatebill_paid($billid,$payment_mode,$status)
    {
        $this->db->query("UPDATE bf_vision_bills SET payment_option='".$payment_mode."',status='".$status."' WHERE  id='".$billid."'");
    }
    public function get_null_bills($old_date)
    {
        return $this->db->query("SELECT bf_vision_bills.id,bill_id,bf_vision_bills.created_on,bf_vision_bills.status from bf_vision_bills 
                            LEFT JOIN bf_vision_sales on bf_vision_bills.id=bill_id WHERE bf_vision_bills.created_on<='".$old_date."' and (bf_vision_bills.status !=2 or bf_vision_bills.status !=4) and bf_vision_sales.id is NULL")->result();
    }
    public function delete_null_bills($billid)
    {
        $this->db->query("UPDATE bf_vision_bills SET status= '0' WHERE id='".$billid."'");
    }
    public function unlock_bill($billid)
    {
        $this->db->query("UPDATE bf_vision_bills SET status  = '4' WHERE id='".$billid."'");
    }
//    public function get_receipt($billId)
//    {
//        return $this->db->query("SELECT bf_vision_sales.*,bf_vision_products.*,bf_vision_sales.vat as VATS FROM bf_vision_sales
//                                      LEFT JOIN bf_vision_products ON bf_vision_sales.product_id = bf_vision_products.id WHERE bill_id='".$billId."' ")->result();
//    }
}