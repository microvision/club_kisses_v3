<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributors_transactions_model extends MY_Model {
	protected $table_name = 'vision_distributor_transactions';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


    public function get_opening_bal($start,$distributor_id)
    {
        return $this->db->query("SELECT SUM(if(transaction_type='1',-amount,amount)) as amount, distributor_id FROM bf_vision_distributor_transactions WHERE date(created_on)<'".$start."' and distributor_id='".$distributor_id."' GROUP BY distributor_id")->row();
    }
    public function get_distributor_transactions($start,$end,$distributor_id)
    {
        return $this->db->query("SELECT bf_vision_distributor_transactions.* FROM bf_vision_distributor_transactions WHERE date(created_on)>='".$start."' and date(created_on)<='".$end."' and distributor_id='".$distributor_id."'")->result();
    }
	
}