<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributors_model extends MY_Model {
	protected $table_name = 'vision_distributors';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

	public function get_distributors()
    {
        return $this->db->query("SELECT bf_vision_distributors.*, ac_bal FROM bf_vision_distributors 
                                  LEFT JOIN (SELECT distributor_id, SUM(if(transaction_type='2',-amount,amount)) as ac_bal FROM bf_vision_distributor_transactions GROUP BY distributor_id)as account_bal ON account_bal.distributor_id=bf_vision_distributors.id")->result();
    }

	
}