<?php if (!defined ('BASEPATH')) exit('No Direct Script access allowed');

$config['module_config'] = array(
    'description'   => 'Sales and distribution',
    'name'          => 'Distribution',
    'version'       => '1.0.0',
    'author'        => 'Edwin Ombego'
);