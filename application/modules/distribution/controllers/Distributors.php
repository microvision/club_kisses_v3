<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Distributors extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

        $this->load->model('distributors_model');
        $this->load->model('issuing_model');
        $this->load->model('distributors_transactions_model');
		
	}
	

    public function index()
    {

        Template::set('distributors', $this->distributors_model->get_distributors());
        Template::set_theme('default');
        Template::set('page_title', 'Manage Distributors');
        Template::render('');
    }

    public function new_distributor(){
        $this->auth->restrict('Vision.Suppliers.Create_supplier');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'type' => $_POST['type'],
                'status' => 1
            );
            $supplier_id = $this->distributors_model->insert($data);
            $data = array('distributor_id'=>$supplier_id);
            $this->issuing_model->update($_POST['order_id'],$data);
            log_activity($this->auth->user_id(),"Created new distributor ".$_POST['name'], 'distribution');
            Template::set_message('The Distributors account was successfully created', 'alert fresh-color alert-success');
        }
        redirect("distribution/distributors", true);
    }
    public function edit_distributor(){
//        $this->auth->restrict('Vision.clients.Edit_client');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'type' => $_POST['type']
            );
            $this->distributors_model->update($_POST['distributor_id'],$data);
            log_activity($this->auth->user_id(),"Edited the distributor details for ".$_POST['name'], 'distribution');
            redirect("distribution/distributors",true);
        }else{
            $distributor_id = $_GET['ch2'];
            $data['distributor'] = $this->distributors_model->as_object()->find($distributor_id);
            $data['url']=base_url()."distribution/distributors/edit_distributor";
            $this->load->view('distributors/edit_distributor',$data);
        }

    }
    public function disable_distributor(){
        if (ISSET($_POST['submit'])){
            $distributor = $this->distributors_model->as_object()->find($_POST['distributor_id']);

            $data = array('status'=> 0);
            if ($this->distributors_model->update($_POST['distributor_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the distributor ".$distributor->name, 'distributor');
                Template::set_message('The sales person '.$distributor->name.' was successfully deactivated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the distributor account. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('distribution/distributors',true);
        }else{
            $data['distributor_id'] = $this->input->get("ch");
            $data['distributor'] = $this->distributors_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."distribution/distributors/disable_distributor";
            $data['action']=$_GET['bt'];
            $this->load->view('distributors/disable_distributor',$data);
        }
    }
    public function enable_distributor(){
//        $this->auth->restrict('Vision.distributors.Delete_distributor');
        if (ISSET($_POST['submit'])){
            $distributor = $this->distributors_model->as_object()->find($_POST['distributor_id']);

            $data = array('status'=> 1);
            if ($this->distributors_model->update($_POST['distributor_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Activated the distributor ".$distributor->name, 'distributors');
                Template::set_message('The distributor '.$distributor->name.' was successfully activated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the distributor account. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('distribution/distributors',true);
        }else{
            $data['distributor_id'] = $this->input->get("ch");
            $data['distributor'] = $this->distributors_model->as_object()->find ($_GET['ch']);
            $data['url']=base_url()."distribution/distributors/enable_distributor";
            $data['action']=$_GET['bt'];
            $this->load->view('distributors/disable_distributor',$data);


        }
    }
    public function receive_payment(){
        if(ISSET($_POST['submit'])){
            $distributor = $this->distributors_model->as_object()->find($_POST['distributor_id']);
            $data = array(
                'distributor_id' => $distributor->id,
                'amount' => $_POST['amount'],
                'transaction_type' => 1
            );
            $this->distributors_transactions_model->insert($data);
            log_activity($this->auth->user_id(),"Received distributor Payments for ".$distributor->name, 'distributors');
            Template::set_message('Payments of <b>Ksh '.number_format($_POST['amount'],2).'</b> for '.$distributor->name.' was successfully received.', 'alert fresh-color alert-success');
            redirect("distribution/distributors/index",true);
        }else{
            $distributor_id = $_GET['ch2'];
            $data['distributor'] = $this->distributors_model->as_object()->find($distributor_id);
            $data['url']=base_url()."distribution/distributors/receive_payment";
            $this->load->view('distributors/received_payment',$data);
        }

    }
    public function distributor_statements(){
        if(ISSET($_POST['submit'])){
            $start_date  = date("d-M-Y",strtotime($_POST['start']));
            $end_date  = date("d-M-Y",strtotime($_POST['end']));
            Template::set('opening_balance', $this->distributors_transactions_model->get_opening_bal($_POST['start'],$_POST['distributor']));
            Template::set('transactions', $this->distributors_transactions_model->get_distributor_transactions($_POST['start'],$_POST['end'],$_POST['distributor']));
            Template::set('startdate', $start_date);
            Template::set('enddate', $end_date);
            Template::set('distributor', $this->distributors_model->as_object()->find($_POST['distributor']));
        }
        Template::set('distributors', $this->distributors_model->get_distributors());
        Template::set_theme('default');
        Template::set('page_title', 'Distributors Statements');
        Template::render('');
    }
}