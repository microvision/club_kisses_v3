<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Distribution extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		

		$this->load->model('measurement_units_model');
		$this->load->model('inventory_model');
		$this->load->model('issuing_model');
		$this->load->model('stock_issuing_model');
		$this->load->model('distributors_model');
		$this->load->model('distributors_transactions_model');
		$this->load->model('pos_model');
		$this->load->model('sales_model');
	}

    public function index($id=null)
    {
        if($id){

            Template::set('stock_issue_details', $this->issuing_model->get_order($id));
            Template::set('issued_items', $this->stock_issuing_model->get_issued_items($id));
        }
        Template::set('stock_issue', $this->issuing_model->get_orders());
        Template::set('distributors', $this->distributors_model->order_by('name','asc')
                        ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Distribution');
        Template::render('');
    }
    public function create_stock_issue_order()
    {
        $today = date('Y-m-d');
        $data = array(
//            'supplier_id'=> $_POST['supplier_id'],
//            'invoice_number'=> $_POST['invoice'],
            'issuing_date'=> $today,
            'status'=> 1
        );
        $order_id = $this->issuing_model->insert($data);
        redirect('distribution/index/'.$order_id,true);
    }
    public function issue_order_item()
    {
        $itemIds = $_POST['items'];
        $quantity = $_POST['qty'];
        //get order details for the distributor id
        $order_details = $this->issuing_model->get_order($_POST['order_id']);
        foreach($itemIds as $itemId){
            //get product details for pricing based on the distributor type
            $product_details = $this->inventory_model->as_object()->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                ->select('bf_vision_products.*,unit_name')
                ->find_by('bf_vision_products.id',$itemId);

            //we check if the quantity requested is available
            $stock_check = $this->inventory_model->stock_check($itemId);
            //we calculate VAT if required

            $checkedValue = $stock_check->active_stock-$_POST['qty'];
            $activestock = $this->inventory_model->get_active_stock($itemId);

            if ($checkedValue >= 0){
                //get active stock

                //check if the value of the active stock is more than the current active stock
                if((!ISSET($activestock->current_qty)) or ($activestock->current_qty  < $quantity)){
                    $orderquantity=$quantity;

                    do{
                        $activestock = $this->inventory_model->get_active_stock($itemId);
                        $stock_check = $this->inventory_model->stock_check($itemId);
                        $currentquantity = $activestock->current_qty-$orderquantity;

                        //$buyquantity = $buyquantity - $activestock->currentQuantity;
                        if ($stock_check->active_stock==0){
                            $x=1;
                        }else if($currentquantity <= 0){
                            //check if the current stock is enough to cover the remaining sale
                            if($activestock->current_qty >= $orderquantity) {
                                $status=1;
                                $this->inventory_model->update_stock($activestock->id, abs($currentquantity),$status);
                                $x=1;
                            }else{
                                $status = 2;
                                //deactivate current stock as complete
                                $this->inventory_model->update_stock($activestock->id, 0,$status);
                                //activate the next stock
                                $latestInactiveStock = $this->inventory_model->get_latest_inactivestockdate($itemId);
                                if(ISSET($latestInactiveStock->purchasedate) and $latestInactiveStock->purchasedate!='') {
                                    $this->inventory_model->activate_stock($itemId, $latestInactiveStock->purchasedate);
                                }
                                $x=0;
                            }
                            //$buyquantity = $currentquantity - (2 * $currentquantity);
                            $orderquantity = abs($currentquantity);

                        }else{
                            $status = 1;
                            $this->inventory_model->update_stock($activestock->id, $currentquantity,$status);
                            $x=1;
                        }



                    }while($x==0);

                    //get product pricing based on the distributor type
                    $price=($order_details->type_dist==1)?$product_details->wholesale_price:$product_details->distribution_price;
                    //save product for ordering
                    $data = array(
                        'order_id'=> $_POST['order_id'],
                        'product_id'=> $itemId,
                        'selling_price'=> $price,
                        'issued_qty'=> $_POST['qty']
                    );
                    $this->stock_issuing_model->insert($data);

                }else{
                    //if it is less we update the current stock value and save the sale
                    if($activestock->current_qty == $quantity){
                        $status = 2;
                        //activate the next stock
                        $latestInactiveStock = $this->inventory_model->get_latest_inactivestockdate($productid);
                        $this->inventory_model->activate_stock($productid, $latestInactiveStock->purchasedate);
                    }else{
                        $status = 1;
                    }
                    //$activestock = $this->pos_model->get_active_stock($productid);
                    $currentquantity = $activestock->current_qty-$quantity;
                    $this->inventory_model->update_stock($activestock->id, $currentquantity,$status);

                    //get product pricing based on the distributor type
                    $price=($order_details->type_dist==1)?$product_details->wholesale_price:$product_details->distribution_price;
                    //save product for ordering
                    $data = array(
                        'order_id'=> $_POST['order_id'],
                        'product_id'=> $itemId,
                        'selling_price'=> $price,
                        'issued_qty'=> $_POST['qty']
                    );
                    $this->stock_issuing_model->insert($data);


                }
            }else{
                Template::set_message('The product  '.$product_details->product_name.' could not be added. You only have '.number_format($stock_check->active_stock).' '.$product_details->unit_name.' availbale.', 'alert fresh-color alert-danger');
            }
        }

        //exit;
        redirect('distribution/index/'.$_POST['order_id'],true);
    }
    public function submit_requisition($id){

        $order_details= $this->issuing_model->get_order($id);

        //update order status
        if($order_details->status==1){
            $data = array('status'=>2);
            $this->issuing_model->update($order_details->id,$data);
        }

        $data['order_details'] = $this->issuing_model->get_order($id);
        $data['receipt'] = $this->stock_issuing_model->get_issued_items($id);
        $data['button'] = 'STOCK ISSUE RECEIPT';
        $data['company'] = $this->issuing_model->get_company();
        $data['username'] =  $this->current_user->display_name;
        $this->load->view("distribution/print_receipt", $data);

    }
    public function submit_reconciliation(){
        if($_POST['submit']){
            $id = $_POST['order_id'];
            $order_details = $this->issuing_model->get_order($id);
            $order_items = $this->stock_issuing_model->get_issued_items($id);
            $user_id = $this->current_user->id;
            if(ISSET($_POST['payment_mode'])){ $payment_option = $_POST['payment_mode']; }else{ $payment_option = 1; }
            //create a bill
            $data = array(
                'user_id' => $user_id,
                'payment_option' => $payment_option,
                'bill_alias' => $order_details->distributor,
                'bill_type' => 3,
                'client_id' => $order_details->id,
                'status' => 2
            );
            $billId = $this->pos_model->insert($data);
            foreach($order_items as $row){

                //update qty reconciled to sales
                $qty_returned = $_POST['re_'.$row->id];
                if($qty_returned<0 or $qty_returned=="" or !$qty_returned){
                    $qty_returned =0;
                }
                $qty_returned = number_format($qty_returned);
                $qty_sold = $row->issued_qty - $qty_returned;

                $data_sale = array(
                    'product_id' => $row->product_id,
                    'bill_id' => $billId,
                    //todo: include the stock id during issuing function on line 50
                    'stock_shop_id' => $row->id,
                    'selling_price' => $row->selling_price,
                    'qty' => $qty_sold,
                    'vat' => 0,
                    'status' => 1
                );
                $this->sales_model->insert($data_sale);
                //update qty reconcilled
                $data_reconcile=array(
                    'returned_qty'=> $qty_returned
                );
                $this->stock_issuing_model->update($row->id,$data_reconcile);

                //update returned stocks back to current stocks
                $current_stock_value = $this->inventory_model->get_stock_value($row->product_id);
                if(ISSET($current_stock_value->current_qty)){
                    //update the current active stock for the item
                    $new_stock_value = $current_stock_value->current_qty + $qty_returned;
                }else{
                    //if there is no active stock
                    $new_stock_value=$qty_returned;
                    $current_stock_value = $this->inventory_model->get_last_inactive_stock($row->product_id);
                    //activate the last stock
                    $this->inventory_model->update_last_inactive_stock($current_stock_value->id);
                }
                $this->inventory_model->update_stock_value($current_stock_value->id, $new_stock_value);

            }
            //get total amount supposed to be paid and create a transaction if there is a deficit
            $bill_details = $this->pos_model->get_bill($billId);
            $payment_deficit = $bill_details->total - $_POST['amount_reconciled'];

            if($payment_deficit<0){$trans_type=1;}else{$trans_type=2;}
            $payment_deficit = abs($payment_deficit);
            if($payment_deficit!=0){
                //create payment transaction for the distributor
                $data_transaction = array(
                    'distributor_id' => $order_details->distributor_id,
                    'transaction_type' => $trans_type,
                    'amount' => $payment_deficit
                );
                $this->distributors_transactions_model->insert($data_transaction);
            }


            //update the order status
            $data = array('status'=>3);
            $this->issuing_model->update($order_details->id,$data);
        }
        $this->load->view("distribution/print_receipt", $data);

        Template::set_message('The stocks were successfully reconcilled', 'alert fresh-color alert-success');
        redirect("distribution/index/".$order_details->id,true);
    }
    public function update_order_fields(){
        $field = $_GET['val'];
        if($field == "distributor"){
            $data = array('distributor_id'=>$_GET['distributor_id']);
            $this->issuing_model->update($_GET['order_id'],$data);
            $order_details = $this->issuing_model->get_order($_GET['order_id']);
            if($order_details->type_dist==2){
                $type="Commission Sales";
            }else{$type="Salaried Distributor";}
            echo <<<eod
             <br>
             <span class="h4 text-danger">$order_details->distributor<br></span>
               <span class="h6"><i class="fa fa-phone"></i> $order_details->phone | <i class="fa fa-truck"></i>$type </span>

eod;

        }elseif($field == "comments"){
            $data = array('comments'=>$_GET['comments']);
            $this->issuing_model->update($_GET['order_id'],$data);
        }elseif($field == "issuing_date"){
            $data = array('issuing_date'=>$_GET['issuing_date']);
            $this->issuing_model->update($_GET['order_id'],$data);
        }
    }
    public function new_distributor(){
        $this->auth->restrict('Vision.Suppliers.Create_supplier');
        if(ISSET($_POST['submit'])){
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'type' => $_POST['type'],
                'status' => 1
            );
            $supplier_id = $this->distributors_model->insert($data);
            $data = array('distributor_id'=>$supplier_id);
            $this->issuing_model->update($_POST['order_id'],$data);
            log_activity($this->auth->user_id(),"Created new distributor ".$_POST['name'], 'stores');
            Template::set_message('The Distributors account was successfully created', 'alert fresh-color alert-success');
        }

        redirect("distribution/index/".$_POST['order_id'],true);
    }
    public function get_search_items()
    {
        $product = $this->input->get('ch');
        $data['allitems'] = $this->inventory_model->get_search_products($product);
        $this->load->view("distribution/get_inventory_search_results", $data);
    }
    public function show_qty_fields(){
        $id=$_GET['ch'];
        $details = $this->inventory_model->as_object()->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,unit_name')
            ->find_by('bf_vision_products.id',$id);
        echo <<<eod
            <div class="block">
            
			<div class="form-group">
				<label class="col-md-3 control-label">Issued Quantity</label>
				<div class="col-md-6">
					<input type="number" name="qty" placeholder="Order Quantity" required="required" class="form-control" />																		
				</div>
                <div class="col-md-1"><h4>$details->unit_name</h4></div>
			</div>
			
		</div>
eod;

    }
    public function delete_stock_order_item(){
        //we get details on the item
        $stock_item_details = $this->stock_issuing_model->get_issued_item($_GET['order_item_id']);

        $current_stock_value = $this->inventory_model->get_stock_value($stock_item_details->product_id);
        if(ISSET($current_stock_value->current_qty)){
            //update the current active stock for the item
            $new_stock_value = $current_stock_value->current_qty + $stock_item_details->issued_qty;
        }else{
            //if there is no active stock
            $new_stock_value=$stock_item_details->issued_qty;
            $current_stock_value = $this->inventory_model->get_last_inactive_stock($stock_item_details->product_id);
            //activate the last stock
            $this->inventory_model->update_last_inactive_stock($current_stock_value->id);
        }
        $this->inventory_model->update_stock_value($current_stock_value->id, $new_stock_value);

        $this->stock_issuing_model->delete($_GET['order_item_id']);

        redirect("distribution/index/".$_GET['order_id'],true);

    }

}