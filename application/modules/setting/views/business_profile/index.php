<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png'); ?>"
                                                         class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <span class="h4 text-danger">Business Profile</span>
                    <hr>
                    <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
                        <div class="form-group">
                            <label class="control-label col-lg-4">Business Name </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" required=""
                                       value="<?php echo $business->name; ?>" placeholder="Business Name" name="name"
                                       id="req" autofocus>
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                       value="<?php echo $this->security->get_csrf_hash(); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">VAT Number </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" value="<?php echo $business->vat_no; ?>"
                                       placeholder="VAT Number" name="vat">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">PIN Number </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" placeholder="PIN Number"
                                       value="<?php echo $business->pin_no; ?>" name="pin">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Address </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" required="" placeholder="Physical Address"
                                          name="address"><?php echo $business->address; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Phone Number </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" required=""
                                       value="<?php echo $business->phone; ?>" placeholder="Phone Number" name="phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Slogan </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" required="" placeholder="Business Slogan"
                                       value="<?php echo $business->slogan; ?>" name="slogan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Mpesa Payment Type </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control"  placeholder="Enter Name of Mpesa Payment Type e.g Paybill,Till ,Agent Number etc"
                                       value="<?php echo $business->mpesa_payment_type; ?>" name="mpesa_payment_type">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-4">Payment type Number </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control"  placeholder="Payment type Number"
                                       value="<?php echo $business->mpesa_payment_number; ?>" name="mpesa_payment_number">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-4"></label>
                            <div class="col-lg-6">
                                <input type="submit" value="Save Changes " name="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div id="txtResult" class="modal fade"></div>
        </div>

        <!-- end content -->

    </div>
