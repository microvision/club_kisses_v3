<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Business_profile extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		$this->load->model('business_model');

	}

    public function index($id=null){
        $this->auth->restrict('Vision.Setting.Business_profile');
		if (ISSET($_POST['submit'])){
			$data = array(
				'name'=> $_POST['name'],
				'vat_no'=> $_POST['vat'],
				'pin_no'=> $_POST['pin'],
				'address'=> $_POST['address'],
				'phone'=> $_POST['phone'],
				'slogan'=> $_POST['slogan'],
				'mpesa_payment_type'=> $_POST['mpesa_payment_type'],
				'mpesa_payment_number'=> $_POST['mpesa_payment_number'],
			);
			if ($this->business_model->update(1,$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Edited the business profile", 'setting');
                Template::set_message('The busines profile has been successfully updated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered while updatin the business profile. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/business_profile/index',true);
		}
        Template::set('business', $this->business_model->as_object()->find(1));
        Template::set_theme('default');
        Template::set('page_title', 'Business Profile');
        Template::render('');

    }
    
   
}