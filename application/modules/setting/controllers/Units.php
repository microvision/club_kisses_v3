<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Units extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

		$this->load->model('units_model');

		
	}
	
	public function index($id=null){
		$this->auth->restrict('Vision.Setting.Units_View');
		Template::set('units', $this->units_model->order_by('unit_name','asc')
                                                 ->find_all());
		Template::set_theme('default');
		Template::set('page_title', 'Measurement Units');
		Template::render('');

    }
    public function create_unit(){
        $this->auth->restrict('Vision.Setting.Units_View');
        if(ISSET($_POST['unit_name'])){
            $data = array(
                'unit_name'=> $_POST['unit_name'],
                'status'=> 1
            );
            if ($this->units_model->insert($data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Created new unit measurement ".$_POST['unit_name'], 'setting');
                Template::set_message('The unit measurement was successfully created.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered creating the unit measurement. Please check the values submitted.', 'alert fresh-color alert-danger');
            }
        }
        redirect('setting/units/index',true);
    }
    public function disable_unit(){
        if (ISSET($_POST['submit'])){
            $unit = $this->units_model->as_object()->find($_POST['unit_id']);

            $data = array('status'=> 0);
            if ($this->units_model->update($_POST['unit_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the unit measurement ".$unit->unit_name, 'setting');
                Template::set_message('The unit measurement was successfully disabled.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the unit measurement. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/units/index',true);
        }else{
            $data['unitId'] = $this->input->get("ch");
            $data['unit'] = $this->units_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."setting/units/disable_unit";
            $data['action']=$_GET['bt'];
            $this->load->view('setting/units/disable_unit',$data);


        }
    }
    public function enable_unit(){
        if (ISSET($_POST['submit'])){
            $unit = $this->units_model->as_object()->find($_POST['unit_id']);

            $data = array('status'=> 1);
            if ($this->units_model->update($_POST['unit_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Enabled the unit measurement ".$unit->unit_name, 'setting');
                Template::set_message('The unit measurement was successfully activated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the unit measurement. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/units/index',true);
        }else{
            $data['unitId'] = $this->input->get("ch");
            $data['unit'] = $this->units_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."setting/units/enable_unit";
            $data['action']=$_GET['bt'];
            $this->load->view('setting/units/disable_unit',$data);


        }
    }
    public function edit_unit(){
        if (ISSET($_POST['submit'])){
            $unit = $this->units_model->as_object()->find($_POST['unit_id']);

            $data = array('unit_name'=> $_POST['unit_name']);
            if ($this->units_model->update($_POST['unit_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Edited the unit measurement ".$unit->unit_name, 'setting');
                Template::set_message('The unit measurement was successfully changed.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered editing the unit measurement. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/units/index',true);
        }else{
            $data['unitId'] = $this->input->get("ch");
            $data['unit'] = $this->units_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."setting/units/edit_unit";
            $this->load->view('setting/units/edit_unit',$data);


        }
    }


}