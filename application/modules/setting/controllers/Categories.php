<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

		$this->load->model('categories_model');

	}

    public function index($id=null){
        $this->auth->restrict('Vision.Setting.View_categories');
        Template::set('categories', $this->categories_model->order_by('name','asc')
            ->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Product Categories');
        Template::render('');

    }
    public function create_category(){
        $this->auth->restrict('Vision.Setting.Create_categories');
        if(ISSET($_POST['category_name'])){
            $data = array(
                'name'=> $_POST['category_name'],
                'status'=> 1
            );
            if ($this->categories_model->insert($data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Created new product category ".$_POST['category_name'], 'setting');
                Template::set_message('The product category was successfully created.', 'alert fresh-color alert-success');
            }else{
                Template::set_message('Error Saving!! A problem was encountered creating the product category. Please check the values submitted.', 'alert fresh-color alert-danger');
            }
        }
        redirect('setting/categories/index',true);
    }
    public function disable_category(){
        if (ISSET($_POST['submit'])){
            $category = $this->categories_model->as_object()->find($_POST['category_id']);

            $data = array('status'=> 0);
            if ($this->categories_model->update($_POST['category_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled the Product category ".$category->name, 'setting');
                Template::set_message('The product category was successfully disabled.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered disabling the product category. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/categories/index',true);
        }else{
            $data['category_id'] = $this->input->get("ch");
            $data['category'] = $this->categories_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."setting/categories/disable_category";
            $data['action']=$_GET['bt'];
            $this->load->view('setting/categories/disable_category',$data);


        }
    }
    public function enable_category(){
        if (ISSET($_POST['submit'])){
            $category = $this->categories_model->as_object()->find($_POST['category_id']);

            $data = array('status'=> 1);
            if ($this->categories_model->update($_POST['category_id'],$data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Enabled the product category ".$category->name, 'setting');
                Template::set_message('The product category was successfully activated.', 'alert fresh-color alert-success');

            }else{
                Template::set_message('Error Saving!! A problem was encountered activating the product category. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/categories/index',true);
        }else{
            $data['category_id'] = $this->input->get("ch");
            $data['category'] = $this->categories_model->as_object()->find($_GET['ch']);
            $data['url']=base_url()."setting/categories/enable_category";
            $data['action']=$_GET['bt'];
            $this->load->view('setting/categories/disable_category',$data);


        }
    }
    public function edit_category()
    {
        if (ISSET($_POST['submit'])) {
            $category = $this->categories_model->as_object()->find($_POST['category_id']);

            $data = array('name' => $_POST['category_name']);
            if ($this->categories_model->update($_POST['category_id'], $data)) {
                // Log the Activity
                log_activity($this->auth->user_id(), "Edited the product category " . $category->name, 'setting');
                Template::set_message('The product category was successfully changed.', 'alert fresh-color alert-success');

            } else {
                Template::set_message('Error Saving!! A problem was encountered editing the product category. Please try again.', 'alert fresh-color alert-danger');
            }
            redirect('setting/categories/index', true);
        } else {
            $data['category_id'] = $this->input->get("ch");
            $data['category'] = $this->categories_model->as_object()->find($_GET['ch']);
            $data['url'] = base_url() . "setting/categories/edit_category";
            $this->load->view('setting/categories/edit_category', $data);


        }
    }
}