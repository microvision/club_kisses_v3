<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Backup extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();
		

		
	}

    public function index(){
        $db_name = 'SignalPOS_'. date("YmdHis") .'.gz';
        // Load the DB utility class
        $this->load->dbutil();
        // Backup your entire database and assign it to a variable
        $backup =& $this->dbutil->backup();
        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file(FCPATH.'/downloads/'.$db_name, $backup);
        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download($db_name, $backup);
    }

}