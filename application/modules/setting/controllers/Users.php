<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('users/auth');
		$this->load->helper('form_helper');
		$this->auth->restrict();

        $this->load->model('user_accounts_model');

		
	}
	
	public function index($id=null){


    }
    public function new_user(){

        if($this->input->post("register")){
            $this->auth->restrict('Vision.Setting.Create_Users');
            $username=$this->input->post("username");
            $fullnames=$this->input->post("display_name");
            $email=$this->input->post("email");
            $phone=$this->input->post("phone");

            if($this->input->post("role")){
                $role = $this->input->post("role");
            }else{
                $role=8;
            }
            //check if username has been used
            $username_check = $this->user_accounts_model->find_by("username",$username);
            $email_check = $this->user_accounts_model->find_by("email",$email);
            if($this->user_accounts_model->find_by("username",$username)){
                Template::set_message('The user was not succesfully created. The username is already in use.', 'alert fresh-color alert-danger');
            }elseif($this->user_accounts_model->find_by("email",$email)){
                Template::set_message('The user was not succesfully created. The email is already in use.', 'alert fresh-color alert-danger');
            }else{
                //generate a random pasword for the user
                $random_pass= mt_rand(100000,999999);
                $password= $this->auth->hash_password($random_pass);
                $data = array(
                    'username'=> $this->input->post('username'),
                    'password_hash'=> $password['hash'],
                    'display_name'=> $this->input->post('display_name'),
                    'email'=> $this->input->post('email'),
                    'phone'=> $this->input->post('phone'),
                    'role_id'=> $role,
                    'language'		=> $this->input->post('language'),
                    'timezone'		=> $this->input->post('timezones'),
//                    'password_iterations' => 2,

                );
                if($this->user_accounts_model->insert($data)){
                    // Log the Activity
                    log_activity($this->auth->user_id(),"Created new user: ".$this->input->post('display_name'), 'vision_setting');
                    Template::set_message('The user account for <b>'.$this->input->post('username').'</b> was succesfully created. An email has been sent to <b>'.$this->input->post('email').'</b> with account details and password.', 'alert fresh-color alert-success');
                }else{
                    Template::set_message('Error Saving!! The was a problem creating the new user. Please check the details submitted.', 'alert fresh-color alert-danger');
                }

                //send the user an email
                header('Content-type: application/json');
                //$user = $this->notifications_model->get_transport_order_detail($orderId);
                $name = @trim(stripslashes("Signal POS"));
                $email = @trim(stripslashes("Account Notification"));
                $subject = @trim(stripslashes("Account Succesfully Created"));
                $message = @trim(stripslashes("A user account has been created for in the Signal Point Of Sale System with the following credentials.\n\n Username: ".$username." \n Password: ".$random_pass." \n You can now be able to manage interact with the system using your account and you will receive notifications from the system through this email.\n For clarifications or assitance please reply to this email." ));
                $email_from = 'support@microvision.or.ke';
                $email_to = $this->input->post('email');
                $body = 'Name: ' . $name . "\n\n" . 'Email: ' . $email . "\n\n" . 'Subject: ' . $subject . "\n\n" . 'Message: ' . $message;
                $success = @mail($email_to, $subject, $body, 'From: <'.$email_from.'>');
                redirect("setting/users/user_accounts");
            }
        }

        $this->auth->restrict('Vision.Setting.View_Users');
        Template::set_theme('default');
        Template::set('page_title', 'New User ');
        Template::render('');
    }
    public function user_accounts(){
        $this->auth->restrict('Vision.Setting.View_Users');
        $user = $this->current_user->id;
        Template::set('users', $this->user_accounts_model->get_users_details());
        Template::set('roles', $this->user_accounts_model->get_user_roles());
        Template::set_theme('default');
        Template::set('page_title', 'User Accounts');
        Template::render('');
    }
    public function change_password(){

        if($this->input->post("submit")){
            $id=$this->input->post("userId");
            $password = $this->input->post("password");
            $repeat_password = $this->input->post("repeat_password");
            if($password == $repeat_password){
                $password= $this->auth->hash_password($password);
                $data['password_hash']=$password['hash'];
                if ($this->user_accounts_model->update($id, $data))
                {
                    // Log the Activity
                    log_activity($this->auth->user_id(),"Changed password for: User id ".$id, 'setting');
                    Template::set_message('The user password was successfully changed.', 'alert fresh-color alert-success');
                    redirect('setting/users/user_accounts',true);
                }
            }else{
                Template::set_message('Sorry the password was not changed.The passwords submitted did not match.', 'alert fresh-color alert-danger');
                redirect('setting/users/user_accounts',true);
            }
        }else{
            $id = $this->input->get("ch2");
            $details = $this->user_accounts_model->as_object()->find($id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."setting/users/change_password";
            echo <<<eod
					<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>Change Password</h3>
						</div>
						<div class="modal-body">
						<h4 align="center">Change the password for user <u> $details->display_name </u></h4>
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<div class="form-group">
									<label class="col-lg-5 control-label" for="username">New Password:</label>
										<div class="col-lg-3">
											<input id="password" name="password" required="required" type="password" data-validate-length="6,8" class="form-control" />							
										<input type="hidden" name="userId" value="$id">								
										<input type="hidden" name="$security_name" value="$security_code" > 
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-5 control-label" for="username">Confirm Password:</label>
										<div class="col-lg-3">
											<input id="repeat_password" name="repeat_password" data-validate-linked="password" required="required" type="password"  class="form-control" />							
										
									</div>
								</div>
							
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-vcard white"></span> Change Password</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
					</div>
eod;
        }
    }
    public function edit_password(){

        if($this->input->post("submit")){
            $id=$this->input->post("userId");
            $password = $this->input->post("password");
            $repeat_password = $this->input->post("repeat_password");
            if($password == $repeat_password){
                $password= $this->auth->hash_password($password);
                $data['password_hash']=$password['hash'];
                if ($this->user_accounts_model->update($id, $data))
                {
                    Template::set_message('The user password was successfully changed.', 'success');
                    redirect('',true);
                }
            }else{
                Template::set_message('Sorry the password was not changed.The passwords submitted did not match.', 'error');
                redirect('',true);
            }
        }else{
            $id = $this->input->get("ch2");
            $details = $this->user_accounts_model->as_object()->find($id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."setting/users/edit_password";
            echo <<<eod
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
								<h3>Change Your Password</h3>
							</div>
							<div class="modal-body">
							<h4 align="center">Change the password for  <u> $details->display_name </u></h4>
							<form class="form-horizontal" method="post" action="$url" role="form">
								<div class="row">
									<div class="form-group">
										<label class="col-lg-5 control-label" for="username">New Password:</label>
											<div class="col-lg-3">
												<input id="password" name="password" required="required" type="password"  class="form-control" />							
											<input type="hidden" name="userId" value="$id">								
											<input type="hidden" name="$security_name" value="$security_code" > 
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-5 control-label" for="username">Confirm Password:</label>
											<div class="col-lg-3">
												<input id="repeat_password" name="repeat_password" required="required" type="password"  class="form-control" />							
											
										</div>
									</div>
								
								<div class="modal-footer">						
									<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-vcard white"></span> Change Password</button>
									<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								</div>
								</form>
							</div>
						</div>
					</div>
eod;
        }
    }
    public function edit_user(){

        if($this->input->post("submit")){
            $id=$this->input->post("userId");

            $data = array(
                'display_name'=> $this->input->post('fullnames'),
                'email'=> $this->input->post('email'),
                'role_id'=> $this->input->post('role'),
                'phone'=> $this->input->post('phone')
            );
            if ($this->user_accounts_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Changed password user details for: User id ".$this->input->post('fullnames'), 'setting');
                Template::set_message('The user details were successfully edited.', 'alert fresh-color alert-success');
                redirect('setting/users/user_accounts',true);
            }else{
                Template::set_message('Error Saving!! A problem was encountered editing user details. Please check the values submitted.', 'alert fresh-color alert-danger');
                redirect('setting/users/user_accounts',true);
            }

        }else{

            $id = $this->input->get("ch2");
            $details = $this->user_accounts_model->get_user_details($id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $roles = $this->user_accounts_model->get_user_roles();
            $url = base_url()."setting/users/edit_user";
            echo <<<eod
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Edit User Infomation</h3>
					</div>
					<div class="modal-body">
					
					<form class="form-horizontal" method="post" action="$url" role="form">
						<div class="row">
							<div class="form-group">
								<label class="col-lg-5 control-label" for="fullnames">Full Names</label>
									<div class="col-lg-3">
										<input id="fullnames" name="fullnames" required="required" type="text" value="$details->display_name" class="form-control" />							
									<input type="hidden" name="userId" value="$id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="email">email</label>
									<div class="col-lg-3">
										<input id="email" name="email" required="required" type="email" value="$details->email" class="form-control" />							
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="phone">Phone Contact</label>
									<div class="col-lg-3">
										<input id="phone" name="phone" required="required"  type="number" step="0.01"  value="$details->phone" class="form-control" />									
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="phone">User Level</label>
									<div class="col-lg-3">
										<select name="role" required="required" id="role" class="form-control">
                                                <option  value="">Select User level</option>
eod;
                                                foreach($roles as $role){
                                                    $selected = ($role->role_id==$details->role_id)?"selected=\"selected\"":"";
                                                    echo '<option '.$selected.' value="'.$role->role_id.'">'.$role->role_name.'</option>';
                                                }
echo <<<eod
                                            </select>									
								</div>
							</div>
													
						<div class="modal-footer">						
							<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
							<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
						</form>
					</div>
				</div>
eod;
        }
    }
    public function edit_profile(){
        if($this->input->post("submit")){
            $id=$this->input->post("userId");
            $data = array(
                'display_name'=> $this->input->post('fullnames'),
                'email'=> $this->input->post('email'),
                'phone'=> $this->input->post('phone')
            );
            if ($this->user_accounts_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Changed password user details for: User id ".$this->input->post('fullnames'), 'setting');
                Template::set_message('The user details were successfully edited.', 'alert fresh-color alert-success');
                redirect('',true);
            }else{
                Template::set_message('Error Saving!! A problem was encountered editing user details. Please check the values submitted.', 'alert fresh-color alert-danger');
                redirect('',true);
            }
        }else{

            $id = $this->input->get("ch2");
            $details = $this->user_accounts_model->get_user_details($id);;
            //$departments = $this->user_accounts_model->get_departments();
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."setting/users/edit_profile";
            echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>Edit Your Profile</h3>
						</div>
						<div class="modal-body">
						
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<div class="form-group">
									<label class="col-lg-5 control-label" for="fullnames">Full Names</label>
										<div class="col-lg-3">
											<input id="fullnames" name="fullnames" required="required" type="text" value="$details->display_name" class="form-control" />							
										<input type="hidden" name="userId" value="$id">								
										<input type="hidden" name="$security_name" value="$security_code" > 
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-5 control-label" for="email">email</label>
										<div class="col-lg-3">
											<input id="email" name="email" required="required" type="email" value="$details->email" class="form-control" />							
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-5 control-label" for="phone">Phone Contact</label>
										<div class="col-lg-3">
											<input id="phone" name="phone" required="required"  type="number" step="0.01"  value="$details->phone" class="form-control" />									
									</div>
								</div>
								
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
        }
    }
    public function disable_user(){
        if($this->input->post("submit")){
            $id=$this->input->post("userId");
            //echo $this->input->post('todo');exit;
            $data = array(
                'banned' => $this->input->post('todo')
            );
            if ($this->user_accounts_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Banned user account for: User id ".$this->input->post('fullnames'), 'setting');
                Template::set_message('The user account status was succesfully changed.', 'alert fresh-color alert-success');
                redirect('setting/users/user_accounts',true);
            }else{
                Template::set_message('A problem was encountered changing the user account. Please try again.', 'alert fresh-color alert-danger');
                redirect('setting/users/user_accounts',true);
            }
        }else{
            $user_id = $this->input->get("ch2");
            $details = $this->user_accounts_model->as_object()->find($user_id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."setting/users/disable_user";
            if($this->input->get("ch")=="disable"){
                $header="Disable";
                $content="Note: When you disable a user they cannot be able to login or make any transactions. Their previous transactions remain in records though.";
                $form="<input type=\"hidden\" name=\"todo\" value=\"1\">";
            }elseif($this->input->get("ch")=="enable"){
                $header="Activate";
                $content="Note: Activating a user allows them to log in and make transactions. Previous records are still kept for reporting";
                $form="<input type=\"hidden\" name=\"todo\" value=\"0\">";
            }
            echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>$header User</h3>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<h3 align="center">Are you sure you want to <u> $header </u>the user <u> $details->display_name </u></h3>
								<h5 align="center">$content</h5>
								<div class="form-group">							
								<div class="col-lg-3">	
									$form;
									<input type="hidden" name="userId" value="$details->id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							</div>
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-users-2 white"></span> $header User</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
        }
    }


}