<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Units_model extends MY_Model {
	protected $table_name = 'vision_retail_units';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


    public function get_active_measurement_units()
    {
        return $this->db->query("SELECT * FROM bf_vision_retail_units WHERE status=1")->result();
    }
    public function get_product_categories()
    {
        return $this->db->query("SELECT * FROM bf_vision_product_categories WHERE status=1")->result();
    }
	
}