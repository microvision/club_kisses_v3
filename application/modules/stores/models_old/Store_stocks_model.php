<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_stocks_model extends MY_Model {
	protected $table_name = 'stores_stocks';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';


	public function get_received_items($id)
    {
        return $this->db->query("SELECT bf_vision_stock_store.*,unit_name,product_name,description FROM bf_vision_stock_store
									LEFT JOIN (SELECT bf_vision_products.*,unit_name FROM bf_vision_products
												LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=unit_id) as products ON products.id=bf_vision_stock_store.product_id
									WHERE supply_order_id='".$id."'")->result();
    }
    public function get_store_stocks()
    {
        return $this->db->query("SELECT SUM(current_qty) as current_qty,product_id,item,items.description,unit_name,store_name FROM  bf_stores_stocks                                    
									LEFT JOIN (SELECT bf_stores_inventory.*,unit_name FROM bf_stores_inventory 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=measurement_id) as items ON items.id=product_id
									LEFT JOIN bf_stores ON bf_stores.id=items.store_id
											  GROUP BY  bf_stores_stocks.product_id")->result();
    }
    public function get_product_store_stocks($item_id)
    {
        return $this->db->query("SELECT * FROM  bf_stores_stocks WHERE product_id='".$item_id."' LIMIT 7")->result();
    }
	public function get_total_store_stock($itemid)
    {
        return $this->db->query("SELECT SUM(current_qty)as stock FROM bf_stores_stocks
									WHERE product_id='".$itemid."' AND status=1")->row();
    }
	public function get_item_store_stock($itemid)
    {
        return $this->db->query("SELECT MIN(id)as miId, bf_stores_stocks.* FROM bf_stores_stocks
									WHERE product_id='".$itemid."' AND status=1 AND current_qty>0")->row();
    }

}