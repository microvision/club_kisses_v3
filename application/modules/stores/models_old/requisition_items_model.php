<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requisition_items_model extends MY_Model {
	protected $table_name = 'stores_stocks';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_ordered_items($id)
    {
        return $this->db->query("SELECT bf_stores_stocks.*,unit_name as measurement_unit,item,items.store_id,description,current_stock,store_name FROM bf_stores_stocks
                                    LEFT JOIN (SELECT SUM(current_qty)as current_stock,product_id FROM bf_stores_stocks WHERE status=1 GROUP BY product_id)as s ON s.product_id=bf_stores_stocks.product_id
									LEFT JOIN (SELECT bf_stores_inventory.*,unit_name,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=bf_stores_inventory.store_id) as items ON items.id=bf_stores_stocks.product_id
									WHERE order_id='".$id."'")->result();
    }
    public function get_ordered_item($product_id)
    {
        return $this->db->query("SELECT bf_stores_inventory.*,unit_name,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=bf_stores_inventory.store_id
													WHERE bf_stores_inventory.id='".$product_id."'")->row();
    }
}