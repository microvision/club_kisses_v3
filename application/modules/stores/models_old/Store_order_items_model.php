<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_order_items_model extends MY_Model {
	protected $table_name = 'stores_order_items';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_ordered_items($id)
    {
        return $this->db->query("SELECT bf_stores_order_items.*,unit_name,item,description FROM bf_stores_order_items
									LEFT JOIN (SELECT bf_stores_inventory.*,unit_name FROM bf_stores_inventory 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=measurement_id) as items ON items.id=bf_stores_order_items.item_id
									WHERE order_id='".$id."'")->result();
    }
    public function get_user_orders($user_id)
    {
        return $this->db->query("SELECT bf_stores_orders.*,display_name FROM bf_stores_orders 
									LEFT JOIN bf_users ON bf_users.id = order_by
									WHERE order_by='".$user_id."' ORDER BY id DESC LIMIT 20")->result();
    }
    public function get_store_order_item($id)
    {
        return $this->db->query("SELECT bf_stores_order_items.*,item,unit_name,description FROM bf_stores_order_items
									LEFT JOIN(SELECT bf_stores_inventory.*,unit_name FROM bf_stores_inventory 
													LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=measurement_id) as items ON items.id=bf_stores_order_items.item_id
									WHERE bf_stores_order_items.id='".$id."'")->row();
    }
    public function delete_item($id)
    {
        $this->db->query("DELETE FROM bf_stores_order_items WHERE id='".$id."'");
    }
}