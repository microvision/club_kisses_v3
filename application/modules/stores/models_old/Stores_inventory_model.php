<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stores_inventory_model extends MY_Model {
	protected $table_name = 'stores_inventory';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = true;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_search_products($product)
    {
        return $this->db->query("SELECT bf_stores_inventory.*,unit_name as measurement_unit,store_name FROM bf_stores_inventory  
									LEFT JOIN bf_vision_retail_units ON bf_vision_retail_units.id=measurement_id
									LEFT JOIN bf_stores ON bf_stores.id=store_id									
									WHERE (item LIKE '%".$product."%') and bf_stores_inventory.status=1 ORDER BY item")->result();
    }
	
}