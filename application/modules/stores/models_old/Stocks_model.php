<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stocks_model extends MY_Model {
	protected $table_name = 'stores_stocks';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_item_store_stock($itemid)
    {
        return $this->db->query("SELECT MIN(id)as miId, bf_stores_stocks.* FROM bf_stores_stocks
									WHERE product_id='".$itemid."' AND status=1 AND current_qty>0")->row();
    }
    public function get_total_store_stock($itemid)
    {
        return $this->db->query("SELECT SUM(current_qty)as stock FROM bf_stores_stocks
									WHERE product_id='".$itemid."' AND status=1")->row();
    }
    public function get_store_stocks()
    {
        return $this->db->query("SELECT SUM(current_qty) as current_qty,product_id,item,description,measurement_unit,store_name FROM  bf_stores_stocks
									LEFT JOIN (SELECT bf_stores_inventory.*,measurement_unit,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_stores_measurement_units ON bf_stores_measurement_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=store_id) as items ON items.id=bf_stores_stocks.product_id
									GROUP BY  bf_stores_stocks.product_id")->result();
    }
    public function get_store_stocks2($store_id)
    {
        return $this->db->query("SELECT SUM(current_qty) as current_qty,product_id,item,description,measurement_unit,store_name,store_id FROM  bf_stores_stocks
									LEFT JOIN (SELECT bf_stores_inventory.*,measurement_unit,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_stores_measurement_units ON bf_stores_measurement_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=store_id) as items ON items.id=bf_stores_stocks.product_id
									WHERE store_id='".$store_id."' GROUP BY  bf_stores_stocks.product_id")->result();
    }
    public function get_product_store_stocks($item_id)
    {
        return $this->db->query("SELECT * FROM bf_stores_stocks WHERE product_id='".$item_id."' and status=1")->result();
    }

	
}