<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requisitions_model extends MY_Model {
	protected $table_name = 'vision_supply_orders';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_orders()
    {
        return $this->db->query("SELECT bf_vision_supply_orders.*,display_name,bf_vision_suppliers.name as supplier_name,bf_vision_suppliers.phone as phone,bf_vision_suppliers.address as address,bf_vision_suppliers.email as email FROM bf_vision_supply_orders 
									LEFT JOIN bf_users ON bf_users.id = created_by
									LEFT JOIN bf_vision_suppliers ON bf_vision_suppliers.id=supplier_id
									WHERE bf_vision_supply_orders.status=1 or bf_vision_supply_orders.status=3
									ORDER BY id DESC LIMIT 20")->result();
    }
    public function get_order($id)
    {
        return $this->db->query("SELECT bf_vision_supply_orders.*,display_name,bf_vision_suppliers.name as supplier_name,bf_vision_suppliers.phone as phone,bf_vision_suppliers.address as address,bf_vision_suppliers.email as email FROM bf_vision_supply_orders 
									LEFT JOIN bf_users ON bf_users.id = created_by
									LEFT JOIN bf_vision_suppliers ON bf_vision_suppliers.id=supplier_id
									WHERE bf_vision_supply_orders.id='".$id."'")->row();
    }
   
}