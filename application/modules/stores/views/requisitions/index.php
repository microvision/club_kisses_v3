<div class="header_bg">

    <div class="header">
        <div class="head-t">
            <div class="logo">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png'); ?>"
                                                         class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">
                <div class="col-lg-3">

                    <div class="block">
                        <a href="<?php echo base_url() . "stores/requisitions/create_purchase_order"; ?>"
                           class="btn btn-danger btn-block"><span class="fa fa-inbox"></span>New Purchase
                            Requisition</a>
                    </div>
                    <div class="block">
                        <div class="list-group border-bottom">
                            <a href="#" class="list-group-item active"> Purchases Requisitions </a>
                            <?php foreach ($orders as $order): ?>
                                <a href="<?php echo base_url() . "stores/requisitions/index/" . $order->id; ?>"
                                   class="list-group-item"><span
                                            class="fa fa-caret-right">&nbsp;<?php echo $order->id; ?></span> <?php echo $order->display_name; ?>
                                    <span class="badge badge-default"><?php echo date('d-M-Y', strtotime($order->order_date)); ?></span></a>
                            <?php endforeach; ?>

                        </div>
                    </div>

                </div>
                <div class="col-lg-9">

                    <?php if (isset($order_details)): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="h3 text-info">Purchase Requisition <?php echo $order_details->id; ?></span>
                                <div class="pull-right" style="width: 250px;">
                                    <?php if ($order_details->status == 1): ?>
                                        <div class="input-group pull-right">
                                            <button type="button" class="btn btn-info pull-right" data-toggle="modal"
                                                    data-target=".new_items"><span class="fa fa-plus"></span> Add Items
                                                to Order
                                            </button>
                                        </div>
                                    <?php elseif ($order_details->status == 2): ?>
                                        <span class="h4 text-warning pull-right">Requisition Submitted</span>
                                    <?php elseif ($order_details->status == 3): ?>
                                        <span class="h4 text-success pull-right">Received on <?php date('d-M-Y', strtotime($order_details->modified_on)); ?></span>
                                    <?php endif; ?>

                                </div>
                            </div>
                            <div class="panel-body mail">
                                <table class="table table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th width="50%">Description</th>
                                        <th>Ordered Qty</th>
                                        <th width="5%">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($received_items): foreach ($received_items as $row): ?>
                                        <tr>
                                            <td><?php echo $row->item; ?></td>
                                            <td><?php echo $row->description; ?></td>
                                            <td><?php echo $row->received_qty . " " . $row->measurement_unit; ?></td>
                                            <td><?php if ($order_details->status == 1): ?><a
                                                    href="<?php echo base_url() . "stores/requisitions/delete_order_item/" . $row->id . "?order=" . $order_details->id; ?>"
                                                    class="btn btn-default"><span class="fa fa-trash-o"></span>
                                                    </a><?php endif; ?></td>
                                        </tr>

                                    <?php endforeach; endif; ?>
                                    </tbody>
                                </table>

                            </div>
                            <div class="panel-footer">
                                <a href="" class="btn btn-default"><span class="fa fa-edit"></span></a>

                                <ul class="pagination pagination-sm pull-right">
                                    <?php if($order_details->status==1): ?>
                                        <!--                                            <li class="active"><a href="--><?php //echo base_url();?><!--stores/requisitions/submit_requisition/--><?php //echo $order_details->id; ?><!--" class="btn btn-warning btn-lg">Submit Requisition</a></li>-->
                                        <li class="active"><a href="<?php echo base_url();?>stores/requisitions/receive_requisition/<?php echo $order_details->id; ?>" class="btn btn-warning btn-lg">Submit Requisition</a></li>
                                    <?php elseif($order_details->status==2): ?>
                                        <a href="<?php echo base_url();?>stores/requisitions/receive_requisition/<?php echo $order_details->id; ?>" class="btn btn-info btn-lg">Receive Items Stocks</a>
                                    <?php endif;?>
                                </ul>
                            </div>

                        </div>
                    <?php endif; ?>


                </div>
            </div>

        </div>
        <div id="txtResult" class="modal fade"></div>
        <!-- end content -->

    </div>


    <!-- Modal for receiving item -->

    <div class="modal fade new_items" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="post" id="new_prescription" name="new_prescription" class="form-horizontal" role="form"
                      action="<?php echo base_url(); ?>stores/requisitions/receive_purchase_item" accept-charset="utf-8"
                      autocomplete="off">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add Item</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                               value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" value="<?php echo $order_details->id; ?>" name="order_id">
                        <input type="hidden" value="1" name="payment_method">
                        <div class="row">
                            <div class="block">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Search Item</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                            <input type="text" class="form-control" id="box1Filter" name="search"
                                                   placeholder="Search.." required="required"
                                                   onkeypress="htmlData2('<?php echo base_url(); ?>stores/orders/get_store_search_items', 'ch='+this.value)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="txtResult2"></div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <input type="submit" name="submit" class="btn btn-info" value="Add Item">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>