<div class="header_bg">

    <div class="header">
        <div class="head-t">
            <div class="logo">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png'); ?>"
                                                         class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">
                <div class="col-lg-4">
                    <div class="block">
                        <form id="form_stores" name="form_stores" method="post"
                              action="<?php echo base_url(); ?>stores/create_store">
                            <h4>Create Store</h4>

                            <div class="form-group">
                                <label>Store Name</label>
                                <input type="text" class="form-control" required="required" name="store_name">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                       value="<?php echo $this->security->get_csrf_hash(); ?>">
                            </div>
                            <div class="form-group">
                                <label class="">Description</label>
                                <textarea class="form-control" name="description" required="required"
                                          id="description"></textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary pull-right" name="submit"
                                       value="Create Store">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-8">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Store</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if ($stores): $num = 0;
                            foreach ($stores as $rows): $num++; ?>
                                <tr>
                                    <td width="2%"><?php echo $num; ?></td>
                                    <td><?php echo $rows->store_name; ?></td>
                                    <td>
                                        <?php echo $rows->description; ?>
                                    </td>
                                    <td><?php echo ($rows->status == 1) ? "Active" : "<span class='text-danger'>Disabled</span>"; ?></td>
                                    <td width="10%">
                                        <?php if ($rows->status == 1): ?>
                                            <a href="#txtResult" data-toggle="modal" title="Edit Store"
                                               class="btn btn-default btn-xs"
                                               onclick="htmlData('<?php echo base_url(); ?>stores/edit_store', 'ch=<?php echo $rows->id; ?>')"><i
                                                        class="fa fa-edit"></i></a>
                                            <a href="#txtResult" data-toggle="modal" title="Deactivate Store"
                                               class="btn btn-default btn-xs"
                                               onclick="htmlData('<?php echo base_url(); ?>stores/disable_store', 'ch=<?php echo $rows->id; ?>&ch2=disable')"><i
                                                        class="fa fa-lock"></i></a>
                                        <?php else: ?>
                                            <a href="#txtResult" data-toggle="modal" title="Activate Store"
                                               class="btn btn-danger btn-xs"
                                               onclick="htmlData('<?php echo base_url(); ?>stores/disable_store', 'ch=<?php echo $rows->id; ?>&ch2=enable')"><i
                                                        class="fa fa-unlock-alt"></i></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                            <h4>There are no stores created!!</h4>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>

        <div class="women_main">

        </div>
        <div class="catalog">
            <h3 class="panel-title">Measurement Units</h3>
            <div class="row">
                <div class="col-lg-4">
                    <div class="block">
                        <form id="form_stores" name="form_units" method="post"
                              action="<?php echo base_url(); ?>stores/create_measurement_unit">
                            <h4>Create Unit</h4>

                            <div class="form-group">
                                <label>Unit Measurement</label>
                                <input type="text" class="form-control" required="required" name="measurement_unit">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                       value="<?php echo $this->security->get_csrf_hash(); ?>">
                            </div>


                            <div class="form-group">
                                <input type="submit" class="btn btn-primary pull-right" name="submit"
                                       value="Create Unit Measurement">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-8">
                    <table class="table datatable table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Measurement Unit</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if ($units): $num = 0;
                            foreach ($units as $rows): $num++; ?>
                                <tr>
                                    <td><?php echo $num; ?></td>
                                    <td><?php echo $rows->measurement_unit; ?></td>

                                    <td><?php echo ($rows->status == 1) ? "Active" : "<span class='text-danger'>Disabled</span>"; ?></td>
                                    <td>
                                        <?php if ($rows->status == 1): ?>
                                            <a href="#txtResult" data-toggle="modal" title="Edit Measurement Unit"
                                               class="btn btn-link btn-xs"
                                               onclick="htmlData('<?php echo base_url(); ?>stores/edit_measurement_unit', 'ch=<?php echo $rows->id; ?>')"><i
                                                        class="fa fa-edit"></i></a>
                                            <a href="#txtResult" data-toggle="modal"
                                               title="Deactivate  Measurement Unit" class="btn btn-link btn-xs"
                                               onclick="htmlData('<?php echo base_url(); ?>stores/disable_measurement_unit', 'ch=<?php echo $rows->id; ?>&ch2=disable')"><i
                                                        class="fa fa-lock"></i></a>
                                        <?php else: ?>
                                            <a href="#txtResult" data-toggle="modal" title="Activate  Measurement Unit"
                                               class="btn btn-link btn-xs"
                                               onclick="htmlData('<?php echo base_url(); ?>stores/disable_measurement_unit', 'ch=<?php echo $rows->id; ?>&ch2=enable')"><i
                                                        class="fa fa-unlock-alt"></i></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; else: ?>
                            <h4>There are no Measurement Units Created!!</h4>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div id="txtResult" class="modal fade"></div>
        <!-- end content -->

    </div>


