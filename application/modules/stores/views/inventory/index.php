
<div class="header_bg">

    <div class="header">
        <div class="head-t">
            <div class="logo">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">
                <div class="col-lg-3">
                    <div class="block">
                        <button class="btn btn-danger btn-block" data-toggle="modal" data-target=".new_item"><span class="fa fa-plus"></span> Create New Item</button>
                    </div>
                    <div class="block">
                        <div class="list-group border-bottom">

                            <?php $classe = array("danger","primary", "success", "info", "warning","default","danger"); $num=0; foreach($stores as $row):  ?>
                                <a href="<?php echo base_url()."stores/inventory/index/".$row->id;?>" class="list-group-item <?php echo ((ISSET($store))and($row->id==$store->id))?"active":""; ?>">
                                    <span class="fa fa-circle text-<?php echo $classe[$num]; ?>"></span>
                                    <?php echo $row->store_name; ?>

                                </a>
                                <?php $num++; $num=($num==8)?0:$num; endforeach; ?>
                            <a href="<?php echo base_url()."stores/inventory/index/";?>" class="list-group-item <?php echo (!ISSET($store))?"active":""; ?>">
                                <span class="fa fa-circle-o text-white"></span>
                                <b class="text-default">Show All Items</b>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="h3 text-info"><?php echo (ISSET($store))?$store->store_name." Items":"All Store Items"; ?></span>
                        </div>
                        <div class="panel-body mail">
                            <p><table class="table datatable table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Unit Measurement</th>
                                    <th>Store</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($items): $num=0; foreach($items as $row): $num++; ?>
                                    <tr>
                                        <td><?php echo $num; ?></td>
                                        <td width="30%"><?php echo $row->item; ?><?php echo ($row->description)?"<br><small>".$row->description."</small>":""; ?></td>
                                        <td><?php echo $row->measurement_unit; ?></td>
                                        <td><?php echo $row->store_name; ?></td>
                                        <td><?php echo ($row->status==1)?"Active":"<span class='text-danger'>Disabled</span>"; ?></td>
                                        <td>
                                            <?php if($row->status==1): ?>
                                                <a href="#txtResult" data-toggle="modal" title="Edit Item" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>stores/inventory/edit_item', 'ch=<?php echo $row->id?>')"><i class="fa fa-edit"></i></a>
                                                <a href="#txtResult" data-toggle="modal" title="Deactivate Item" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>stores/inventory/disable_item', 'ch=<?php echo $row->id?>&ch2=disable')"><i class="fa fa-lock"></i></a>
                                            <?php else: ?>
                                                <a href="#txtResult" data-toggle="modal" title="Activate Item" class="btn btn-link btn-xs" onclick="htmlData('<?php echo base_url();?>stores/inventory/disable_item', 'ch=<?php echo $row->id?>&ch2=enable')"><i class="fa fa-unlock"></i></a>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach; endif; ?>
                                </tbody>
                            </table>

                        </div>
                        <div class="panel-footer">

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->

    </div>


				<!-- Modal for receiving item --> 
							
								<div class="modal fade new_item" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
										<form method="post" id="new_item" name="new_item" class="form-horizontal" role="form" action="<?php echo base_url()."stores/inventory/create_item"; ?>" accept-charset="utf-8" autocomplete="off">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">New Item</h4>
                                            </div>
                                            <div class="modal-body">                                                
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >

												<div class="row">
													<div class="block">
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Store Allocation</label>
                                                            <div class="form-group col-md-5">
                                                                <select name="store" class="form-control"  required="required" id="store">
                                                                    <option value="" selected disabled>Select Store</option>
                                                                    <?php foreach($stores as $store): ?>
                                                                        <option value="<?php echo $store->id; ?>"><?php echo $store->store_name; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Item name</label>
                                                            <div class="form-group col-md-5">
                                                                <input type="text" name="item_name" class="form-control" required="required" id="item_name">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Description</label>
                                                            <div class="form-group col-md-5">
                                                                <textarea name="description" class="form-control" id="description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Measurement Unit</label>
                                                            <div class="form-group col-md-5">
                                                                <select name="units" class="form-control"  required="required" id="units">
                                                                    <option value="" selected disabled>Select Measurement Unit</option>
                                                                    <?php foreach($units as $unit): ?>
                                                                        <option value="<?php echo $unit->id; ?>"><?php echo $unit->unit_name; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
													</div>
												</div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <input type="submit" name="submit"  class="btn btn-primary" value="Create Item">
                                            </div>
											</div>
										</form>
                                        </div>
                                    </div>
                                </div>