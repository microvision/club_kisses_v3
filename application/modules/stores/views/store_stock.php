
<div class="header_bg">

    <div class="header">
        <div class="head-t">
            <div class="logo">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>
            <!-- start header_right -->
            <div class="header_right">
                <h3>Stores Stocks List <a href="<?php echo base_url(); ?>stocks/shop_stock" class="btn btn-primary pull-right">View Uniform Stores</a></h3>
                <div class="search">

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <button id="btnExport" class="btn btn-info btn-sm pull-right"><i class="fa fa-download"></i> Export Report to Excel</button><br><hr>
        <div class="catalog" id="table_wrapper">
            <table class="table datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Store</th>
                    <th>Current Stock</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $num=0; foreach($items as $row): $num++;?>
                    <tr>
                        <td><?php echo $num; ?></td>
                        <td><?php echo $row->item; ?></td>
                        <td align="left" width="25%"><?php echo $row->description; ?></td>
                        <td align="left"><?php echo $row->store_name; ?></td>
                        <td align="left"><b><?php echo number_format($row->current_qty)." ".$row->measurement_unit; ?> </b></td>

                        <td>
                            <button class="btn btn-info btn-xs" data-toggle="modal" href="#txtResult" title="edit_stock" onclick="htmlData('<?php echo base_url();?>stores/edit_store_stock', 'ch2=<?php echo $row->product_id; ?>')"><i class="fa fa-edit"></i> edit</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
        <div id="txtResult" class="modal fade"> </div>
        <!-- end content -->

    </div>
