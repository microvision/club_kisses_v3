<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <div class="row">
            <!--Start of orders list -->
            <div class="col-lg-3 col-sm-3">
                <div class="block">
                    <a href="<?php echo base_url()."stores/purchase_orders/create_purchase_order"; ?>" class="btn btn-danger btn-block "><span class="fa fa-inbox"></span> Receive New Stock</a>
                </div>
                <div class="block">
                    <div class="list-group border-bottom">
                        <h5> Received Stocks </h5>
                        <table class="table table-condensed">
                        <?php foreach($orders as $order): ?>
                            <tr onclick="window.location='<?php echo base_url()."stores/purchase_orders/index/".$order->id;?>';" <?php echo((ISSET($order_details)) and ($order->id==$order_details->id))?'class="active"':''; ?>>
                                <td><span class="h6"><span <?php echo((ISSET($order_details))and($order->id==$order_details->id))?'class="fa fa-caret-right"':'class="fa fa-angle-right"'; ?> >&nbsp;<?php echo $order->id; ?></span></td>
                                <td><span class="h6"><?php echo ($order->supplier_name)?$order->supplier_name:'<span class="text-danger">Currently Receiving</span>'; ?></span></td>
                                <td width="35%"><span class="h6"><?php echo date('d-M-Y', strtotime($order->delivery_date)); ?></span></td>

                            </tr>
                        <?php endforeach; ?>
                        </table>
                    </div>
                </div>

            </div>
            <!-- End of orders list -->
            <!-- START CONTENT FRAME BODY -->
            <div class="col-lg-9 col-sm-9">
                <?php if(ISSET($order_details)): ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="h5 text-white">Receiving Stock # <?php echo $order_details->id; ?></span>
                            <div class="pull-right" style="width: 250px;">
                                <?php if($order_details->status==0): ?>
                                    <div class="input-group pull-right">
                                        <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target=".new_items"> <span class="fa fa-plus"></span> Product Stock</button>
                                    </div>
                                <?php elseif($order_details->status==2): ?>
                                    <span class="h4 text-warning pull-right">Requisition Submitted</span>
                                <?php elseif($order_details->status==1): ?>
                                    <span class="h5 text-danger pull-right">Received on <?php echo date('d/M/Y', strtotime($order->modified_on))." | <i class='fa fa-user'></i> ".$order_details->display_name; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="panel-body mail">
                            <form class="form-vertical" id="receive_stock" name="receive_stock" method="post" action="">
                                <div class="row">
                                    <div class="col-lg-5 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>

                                                <div class="input-group">
                                                    <select class="form-control" id="supplier" required="required" name="supplier" onchange="htmlData4('<?php echo base_url(); ?>stores/purchase_orders/update_order_fields', 'val=supplier&order_id=<?php e($order_details->id);?>&supplier_id='+this.value)">
                                                        <option value="" selected disabled>Select Supplier</option>
                                                        <?php foreach($suppliers as $row):?>
                                                        <option value="<?php e($row->id); ?>"><?php e($row->name); ?> </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="input-group-btn">
												        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".new_supplier">New Supplier</button>
                                                    </span>
                                                </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-sm-6" id="txtResult4">
                                        <br>
                                        <span class="h4 text-danger"><?php echo($order_details->supplier_name)?$order_details->supplier_name:""; ?><br></span>
                                        <span class="h6"><i class="fa fa-phone"></i> <?php echo($order_details->phone)?$order_details->phone:""; ?> | <i class="fa fa-envelope"></i> <?php echo($order_details->email)?$order_details->email:""; ?>  | <i class="fa fa-home"></i> <?php echo($order_details->address)?$order_details->address:""; ?></span>

                                    </div>
                                </div><br><br>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Invoice </label>
                                            <input type="text" class="form-control" required="" placeholder="Invoice Number" value="<?php echo($order_details->invoice_number)?$order_details->invoice_number:""; ?>" name="invoice" id="req" onfocusout="htmlData5('<?php echo base_url(); ?>stores/purchase_orders/update_order_fields', 'val=invoice&order_id=<?php e($order_details->id);?>&invoice='+this.value)">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-4">

                                    </div>
                                    <div class="col-lg-3 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Delivery Date</label>
                                            <input type="date" class="form-control" required="" value="<?php echo($order_details->delivery_date)?date('Y-m-d',strtotime($order_details->delivery_date)):""; ?>" name="delivery_date" id="req" onfocusout="htmlData5('<?php echo base_url(); ?>stores/purchase_orders/update_order_fields', 'val=delivery_date&order_id=<?php e($order_details->id);?>&delivery_date='+this.value)">
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <table class="table table-responsive ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product</th>
                                            <th>Received Qty</th>
                                            <th><span class="pull-right">Total Cost</span></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($received_items): $num=0; foreach($received_items as $row): $num++; ?>
                                        <tr>
                                            <td width="2%"><?php echo $num;?></td>
                                            <td width="50%"><?php echo $row->product_name; echo ($row->description)?" <br> <mute><span class='h6'>".$row->description."</span></mute>":"";?></td>
                                            <td><?php echo $row->purchased_qty." ".$row->unit_name; ?></td>
                                            <td align="right">Ksh <?php echo number_format($row->buying_price*$row->purchased_qty,2); ?></td>
                                            <td width="7%" align="right">
                                                <?php if($order_details->status==0): ?>
                                                <a  class="btn btn-danger btn-xs" title="Delete Stock" data-toggle="modal" href="<?php echo base_url(); ?>stores/purchase_orders/delete_stock_order_item?order_item_id=<?php echo $row->id; ?>&order_id=<?php echo $order_details->id; ?>" ><i class="fa fa-times"></i></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; endif; ?>
                                    </tbody>
                                </table>



                            </div>
                            <div class="panel-footer">
                                <?php if($order_details->status==0): ?>
                                    <button class="btn btn-default" title="Cancel Order"><span class="fa fa-trash-o"></span></button>
                                    <a href="<?php echo base_url();?>stores/purchase_orders/submit_requisition/<?php echo $order_details->id; ?>" class="btn btn-primary pull-right">Submit Products</a>
                                <?php endif;?>

                            </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
            <!-- END CONTENT FRAME BODY -->
        <!-- MODAL FOR RECEIVING PRODUCT STOCK-->
                <div class="modal fade new_items" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form method="post" id="new_prescription" name="new_prescription" class="form-horizontal" role="form" action="<?php echo base_url(); ?>stores/purchase_orders/receive_purchase_item" accept-charset="utf-8" autocomplete="off">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Receive Product Stock</h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                    <input type="hidden" value="<?php echo $order_details->id;?>" name="order_id">
                                    <input type="hidden" value="1" name="payment_method">
                                    <div class="row">
                                        <div class="block">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Search Product</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                                        <input type="text" class="form-control" id="box1Filter" name="search" placeholder="Search.." required="required" onkeypress="htmlData2('<?php echo base_url();?>stores/purchase_orders/get_store_search_items', 'ch='+this.value)">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="txtResult2"> </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <input type="submit" name="submit"  class="btn btn-danger" value="Receive Product">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        <!-- END MODAL FOR RECEIVING PRODUCT STOCK-->
        <!-- MODAL FOR CREATING SUPPLIER-->
                <div class="modal fade new_supplier" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="wizard" class="form-horizontal" role="form" action="<?php echo base_url(); ?>stores/purchase_orders/new_supplier" accept-charset="utf-8" autocomplete="off">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">New Supplier Account</h4>
                        </div>
                        <div class="modal-body">


                            <div class="wizard-steps clearfix"></div>
                            <div class="step" id="account-details"><br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Company Name</label>
                                    <div class="col-lg-7">
                                        <input id="name" name="name" required="required" type="text" class="form-control col-md-7 col-xs-12" />
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                        <input type="hidden" value="<?php echo $order_details->id;?>" name="order_id">
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Phone</label>
                                    <div class="col-lg-7">
                                        <input id="phone" required="required" name="phone" type="text" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Email</label>
                                    <div class="col-lg-7">
                                        <input id="email"  name="email" type="email" class="form-control col-md-7 col-xs-12" />
                                    </div>
                                </div><!-- End .form-group  -->
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="username">Address</label>
                                    <div class="col-lg-7">
                                        <textarea id="autosize" name="address" class="form-control col-md-7 col-xs-12"></textarea>
                                    </div>
                                </div><!-- End .form-group  -->

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" name="submit"  class="btn btn-primary" value="Create Supplier">
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL FOR CREATING SUPPLIER-->
        </div>
    </div>
</div>