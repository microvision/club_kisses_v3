<!-- PAGE CONTENT WRAPPER -->

<div class="content-frame">
    <!-- START CONTENT FRAME TOP -->
    <div class="content-frame-top">
        <div class="page-title">
            <h2><span class="fa fa-inbox"></span> Dispense Store Orders</small></h2>

        </div>
        <div class="btn-group btn-sm pull-right">
            <a href="#" data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">Filter Orders <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url()."stores/orders/dispense_store_orders?filter=2"; ?>"><span class="fa fa-certificate"></span> Approved Orders</a></li>
                <li><a href="<?php echo base_url()."stores/orders/dispense_store_orders?filter=3"; ?>"><span class="fa fa-thumbs-up" ></span> Dispensed Orders</a></li>
                <li><a href="<?php echo base_url()."stores/orders/dispense_store_orders?filter=0"; ?>"><span class="fa fa-times"></span> Cancelled Orders</a></li>
            </ul>
        </div>
    </div>
    <div class="header_bg">

        <div class="header">
            <div class="head-t">
                <div class="logo">
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>

    </div>
    <div class="content">
        <div class="women_main">
            <!-- start content -->
            <div class="catalog">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="block">

                            <div class="list-group border-bottom">
                                <a href="#" class="list-group-item active"> Approved Orders </a>
                                <?php foreach($orders as $order):?>
                                    <a href="<?php echo base_url()."stores/orders/dispense_store_orders/".$order->id;?>" class="list-group-item">
                                        <?php if($order->status==2): ?><span class="fa fa-certificate" title="Approved"><?php elseif($order->status==3): ?> <span class="fa fa-thumbs-up" title="Dispensed"> <?php else:?> <span class="fa fa-times"> <?php endif; ?>
											&nbsp;<?php echo $order->id; ?>
										</span>
										<?php echo $order->display_name; ?> <br>
										<?php echo "<span class=\"text-warning\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$order->department." </span>";?>
										<span class="badge badge-default"><?php echo date('d-M-Y', strtotime($order->order_date)); ?></span>
                                    </a>
                                <?php endforeach; ?>

                            </div>
                        </div>

                    </div>

                    <div class="col-lg-9">
                        <?php if(ISSET($order_details)): ?>
                            <div class="panel panel-default">
                                <form id="order_dispense_form" name="order_dispense_form" method="post" action="<?php echo base_url();?>stores/orders/dispense_store_orders">
                                    <div class="panel-heading">
                                        <span class="h3 text-warning">Order <?php echo $order_details->id; ?></span>&nbsp;&nbsp;
                                        <small class="text-muted">
                                            | Order Date :<?php echo date('d-M-Y', strtotime($order->order_date)); ?>
                                            &nbsp; <b><?php echo "<span class=\"text-warning\">| Department: ".$order->department."</span>";?></b>
                                        </small>
                                        <div class="pull-right" style="width: 300px;">
                                            <div class="input-group pull-right">
                                                <?php if($order_details->status==2): ?>
                                                    <span class="h3 text-warning ">Ordered By: <?php echo $order_details->display_name; ?></span>
                                                <?php elseif($order_details->status==0): ?>
                                                    <span class="h3 text-danger"><span class="fa fa-times"></span>Order Cancelled</span>
                                                <?php elseif($order_details->status==3): ?>
                                                    <span class="h3 text-success"><span class="fa fa-check"></span>Order Dispensed</span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body mail">
                                        <?php if($received_items): foreach($received_items as $row):?>
                                            <div class="mail-item mail-warning">
                                                <div class="mail-checkbox">
                                                    <span class="fa fa-caret-right"></span>
                                                </div>
                                                <div class="mail-user"><?php echo $row->item." <span class='text-info'>(".$row->store_name.")</span>"; echo ($row->description)?" <br> <mute><small>".$row->description."</small></mute>":"";?></div>
                                                <a href="pages-mailbox-message.html" class="mail-text">&nbsp;<span class="text-muted">Approved Qty |</span> <?php echo number_format($row->approved_qty);  echo ($row->comments)?" <br> <mute><small>".$row->comments."</small></mute>":"";?> </a>

                                                <div class="mail-attachments pull-right">
                                                    <input type="number"  name="txt_<?php echo $row->id; ?>" readonly="readonly" value="<?php echo ($row->approved_qty)?$row->approved_qty:$row->received_qty; ?>" class="form-control" required="required">
                                                </div>
                                                <a href="pages-mailbox-message.html" class="mail-text pull-right">&nbsp;<span class="small text-warning"><?php echo ($row->current_stock)?number_format($row->current_stock)." ".$row->measurement_unit:"0 ".$row->measurement_unit; ?> Available </span>&nbsp;&nbsp; </a>
                                            </div>
                                        <?php endforeach; endif; ?>
                                        <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_details->id; ?>">
                                    </div>
                                    <div class="panel-footer">
                                        <?php if($order_details->status==2): ?>
                                            <button class="btn btn-primary btn-lg pull-left" >Purchase Order</button>
                                        <?php endif;?>
                                        <ul class="pagination pagination-sm pull-right">
                                            <?php if($order_details->status==2): ?>

                                                <li class="active">

                                                    <input type="submit" class="btn btn-warning btn-lg" name="submit" value="Dispense Order">
                                                </li>
                                                <?php //elseif($order_details->status==3): ?>
                                                <!--<li class="active"><input type="submit" class="btn btn-primary btn-lg" name="submit" value="Save Changes"></li> -->
                                            <?php endif;?>
                                        </ul>
                                    </div>
                                </form>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- END CONTENT FRAME BODY -->
</div>


