<!-- PAGE CONTENT WRAPPER -->

<div class="content-frame">
    <!-- START CONTENT FRAME TOP -->
    <div class="content-frame-top">
        <div class="page-title">
            <h2><span class="fa fa-certificate"></span> Approve Store Orders</small></h2>
        </div>
        <div class="btn-group btn-sm pull-right">
            <a href="#" data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">Filter Orders <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url()."stores/orders/approve_store_orders?filter=-1"; ?>"><span class="fa fa-refresh"></span> Submitted Orders</a></li>
                <li><a href="<?php echo base_url()."stores/orders/approve_store_orders?filter=2"; ?>"><span class="fa fa-check" ></span> Approved Orders</a></li>
                <li><a href="<?php echo base_url()."stores/orders/approve_store_orders?filter=0"; ?>"><span class="fa fa-exclamation-triangle"></span> Cancelled Orders</a></li>
            </ul>
        </div>
    </div>

    <div class="header_bg">

        <div class="header">
            <div class="head-t">


                <div class="clearfix"> </div>
            </div>
        </div>

    </div>

    <div class="content">
        <div class="women_main">
            <!-- start content -->
            <div class="catalog">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="block">
                            <div class="list-group border-bottom">
                                <a href="#" class="list-group-item active"> Submitted Orders </a>
                                <?php foreach($orders as $row):?>
                                    <a href="<?php echo base_url()."stores/orders/approve_store_orders/".$row->id;?>" class="list-group-item">
                                        <?php if($row->status==2): ?>
                                        <span class="fa fa-check">
                                        <?php elseif($row->status==-1): ?>
                                            <span class="fa fa-refresh">
                                        <?php else:?>
                                            <span class="fa fa-exclamation-triangle">
                                        <?php endif; ?>
                                        <?php echo $row->id; ?>
										</span>
										<?php echo $row->display_name; ?> <br>
										<?php echo "<span class=\"text-info\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$row->department."</span>";?>
										<span class="badge badge-default"><?php echo date('d-M-Y', strtotime($row->order_date)); ?></span>
                                    </a>
                                <?php endforeach; ?>

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-9">
                        <?php if(ISSET($order_details)): ?>
                            <div class="panel panel-default">
                                <form id="order_approval_form" name="order_approval_form" method="post" action="<?php echo base_url();?>stores/orders/approve_store_orders">
                                    <div class="panel-heading">
                                        <span class="h3 text-info">Order <?php echo $order_details->id; ?></span>&nbsp;&nbsp;
                                        <small class="text-muted">
                                            | Order Date :<?php echo date('d-M-Y', strtotime($order_details->order_date)); ?>
                                            &nbsp; <b><?php echo "<span class=\"text-info\">| Department: ".$order_details->department."</span>";?></b>
                                        </small>
                                        <div class="pull-right" style="width: 250px;">
                                            <div class="input-group pull-right">
                                                <?php if($order_details->status==-1): ?>
                                                    <span class="h3 text-info">Ordered By: <?php echo $order_details->display_name; ?></span>
                                                <?php elseif($order_details->status==0): ?>
                                                    <span class="h3 text-danger"><span class="fa fa-exclamation-triangle"></span>Order Cancelled</span>
                                                <?php elseif($order_details->status==2): ?>
                                                    <span class="h3 text-success"><span class="fa fa-check"></span>Order Approved</span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body mail">
                                        <?php if($received_items): foreach($received_items as $row):?>
                                            <div class="mail-item mail-info">
                                                <div class="mail-checkbox">
                                                    <span class="fa fa-caret-right"></span>
                                                </div>
                                                <div class="mail-user"><?php echo $row->item; echo ($row->description)?" <br> <mute><small>".$row->description."</small></mute>":"";?></div>
                                                <a href="pages-mailbox-message.html" class="mail-text">&nbsp;<span class="text-muted">Order Qty |</span> <?php echo number_format($row->order_qty);  echo ($row->comments)?" <br> <mute><small>".$row->comments."</small></mute>":"";?> </a>

                                                <div class="mail-attachments pull-right">
                                                    <input type="number" name="txt_<?php echo $row->id; ?>" value="<?php echo ($row->approved_qty)?$row->approved_qty:$row->order_qty; ?>" class="form-control" required="required">
                                                </div>
                                                <a href="pages-mailbox-message.html" class="mail-text pull-right">&nbsp;<span class="small text-warning"><?php echo ($row->current_stock)?number_format($row->current_stock)." ".$row->measurement_unit:"0 ".$row->measurement_unit; ?> Available </span>&nbsp;&nbsp; </a>
                                            </div>
                                        <?php endforeach; endif; ?>
                                        <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_details->id; ?>">
                                    </div>
                                    <div class="panel-footer">
                                        <?php if($order_details->status==-1 or $order_details->status==2): ?>
                                            <a href="<?php echo base_url()."stores/orders/cancel_store_order/".$order_details->id;?>"  class="btn btn-default btn-lg  pull-left" title="Cancel Order">Cancel Order</a>
                                        <?php elseif($order_details->status==0): ?>
                                            <a href="<?php echo base_url()."stores/orders/activate_store_order/".$order_details->id;?>"  class="btn btn-danger btn-lg  pull-left" title="Cancel Order">Activate Order</a>
                                        <?php endif;?>
                                        <ul class="pagination pagination-sm pull-right">
                                            <?php if($order_details->status==-1): ?>
                                                <li class="active"><input type="submit" class="btn btn-info btn-lg" name="submit" value="Approve Order"></li>
                                            <?php elseif($order_details->status==2): ?>
                                                <li class="active"><input type="submit" class="btn btn-info btn-lg" name="submit" value="Save Approval Changes"></li>
                                            <?php endif;?>
                                        </ul>
                                    </div>
                                </form>
                            </div>
                        <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>




</div>
                
				
				