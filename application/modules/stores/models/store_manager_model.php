<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_manager_model extends MY_Model {
	protected $table_name = 'stores_managers';
    protected $key = 'id';
	protected $set_created = false;
	protected $log_user = false;
	protected $set_modified = false;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_store_managers()
    {
        return $this->db->query("SELECT * FROM bf_users WHERE role_id=18 or role_id=7 and banned=0")->result();
    }

    public function get_specific_store_managers($id)
    {
        return $this->db->query("SELECT * FROM bf_stores_managers WHERE store_id='".$id."'")->result();
    }

    public function get_assigned_store_managers()
    {
        return $this->db->query("SELECT bf_stores_managers.*,display_name FROM bf_stores_managers LEFT JOIN bf_users ON bf_users.id=bf_stores_managers.user_id")->result();
    }

    public function delete_store_managers($id)
    {
        $this->db->query("DELETE FROM bf_stores_managers WHERE store_id='".$id."'");
    }

}