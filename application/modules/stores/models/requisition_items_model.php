<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requisition_items_model extends MY_Model {
	protected $table_name = 'stores_purchase_order_items';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_ordered_items($id)
    {
        return $this->db->query("SELECT bf_stores_purchase_order_items.*,measurement_unit,item,store_id,description,current_stock,store_name FROM bf_stores_purchase_order_items
                                    LEFT JOIN (SELECT SUM(current_qty)as current_stock,product_id FROM bf_stores_stocks WHERE status=1 GROUP BY product_id)as s ON s.product_id=bf_stores_purchase_order_items.item_id
									LEFT JOIN (SELECT bf_stores_inventory.*,measurement_unit,store_name FROM bf_stores_inventory 
													LEFT JOIN bf_stores_measurement_units ON bf_stores_measurement_units.id=measurement_id
													LEFT JOIN bf_stores ON bf_stores.id=store_id) as items ON items.id=bf_stores_purchase_order_items.item_id
									WHERE purchase_order_id='".$id."'")->result();
    }
}