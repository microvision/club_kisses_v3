<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requisitions_model extends MY_Model {
	protected $table_name = 'stores_purchase_orders';
    protected $key = 'id';
	protected $set_created = true;
	protected $log_user = true;
	protected $set_modified = true;
	protected $soft_deletes = false;
	protected $date_format = 'datetime';
	
	protected $created_field    = 'created_on';
	protected $created_by_field = 'created_by';
	protected $modified_field   = 'modified_on';
	protected $modified_by_field = 'modified_by';

    public function get_orders()
    {
        return $this->db->query("SELECT bf_stores_purchase_orders.*,display_name FROM bf_stores_purchase_orders 
									LEFT JOIN bf_users ON bf_users.id = order_by
									ORDER BY id DESC LIMIT 20")->result();
    }
    public function get_order($id)
    {
        return $this->db->query("SELECT bf_stores_purchase_orders.*,display_name FROM bf_stores_purchase_orders 
									LEFT JOIN bf_users ON bf_users.id = order_by
									WHERE bf_stores_purchase_orders.id='".$id."'")->row();
    }
    public function create_finance_purchase_requisition($requisition_id,$order_by,$store,$order_date)
    {
        $accountingdb=$this->load->database('accounts', TRUE);
        $accountingdb->query("INSERT INTO bf_vision_accounts_purchase_requisitions (id,order_by,store_name,order_date,status) VALUES ('".$requisition_id."','".$order_by."','".$store."', '".$order_date."','1')");
        //return $accountingdb->query("SELECT MAX(id)as id FROM bf_vision_accounts_insurance_claims")->row();
    }
    public function finance_purchase_requisition_items($id,$requisition_id,$item,$qty,$comments)
    {
        $accountingdb=$this->load->database('accounts', TRUE);
        $accountingdb->query("INSERT INTO bf_vision_accounts_purchase_requisition_items (id,requisition_id,item,qty,comments) VALUES ('".$id."','".$requisition_id."','".$item."','".$qty."', '".$comments."')");
        //return $accountingdb->query("SELECT MAX(id)as id FROM bf_vision_accounts_insurance_claims")->row();
    }
}