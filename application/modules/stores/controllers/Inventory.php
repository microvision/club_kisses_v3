<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('users/auth');
        $this->load->helper('form_helper');
        $this->auth->restrict();

        //Assets::add_css('jquery-ui-timepicker.css');
        $this->load->model('stores_model');
        $this->load->model('measurement_units_model');
        $this->load->model('inventory_model');
        Assets::add_module_js('billables', 'jquery.dataTables.min');

    }

    public function index($id=null)
    {
        //$this->auth->restrict('Vision.Triage.View');

        Template::set('stores', $this->stores_model->where(array('status'=>1))
            ->find_all());
        Template::set('units', $this->measurement_units_model->where(array('status'=>1))
            ->find_all());
        if($id){
            Template::set('items', $this->inventory_model->join('bf_stores','bf_stores.id=store_id','left')
                ->join('bf_stores_measurement_units','bf_stores_measurement_units.id=measurement_id','left')
                ->select('bf_stores_inventory.*,store_name,measurement_unit')
                ->where(array('store_id'=>$id))
                ->find_all());
            Template::set('store', $this->stores_model->as_object()->find_by('id',$id));
        }else{
            Template::set('items', $this->inventory_model->join('bf_stores','bf_stores.id=store_id','inner')
                ->join('bf_stores_measurement_units','bf_stores_measurement_units.id=measurement_id','left')
                ->select('bf_stores_inventory.*,store_name,measurement_unit')
                ->where(array('bf_stores.status'=>1))
                ->find_all());
        }

        Template::set_theme('default');
        Template::set('page_title', 'Manage');
        Template::render('');

    }
    public function create_item(){
        if($this->input->post("submit")) {

            $data = array(
                'store_id' => $_POST['store'],
                'item' => $_POST['item_name'],
                'description' => $_POST['description'],
                'measurement_id' => $_POST['units'],
                'status' => 1
            );
            if ($this->inventory_model->insert($data)) {
                // Log the Activity
                log_activity($this->auth->user_id(), "Created New item: " . $this->input->post('item_name'), 'stores');
                Template::set_message('The item '.$this->input->post('item_name').' was successfully created .', 'alert fresh-color alert-success');
                redirect('stores/inventory/index', true);
            } else {
                Template::set_message('Error Saving!! A problem was encountered creating the product. Please check the values submitted.', 'alert fresh-color alert-danger');
                redirect('stores/inventory/index', true);
            }
        }
    }
    public function edit_item(){

        if($this->input->post("submit")){
            $id=$this->input->post("item_id");
            $data = array(
                'store_id' => $_POST['store'],
                'item' => $_POST['item_name'],
                'description' => $_POST['description'],
                'measurement_id' => $_POST['units'],
                'status' => 1
            );
            if ($this->inventory_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Changed item details for: item ".$this->input->post('item_id'), 'stores');
                Template::set_message('The Item details for '.$_POST['item_name'].' were successfully changed.', 'alert fresh-color alert-success');
                redirect('stores/inventory/index',true);
            }else{
                Template::set_message('Error Saving Changes!! A problem was encountered editing item details. Please check the values submitted.', 'alert fresh-color alert-danger');
                redirect('stores/inventory/index',true);
            }

        }else{

            $id = $this->input->get("ch");
            $details = $this->inventory_model->as_object()->find_by('id',$id);
            $stores= $this->stores_model->where(array('status'=>1))
                ->find_all();
            $units= $this->measurement_units_model->where(array('status'=>1))
                ->find_all();
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/inventory/edit_item";
            echo <<<eod
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Edit Item Details</h3>
					</div>
					<div class="modal-body">
					
					<form class="form-horizontal" method="post" action="$url" role="form">
						<div class="row">
						    <div class="form-group">
								<label class="col-lg-5 control-label" for="phone">Store Allocation</label>
									<div class="col-lg-3">
										<select id="store" name="store" required="required"  class="form-control" />
											<option value="">Select Store</option>	
eod;
            foreach($stores as $store){
                if($store->id==$details->store_id){$s="selected";}else{$s="";};
                echo "<option ".$s." value=\"".$store->id."\">".$store->store_name."</option>";
            }
            echo <<<eod
										</select>
								    </div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="store_name">Item Name</label>
									<div class="col-lg-3">
									<input type="text" name="item_name" class="form-control" value="$details->item" required="required" id="item_name">																	
									<input type="hidden" name="item_id" value="$id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="description">Description</label>
									<div class="col-lg-3">
										<textarea class="form-control" name="description" required="required" id="description">$details->description</textarea>							
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="phone">Measurement Unit</label>
									<div class="col-lg-3">
										<select id="units" name="units" required="required"  class="form-control" />
											<option value="">Select Measurement Unit</option>	
eod;
            foreach($units as $unit){
                if($unit->id==$details->measurement_id){$s="selected";}else{$s="";};
                echo "<option ".$s." value=\"".$unit->id."\">".$unit->measurement_unit."</option>";
            }
            echo <<<eod
										</select>
								    </div>
							</div>			
						<div class="modal-footer">						
							<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
							<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
						</form>
					</div>
				</div>
eod;
        }
    }

    public function disable_item(){
        if($this->input->post("submit")){
            $id=$this->input->post("item_id");
            //echo $this->input->post('todo');exit;
            $data = array('status' => $this->input->post('todo'));
            if ($this->inventory_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled Item, Item id ".$this->input->post('item_id'), 'stores');
                Template::set_message('The item was succesfully disabled.', 'alert fresh-color alert-success');
                redirect('stores/inventory/index',true);
            }else{
                Template::set_message('A problem was encountered disabling the item. Please try again.', 'alert fresh-color alert-danger');
                redirect('stores/inventory/index',true);
            }
        }else{
            $id = $this->input->get("ch");
            $details = $this->inventory_model->as_object()->find_by('id',$id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/inventory/disable_item";
            if($this->input->get("ch2")=="disable"){
                $header="Disable";
                $content="Note: When you disable an item users cannot make new orders for the item. Previous transactions remain in records though.";
                $form="<input type=\"hidden\" name=\"todo\" value=\"0\">";
            }elseif($this->input->get("ch2")=="enable"){
                $header="Activate";
                $content="Note: Activating an item allows users to continue making orders for the item. Previous records are still kept for reporting";
                $form="<input type=\"hidden\" name=\"todo\" value=\"1\">";
            }
            echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>$header Item</h3>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<h3 align="center">Are you sure you want to <u> $header </u>the item<u> $details->item </u></h3>
								<h5 align="center">$content</h5>
								<div class="form-group">							
								<div class="col-lg-3">	
									$form;
									<input type="hidden" name="item_id" value="$details->id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							</div>
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-users-2 white"></span> $header Item</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
        }
    }

}