<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Requisitions extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('users/auth');
        $this->load->helper('form_helper');
        $this->auth->restrict();

        //Assets::add_css('jquery-ui-timepicker.css');
        $this->load->model('stores_model');
        $this->load->model('measurement_units_model');
        $this->load->model('inventory_model');
        $this->load->model('orders_model');
        $this->load->model('order_items_model');
        $this->load->model('stocks_model');
        $this->load->model('requisitions_model');
        $this->load->model('requisition_items_model');


    }

    public function index($id=null)
    {
        if($id){

            Template::set('order_details', $this->requisitions_model->as_object()->find_by('id',$id));
            Template::set('received_items', $this->requisition_items_model->get_ordered_items($id));
        }
        Template::set('orders', $this->requisitions_model->get_orders());
        Template::set_theme('default');
        Template::set('page_title', 'Purchase Requisitions');
        Template::render('');
    }
    public function create_purchase_order()
    {
        $today = date('Y-m-d');
        $data = array(
            'order_date'=> $today,
            'order_by'=> $this->current_user->id,
            'approved_by'=> $this->current_user->id,
            'status'=> 1
        );
        $order_id = $this->requisitions_model->insert($data);
        redirect('stores/requisitions/index/'.$order_id,true);
    }
    public function receive_purchase_item()
    {
        $itemIds = $_POST['items'];
        $today = date('Y-m-d');
        foreach($itemIds as $itemId){
            $data = array(
                'purchase_order_id'=> $_POST['order_id'],
                'item_id'=> $itemId,
                'received_qty'=> $_POST['qty'],
                'purchase_date'=> $today,
                'status'=> 0
            );
            $this->requisition_items_model->insert($data);
        }
        redirect('stores/requisitions/index/'.$_POST['order_id'],true);
    }
    public function submit_requisition($id){

        $order_details= $this->requisitions_model->get_order($id);
        $received_items= $this->requisition_items_model->get_ordered_items($id);
        //submit requisition to finance
        $this->requisitions_model->create_finance_purchase_requisition($order_details->id,$order_details->display_name,"EMR Stores",$order_details->order_date);

        //add ordered items to finance
        foreach($received_items as $row){
            $this->requisitions_model->finance_purchase_requisition_items($row->id,$order_details->id,$row->item,$row->received_qty,$row->description);
        }
        //update order status
        $data3 = array('status'=>2);
        $this->requisitions_model->update($order_details->id,$data3);
        Template::set_message('The purchase requisition was successfully submitted to accounts for processing.', 'alert fresh-color alert-success');
        redirect('stores/requisitions/index/',true);
    }

    public function receive_requisition($id){
        $order_details= $this->requisitions_model->as_object()->find_by('id',$id);
        $received_items= $this->requisition_items_model->get_ordered_items($id);
        //receive items to stock
        foreach($received_items as $row){
            //update store stock
            $data = array(
                'store_id'=> $row->store_id,
                'order_id'=> $row->purchase_order_id,
                'product_id'=> $row->item_id,
                'purchased_qty'=> $row->received_qty,
                'current_qty'=> $row->received_qty,
                'status'=> 1
            );
            $this->stocks_model->insert($data);
        }
        //update order status
        $data3 = array('status'=>3);
        $this->requisitions_model->update($order_details->id,$data3);
        Template::set_message('The purchase requisition was successfully received.', 'alert fresh-color alert-success');
        redirect('stores/requisitions/index/',true);
    }

    public function delete_item($id){
        $item_details = $this->requisition_items_model->as_object()->find_by('id',$id);
        $this->requisition_items_model->delete($id);
        redirect('stores/requisitions/index/'.$item_details->purchase_order_id,false);
    }
    public function edit_item(){
        if(ISSET($_POST['submit'])){
            $data = array( 'received_qty'=> $_POST['qty']);
            $this->requisition_items_model->update($_POST['item_id'],$data);
            $item_details = $this->requisition_items_model->as_object()->find_by('id',$_POST['item_id']);
            redirect('stores/requisitions/index/'.$item_details->purchase_order_id,false);
        }

        $item_details = $this->requisition_items_model->as_object()
            ->join('bf_stores_inventory','bf_stores_inventory.id=bf_stores_purchase_order_items.item_id','inner')
            ->select('bf_stores_purchase_order_items.*,bf_stores_inventory.item')
            ->find_by('bf_stores_purchase_order_items.id',$_GET['ch']);
        $security_name = $this->security->get_csrf_token_name();
        $security_code = $this->security->get_csrf_hash();
        $url = base_url()."stores/requisitions/edit_item";
        echo <<<eod
			<div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Item Requistion </h4>
				  </div>
				  <form method="post" class="form-horizontal" action="$url">
                      <input type="hidden" name="item_id" value="$item_details->id">								
                      <input type="hidden" name="$security_name" value="$security_code" >
                      <div class="block">
                          <div class="form-group"><br>
                            <label class="col-lg-4 control-label" for="qty">Product Name</label>
                            <div class="col-lg-6">
                                <input id="qty" name="qty" required="required" type="text" value="$item_details->item" readonly class="form-control" />							
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-4 control-label" for="qty">Order Quantity</label>
                            <div class="col-lg-6">
                                <input id="qty" name="qty" required="required" type="text" value="$item_details->received_qty" class="form-control" />							
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">Comments</label>
                            <div class="col-md-6">
                                <textarea name="comments" placeholder="Comments/Description" class="form-control"></textarea>
                            </div>
                          </div>
					   </div>
                      <div class="modal-footer">
                        <input type="submit" name="submit" value="Save Changes" class="btn btn-info" >
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
				  </form>
				</div>
		    </div>
eod;



    }

}