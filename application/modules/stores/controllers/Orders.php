<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('users/auth');
        $this->load->helper('form_helper');
        $this->auth->restrict();

        //Assets::add_css('jquery-ui-timepicker.css');
        $this->load->model('stores_model');
        $this->load->model('measurement_units_model');
        $this->load->model('inventory_model');
        $this->load->model('orders_model');
        $this->load->model('order_items_model');
        $this->load->model('stocks_model');


    }

    public function store_orders($id=null)
    {
        if($id){
            Template::set('order_details', $this->orders_model->as_object()->find_by('id',$id));
            Template::set('received_items', $this->orders_model->get_ordered_items($id));
        }
        Template::set('orders', $this->orders_model->get_user_orders($this->current_user->id));
        Template::set_theme('default');
        Template::set('page_title', 'Store Orders');
        Template::render('');
    }
    public function create_store_order()
    {
        //check if user has been assigned a deparmtent
//        if($this->current_user->department_id){
            $today = date('Y-m-d');
            $data = array(
                'order_date'=> $today,
                'order_by'=> $this->current_user->id,
                'department_id'=> 0,
                'status'=> 1
            );
            $order_id = $this->orders_model->insert($data);
            redirect('stores/orders/store_orders/'.$order_id,true);
//        }else{
//            Template::set_message('You cannot create orders at the moment. Please contact Technical Support to be assigned to a department.', 'alert fresh-color alert-danger');
//            redirect('stores/orders/store_orders',true);
//        }

    }
    public function submit_order($id){
        $data = array('status'=> -1);
        $this->orders_model->update($id,$data);
        Template::set_message('The order has been submitted successfully. Please await approval before you can collect from the store.', 'alert fresh-color alert-success');
        redirect('stores/orders/store_orders',true);
    }
    public function get_store_search_items()
    {
        $product = $this->input->get('ch');
        $data['allitems'] = $this->inventory_model->get_search_products($product);
        $this->load->view("stores/orders/get_inventory_search_results", $data);
    }
    public function show_qty_fields(){
        $id=$_GET['ch'];
        $details = $this->inventory_model->as_object()->join('bf_stores','bf_stores.id=store_id','left')
            ->join('bf_stores_measurement_units','bf_stores_measurement_units.id=measurement_id','left')
            ->select('bf_stores_inventory.*,store_name,measurement_unit')
            ->find_by('bf_stores_inventory.id',$id);
        echo <<<eod
            <div class="block">
            <div class="form-group">
				<div class="col-md-5">
                <span class="h4 pull-right"><u>$details->store_name</u></span></div></div>
			<div class="form-group">
				<label class="col-md-3 control-label">Order Quantity</label>
				<div class="col-md-6">
					<input type="number" name="qty" placeholder="Order Quantity" required="required" class="form-control" />																		
				</div>
                <div class="col-md-1"><h4>$details->measurement_unit</h4></div>
			</div>
            <div class="form-group">
                <label class="col-md-3 control-label">Comments</label>
                <div class="col-md-7">
                    <textarea name="comments" placeholder="Comments/Description" class="form-control"></textarea>
                </div>
            </div>
		</div>
eod;

    }
    public function add_order_item()
    {
        $itemIds = $_POST['items'];
        $today = date('Y-m-d');

        //check if stock is available is store
        foreach($itemIds as $itemId){

            $data = array(
                'order_id'=> $_POST['order_id'],
                'item_id'=> $itemId,
                'comments'=> $_POST['comments'],
                'order_qty'=> $_POST['qty'],
                'status'=> 1
            );
            $this->order_items_model->insert($data);

        }

        redirect('stores/orders/store_orders/'.$_POST['order_id'],true);



    }
    public function edit_store_order_item()
    {
        if(ISSET($_POST['submit'])){
            $details = $this->order_items_model->get_store_order_item($_POST['id']);
            $data = array(
                'comments'=> $_POST['comments'],
                'order_qty'=> $_POST['qty']
            );
            if($this->order_items_model->update($_POST['id'],$data)){
                // Log the Activity
                log_activity($this->auth->user_id(),"Edited order Item: order item id ".$_POST['id'], 'Stores');
                Template::set_message('Changes to the order were succesfully made.', 'alert fresh-color alert-success');
            }
            redirect('stores/orders/store_orders/'.$details->order_id,true);
        }
        $id = $this->input->get("ch2");
        $details = $this->order_items_model->get_store_order_item($id);
        $security_name = $this->security->get_csrf_token_name();
        $security_code = $this->security->get_csrf_hash();
        $url = base_url()."stores/orders/edit_store_order_item";
        $delete_url = base_url()."stores/orders/delete_store_order_item/".$id;
        echo <<<eod
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
								<h3>Edit order Item</h3>
							</div>
							<div class="modal-body">
								<form class="form-horizontal" method="post" action="$url" role="form">
								<div class="row">
									<div class="form-group">
										<label class="col-lg-4 control-label" for="order_item">Order Item</label>
											<div class="col-lg-5">
												<input id="order_item" disabled name="order_item" required="required" type="text" class="form-control"  value="$details->item"/>
											</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label" for="qty">Order Quantity</label>
											<div class="col-lg-5">
												<input id="qty" name="qty" required="required" type="text" class="form-control"  value="$details->order_qty"/>							
												<input type="hidden" name="id" value="$id">								
												<input type="hidden" name="$security_name" value="$security_code" > 
											</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label" for="qty">Comments</label>
											<div class="col-lg-5">
												<textarea id="comments" name="comments"   class="form-control"  >$details->comments</textarea>
											</div>
									</div>
								
								<div class="modal-footer">						
									<a href="$delete_url"  class="btn btn-primary btn-rounded pull-left">Delete Item</a>
									<input type="submit" name="submit" value="Save Changes" class="btn btn-info">
									<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								</div>
								</form>
							</div>
						</div>
					</div>
eod;
    }
    public function delete_store_order_item($id)
    {
        $details = $this->order_items_model->get_store_order_item($id);
        $this->order_items_model->delete($id);
        // Log the Activity
        log_activity($this->auth->user_id(),"Deleted order Item: ".$details->item, 'stores');
        Template::set_message($details->item.' was successfully deleted from this order.', 'alert fresh-color alert-success');
        redirect('stores/orders/store_orders/'.$details->order_id,true);
    }
    public function approve_store_orders($id=null)
    {
        if(ISSET($_POST['submit'])){
            $today = date('Y-m-d');
            $order_details = $this->orders_model->get_order($_POST['order_id']);
            $order_items = $this->orders_model->get_ordered_items($_POST['order_id']);
            foreach($order_items as $row){
                $orderqty = $_POST['txt_'.$row->id];
                //update approved items on order_table
                $data3 = array('approved_qty' => $_POST['txt_'.$row->id]);
                $this->order_items_model->update($row->id,$data3);
            }

            //update order status
            $data4 = array(
                'approved_by' => $this->current_user->id,
                'status' => 2
            );
            $this->orders_model->update($order_details->id,$data4);
            //log activity
            log_activity($this->auth->user_id(),"Approved store order: ".$order_details->id, 'stores');
            Template::set_message('Order '.$order_details->id.' was succesfully approved and the order submmited to the stores.', 'alert fresh-color alert-success');
            redirect('stores/orders/approve_store_orders',true);

        }
        if($id){
            Template::set('order_details', $this->orders_model->get_order($id));
            Template::set('received_items', $this->orders_model->get_ordered_items($id));
        }
        if(ISSET($_GET['filter'])){
            Template::set('orders', $this->orders_model->get_orders2_filtered($_GET['filter']));
        }else {
            Template::set('orders', $this->orders_model->get_orders2());
        }
        Template::set_theme('default');
        Template::set('page_title', 'Approve Store Orders');
        Template::render('');
    }

    public function dispense_store_orders($id=null)
    {
        if(ISSET($_POST['submit'])){
            $today = date('Y-m-d');
            $order_details = $this->orders_model->get_order($_POST['order_id']);
            $order_items = $this->orders_model->get_ordered_items($_POST['order_id']);
            $store = $this->orders_model->get_manager_store_id($this->current_user->id);
            //first check for stocks availability of all items
//            print_r($order_items);
//            exit;
            foreach($order_items as $row){

                if(ISSET($_POST['txt_'.$row->id])) {
                    $orderqty = $_POST['txt_'.$row->id];

                    $total_store_stock = $this->stocks_model->get_total_store_stock($row->item_id);
                    if ($total_store_stock->stock < $orderqty) {
                        Template::set_message('Warning!! The order was not dispensed. The item  ' . $row->item . ' cannot not dispensed. The current available stock is less than the dispensed stock.', 'alert fresh-color alert-danger');
                        redirect('stores/orders/dispense_store_orders/' . $order_details->id, true);
                    }
                }
            }
            foreach($order_items as $row){
                if(ISSET($_POST['txt_'.$row->id])) {
                    $orderqty = $_POST['txt_'.$row->id];
                    while ($orderqty > 0) {
                        //check available store stock
                        $total_store_stock = $this->stocks_model->get_total_store_stock($row->item_id);
                        if ($total_store_stock->stock >= $orderqty) {
                            //get ealiest received stock
                            $avail_stock = $this->stocks_model->get_item_store_stock($row->item_id);
                            if ($orderqty <= $avail_stock->current_qty) {
                                $order_qty = $orderqty;
                                $new_stock = $avail_stock->current_qty - $orderqty;
                            } else {
                                $order_qty = $avail_stock->current_qty;
                                $new_stock = 0;
                            }
                            //update store stock
                            $data1 = array('current_qty' => $new_stock);
                            $this->stocks_model->update($avail_stock->id, $data1);
                            //update received Qty
                            $data3 = array('received_qty' => $orderqty, 'status' => 2);
                            $this->order_items_model->update($row->item_id, $data3);

                            //order_balance
                            $orderqty = $orderqty - $avail_stock->current_qty;
                        } else {
                            Template::set_message('Warning!! The item  ' . $row->item . ' was not dispensed. The current available stock is less than the dispensed stock.', 'alert fresh-color alert-danger');
                            break;
                        }
                    }
                }
            }
            //update order status
            $data4 = array(
                'received_by' => $this->current_user->id,
                'status' => 3
            );
            $this->orders_model->update($order_details->id,$data4);
            //log activity
            log_activity($this->auth->user_id(),"Dispensed store order: ".$order_details->id, 'stores');
            Template::set_message('Order '.$order_details->id.' was successfully dispensed. Please remind the user to receive the order.', 'alert fresh-color alert-success');
            redirect('stores/orders/dispense_store_orders',true);

        }
        if($id){
            Template::set('order_details', $this->orders_model->get_order($id));

            //todo: find a way to show more than one store for storemanagers managing multiple stores
            if($this->current_user->role_id==18 or $this->current_user->role_id==7){
                //get store they manage
                $store = $this->orders_model->get_manager_store_id($this->current_user->id);
                if(ISSET($store->store_id)){
                    Template::set('received_items', $this->orders_model->get_store_ordered_items($id,$store->store_id));
                }else{
                    Template::set_message('You have not been assigned to a store. Please contact your local Administrator for assistance.', 'alert fresh-color alert-danger');
                    Template::set('received_items', $this->orders_model->get_store_ordered_items($id,0));
                }

            }else{
                Template::set('received_items', $this->orders_model->get_ordered_items($id));
            }


        }
        //check for user leverl and only display appropriate orders and order items for store
        if($this->current_user->role_id==18 or $this->current_user->role_id==7){
            //get store they manage
            $store = $this->orders_model->get_manager_stores($this->current_user->id);
            if($store){
                //implode array of objects for stores to normal array
                $stores='('.implode(', ', array_column($store, 'store_id')).')';
                if(ISSET($_GET['filter'])){
                    Template::set('orders', $this->orders_model->get_orders4_filtered($stores,$_GET['filter']));
                }else{
                    Template::set('orders', $this->orders_model->get_orders4($stores));
                }

            }else{
                $stores = '(0)';
                Template::set('orders', $this->orders_model->get_orders4($stores));
            }

        }else{
            if(ISSET($_GET['filter'])){
                Template::set('orders', $this->orders_model->get_orders3_filtered($_GET['filter']));
            }else {
                Template::set('orders', $this->orders_model->get_orders3());
            }
        }

        Template::set_theme('default');
        Template::set('page_title', 'Dispense Store Orders');
        Template::render('');
    }
    public function delete_store_order($id){
        //delete the order from orders
        $this->orders_model->delete($id);
        //delete the order items from the
        $this->orders_model->delete_order_items($id);
        Template::set_message('The order was successfully deleted.', 'alert fresh-color alert-success');
        redirect('stores/orders/store_orders',true);
    }
    public function cancel_store_order($id){
        //delete the order from orders
        $data = array('status' => 0);
        $this->orders_model->update($id,$data);
        //delete the order items from the
        $this->orders_model->cancel_order_items($id);
        Template::set_message('The order was successfully cancelled.', 'alert fresh-color alert-success');
        redirect('stores/orders/approve_store_orders',true);
    }
    public function activate_store_order($id){
        //delete the order from orders
        $data = array('status' => -1);
        $this->orders_model->update($id,$data);
        //delete the order items from the
        $this->orders_model->activate_order_items($id);
        Template::set_message('The order was successfully activated.', 'alert fresh-color alert-success');
        redirect('stores/orders/approve_store_orders/'.$id,true);
    }
}