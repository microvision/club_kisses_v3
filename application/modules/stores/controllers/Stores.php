<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stores extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('users/auth');
        $this->load->helper('form_helper');
        $this->auth->restrict();

        //Assets::add_css('jquery-ui-timepicker.css');
        $this->load->model('stores_model');
        $this->load->model('inventory_model');
        $this->load->model('stocks_model');
        $this->load->model('measurement_units_model');
        $this->load->model('store_manager_model');
        Assets::add_module_js('billables', 'jquery.dataTables.min');


    }

    public function manage()
    {
        //$this->auth->restrict('Vision.Triage.View');

        Template::set('stores', $this->stores_model->find_all());
        Template::set('managers', $this->store_manager_model->get_assigned_store_managers());
        Template::set('units', $this->measurement_units_model->find_all());
        Template::set_theme('default');
        Template::set('page_title', 'Manage');
        Template::render('');

    }
    public function create_store(){
        if($_POST['submit']){
            $data = array(
                'store_name'=> $_POST['store_name'],
                'description'=> $_POST['description'],
                'status'=> 1
            );
            $order_id = $this->stores_model->insert($data);
        }
        redirect('stores/manage',true);
    }
    public function orders(){

        Template::set_theme('default');
        Template::set('page_title', 'Store Orders');
        Template::render('');
    }
    public function edit_store(){

        if($this->input->post("submit")){
            $id=$this->input->post("store_id");
            $data = array(
                'store_name'=> $_POST['store_name'],
                'description'=> $_POST['description'],
                'status'=> 1
            );
            if ($this->stores_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Changed password store details for: Store ".$this->input->post('store_id'), 'stores');
                Template::set_message('The store details were successfully changed.', 'alert fresh-color alert-success');
                redirect('stores/manage',true);
            }else{
                Template::set_message('Error Saving Changes!! A problem was encountered editing store details. Please check the values submitted.', 'alert fresh-color alert-danger');
                redirect('stores/manage',true);
            }

        }else{

            $id = $this->input->get("ch");
            $details = $this->stores_model->as_object()->find_by('id',$id);

            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/edit_store";
            echo <<<eod
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Edit Store Details</h3>
					</div>
					<div class="modal-body">
					
					<form class="form-horizontal" method="post" action="$url" role="form">
						<div class="row">
							<div class="form-group">
								<label class="col-lg-5 control-label" for="store_name">Store Name</label>
									<div class="col-lg-3">
										<input id="store_name" name="store_name" required="required" type="text" value="$details->store_name" class="form-control" />							
									<input type="hidden" name="store_id" value="$id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-5 control-label" for="description">Description</label>
									<div class="col-lg-3">
										<textarea class="form-control" name="description" required="required" id="description">$details->description</textarea>							
									
								</div>
							</div>
										
						<div class="modal-footer">						
							<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
							<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
						</form>
					</div>
				</div>
eod;
        }
    }
    public function store_manager(){

        if($this->input->post("submit")){
            $id=$this->input->post("store_id");
            $mg = $_POST['mg'];
            //delete al previous manager assignments to this store
            $this->store_manager_model->delete_store_managers($id);
            foreach($mg as $m){
                $data = array(
                    'store_id'=> $_POST['store_id'],
                    'user_id'=> $m
                );
                $this->store_manager_model->insert($data);
            }

            // Log the Activity
            log_activity($this->auth->user_id(),"Added store Manager: Store ".$this->input->post('store_id'), 'stores');
            Template::set_message('The store manager was successfully added.', 'alert fresh-color alert-success');
            redirect('stores/manage',true);


        }else{

            $id = $this->input->get("ch");
            $details = $this->stores_model->as_object()->find_by('id',$id);
            //ge all users with the role of store manager
            $managers = $this->store_manager_model->get_store_managers();
            //get managers assigned to this store
            $store_mgrs = $this->store_manager_model->get_specific_store_managers($id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/store_manager";
            echo <<<eod
            <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Add Store Manager to $details->store_name</h3>
					</div>
					<div class="modal-body">
					
					<form class="form-horizontal" method="post" action="$url" role="form">
						<div class="row">
							<table class="table">
							    <thead>
							        <tr>
							            <th> </th>
							            <th>Store Manager</th>
							            <th>email</th>
							            <th>Last Login</th>
							        </tr>
                                </thead>
                                <tbody>
eod;
            foreach ($managers as $row) {
                $checked=""; foreach($store_mgrs as $mgr){if($mgr->user_id == $row->id){$checked="checked='checked'";}}
                echo "<tr >
                                        <td><input type='checkbox' ".$checked." class='icheckbox' name='mg[]' value='".$row->id."'></td>
                                        <td>".$row->display_name."</td >
                                        <td><a href='mailto:".$row->email."'>".$row->email."</a></td >
                                        <td>".date('d-M-Y h:i a',strtotime($row->last_login))."</td >
                                    </tr>";
            }
            echo <<<eod
                                </tbody>
                            </table>
							<input type="hidden" name="store_id" value="$id">								
							<input type="hidden" name="$security_name" value="$security_code" > 			
						<div class="modal-footer">						
							<button type="submit" name="submit" value="submit" class="btn btn-info"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Assign Managers</button>
							<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
						</form>
					</div>
				</div>
				</div>
eod;
        }
    }
    public function disable_store(){
        if($this->input->post("submit")){
            $id=$this->input->post("store_id");
            //echo $this->input->post('todo');exit;
            $data = array('status' => $this->input->post('todo'));
            if ($this->stores_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled Store, Store id ".$this->input->post('store_id'), 'stores');
                Template::set_message('The store was succesfully disabled.', 'alert fresh-color alert-success');
                redirect('stores/manage',true);
            }else{
                Template::set_message('A problem was encountered disabling the store. Please try again.', 'alert fresh-color alert-danger');
                redirect('stores/manage',true);
            }
        }else{
            $id = $this->input->get("ch");
            $details = $this->stores_model->as_object()->find_by('id',$id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/disable_store";
            if($this->input->get("ch2")=="disable"){
                $header="Disable";
                $content="Note: When you disable a store users cannot make new orders to the store. Previous transactions remain in records though.";
                $form="<input type=\"hidden\" name=\"todo\" value=\"0\">";
            }elseif($this->input->get("ch2")=="enable"){
                $header="Activate";
                $content="Note: Activating a user allows users to continue making orders through the store. Previous records are still kept for reporting";
                $form="<input type=\"hidden\" name=\"todo\" value=\"1\">";
            }
            echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>$header Store</h3>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<h3 align="center">Are you sure you want to <u> $header </u>the store <u> $details->store_name </u></h3>
								<h5 align="center">$content</h5>
								<div class="form-group">							
								<div class="col-lg-3">	
									$form;
									<input type="hidden" name="store_id" value="$details->id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							</div>
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-users-2 white"></span> $header Store</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
        }
    }

    public function create_measurement_unit(){
        if($_POST['submit']){
            $data = array(
                'measurement_unit'=> $_POST['measurement_unit'],
                'status'=> 1
            );
            $order_id = $this->measurement_units_model->insert($data);
        }
        redirect('stores/manage',true);
    }
    public function edit_measurement_unit(){

        if($this->input->post("submit")){
            $id=$this->input->post("measurement_unit_id");
            $data = array(
                'measurement_unit'=> $_POST['measurement_unit'],

            );
            if ($this->measurement_units_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Changed measurement unit name for: id ".$this->input->post('measurement_unit_id'), 'stores');
                Template::set_message('The measurement unit was successfully changed.', 'alert fresh-color alert-success');
                redirect('stores/manage',true);
            }else{
                Template::set_message('Error Saving Changes!! A problem was encountered editing measurement unit details. Please check the values submitted.', 'alert fresh-color alert-danger');
                redirect('stores/manage',true);
            }

        }else{

            $id = $this->input->get("ch");
            $details = $this->measurement_units_model->as_object()->find_by('id',$id);

            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/edit_measurement_unit";
            echo <<<eod
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
						<h3>Edit Measurement Unit Details</h3>
					</div>
					<div class="modal-body">
					
					<form class="form-horizontal" method="post" action="$url" role="form">
						<div class="row">
							<div class="form-group">
								<label class="col-lg-5 control-label" for="measurement_unit">Measurement Unit</label>
									<div class="col-lg-3">
										<input id="measurement_unit" name="measurement_unit" required="required" type="text" value="$details->measurement_unit" class="form-control" />							
									<input type="hidden" name="measurement_unit_id" value="$id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
								
						<div class="modal-footer">						
							<button type="submit" name="submit" value="submit" class="btn btn-primary"> <span class="icon16 icomoon-icon-pencil-3 white"></span> Save Changes</button>
							<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						</div>
						</form>
					</div>
				</div>
eod;
        }
    }
    public function disable_measurement_unit(){
        if($this->input->post("submit")){
            $id=$this->input->post("measurement_unit_id");
            //echo $this->input->post('todo');exit;
            $data = array('status' => $this->input->post('todo'));
            if ($this->measurement_units_model->update($id, $data))
            {
                // Log the Activity
                log_activity($this->auth->user_id(),"Disabled measurement unit, id ".$this->input->post('measurement_unit_id'), 'stores');
                Template::set_message('The measurement unit was succesfully disabled.', 'alert fresh-color alert-success');
                redirect('stores/manage',true);
            }else{
                Template::set_message('A problem was encountered disabling the measurement unit. Please try again.', 'alert fresh-color alert-danger');
                redirect('stores/manage',true);
            }
        }else{
            $id = $this->input->get("ch");
            $details = $this->measurement_units_model->as_object()->find_by('id',$id);
            $security_name = $this->security->get_csrf_token_name();
            $security_code = $this->security->get_csrf_hash();
            $url = base_url()."stores/disable_measurement_unit";
            if($this->input->get("ch2")=="disable"){
                $header="Disable";
                $content="Note: When you disable a measurement unit it cannot be used for other items. Previous items remain in records though.";
                $form="<input type=\"hidden\" name=\"todo\" value=\"0\">";
            }elseif($this->input->get("ch2")=="enable"){
                $header="Activate";
                $content="Note: Activating a measurement unit allows items to continue to be used in in this unit. Previous records are still kept for reporting";
                $form="<input type=\"hidden\" name=\"todo\" value=\"1\">";
            }
            echo <<<eod
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
							<h3>$header Measurement Unit</h3>
						</div>
						<div class="modal-body">
						<form class="form-horizontal" method="post" action="$url" role="form">
							<div class="row">
								<h3 align="center">Are you sure you want to <u> $header </u>the measurement unit <u> $details->measurement_unit </u></h3>
								<h5 align="center">$content</h5>
								<div class="form-group">							
								<div class="col-lg-3">	
									$form;
									<input type="hidden" name="measurement_unit_id" value="$details->id">								
									<input type="hidden" name="$security_name" value="$security_code" > 
								</div>
							</div>
							</div>
							<div class="modal-footer">						
								<button type="submit" name="submit" value="submit" class="btn btn-danger"> <span class="icon16 icomoon-icon-users-2 white"></span> $header Measurement Unit</button>
								<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
							</div>
							</form>
						</div>
					</div>
				</div>
eod;
        }
    }
    public function store_stock()
    {
        if($this->current_user->role_id==18){
            //get store they manage
            $store = $this->orders_model->get_manager_store_id($this->current_user->id);
            if($store->store_id){
                Template::set('items', $this->stocks_model->get_store_stocks2($store->store_id));
            }else{
                Template::set_message('You have not been assigned to a store. Please contact your local Administrator for assistance.', 'alert fresh-color alert-danger');
                Template::set('items', $this->stocks_model->get_store_stocks2($store->store_id));
            }
        }else{
            Template::set('items', $this->stocks_model->get_store_stocks());
        }

        Template::set_theme('default');
        Template::set('page_title', 'Stock in Store');
        Template::render('');
    }
    public function print_store_stock(){
        if($this->current_user->role_id==18){
            //get store they manage
            $store = $this->orders_model->get_manager_store_id($this->current_user->id);
            if($store->store_id){
                $data['items']= $this->stocks_model->get_store_stocks2($store->store_id);
            }else{
                echo "You have not been assigned to a store. Please contact your local Administrator for assistance.";

            }
        }else{
            $data['items']=$this->stocks_model->get_store_stocks();
        }
        $this->load->view('stores/print_store_stock',$data);
    }
    public function edit_store_stock()
    {
        if(ISSET($_POST['submit'])){
            $itemId = $_POST['productId'];
            $product = $this->inventory_model->as_object()->join('bf_stores','bf_stores.id=store_id','left')
                ->join('bf_stores_measurement_units','bf_stores_measurement_units.id=measurement_id','left')
                ->select('bf_stores_inventory.*,store_name,measurement_unit')
                ->find_by('bf_stores_inventory.id',$itemId);
            $active_stocks = $this->stocks_model->get_product_store_stocks($itemId);
            //we check for active fields using the stock ids
            foreach ($active_stocks as $rows){
                if($_POST['p_'.$rows->id]){
                    $data = array('purchased_qty'=> $_POST['p_'.$rows->id]);
                    $this->stocks_model->update($rows->id,$data);
                }
                if($_POST['c_'.$rows->id]){
                    $data = array( 'current_qty'=> $_POST['c_'.$rows->id]);
                    $this->stocks_model->update($rows->id,$data);
                }
            }
            Template::set_message('Stock for the drug was succefully updated', 'alert alert-success');
            redirect ('stores/store_stock',true);
        }

        $item_id = $this->input->get("ch2");
        $product = $this->inventory_model->as_object()->join('bf_stores','bf_stores.id=store_id','left')
            ->join('bf_stores_measurement_units','bf_stores_measurement_units.id=measurement_id','left')
            ->select('bf_stores_inventory.*,store_name,measurement_unit')
            ->find_by('bf_stores_inventory.id',$item_id);
        $stocks = $this->stocks_model->get_product_store_stocks($item_id);
        $url=base_url()."stores/edit_store_stock";
        echo <<<eod
			<div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Requisition History for <b>$product->item </b></h4>
				  </div>
				  <form method="post" class="form-horizontal" action="$url">
					  <div class="modal-body">
					  <h6>
						<table class="table responsive-table">
						  <thead>
							<tr>
							  
							  <th>Requistion Date</th>
							  <th>Received Stock</th>
							  <th>Current Stock</th>
							  <th>Status</th>
							</tr>
						  </thead>
eod;
        foreach ($stocks as $rows){
            if($rows->status != 0){
                $label= "<p class=\"text-danger\">In-active</p>";
            }else{
                $label= "<p class=\"text-primary\">Active</p>";
            }
            echo "<tr>
													
													<td>".$rows->created_on."</td>
													<td width=\"20%\">".$rows->purchased_qty." ".$product->measurement_unit."</td>
													<td width=\"20%\">".$rows->current_qty." ".$product->measurement_unit."</td>
													
													<td><a href=\"#\" class=\"btn btn-warning btn-xs btn-circle btn-grad\" data-original-title=\"\" title=\"\" onClick=\"mover(".$rows->id.")\"><span class=\"fa fa-angle-down\"></span></a></td>
											 </tr>
											 <tr  class=\"row even\" id = \"w".$rows->id."\"  style=\"background-color:rgb(255, 255, 255); display: none;\">
												 <td><input type=\"number\" placeholder=\"".$rows->purchased_qty."\" name=\"p_".$rows->id."\" class=\"form-control\"></td>
												 <td><input type=\"number\" placeholder=\"".$rows->current_qty."\" name=\"c_".$rows->id."\" class=\"form-control\"></td>
												 
											 </tr>
											 ";

        }

        echo <<<eod
					  </table></h6>
					  
					  </div>
					  <input type="hidden" name="productId" value="$item_id">
				  
				  <div class="modal-footer">
					<button type="submit" name="submit" value="submit" class="btn btn-info" >Save Changes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				  </form>
				</div><!-- /.modal-content -->
		  </div>
eod;
    }
}