<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_stores_permissons extends Migration
{

	/**
	 * Migrate Permissions for the finance module
	 *
	 * @var Array
	 */
	private $permission_values = array(
		array(
			'name'          => 'Vision.Stores.Purchase_order',
			'description'   => 'Create purchase orders and receive store stocks.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Stores.Approve_orders',
			'description'   => 'Approve shop orders from store.',
			'status'        => 'active',
		),
		array(
			'name'          => 'Vision.Stores.Edit_stocks',
			'description'   => 'Edit and delete store stocks.',
			'status'        => 'active',
		),
        array(
            'name'          => 'Vision.Stores.Receive_payment',
            'description'   => 'Receive supplier payments for stocks bought.',
            'status'        => 'active',
        ),
		array(
			'name'          => 'Vision.Stores.View',
			'description'   => 'View store stocks and store orders.',
			'status'        => 'active',
		),
        array(
            'name'          => 'Vision.Stores.Manage',
            'description'   => 'View store stocks and store orders.',
            'status'        => 'active',
        ),
	);

    /**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $table_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->table_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->table_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->table_name, array('name' => $permission_value['name']));
		}
	}

	//--------------------------------------------------------------------

}