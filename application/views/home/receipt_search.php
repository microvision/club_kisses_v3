<?php
$delete_url = base_url()."home/delete_item";
$receipt_url = base_url()."home/receipt";
$total=0;
?>
<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo pull-right">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo Template::theme_url('images/logo.png');?>" class="img-responsive" alt=""> </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>

</div>
<!-- //header-ends -->

<!--content-->
<div class="content">
    <div class="women_main">
        <!-- start content -->
        <div class="catalog">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <span class="h4 text-danger">Receipt Search</span><hr>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <h4>Receipt #: <?php echo $bill_details->id ?></h4>
                            <p >Sold to: <?php echo $bill_details->bill_alias; ?></p>
                            <p class="text-warning">Receipt Created on <?php echo date('d-M-Y H:i',strtotime($bill_details->created_on)); ?></p>
                            <table class="table  sortableTable responsive-table tablesorter tablesorter-default">
                                <thead>
                                <tr>

                                    <th>Product</th>
                                    <th>Qty</th>
                                    <th>Unit Cost</th>
                                    <th>Sale </th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($receipt as $rows){
                                    if ($bill_details->status==1 or $bill_details->status==4){

                                        $delete_button = "<a href=\"#txtResult2\" class=\"btn btn-primary btn-sm btn-circle btn-grad\" data-dismiss=\"modal\"  data-original-title=\"Return to Stock\" title=\"Return to Stock\" onClick=\"htmlData2('$delete_url', 'ch=$bill_id&ch2=$rows->sale_id')\"><i class=\"fa fa-undo\"></i></a>";
                                    }else{

                                        $delete_button="";
                                    }

                                    $pricesold = $rows->selling_price*$rows->qty;
                                    $total = $total + $pricesold;

                                    echo "
											<tr>
											  
											  <td>".$rows->product_name."</td>
											  <td>".$rows->qty."</td>
											  <td>Ksh.".abs(number_format($rows->selling_price))."</td>
											  <td>Ksh ".abs(number_format($pricesold))."</td>
											  
											</tr>";


                                }
                                ?>
                                </tbody>
                                <footer>
                                    <tr class="text-primary">
                                        <td colspan=3 align="right"><strong pull-right>Total Sale</strong></td>
                                        <td colspan=1><strong> Ksh <?php echo number_format($total);?> </strong></td>
                                    </tr>
                                </footer>
                            </table>
                            <h5 align="center">Served by <?php echo $bill_details->display_name; ?></h5>
                            <h5 align="center">Sale Status <?php if($bill_details->status==1){echo "UNPAID";}elseif($bill_details->status==2){echo "PAID";}elseif($bill_details->status==3){echo "DEBT";}elseif($bill_details->status==4){echo "ACTIVATED";} ?></h5>
                            <br>&nbsp;
                            <hr>
                            <?php if($bill_details->payment_option==1){$pay="cash";}elseif($bill_details->payment_option==2){$pay="mpesa";}else{$pay="credit";}?>
                            <?php echo "<a href=\"".$receipt_url."\" class=\"btn btn-success\" onclick =\"return popitup2('".$receipt_url."?billid=".$bill_id."&button=FISCAL RECEIPT&pay=".$pay."')\"><i class=\"fa fa-money\"></i> PRINT RECEIPT</a>"; ?>

                            <a href="<?php echo base_url(); ?>home/activate_receipt?d=<?php echo $bill_id; ?>" value="Activate Receipt" class="btn btn-danger pull-right">Activate Receipt</a>
                            <br>&nbsp;

                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
                
			</div>
            <div id="txtResult" class="modal fade"> </div>
        </div>

        <!-- end content -->

    </div>
