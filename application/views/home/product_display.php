

<!-- grids_of_4 -->
<?php
$row_num=0;
$new_row="<div class=\"clearfix\"></div>";
$keypad_url = base_url()."home/keypad_modal";


?>
<ul class="nav nav-tabs">
    <?php $n=0; foreach($product_categories as $rows): $n++; ?>
        <li role="presentation"  <?php if($n==1){ echo "class=\"active\"";} ?>><a href="#<?php echo "category_".$rows->id."_".$billid; ?>" data-toggle="tab"><?php echo ucfirst(strtolower($rows->name)); ?></a></li>
    <?php endforeach; ?>

</ul>
<div class="tab-content">
    <?php $n=0; foreach($product_categories as $cat): $n++; ?>
    <div class="tab-pane <?php if($n==1){ echo "active"; }?>" id="<?php echo "category_".$cat->id."_".$billid; ?>" style="height:600px; overflow:auto">
        <table class="table table-hover">
            <tr>
                <?php
                    $row_num=0; foreach($products as $rows): if($cat->id == $rows->category_id):
                    if($row_num==3){
                        $new_row="</tr><tr>"; $row_num=0;
                    }else{
                        $new_row=""; $row_num++;
                    }
                    $buying_price =$rows->retail_price;
                    $price_display = number_format($rows->retail_price,2);
                    foreach($daily_promo_products as $promo1){
                        if (($promo1->product_id == $rows->id) and ($now_time>=$promo1->start_time) and($now_time<=$promo1->end_time)){
                            $buying_price = $promo1->promotion_price;
                            $price_display = "<strike>".number_format($rows->retail_price,2)."</strike> ".number_format($buying_price,2);
                            $class = "success";
                        }
                    }
                    foreach($promo_products as $promo){
                        if ($promo->product_id == $rows->id){
                            $buying_price = $promo->promotion_price;
                            $price_display = "<strike>".number_format($rows->retail_price,2)."</strike> ".number_format($buying_price,2);
                            $class = "success";
                        }
                    }
                ?>
                <td align="center" style="width: 150px;">
                    <div class="content_box"><a href="details.html">
                            <?php if($rows->image_url): ?>
                                <img src="<?php echo Template::theme_url(''.$rows->image_url.'');?>" class="img-responsive" alt="">
                            <?php endif;?>
                        </a>
                        <span class="h5">
                            <?php echo $rows->product_name; ?>
<!--                            <span class="h6 text-muted"><br>--><?php //echo $rows->category; ?><!--</span>-->
                        </span>

                        <div class="grid_1 simpleCart_shelfItem">
                            <div class="item_add"><span class="item_price h6 text-muted">Ksh <?php echo $price_display; ?></span></div>
                            <div class="item_add"><span class="item_price"><a href="#txtResult" data-toggle="modal" title="Sell Product" onClick="htmlData('<?php echo $keypad_url; ?>', 'ch=<?php echo $billid; ?>&ch1=<?php echo $rows->id; ?>&ch4=<?php echo $buying_price; ?>')">Add to Sale</a></span></div>
                        </div>
                    </div>

                </td>
                <?php echo $new_row; endif;endforeach; if($row_num!=5){echo "</tr>";}?>
        </table>
    </div>
    <?php  endforeach; ?>
</div>
<!-- end grids_of_4 -->