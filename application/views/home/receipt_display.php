
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
$url = base_url()."services/edit_service";
$delete_url = base_url()."home/delete_item";
$receipt_url = base_url()."home/receipt";
$url4 = base_url()."home/balance";
if(ISSET($_GET['ch'])){
    $bill_id = $_GET['ch'];
}else{
    $bill_id = $bill;
}

$total=0;
//display for the print receipt button
?>


<!--    <form method="POST" class="form-vertical" action="--><?php //echo base_url()."home/new_sale"; ?><!--" id="quick_sale" name="quick_sale">-->
<!--        <input type="hidden" name="bill_name" value="">-->
<!--        <input type="hidden" name="--><?php //echo $this->security->get_csrf_token_name(); ?><!--" value="--><?php //echo $this->security->get_csrf_hash(); ?><!--" >-->
<!--        <button type="submit" name="submit" class="btn btn-danger btn-block">Create New Sale</button>-->
<!--    </form>-->
    <hr>
    <div class="clearfix"></div>
    <h4>Sale Summary
        <?php if($bill_details->status!=5):?>
            <a  href="#receipt_<?php echo $bill_id; ?>" data-toggle="modal" class="btn-danger btn-xs pull-right" href="" title="Edit Sale Details"><i class="fa fa-edit"></i></a></h4>
        <?php endif; ?>
    <table width="100%" cellpadding="0" cellspacing="0" >
        <?php
        $Discount_amount = number_format(0,2);
        $vat = number_format(0,2);
        foreach($receipt as $rows){
            $pricesold = $rows->selling_price*$rows->qty;
        if ($rows->product_id==0){
            //$d = $pricesold+$d;
            $Discount_amount=abs($Discount_amount-$pricesold);
        }else {
            $total = $total + $pricesold;
            $prices = number_format(abs($pricesold));
            $vat = $rows->VATS + $vat;
            echo <<<eod
                        <tr>
                          
                          <td><span class="small">$rows->product_name</span></td>
                          <td><span class="small pull-right">$prices</span></td>
                        </tr>
eod;
        }
        }
        ?>

        <tr bgcolor="#fafad2">
            <td><span class="h5"><b>Total</b></span></td>
            <td width="20%"><span class="h5 pull-right"><b><?php echo number_format($total);?></b></span></td>
        </tr>
    </table>

    <table class="table table-condensed"  >
        <tr bgcolor="gray">
            <td><a href="#discount_<?php echo $bill_id; ?>" data-toggle="modal" data-original-title="discount" title="Add Discount"> <span class="small"><font color="white">Discount</font></span></a></td>
            <td width="20%" align="right"><span class="small"><font color="white"><?php echo $Discount_amount; ?></font></span></td>
        </tr>
        <tr bgcolor="gray">
            <td><span class="small"><font color="white">Vat<small>(exclusive)</small></font></span></td>
            <td width="20%" align="right"><span class="small"><font color="white"><?php echo $Discount_amount; ?></font></span></td>
        </tr>
        </tr>
        <tr bgcolor="gray">
            <td><span class="small"><font color="white"><strong>Total Sale</strong></font></span></td>
            <td width="20%" align="right"><span class="small"><font color="white"><strong><?php echo number_format($total-$Discount_amount,2)?></strong></font></span></td>
        </tr>
    </table>
    <form id="balanc">
        <input type="text" placeholder="amount rendered" id="payment2" name="payment2" class="form-control" onKeyUp="htmlData4('<?php echo $url4; ?>', 'ch=<?php echo $total; ?>&ch2='+this.value)">
        <div id="txtResult4" class="text-warning" align="center"> <form id="balanc"><input type="hidden" value="0" name="balance" id="balance"></form> </div>
    </form>
    <br>
    <div class="clearfix"></div>
		<?php if($bill_details->status==5):?>
            <a href="<?php echo $receipt_url; ?>" class="btn btn-danger btn-block" btn-block" onclick ="return popitup('<?php echo $receipt_url; ?>?billid=<?php echo $bill_id; ?>&pay=cash&button=FISCAL RECEIPT&balance='+balanc.balance.value)"> Cash Sale </a>
            <a href="<?php echo $receipt_url; ?>" class="btn btn-success btn-block" btn-block" onclick ="return popitup('<?php echo $receipt_url; ?>?billid=<?php echo $bill_id; ?>&pay=mpesa&button=FISCAL RECEIPT&balance='+balanc.balance.value)"> Mpesa Sale </a>
            <?php if($bill_details->bill_type==2):?>
                <a href="<?php echo $receipt_url; ?>" class="btn btn-info btn-block" btn-block" onclick ="return popitup('<?php echo $receipt_url; ?>?billid=<?php echo $bill_id; ?>&pay=credit&button=CREDIT&balance='+balanc.balance.value)"> Credit Sale </a>
            <?php endif; ?>
            <hr>
		<?php else: ?>
            <a href="<?php echo $receipt_url; ?>?billid=<?php echo $bill_id; ?>&button=INVOICE" class="btn btn-default btn-block" onclick ="return popitup('<?php echo $receipt_url; ?>?billid=<?php echo $bill_id; ?>&pay=credit&button=INVOICE&balance='+balanc.balance.value)" ><i class="fa fa-list-alt"></i> Print Invoice</a>
		<?php endif; ?>
<?php
$submit_url = base_url()."home/discount";
//discount modal
$total=0;
echo <<<eod
<div id="discount_$bill_id" class="modal fade" data-backdrop="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Give Discount to $bill_details->bill_alias</h4>
            </div>
            <div class="modal-body">
                <p class="text-warning">Created on $bill_details->created_on</p>
                <table class="table  sortableTable responsive-table tablesorter tablesorter-default">
                    <thead>
                    <tr>

                        <th>Product</th>

                        <th><span class="pull-right">Sale (Ksh)</span> </th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
eod;
                    foreach($receipt as $rows){
                    if ($bill_details->status==1){

                    $delete_button = "<a href=\"#txtResult2\" class=\"btn btn-warning btn-sm btn-circle btn-grad\" data-dismiss=\"modal\"  data-original-title=\"Return to Stock\" title=\"Return to Stock\" onClick=\"htmlData2('$delete_url', 'ch=$bill_id&ch2=$rows->sale_id')\"><i class=\"fa fa-undo\"></i></a>";
                    }else{

                    $delete_button="";
                    }

                    $pricesold = $rows->selling_price*$rows->qty;
                    $total = $total + $pricesold;
                    echo "
                    <tr>

                        <td width='60%'>".$rows->product_name."</td>

                        <td align='right'>".number_format($pricesold,2)."</td>
                        <td width='5%'> ".$delete_button." </td>
                    </tr>";


                    }
                    echo <<<eod
                    </tbody>
                    <footer>
                        <tr class="text-primary">


                            <td colspan=1 ><strong>Total Sale</strong></td>

                            <td align="right"><strong>
eod;

                                    echo number_format($total,2);
                                    echo <<<eod
                                </strong></td>
                            <td></td>
                        </tr>

                    </footer>
                </table>
                <hr>
                <form class="form-horizontal" method="post" action="#" id="discounts" name="discounts">
                    <div class="form-group">
                        <label class="control-label col-lg-4">Discount Amount(Ksh)</label>
                        <div class="col-lg-6">
                            <input type="text" class="validate[required] form-control" name="discount_amount" id="amount_paid">
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <a href="#txtResult2" class="btn btn-danger" data-dismiss="modal"  onClick="htmlData2('$submit_url', 'ch=$bill_id&ch1=0&ch4=-1&ch3='+discounts.discount_amount.value)"> Add Discount</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --><!-- /#helpModal -->

eod;



//modal for editing receipt
$total=0;
echo <<<eod
		<div id="receipt_$bill_id" class="modal fade" data-backdrop="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Receipt for $bill_details->bill_alias</h4>
          </div>
          <div class="modal-body">
			<p class="text-warning">Created on $bill_details->created_on</p>
			<table class="table  sortableTable responsive-table tablesorter tablesorter-default">
                      <thead>
                        <tr>
                          
                          <th >Product</th>
                          <th>Qty</th>
						  <th><span class="pull-right">Unit Cost(Ksh)</span></th>
                          <th><span class="pull-right">Sale (Ksh)</span></th>
						  <th>&nbsp;</th>
                        </tr>
                      </thead>
                      <tbody>
eod;
foreach($receipt as $rows){
    if ($bill_details->status!=2){

        $delete_button = "<a href=\"#txtResult2\" class=\"btn btn-primary btn-sm btn-circle btn-grad\" data-dismiss=\"modal\"  data-original-title=\"Return to Stock\" title=\"Return to Stock\" onClick=\"htmlData2('$delete_url', 'ch=$bill_id&ch2=$rows->sale_id')\"><i class=\"fa fa-undo\"></i></a>";
    }else{

        $delete_button="";
    }

    $pricesold = $rows->selling_price*$rows->qty;
    $total = $total + $pricesold;

    echo "
                        <tr>
                          
                          <td width=\"50%\">".$rows->product_name."</td>
                          <td>".$rows->qty."</td>
						  <td align='right'>".number_format(abs($rows->selling_price),2)."</td>
                          <td align='right'>".number_format(abs($pricesold),2)."</td>
						  <td> ".$delete_button." </td>
                        </tr>";


}
echo <<<eod
           </tbody>
					  <footer>
					  <tr class="text-danger">
						  
						  
                          <td colspan=3 align="left"><strong pull-right>TOTAL SALE</strong></td>
                          
                          <td  align="right"><strong> 	
eod;

echo number_format($total,2);
echo <<<eod
					</strong></td>		
						  <td></td>
                        </tr>	
						
						
						</footer>
                    </table>
		  </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --><!-- /#helpModal -->
		
eod;

?>