
<script src='<?php echo Template::theme_url('js/jquery-1.10.2.min.js'); ?>' type='text/javascript'></script>

				<!--content-->
				
			<div class="content">
			<div class="row">
                <div class="col-md-10 col-sm-10">
                    <div class="women_main">
                        <!-- start content -->

                       <div class="w_content">
                           <div class="tab-content">
                               <!--START OF FIRST TAB CREATING BILLS -->
                               <div class="tab-pane active" id="new_bill">
                                   <div class="row">

                                       <div class="col-lg-12">
                                           <h4> <b><u>Active Sales </u></b></h4><br>
                                           <table class="table table-hover">
                                               <tr>
                                                   <?php

                                                   foreach($user_bills as $row){
                                                       $reverse_url = base_url()."home/reverse_sale/".$row->id;
                                                       //if($row->school==1){$sch="Damacrest Schools Thogoto";}else{$sch="Damacrest Schools 87";}
                                                       echo "
                                                            <tr>
                                                                <td width='2%'><a href='".$reverse_url."'><span class='text-danger'><i class='fa fa-undo'></i></span> </a></td>
                                                                <td valign=\"top\" width=\"60%\" >
                                                                    <span  class=\"pull-left\"><b>".$row->bill_alias."</b> <small>( ".date('d-M-Y h:i',strtotime($row->created_on)).")</small><br>
                                                                    <span class='small text-muted pull-left'>".$row->display_name."</span></span>
                                                                </td>
                                                                <td align=\"right\">
                                                                    <span class='text-danger'>Ksh. ".number_format($row->sale_amount,2)."</span>
                                                                <td>
                                                                <td>
                                                                <a  class=\"btn btn-primary btn-grad btn-sm btn-block userbills\" id=\"".$row->id."\" data-original-title=\"Load Sale\"
                                                                    title=\"Load Sale\" >Load Sale</a>
                                                                </td>
                                                            </tr>
                                                            ";

                                                       }
                                                   ?>
                                               </tr>
                                           </table>
                                           <div class="clearfix"></div>
                                       </div>
                                   </div>
                               </div>
                               <!--END OF FIRST TAB CREATING BILLS -->
                               <!--START OF SECOND USER BILLS -->
                                <?php
                                    $keypad_url = base_url()."keypad_modal";
                                    foreach ($user_bills as $rows_bills){
                                        echo "
                                            <div class=\"tab-pane billtab\" id=\"".$rows_bills->id."\">
                                                <br />
                                                <b>Loading...</b>						
                                                <br />
                                            </div>";

                                    }
                                ?>
                               <!--END OF SECOND USER BILLS -->

                           </div>

                        </div>

                       <div class="clearfix"></div>
                        <!-- end content -->


                    </div>

                </div>
                <?php $url = base_url()."home/add_quantity"; ?>
                <div id="txtResult" class="modal fade in"> </div>
                <div class="col-md-2 col-sm-2" id="txtResult2">

                        <a href="<?php echo base_url(); ?>"  class="btn btn-danger btn-block">Create New Sale</a>

                    <hr>
                    <div class="clearfix"></div>


                </div>
            </div>
</div>

			<!--content-->
		</div>
</div>
				<!--//content-inner-->
<script type="text/javascript">
    var siteurl = "<?php echo base_url(); ?>";

    //when user bills is clicked
    $(".userbills").click(function () {
        var billid=$(this).attr("id");
        $.ajax({
            type: "POST",
            data: {bill: billid},
            url: siteurl + "home/get_bill_details/",
            success: function(data) {
                $("#txtResult2").html(data);

            }
        });

    });
</script>

<!--script for automatically refreshing the page after 8 secs -->
<script>
    var time = new Date().getTime();
    $(document.body).bind("mousemove keypress", function(e) {
        time = new Date().getTime();
    });

    function refresh() {
        if(new Date().getTime() - time >= 5000)
            window.location.reload(true);
        else
            setTimeout(refresh, 10000);
    }

    setTimeout(refresh, 10000);
</script>