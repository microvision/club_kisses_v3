<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Signal POS - Receipt</title>
    <script>
        function popitup(url) {
            newwindow = window.open(url, 'name', 'height=540,width=340,top=50,left=500');
            if (window.focus) {
                newwindow.focus()
            }
            return false;
        }

        //window.onunload = refreshParent;
        function refreshParent() {
            //window.opener.location.reload();
            window.opener.location.href = "<?php echo base_url();?>logout";
        }

        window.onload = function () {
            <?php if(substr($_SERVER["HTTP_REFERER"], strrpos($_SERVER["HTTP_REFERER"], '/') + 1)!='cashier'): ?>
            refreshParent();
            <?php endif; ?>
            //printreceipt();
            var count = 0;
            while (count < 3) {
                window.print();
                count = count + 1;
            }
            <?php //if($button != 'RECEIPT COPY'): ?>
//        popitup('<?php //echo base_url()."home/receipt2?billid=".$bill_details->id; ?>//&button=RECEIPT COPY&pay=cash&balance=<?php //echo $change_balance;?>//');
            <?php //endif; ?>
        }

        window.onfocus = function () {
            window.print();
            window.close();
        }

        //setTimeout(funtion(){window.close();},9);
    </script>
    <style type="text/css">
        .style1 {
            font-family: cambria, Agency FB;
            font-size: 22px
        }

        .style9 {
            font-family: cambria;
            font-size: 16px;
        }

        .style2 {
            font-family: calibri;
            font-size: 17px;
        }

        .style5 {
            font-family: calibri;
            font-size: 18px;
        }


        .style3 {
            font-family: calibri;
            font-size: 13px;
        }

        .style10 {
            font-family: calibri;
            font-size: 20px;
        }

        .style15 {
            font-family: calibri;
            font-size: 17px;
        }

    </style>
</head>

<body onFocus="window.close()">
<table align="center" width="338" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
    <tr>
        <td colspan="4" align="center" valign="middle"><span class="style1">
	<?php
    $button = ($button == "CASH SALE") ? "FISCAL RECEIPT" : $button;
    echo $company->name;
    echo "<br><span class=\"style9\"> " . $company->address . " <BR> " . $company->phone . " </span>";
    echo "<br><span class=\"style9\">" . $button . " </span>";
    ?>
	</span>
        </td>
    </tr>

    <tr>
        <td colspan="3" align="left" bordercolor="#000000"><span class="style15">Date:
        <?php $currenttime = date("Y-m-d H:i:s");
        echo $bill_details->created_on; ?>
    </span></td>

        <td colspan="1" align="right" bordercolor="#000000"><span class="style15">
        <?php echo " #:" . $bill_details->id; ?>
    </span></td>
    </tr>
    <tr>
        <td colspan="4" align="center" class="style3">
            ------------------------------------------------------------------------------------
        </td>
    </tr>
</table>
<?php
$total = 0;
$vat = 0;
echo <<<eod

			<table align="center" width="338"  border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                      
					  <thead>
                        <tr>
                          
                          <td align="left" width="63%" class="style9" colspan="2">ITEM</td>
						  <td align="right" width="11%" class="style9">UNIT</td>
                          <td align="center" width="11%" class="style9">QTY</td>						  
                          <td align="Right" width="15%" class="style9">SALE</td>
						  
                        </tr>
                      </thead>
                      <tbody>
					  <tr><td colspan="5" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
eod;
$Discount_amount = 0;
foreach ($receipt as $rows) {
    $pricesold = $rows->selling_price * $rows->qty;
    $vat = $rows->VATS + $vat;
    $total = $total + $pricesold;
    if ($rows->product_name == "DISCOUNT") {
        $Discount_amount = abs($Discount_amount - $pricesold);
    } else {
        echo "
                        <tr>
                          
                          <td class=\"style2\" colspan=\"2\">" . ucwords(strtolower($rows->product_name)) . "</td>
                          <td class=\"style2\" align=\"right\" >" . number_format($rows->selling_price, 2) . "</td>
						  <td class=\"style2\" align=\"center\" >" . $rows->qty . "</td>
                          <td class=\"style2\" align=\"right\">" . number_format($pricesold, 2) . "</td>
						  
                        </tr>";


    }
}
if (!isset($Discount_amount)) {
    $Discount_amount = 0;
}
$sale = $Discount_amount + $total;
$sale = number_format($sale, 2);
$Discount_amount = number_format($Discount_amount, 2);
$tourism_fund = number_format(0.02*$total,2);
echo <<<eod
           </tbody>
					  
					   <tr><td colspan="5" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
					  <tr class="text-primary">
                          <td colspan=4 align="right" class="style2">Sale &nbsp;</td>
                          <td class="style2" align="right" width="30%"> $sale </td>
					  </tr>	
					  <tr class="text-primary">
                          <td colspan=4 align="right" class="style2">Tourism Fund &nbsp;</td>
                          <td class="style2" align="right">  $tourism_fund </td>
					  </tr>
					  <tr class="text-primary">
                          <td colspan=4 align="right" class="style2">Discount &nbsp;</td>
                          <td class="style2" align="right">  $Discount_amount </td>
					  </tr>
					  
					  <tr class="text-primary">	
                          <td colspan=4 align="right" class="style2">Total Sale &nbsp;</td>
                          <td class="style2" align="right"> 	
eod;


//$balance = $total - $amount_paid;
//$amount_paid = number_format($amount_paid);
//$balance = number_format($balance);
echo number_format($total, 2);
$amount_paid = $change_balance + $total;
$amount_paid = number_format($amount_paid, 2);
$change_balance = number_format($change_balance, 2);
$year = Date('Y');
$mpesa_payment_type = $company->mpesa_payment_type;
$mpesa_payment_number = $company->mpesa_payment_number;

//$VAT = 0.16*$total;
echo <<<eod
					</td>
						<tr><td colspan="4"></td><td colspan="2" align="right" class="style3">-------------</td></tr>
                        </tr>
						<tr class="text-primary">
                          <td colspan=4 align="right" class="style9">Amount Paid &nbsp;</td>
                          <td align="right" class="style9"> $amount_paid </td>
					  </tr>
						<tr class="text-primary">
                          <td colspan=4 align="right" class="style9">Balance &nbsp;</td>
                          <td align="right" class="style9"> $change_balance </td>
					  </tr>
					  <tr><td colspan="5" align="center" class="style3">------------------------------------------------------------------------------------</td></tr>
					  <tr><td colspan="5" align="center" class="style3"><b>$mpesa_payment_type: $mpesa_payment_number</b></td></tr>
						<tr>
						<td colspan="5" align="center" valign="middle" bordercolor="#000000" class="style9"><br> Served by:&nbsp;$username
						| Billed To $bill_details->bill_alias
						</td>
					  </tr>
					  <tr>
					    <td colspan="5" align="center" valign="middle" bordercolor="#000000" class="style3">
							Thank you & Welcome again<br>  $company->slogan<br>
							
						</td>
						<tr>
					  <tr>
						<td colspan="5" align="center" valign="middle" bordercolor="#000000" class="style3">
						 <br> $year &copy; SignalPOS <br>info@microvision.co.ke
						</td>
					  </tr>
                    </table>
eod;

?>

</body>
</html>

<!-- **removed VAT from receipt line 143				  
					  <tr><td colspan="2"></td><td colspan="2"><hr></td></tr>
                        </tr>
						<tr class="text-primary">
                          <td colspan=3 align="right" class="style9">VAT &nbsp;</td>
                          <td class="style9"> Ksh $vat </td>
					  </tr> -->
					  