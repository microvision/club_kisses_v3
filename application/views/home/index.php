<script src='<?php echo Template::theme_url('js/jquery-1.10.2.min.js'); ?>' type='text/javascript'></script>
				<div class="header_bg">
						
							<div class="header">

								<div class="head-t">
									<div class="row">
                                        <div class="col-lg-7">
									        <div id="tabs" class="tabs pull-left">
									            <nav class="pull-left"><?php $receipt_url = base_url()."home/receipt_display"; ?>
													<ul>
                                                        <li class="active"> <a href="#new_bill" class="icon-shop" data-toggle="tab" onClick="htmlData2('<?php echo $receipt_url; ?>', 'ch=0')"">New Sale</a></li>
                                                           <?php
                                                              foreach ($user_bills as $rows){
                                                                if ($rows->status==3){$class="<i class=\"fa fa-lock\"></i>";}elseif($rows->status==1){$class=($rows->sale_type==0)?"<i class=\"fa fa-shopping-cart\"></i> ":"<i class=\"fa fa-tags\"></i> ";}else{$class="";}
                                                                    echo "
                                                                       <li> <a class=\"billclick\" href=\"#".$rows->id."\" data-toggle=\"tab\" onClick=\"htmlData2('$receipt_url', 'ch=$rows->id')\"> ".$class."".$rows->bill_alias."</a>
                                                                          </li>";
                                                                }
                                                           ?>
														</ul>
                                                </nav>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="pull-right"><br>
                                                <form class="form-horizontal">
                                                    <div class="form-group">
                                                        <table cellpadding="3" cellspacing="2">
                                                            <tr>
                                                               <td width="50%">
                                                                   <div class="input-group">
                                                                      <input type="text" id="productsearch2" class="form-control pull-right" placeholder="Product Search ..." >
                                                                         <span class="input-group-btn">
                                                                            <button id="box1Clear" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                                                         </span>
                                                                   </div>
                                                               </td>
                                                               <td width="50%">
                                                                        <select id="productsearch" class="form-control pull-right" placeholder="Product Search ...">
                                                                            <option value="all">Filter by Category</option>
                                                                            <?php
                                                                            foreach($product_categories as $rows){
                                                                                echo "<option value=\"".$rows->id."\">".$rows->name."</options>";
                                                                            }
                                                                            ?>
                                                                        </select>
                                                               </td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
							</div>
						</div>
					
				</div>
					<!-- //header-ends -->
				
				<!--content-->
				
			<div class="content">
			<div class="row">
                <div class="col-md-10 col-sm-10">

                    <div class="women_main">
                        <!-- start content -->

                       <div class="w_content">
                           <div class="tab-content">
                               <!--START OF FIRST TAB CREATING BILLS -->
                               <div class="tab-pane active" id="new_bill">
                                   <div class="row">
                                       <div class="col-lg-1">

                                       </div>
                                       <div class="col-lg-10">
                                           <br><br>
                                           <ul class="nav nav-tabs" id="sales_tab">
                                               <li role="presentation" class="active"><a href="#new_client" data-toggle="tab">New Client</a></li>
                                               <li role="presentation"><a href="#corporate_client" data-toggle="tab">Corporate Client</a></li>
                                           </ul>
                                           <div class="tab-content" id="sales_tab">
                                               <!--First tab pane for creating bills -->
                                               <div class="tab-pane active" id="new_client">
                                                   <form method='POST' class="form-vertical" action="<?php echo base_url()."home/new_sale"; ?>" id="normal_sale" name="normal_sale">
                                                       <br><br><br>
                                                       <div class="row">
                                                           <div class="col-lg-2 col-sm-1"></div>
                                                           <div class="form-group col-lg-9 col-sm-9">
                                                               <input type="text" name="bill_alias" placeholder="Enter Client Name...   (Optional)" class="form-control col-lg-5">
                                                               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                                           </div>
                                                           <br>
                                                       </div>

                                                       <div class="footer">
                                                           <div class="row">
                                                               <div class="col-lg-3"></div>
                                                               <div class="col-lg-7">
                                                                   <button class="btn btn-lg btn-danger btn-block btn-grad" type="submit" name="submit" value="new_sale">Create New Sale</button>
                                                               </div>
                                                               <div class="col-lg-2">

                                                               </div>
                                                           </div>


                                                       </div>
                                                   </form>
                                               </div>
                                               <div class="tab-pane" id="corporate_client">

                                                   <form method='POST' class="form-horizontal" id="corporate_sale" name="corporate_sale" action="<?php echo base_url()."home/new_sale"; ?>"  class="form-signin">
                                                       <br><br><br>

                                                       <div class="form-group">
                                                           <label class="control-label col-lg-3">Corporation</label>
                                                           <div class="col-lg-7">
                                                               <select name="client" class="form-lg form-control" required="">
                                                                   <option disabled selected value="">Select Corporation</option>
                                                                   <?php foreach($clients as $client):?>
                                                                       <option value="<?php echo $client->id; ?>"><?php echo $client->name; ?></option>
                                                                   <?php endforeach;?>
                                                               </select>
                                                               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                                                           </div>
                                                       </div>
                                                       <!--                                                               <div class="form-group">-->
                                                       <!--                                                                   <label class="control-label col-lg-3">Client Name</label>-->
                                                       <!--                                                                   <div class="col-lg-7">-->
                                                       <!--                                                                       <input type="text" name="bill_name" placeholder="Enter Patient Name..." class="form-lg form-control" required="">-->
                                                       <!--                                                                   </div>-->
                                                       <!--                                                               </div>-->
                                                       <!--                                                               <div class="form-group">-->
                                                       <!--                                                                   <label class="control-label col-lg-3">Client Number</label>-->
                                                       <!--                                                                   <div class="col-lg-7">-->
                                                       <!--                                                                       <input type="text" name="employee_number" placeholder="Enter employee number... " class="form-lg form-control" required="">-->
                                                       <!--                                                                   </div>-->
                                                       <!--                                                               </div>-->
                                                       <!--                                                               <div class="form-group">-->
                                                       <!--                                                                   <label class="control-label col-lg-3">Client Type</label>-->
                                                       <!--                                                                   <div class="col-lg-7">-->
                                                       <!--                                                                       <select name="beneficiary" class="form-lg form-control" required="">-->
                                                       <!--                                                                           <option value="">Select patient type</option>-->
                                                       <!--                                                                           <option value="1">Beneficiary</option>-->
                                                       <!--                                                                           <option value="0">Dependant</option>-->
                                                       <!--                                                                       </select>-->
                                                       <!--                                                                   </div>-->
                                                       <!--                                                               </div>-->

                                                       <div class="footer">
                                                           <div class="row">
                                                               <div class="col-lg-3"></div>
                                                               <div class="col-lg-7">
                                                                   <button class="btn btn-lg btn-danger btn-block btn-grad" type="submit" name="submit" value="new_sale">Create New Sale</button>
                                                               </div>
                                                               <div class="col-lg-2">

                                                               </div>
                                                           </div>
                                                       </div>
                                                   </form>


                                               </div>


                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <!--END OF FIRST TAB CREATING BILLS -->
                               <!--START OF SECOND USER BILLS -->
                                <?php
                                    $keypad_url = base_url()."keypad_modal";
                                    foreach ($user_bills as $rows_bills){
                                        echo "
                                            <div class=\"tab-pane billtab\" id=\"".$rows_bills->id."\">
                                                <br />
                                                <b>Loading...</b>						
                                                <br />
                                            </div>";

                                    }
                                ?>
                               <!--END OF SECOND USER BILLS -->

                           </div>

                        </div>

                       <div class="clearfix"></div>
                        <!-- end content -->


                    </div>

                </div>
                <?php $url = base_url()."home/add_quantity"; ?>
                <div id="txtResult" class="modal fade in"> </div>
                <div class="col-md-2 col-sm-2" id="txtResult2">
<!--                    <form method="POST" class="form-vertical" action="--><?php //echo base_url()."home/new_sale"; ?><!--" id="quick_sale" name="quick_sale">-->
<!--                        <input type="hidden" name="bill_name" value="">-->
<!--                        <input type="hidden" name="--><?php //echo $this->security->get_csrf_token_name(); ?><!--" value="--><?php //echo $this->security->get_csrf_hash(); ?><!--" >-->
<!--                        <button type="submit" name="submit" class="btn btn-danger btn-block">Create New Sale</button>-->
<!--                    </form>-->
                    <hr>
                    <div class="clearfix"></div>


                </div>
            </div>
</div>

			<!--content-->
		</div>
</div>
				<!--//content-inner-->

                <script>
                    var siteurl = "<?php echo base_url(); ?>";
                    $(document).ready(function() {
                        $.ajaxSetup({
                            async: true
                        });

                        $(".billclick").click(function() {
                            var billhref = $(this).attr("href");
                            var billid = billhref.substr(1);
                            $("#productsearch2").val(" ");
                            $.ajax({
                                type: "GET",
                                url: siteurl + "home/product_display_type/" + billid,
                                success: function(data) {
                                    $("#" + billid).html(data);
                                    $("#holder" + billid).jPages({
                                        containerID: "dataTable"+billid,
                                        perPage: 18,
                                        midRange: 15
                                    });
                                }
                            });
                        });

                        $("#productsearch2").keyup(function(){
                            var catid = $(this).val();
                            var billid = $('.billtab.active').attr("id");
                            $("#" + billid).html("<br>Loading...<br>");
                            $.ajax({
                                type: "GET",
                                url: siteurl + "home/product_display_type/" + billid + "/" + catid,
                                success: function(data) {
                                    $("#" + billid).html(data);
                                    $("#holder" + billid).jPages({
                                        containerID: "dataTable"+billid,
                                        perPage: 18,
                                        midRange: 15
                                    });
                                }
                            });
                        });

                        $("#productsearch").change(function(){
                            var catid = $(this).val();
                            var billid = $('.billtab.active').attr("id");
                            $("#" + billid).html("<br>Loading...<br>");
                            $.ajax({
                                type: "GET",
                                url: siteurl + "home/product_display/" + billid + "/" + catid,
                                success: function(data) {
                                    $("#" + billid).html(data);
                                    $("#holder" + billid).jPages({
                                        containerID: "dataTable"+billid,
                                        perPage: 18,
                                        midRange: 15
                                    });
                                }
                            });
                        });


                        dashboard();


                    });
                </script>