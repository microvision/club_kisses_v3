<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Bonfire
 *
 * An open source project to allow developers to jumpstart their development of
 * CodeIgniter applications.
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2014, Bonfire Dev Team
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

/**
 * Home controller
 *
 * The base controller which displays the homepage of the Bonfire site.
 *
 * @package    Bonfire
 * @subpackage Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Home extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('users/auth');
        $this->auth->restrict();
		$this->load->helper('application');
		$this->load->library('Template');
		$this->load->library('Assets');
		$this->lang->load('application');
		$this->load->library('events');

        $this->load->library('installer_lib');
        if (! $this->installer_lib->is_installed()) {
            $ci =& get_instance();
            $ci->hooks->enabled = false;
            redirect('install');
        }

        $this->load->model('pos_model');
        $this->load->model('clients_model');
        $this->load->model('inventory_model');
        $this->load->model('sales_model');
        $this->load->model('categories_model');
        $this->load->model('client_accounts_model');
        // Make the requested page var available, since
        // we're not extending from a Bonfire controller
        // and it's not done for us.
        $this->requested_page = isset($_SESSION['requested_page']) ? $_SESSION['requested_page'] : null;
	}

	//--------------------------------------------------------------------

	/**
	 * Displays the homepage of the Bonfire app
	 *
	 * @return void
	 */
	public function index($type=null){

        //delete all open sessions that are more than 1hour and do not have any entries
        $old_date = date('Y-m-d H:m:s',strtotime('-1 hours'));
        $null_bills = $this->pos_model->get_null_bills($old_date);
        Template::set('type',$type);
        if($null_bills){
            foreach($null_bills as $rows){
               $this->pos_model->delete_null_bills($rows->id);
            }
        }
	    //$this->load->library('users/auth');
		$this->set_current_user();
        $year=date('Y');
        $month=date('m');
        Template::set('products', $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
            ->find_all());
        $now_date = date('Y-m-d H:i:s');
        Template::set('promo_products', $this->pos_model->get_promo_products($now_date));
        Template::set('categories', $this->categories_model->order_by('name','asc')
            ->find_all());
        Template::set('clients', $this->clients_model->order_by('name','asc')
            ->find_all());
        Template::set('user_bills', $this->pos_model->get_user_bills($this->current_user->id));
        Template::set('product_categories', $this->pos_model->get_product_categories());
        Template::set_theme('default');
        Template::set('page_title', 'Point Of Sale');
		Template::render();
	}//end index()
    public function manual_receipt($type=null){

        //delete all open sessions that are more than 1hour and do not have any entries
        $old_date = date('Y-m-d H:m:s',strtotime('-1 hours'));
        $null_bills = $this->pos_model->get_null_bills($old_date);
        Template::set('type',$type);
        if($null_bills){
            foreach($null_bills as $rows){
                $this->pos_model->delete_null_bills($rows->id);
            }
        }
        //$this->load->library('users/auth');
        $this->set_current_user();
        $year=date('Y');
        $month=date('m');
        Template::set('products', $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
            ->find_all());
        Template::set('categories', $this->categories_model->order_by('name','asc')
            ->find_all());
        Template::set('clients', $this->clients_model->order_by('name','asc')
            ->find_all());
        Template::set('user_bills', $this->pos_model->get_user_bills($this->current_user->id));
        Template::set('product_categories', $this->pos_model->get_product_categories());
        Template::set_theme('default');
        Template::set('page_title', 'Point Of Sale');
        Template::render();
    }//end index()
    public function cashier(){
        $this->set_current_user();
        $year=date('Y');
        $month=date('m');
        Template::set('products', $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
            ->find_all());
        Template::set('categories', $this->categories_model->order_by('name','asc')
            ->find_all());
        Template::set('clients', $this->clients_model->order_by('name','asc')
            ->find_all());
        Template::set('user_bills', $this->pos_model->get_cashier_bills());
        Template::set_theme('default');
        Template::set('page_title', 'Point Of Sale');
        Template::render();
    }
    public function new_sale()
    {

        $this->set_current_user();
        $bill_name=$_POST['bill_name'];
        $user_id = $this->current_user->id;
        $last_bill = $this->pos_model->get_last_bill();
        if($_POST['submit']=='receipted_sale'){
            $sale_type=1;
            $time=date('H:i:s');
            $created_on = date('Y-m-d H:i:s',strtotime($_POST['receipt_date']." ".$time));
            $receipt = $_POST['receipt_number'];

        }else{
            $sale_type=0;
            $created_on = date('Y-m-d H:i:s');
            $receipt = "";
        }
        if((!ISSET($bill_name)) or ($bill_name=="")){
            if($_POST['client'] and $_POST['client']!="") {
                $client = $this->clients_model->as_object()->find($_POST["client"]);

                //corporate client bill
                $data = array(
                    'user_id' => $user_id,
                    'bill_alias' => $client->name,
                    'bill_type' => 2,
                    'sale_type' => $sale_type,
                    'client_id' => $_POST["client"],
                    'manual_receipt' => $receipt,
                    'billing_time' => $created_on,
                    'status' => 1
                );
            }else {
                if($_POST['bill_alias'] and $_POST['bill_alias']!=""){
                    $bill_name = $_POST['bill_alias'];
                }else{
                    if ((!ISSET($last_bill->bill_alias)) or ($last_bill->bill_alias == "")) {
                        //first bill being created or resenting from a named bill
                        $billAlias = 1;
                        $bill_name = "Sale " . $billAlias;
                    } else if ($last_bill->bill_alias == 199) {
                        //maximum biils reset back to zero
                        $billAlias = 1;
                        $bill_name = "Sale " . $billAlias;
                    } else {
                        //normal counting of bills
                        $billAlias = $last_bill->bill_alias + 1;
                        $bill_name = "Sale " . $billAlias;
                    }
                }

                $data = array(
                    'user_id' => $user_id,
                    'bill_alias' => $bill_name,
                    'bill_type' => 1,
                    'sale_type' => $sale_type,
                    'manual_receipt' => $receipt,
                    'billing_time' => $created_on,
                    'status' => 1
                );
            }
            $this->pos_model->insert($data);
        }

        redirect('home');
    }
    public function product_display($billid, $catid = null)
    {
        if($catid == null) {
            $data['products'] = $this->pos_model->get_products();

        } else {
            $data['products'] = $this->pos_model->get_cat_products($catid);
        }
        $data['billid'] = $billid;
        $now_date = date('Y-m-d H:i:s');
        $data['promo_products'] = $this->pos_model->get_promo_products($now_date);
        $data['daily_promo_products'] = $this->pos_model->get_daily_promo_products($now_date);
		$data['product_categories'] = $this->pos_model->get_product_categories();
        $this->load->view("home/product_display", $data);
    }
    public function product_display_type($billid, $catid = null)
    {
        if($catid == null) {
            $data['products'] = $this->pos_model->get_products();
        } else {
            $data['products'] = $this->pos_model->get_cat_products_type($catid);
        }
        $data['billid'] = $billid;
        $now_date = date('Y-m-d H:i:s');
        $data['promo_products'] = $this->pos_model->get_promo_products($now_date);
        $data['daily_promo_products'] = $this->pos_model->get_daily_promo_products($now_date);
		$data['product_categories'] = $this->pos_model->get_product_categories();
        $this->load->view("home/product_display", $data);
    }
    public function receipt_display()
    {
        $billid = $this->input->get('ch');
        if($billid!=0) {

            $data['bill_details'] = $this->pos_model->as_object()->find($billid);
            $data['receipt'] = $this->pos_model->get_receipt($billid);
            $this->load->view("home/receipt_display", $data);
        }
    }
    public function keypad_modal(){

        $this->set_current_user();
        $data['current_user'] = $this->current_user->id;
        //checks if the quantity has been submitted
        if(count($_POST) != 0) {

            //redirect('services/manage_services');
        }else{
            //loads the keypad modal if there has been no submission
            $billid = $this->input->get('ch');
            $productid = $this->input->get('ch1');
            $user_id = $this->session->userdata('user_id');
            //check if the product is available on stock
            $stock_check = $this->inventory_model->stock_check($productid);
            $data['product_details'] = $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                ->find_by(array("bf_vision_products.id" => $productid));
            $data['selling_price'] = $this->input->get('ch4');
            $data['bill_details'] = $this->pos_model->get_bill($billid);
            $data['checkedValue'] = $stock_check->active_stock;
            $this->load->view("home/keypad_modal",$data);
        }
    }
    public function sell_product(){
        $billid = $this->input->get('ch');
        $productid = $this->input->get('ch1');
        $quantity = $this->input->get('ch3');
        $selling_price = $this->input->get('ch4');
        $product_details = $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                            ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                            ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                            ->find_by(array("bf_vision_products.id" => $productid));
        $bill_details = $this->pos_model->as_object()->find($billid);
        //check if a product quantity is entered
        if($quantity){
            //we check if the quantity requested is available
            $stock_check = $this->inventory_model->stock_check($productid);
            //we calculate VAT if required
            if($product_details->isvatable==1){$VAT=0.14*(($selling_price-$stock_check->buying_price)*$quantity);}else{$VAT=0;}
            $checkedValue = $stock_check->active_stock-$quantity;
            $activestock = $this->inventory_model->get_active_stock($productid);
            if ($checkedValue >= 0){
                //get active stock

                //check if the value of the active stock is more than the current active stock
                if((!ISSET($activestock->current_qty)) or ($activestock->current_qty  < $quantity)){

                    $buyquantity=$quantity;

                    do{
                        $activestock = $this->inventory_model->get_active_stock($productid);
                        $stock_check = $this->inventory_model->stock_check($productid);
                        $currentquantity=0;
                        if(isset($activestock->current_qty)){
                            $currentquantity = $activestock->current_qty-$buyquantity;
                        }


                        //$buyquantity = $buyquantity - $activestock->current_qty;
                        if ($stock_check->active_stock==0){
                            //there is no stock
                            $x=1;
                        }else if($currentquantity <= 0){
                            //check if the current stock is enough to cover the remaining sale
                            if(isset($activestock->current_qty) && $activestock->current_qty >= $buyquantity) {
                                $status=1;
                                $this->inventory_model->update_stock($activestock->id, abs($currentquantity),$status);
                                $x=1;
                            }else{
                                $status = 2;
                                //deactivate current stock as complete
//                                if $activestock is null we are on the first stock
                                if(isset($activestock->id)) {
                                    $this->inventory_model->update_stock($activestock->id, 0, $status);
                                }
                                //activate the next stock
                                $latestInactiveStock = $this->inventory_model->get_latest_inactivestockdate($productid);
                                if(ISSET($latestInactiveStock->purchasedate) and $latestInactiveStock->purchasedate!='') {
                                    $this->inventory_model->activate_stock($productid, $latestInactiveStock->purchasedate);
                                }
                                $x=0;
                            }
                            //$buyquantity = $currentquantity - (2 * $currentquantity);
                            $buyquantity = abs($currentquantity);


                        }else{
                            $status = 1;
                            $this->inventory_model->update_stock($activestock->id, $currentquantity,$status);
                            $x=1;
                        }

                    }while($x==0);
                    //adjust selling price for stock transfer
                    if($bill_details->bill_type==3){$selling_price=$stock_check->buying_price;}
                    $data_sale = array(
                        'product_id' => $productid,
                        'bill_id' => $billid,
                        'stock_shop_id' => $activestock->id,
                        'selling_price' => $selling_price,
                        'qty' => $quantity,
                        'vat' => $VAT,
                        'status' => 1
                    );
                    $this->sales_model->insert($data_sale);

                }else{

                    //if it is less we update the current stock value and save the sale
                    if($activestock->current_qty == $quantity){
                        $status = 2;
                        //activate the next stock
                        $latestInactiveStock = $this->inventory_model->get_latest_inactivestockdate($productid);
                        if(ISSET($latestInactiveStock->purchasedate) and $latestInactiveStock->purchasedate!='') {
                            $this->inventory_model->activate_stock($productid, $latestInactiveStock->purchasedate);
                        }
                    }else{
                        $status = 1;
                    }
                    //$activestock = $this->pos_model->get_active_stock($productid);
                    $currentquantity = $activestock->current_qty-$quantity;
                    $this->inventory_model->update_stock($activestock->id, $currentquantity,$status);
                    //adjust selling price for stock transfer
                    if($bill_details->bill_type==3){$selling_price=$activestock->buying_price;}
                    $data_sale = array(
                        'product_id' => $productid,
                        'bill_id' => $billid,
                        'stock_shop_id' => $activestock->id,
                        'selling_price' => $selling_price,
                        'qty' => $quantity,
                        'vat' => $VAT,
                        'status' => 1
                    );
                    $this->sales_model->insert($data_sale);


                }
            }
            //redirect to buy interface
            $data['bill_details'] = $this->pos_model->get_bill($billid);
            $data['receipt'] = $this->pos_model->get_receipt($billid);
            $this->load->view("home/receipt_display", $data);
        }else{
            $data['checkedValue'] = 0;
            $data['product_details'] = $this->inventory_model->join('bf_vision_product_categories','bf_vision_product_categories.id=category_id','left')
                                        ->join('bf_vision_retail_units','bf_vision_retail_units.id=unit_id','left')
                                        ->select('bf_vision_products.*,bf_vision_product_categories.name as category,unit_name')
                                        ->find_by(array("bf_vision_products.id" => $productid));
            $this->load->view("home/keypad_modal",$data);
        }
    }
    public function balance(){
        $total=$this->input->get("ch");
        $amount_paid=$this->input->get("ch2");
        if($amount_paid>=$total){
            $balance = $amount_paid-$total;
            echo "<form id=\"balanc\"><input type=\"hidden\" value=\"".$balance."\" name=\"balance\" id=\"balance\" ></form>Balance: Ksh ".number_format($balance,2);
        }
    }
    public function receipt(){
        $this->set_current_user();
        $billid = $this->input->get('billid');
        $button = $this->input->get('button');
        $amount=$this->input->get('amount');
        $balance=$this->input->get('balance');
        if(!ISSET($balance)){$balance=0;}
        //check if it a cooperate sale and process transactions for corporate clients
        $bill_details = $this->pos_model->get_bill($billid);
        if($bill_details->bill_type==2){
            //check if transaction has been entered
            $saved_transaction = $this->client_accounts_model->get_sale_transaction($bill_details->id, $bill_details->client_id);
            if($saved_transaction){
                //we update the existing transaction
                $data_client_transaction = array(
                    'client_id' => $bill_details->client_id,
                    'transaction_no' => $bill_details->id,
                    'amount' => $bill_details->total,
                    'transaction_type' => 2
                );
                $this->client_accounts_model->update($saved_transaction->id,$data_client_transaction);
            }else{
               //create a new transaction
                $data_client_transaction = array(
                    'client_id' => $bill_details->client_id,
                    'transaction_no' => $bill_details->id,
                    'amount' => $bill_details->total,
                    'transaction_type' => 2
                );
                $this->client_accounts_model->insert($data_client_transaction);
            }
        }
        if ($button == "INVOICE"){
            $data["change_balance"] = $balance;
            $data['bill_details'] = $this->pos_model->get_bill($billid);
            $data['receipt'] = $this->pos_model->get_receipt($billid);
            $data['button'] = $button;
            $data['company'] = $this->pos_model->get_company();
            $data['username'] =  $this->current_user->display_name;
            $this->pos_model->updatebill_invoiced($billid,5);
            $data['amount_paid'] = $amount;
            $this->load->view("home/print_receipt", $data);
//            redirect('',true);
        }else if ($button == "FISCAL RECEIPT"){
            $data["change_balance"] = $balance;
            $data['bill_details'] = $this->pos_model->get_bill($billid);
            $data['receipt'] = $this->pos_model->get_receipt($billid);
            $data['button'] = "CASH SALE";
            $data['company'] = $this->pos_model->get_company();
            $data['username'] = $this->current_user->display_name;
            if($_GET['pay']=='mpesa'){
                $this->pos_model->updatebill_paid($billid,2,2);
            }else{
                $this->pos_model->updatebill_paid($billid,1,2);
            }
            $this->load->view("home/print_receipt", $data);
        }else if ($button == "CREDIT"){
            $data["change_balance"] = $balance;
            $data['bill_details'] = $this->pos_model->get_bill($billid);
            $data['receipt'] = $this->pos_model->get_receipt($billid);
            $data['button'] = "INVOICE";
            $data['company'] = $this->pos_model->get_company();
            $data['username'] = $this->current_user->display_name;
            $this->pos_model->updatebill_paid($billid,3,3);
            $this->load->view("home/print_receipt", $data);
        }else if ($button == "INVOICE2"){
            $data["change_balance"] = $balance;
            $data['bill_details'] = $this->pos_model->get_bill($billid);
            $data['receipt'] = $this->pos_model->get_receipt($billid);
            $data['button'] = "CASH SALE";
            $data['company'] = $this->pos_model->get_company();
            $data['username'] = $this->current_user->display_name;
            $data['amount_paid'] = $amount;
            $invoice_time = date('Y-m-d H:i:s');
            $this->pos_model->updatebill_paid_tablet($billid,5,$this->session->userdata('user_id'),$balance,$invoice_time);
            redirect("pos/index");
        }else if ($button == "DISCOUNT"){

        }

    }
    public function receipt2(){
        $this->set_current_user();
        $billid = $this->input->get('billid');
        $button = $this->input->get('button');
        $amount=$this->input->get('amount');
        $balance=$this->input->get('balance');
        if(!ISSET($balance)){$balance=0;}
        //check if it a cooperate sale and process transactions for corporate clients
        $data["change_balance"] = $balance;
        $data['bill_details'] = $this->pos_model->get_bill($billid);
        $data['receipt'] = $this->pos_model->get_receipt($billid);
        $data['button'] = "RECEIPT COPY";
        $data['company'] = $this->pos_model->get_company();
        $data['username'] = $this->current_user->display_name;
        $this->load->view("home/print_receipt_copy", $data);
    }
    public function reverse_sale($bill_id){
        $this->pos_model->updatebill_invoiced($bill_id,1);
        redirect ("home/cashier");
    }
    public function discount() {
        $billid = $this->input->get('ch');
        $productid = $this->input->get('ch1');
        $quantity = $this->input->get('ch3');
        $selling_price = $this->input->get('ch4');
        $data_sale = array(
            'product_id' => $productid,
            'bill_id' => $billid,
            'stock_shop_id' => 0,
            'selling_price' => $selling_price,
            'qty' => $quantity,
            'vat' => 0,
            'status' => 1
        );
        $this->sales_model->insert($data_sale);
        //redirect to buy interface
        $data['bill_details'] = $this->pos_model->get_bill($billid);
        $data['receipt'] = $this->pos_model->get_receipt($billid);
        $this->load->view("home/receipt_display", $data);
    }
    public function delete_item(){
        $billid = $this->input->get('ch');
        $saleId = $this->input->get('ch2');

        //we get details of the sale
        $sale_details = $this->sales_model->as_object()->find($saleId);
        //get current stock value of item
        $current_stock_value = $this->inventory_model->get_stock_value($sale_details->product_id);
        if(ISSET($current_stock_value->current_qty)){
            //update the current active stock for the item
            $new_stock_value = $current_stock_value->current_qty + $sale_details->qty;
        }else{
            //if there is no active stock
            $new_stock_value=$sale_details->qty;
            $current_stock_value = $this->inventory_model->get_last_inactive_stock($sale_details->product_id);
            //activate the last stock
            $this->inventory_model->update_last_inactive_stock($current_stock_value->id);
        }
        $this->inventory_model->update_stock_value($current_stock_value->id, $new_stock_value);
        //delete the item sale
        $this->sales_model->delete($saleId);
        //check if its corporate sale and update the transaction amount
        $bill_details = $this->pos_model->get_bill($sale_details->bill_id);
        if($bill_details->bill_type==2){
            //check if transaction has been entered
            $saved_transaction = $this->client_accounts_model->get_sale_transaction($bill_details->id, $bill_details->client_id);
            if($saved_transaction) {
                //we update the existing transaction
                $data_client_transaction = array(
                    'client_id' => $bill_details->client_id,
                    'transaction_no' => $bill_details->id,
                    'amount' => $bill_details->total,
                    'transaction_type' => 2
                );
                $this->client_accounts_model->update($saved_transaction->id,$data_client_transaction);
            }
        }

        //redirect to buy interface
        log_activity($this->auth->user_id(),"Returned sale items for product_id ".$sale_details->product_id." from sale receipt no ".$billid, 'POS');
        $data['bill_details'] = $this->pos_model->get_bill($billid);
        $data['receipt'] = $this->pos_model->get_receipt($billid);
        $this->load->view("home/receipt_display", $data);

    }
    public function receipt_search()
    {
        $this->set_current_user();
        $billid=$_POST['receipt_number'];
        Template::set('bill_details', $this->pos_model->get_bill($billid));
        Template::set('receipt', $this->pos_model->get_receipt($billid));
        Template::set('bill_id', $billid);
        Template::set_theme('default');
        Template::set('page_title', 'Receipt Search');
        Template::render();
    }
    public function get_bill_details(){
        $billid=$this->input->post("bill");
        $data['bill']=$billid;
        $data['bill_details'] = $this->pos_model->get_bill($billid);
        $data['receipt'] = $this->pos_model->get_receipt($billid);
        $this->load->view("home/receipt_display", $data);
    }
    public function activate_receipt() {
        $bill_id = $_GET['d'];
        //updating the bill status to unlock
        $this->pos_model->unlock_bill($bill_id);
        log_activity($this->auth->user_id(),"Reactivated sale for receipt no ".$bill_id, 'POS');
        redirect("home/index");
    }

	//--------------------------------------------------------------------

	/**
	 * If the Auth lib is loaded, it will set the current user, since users
	 * will never be needed if the Auth library is not loaded. By not requiring
	 * this to be executed and loaded for every command, we can speed up calls
	 * that don't need users at all, or rely on a different type of auth, like
	 * an API or cronjob.
	 *
	 * Copied from Base_Controller
	 */
	protected function set_current_user()
	{
        if (class_exists('Auth')) {
			// Load our current logged in user for convenience
            if ($this->auth->is_logged_in()) {
				$this->current_user = clone $this->auth->user();

				$this->current_user->user_img = gravatar_link($this->current_user->email, 22, $this->current_user->email, "{$this->current_user->email} Profile");

				// if the user has a language setting then use it
                if (isset($this->current_user->language)) {
					$this->config->set_item('language', $this->current_user->language);
				}
            } else {
				$this->current_user = null;
			}

			// Make the current user available in the views
            if (! class_exists('Template')) {
				$this->load->library('Template');
			}
			Template::set('current_user', $this->current_user);
		}
	}
}
/* end ./application/controllers/home.php */
