-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: pos_v3_club_kisses
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bf_vision_events`
--

DROP TABLE IF EXISTS `bf_vision_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bf_vision_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` text COLLATE utf8_unicode_ci,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bf_vision_events`
--

LOCK TABLES `bf_vision_events` WRITE;
/*!40000 ALTER TABLE `bf_vision_events` DISABLE KEYS */;
INSERT INTO `bf_vision_events` VALUES (1,NULL,NULL,NULL,1,0,'2021-12-12 06:58:54',1,NULL,NULL),(2,NULL,NULL,NULL,1,0,'2021-12-12 06:59:10',1,NULL,NULL),(3,NULL,NULL,NULL,1,0,'2021-12-12 06:59:24',1,NULL,NULL),(4,'Ed','2021-12-13 10:32:00','2021-12-13 10:32:00',1,1,'2021-12-12 06:59:47',1,NULL,NULL);
/*!40000 ALTER TABLE `bf_vision_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bf_vision_expense_accounts`
--

DROP TABLE IF EXISTS `bf_vision_expense_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bf_vision_expense_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bf_vision_expense_accounts`
--

LOCK TABLES `bf_vision_expense_accounts` WRITE;
/*!40000 ALTER TABLE `bf_vision_expense_accounts` DISABLE KEYS */;
INSERT INTO `bf_vision_expense_accounts` VALUES (1,'Nyalenda Hardware',1,'2019-02-06 00:00:00',1,NULL,NULL),(2,'Club Kisses',1,'2020-02-18 00:00:00',1,NULL,NULL),(3,'Office Expenses',1,'2020-02-18 00:00:00',1,NULL,NULL);
/*!40000 ALTER TABLE `bf_vision_expense_accounts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-12 10:51:19
