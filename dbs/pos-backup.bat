@ECHO OFF

set TIMESTAMP=%DATE:~10,4%%DATE:~4,2%%DATE:~7,2%

REM Export all databases into file E:\scheduled_db_backups\pos_db.[year][month][day].sql
"F:\signal\bin\mysql\mysql5.6.17\bin\mysqldump.exe" --databases pos_v3 --result-file="F:\scheduled_db_backups\pos_db.%TIMESTAMP%.sql" --user=root --password=

REM Change working directory to the location of the DB dump file.
f:
CD \scheduled_db_backups 

REM Compress DB dump file into CAB file (use "EXPAND file.cab" to decompress).
MAKECAB "pos_db.%TIMESTAMP%.sql" "pos_db.%TIMESTAMP%.sql.cab"

REM Delete uncompressed DB dump file.
DEL /q /f "pos_db.%TIMESTAMP%.sql"