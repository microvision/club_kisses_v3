USE pos_v3;
TRUNCATE bf_vision_bills;
TRUNCATE bf_vision_bill_payments;
TRUNCATE bf_vision_corporate_bills;
TRUNCATE bf_vision_corporate_client_transactions;
TRUNCATE bf_vision_corporate_invoices;
TRUNCATE bf_vision_distributor_orders;
TRUNCATE bf_vision_distributor_order_items;
TRUNCATE bf_vision_distributor_transactions;
TRUNCATE bf_vision_expenses;
TRUNCATE bf_vision_sales;
TRUNCATE bf_vision_shop_orders;
TRUNCATE bf_vision_supplier_transactions;
TRUNCATE bf_vision_supply_orders;
TRUNCATE bf_vision_supply_payments;
TRUNCATE bf_vision_product_categories;


TRUNCATE pos_v3.bf_vision_products;
INSERT INTO bf_vision_products (id, product_name, dose, category_id, description, trade_price, unit_id, retail_price, wholesale_price, distribution_price, image_url, reorder_level, store_reorder, barcode, isvatable, status, created_on, created_by, modified_by, modified_date) VALUES
(0, 'DISCOUNT', NULL, 0, 'DISCOUNT', NULL, 1, -1, -1, 0, NULL, 0, NULL, NULL, 0, 1, '2019-07-21 00:00:00', 1, NULL, NULL);
UPDATE bf_vision_products SET id=0 WHERE product_name='DISCOUNT';
ALTER TABLE bf_vision_products AUTO_INCREMENT = 1;
INSERT INTO pos_v3.bf_vision_products 
(id,product_name,category_id,description,trade_price,unit_id,retail_price,wholesale_price,distribution_price,reorder_level,store_reorder,isvatable,status,created_on,created_by)
SELECT itemId,item_name,category_fk,item_name,buying_price,unit_measureID_FK,buying_price,wholesale_price,buying_price,reorder_level,bar_reorder_level,VAT,status,CURDATE(),1 FROM club_kisses.items;
DELETE FROM bf_vision_products WHERE id=1;

TRUNCATE pos_v3.bf_vision_stock_store;
ALTER TABLE pos_v3.bf_vision_stock_store CHANGE batch_no batch_no VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
INSERT INTO pos_v3.bf_vision_stock_store 
(product_id,purchased_qty,current_qty,supply_order_id,purchased_date,buying_price,supplier_id,status,created_on,created_by)
SELECT itemId_fk,purchasedQuantity,currentQuantity,1,purchaseDate,buying_price,supplierId_fk,status,CURDATE(),1 FROM club_kisses.store;

TRUNCATE pos_v3.bf_vision_stock_shop;
INSERT INTO pos_v3.bf_vision_stock_shop 
(product_id,store_stock_id,purchased_qty,current_qty,purchased_date,buying_price,store_order_id,status,created_on,created_by)
SELECT sh.itemId_fk,sh.storeId_fk,sh.purchasedQuantity,sh.currentQuantity,sh.purchaseDate,sh.buying_price,1,sh.status,CURDATE(),1 FROM club_kisses.stock sh LEFT JOIN signal_pos.store st ON sh.storeId_fk=st.storeId_pk;

INSERT INTO pos_v3.bf_vision_shop_orders (id, order_date, comments, approved_date, approved_by, status, created_by, created_on, modified_by, modified_on) VALUES (NULL, '2020-12-24 12:15:56', 'Initial Stocks', '2020-12-24 12:15:56', '1', '1', '1', '2020-12-24 12:15:56', NULL, NULL);

TRUNCATE pos_v3.bf_vision_product_categories;
INSERT INTO pos_v3.bf_vision_product_categories
(id,name,status,created_on,created_by)
SELECT categoryId,category_name,IF(category_status = 0, 1, 0) as status,CURDATE(),1 FROM signal_pos.item_categories;

TRUNCATE pos_v3.bf_vision_retail_units;
INSERT INTO pos_v3.bf_vision_retail_units 
(id,unit_name,status,created_on,created_by)
SELECT unitID_PK,unit_measure,IF(measurement_status = 0, 1, 0) as status,CURDATE(),1 FROM signal_pos.measurements;

UPDATE bf_vision_suppliers SET id = '0' WHERE bf_vision_suppliers.id = 4;
UPDATE bf_vision_suppliers SET id = '4' WHERE bf_vision_suppliers.id = 1;
UPDATE bf_vision_suppliers SET id = '1' WHERE bf_vision_suppliers.id = '0';
UPDATE bf_vision_products SET status=3 WHERE status=0;
UPDATE bf_vision_products SET status=0 WHERE status=1;
UPDATE bf_vision_products SET status=1 WHERE status=3;
