-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 20, 2021 at 11:59 AM
-- Server version: 8.0.21
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_v3_club_kisses`
--

-- --------------------------------------------------------

--
-- Table structure for table `bf_vision_rooms`
--

DROP TABLE IF EXISTS `bf_vision_rooms`;
CREATE TABLE IF NOT EXISTS `bf_vision_rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci,
  `price` double NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint NOT NULL DEFAULT '1' COMMENT '0:disabled, 1:active',
  `occupancy` tinyint DEFAULT '0' COMMENT '0:vacant, 1:occupied',
  `level` text COLLATE utf8_unicode_ci,
  `created_on` datetime NOT NULL,
  `created_by` int NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='hotel rooms';

-- --------------------------------------------------------

--
-- Table structure for table `bf_vision_room_allocation`
--

DROP TABLE IF EXISTS `bf_vision_room_allocation`;
CREATE TABLE IF NOT EXISTS `bf_vision_room_allocation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_id` int NOT NULL,
  `payment_mode` tinyint NOT NULL DEFAULT '1' COMMENT '1:cash, 2:mpesa, 3:credit, 4: cheque, 5:debit-card, 6:cheque',
  `national_id` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupant_name` text COLLATE utf8_unicode_ci,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `check_in` datetime NOT NULL,
  `check_out` datetime NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '0:reserved,1:checked in,2:checked out, -1 cancelled',
  `cost` double NOT NULL,
  `guests` tinyint DEFAULT NULL COMMENT 'number of guests',
  `created_on` datetime NOT NULL,
  `created_by` int NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_allocation` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='allocation of hotel rooms';

-- --------------------------------------------------------

--
-- Table structure for table `bf_vision_room_categories`
--

DROP TABLE IF EXISTS `bf_vision_room_categories`;
CREATE TABLE IF NOT EXISTS `bf_vision_room_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` tinytext COLLATE utf8_unicode_ci,
  `status` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bf_vision_room_levels`
--

DROP TABLE IF EXISTS `bf_vision_room_levels`;
CREATE TABLE IF NOT EXISTS `bf_vision_room_levels` (
  `id` int NOT NULL AUTO_INCREMENT,
  `level` tinytext COLLATE utf8_unicode_ci,
  `status` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bf_vision_room_allocation`
--
ALTER TABLE `bf_vision_room_allocation`
  ADD CONSTRAINT `room_allocation` FOREIGN KEY (`room_id`) REFERENCES `bf_vision_rooms` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
