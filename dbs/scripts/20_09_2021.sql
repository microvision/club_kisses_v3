alter table bf_vision_business_profile
    add mpesa_payment_type varchar(50) null;

alter table bf_vision_business_profile
    add mpesa_payment_number varchar(50) null;

create table bf_stores
(
    id          int auto_increment
        primary key,
    store_name  text              not null,
    description text              null,
    status      tinyint default 1 not null comment '1 Active, 0 Inactive',
    created_by  int               not null,
    created_on  datetime          not null,
    modified_by int               null,
    modified_on datetime          null
);

create table bf_stores_inventory
(
    id             int auto_increment
        primary key,
    item           text              not null,
    description    text              not null,
    measurement_id tinyint           not null,
    store_id       int               not null,
    status         tinyint default 1 not null comment '1 Active, 0, Incative',
    created_by     int               null,
    created_on     datetime          null,
    modified_by    int               null,
    modified_on    datetime          null
);

create table bf_stores_managers
(
    id       int auto_increment
        primary key,
    store_id int not null,
    user_id  int not null
);

create table bf_stores_measurement_units
(
    id               int auto_increment
        primary key,
    measurement_unit text              not null,
    status           tinyint default 1 not null comment '0:inactive, 1:active',
    created_by       int               not null,
    created_on       datetime          not null,
    modified_by      int               null,
    modified_on      datetime          null
);

create table bf_stores_order_items
(
    id           int auto_increment
        primary key,
    order_id     int               not null,
    item_id      int               not null,
    comments     text              null,
    order_qty    double            null,
    approved_qty double            null,
    received_qty double            null,
    status       tinyint default 1 not null comment '0:deleted, 1:active',
    created_by   int               not null,
    created_on   datetime          not null,
    modified_by  int               null,
    modified_on  datetime          null
);

create table bf_stores_orders
(
    id            int auto_increment
        primary key,
    order_date    date              not null,
    order_by      int               not null,
    approved_by   int               null,
    received_by   int               null,
    department_id int               not null,
    comments      text              null,
    status        tinyint default 1 not null comment '0:deleted 1:active 2: approved 3:received 4:Purchasing 5:Cancelled 6:Patial',
    created_by    int               not null,
    created_on    datetime          not null,
    modified_by   int               null,
    modified_on   datetime          null
);

create table bf_stores_purchase_order_items
(
    id                int auto_increment
        primary key,
    purchase_order_id int               not null,
    item_id           int               not null,
    order_qty         double            null,
    received_qty      double            null,
    purchase_date     date              null,
    approved_qty      double            null,
    approved_by       int               null,
    date_approved     datetime          null,
    status            tinyint default 1 not null,
    created_by        int               not null,
    created_on        datetime          not null,
    modified_by       int               null,
    modified_on       datetime          null
);

create table bf_stores_purchase_orders
(
    id            int auto_increment
        primary key,
    order_date    date              not null,
    order_by      int               not null,
    approved_by   int               not null,
    date_approved datetime          null,
    order_id      int               null,
    status        tinyint default 1 not null comment '0: deleted 1: active 2: delivered',
    created_by    int               not null,
    created_on    datetime          not null,
    modified_by   int               null,
    modified_on   datetime          null,
    type          tinyint default 1 not null comment '1--Normal/other inventory items
2--Pharmacy Items'
);

create table bf_stores_stocks
(
    id            int auto_increment
        primary key,
    store_id      int               not null,
    order_id      int               not null,
    product_id    int               not null,
    purchased_qty float             not null,
    current_qty   float             not null,
    comment       text              null,
    status        tinyint default 1 not null comment '1: active 0:deleted',
    created_by    int               null,
    created_on    datetime          null,
    modified_by   int               null,
    modified_on   datetime          null
);

create table bf_departments
(
    id          int auto_increment
        primary key,
    department  text              not null,
    status      tinyint default 1 not null comment '1:active, 0:inactive',
    created_by  int               not null,
    created_on  datetime          not null,
    modified_on int               null,
    modified_by datetime          null
);


alter table bf_users
    add department_id int null;

