function GetXmlHttpObject(handler) {
    var objXMLHttp = null
    if (window.XMLHttpRequest) {
        objXMLHttp = new XMLHttpRequest()
    } else if (window.ActiveXObject) {
        objXMLHttp = new ActiveXObject("Microsoft.XMLHTTP")
    }
    return objXMLHttp
}

function stateChanged() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("txtResult").innerHTML = xmlHttp.responseText;
    } else {
        //alert(xmlHttp.status);
    }
}

// Will populate data on the list box from based on the drop down selection
function htmlData(url, qStr) {
    if (url.length == 0) {
        document.getElementById("txtResult").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = url + "?" + qStr;
    url = url + "&sid=" + Math.random();
    xmlHttp.onreadystatechange = stateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

// Will the sms list selected
function htmlDatas(url, qStr) {
    if (url.length == 0) {
        document.getElementById("myAlert").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = url + "?" + qStr;
    url = url + "&sid=" + Math.random();
    xmlHttp.onreadystatechange = stateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

//script for populating the list boxes moving data from one list box to another
function SelectMoveRows(SS1, SS2) {
    var SelID = '';
    var SelText = '';
    // Move rows from SS1 to SS2 from bottom to top
    for (i = SS1.options.length - 1; i >= 0; i--) {
        if (SS1.options[i].selected == true) {
            SelID = SS1.options[i].value;
            SelText = SS1.options[i].text;
            var newRow = new Option(SelText, SelID);
            SS2.options[SS2.length] = newRow;
            SS1.options[i] = null;
        }
    }
    SelectSort(SS2);
}

function SelectSort(SelList) {
    var ID = '';
    var Text = '';
    for (x = 0; x < SelList.length - 1; x++) {
        for (y = x + 1; y < SelList.length; y++) {
            if (SelList[x].text > SelList[y].text) {
                // Swap rows
                ID = SelList[x].value;
                Text = SelList[x].text;
                SelList[x].value = SelList[y].value;
                SelList[x].text = SelList[y].text;
                SelList[y].value = ID;
                SelList[y].text = Text;
            }
        }
    }
}

function htmlData2(url, qStr) {
    if (url.length == 0) {
        document.getElementById("txtResult2").innerHTML = "";

        return;
    }
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = url + "?" + qStr;
    url = url + "&sid=" + Math.random();
    xmlHttp.onreadystatechange = stateChanged2;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function stateChanged2() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("txtResult2").innerHTML = xmlHttp.responseText;
    } else {
        //alert(xmlHttp.status);
    }
}


function htmlData3(url, qStr) {
    if (url.length == 0) {
        document.getElementById("txtResult3").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = url + "?" + qStr;
    url = url + "&sid=" + Math.random();
    xmlHttp.onreadystatechange = stateChanged3;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function stateChanged3() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("txtResult3").innerHTML = xmlHttp.responseText;
    } else {
        //alert(xmlHttp.status);
    }
}

function htmlData4(url, qStr) {
    if (url.length == 0) {
        document.getElementById("txtResult4").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = url + "?" + qStr;
    url = url + "&sid=" + Math.random();
    xmlHttp.onreadystatechange = stateChanged4;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function stateChanged4() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("txtResult4").innerHTML = xmlHttp.responseText;
    } else {
        //alert(xmlHttp.status);
    }

}

function htmlData5(url, qStr) {
    if (url.length == 0) {
        document.getElementById("txtResult5").innerHTML = "";
        return;
    }
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    url = url + "?" + qStr;
    url = url + "&sid=" + Math.random();
    xmlHttp.onreadystatechange = stateChanged5;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function stateChanged5() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        document.getElementById("txtResult5").innerHTML = xmlHttp.responseText;
    } else {
        //alert(xmlHttp.status);
    }

}


///lets deal with payments from here
let cash_total = 0;
let mpesa_total = 0;
$("#amount_paid_error").css('display', 'none');
$(document).on('click', 'a.payment_modes', function () {

    $("#amount_paid_error").css('display', 'none');
    var amount = $("input[name=number]").val();
    var bill_id = $("input[name=bill_id]").val();
    var bill_total = $("input[name=payments_bill_total]").val();
    let payment_type = $(this).attr('payment_type')
    if (!bill_total || bill_total<1) {
        $("#amount_paid_error").html("<strong>No Items Added to sale !!</strong>");
        $("#amount_paid_error").css('display', 'block');
        return;
    }
    if (!amount) {
        $("#amount_paid_error").html("<strong>Amount is Required !!</strong>");
        $("#amount_paid_error").css('display', 'block');
        return;
    }
    amount=parseInt(amount)
    let t = parseInt(cash_total + mpesa_total + amount);
    if (t > bill_total) {
        $("#amount_paid_error").html("<strong> Amount paid(" + t + ") would be greater than the bill total(" + bill_total + ")!!</strong>");
        $("#amount_paid_error").css('display', 'block');
        return;
    }
    if (payment_type === 'mpesa') {
        mpesa_total +=  amount;
    } else {
        cash_total += amount;
    }
    let span='<span  title="Remove Payment" payment_mode="'+payment_type+'" amount="'+amount+'" class="fa fa-times text-danger remove_receipt_payment_mode"></span>'
    let tr = "<tr><td>" + payment_type + "</td><td>" + amount + " </td><td>"+span+"</td></tr>";

    $("#payments_details").append(tr);
    tr = "<tr><td>Total</td><td colspan='2'><strong>" + parseInt(t) + "</strong></td></tr>";
    $("#payment_totals").html(tr);

    if(t==bill_total){
        let url =receipt_url+"?billid="+bill_id+"&button=FISCAL RECEIPT&print_receipt=0&cash_amount="+cash_total+"&mpesa_amount="+mpesa_total;
        let btnReceipt='<a href="'+url+'"  class="btn btn-danger btn-block  "   onclick ="return popitup('+url+')">Sale-Receipt </a>'
        url =receipt_url+"?billid="+bill_id+"&button=FISCAL RECEIPT&print_receipt=1&cash_amount="+cash_total+"&mpesa_amount="+mpesa_total;
        let btnReceiptPrint='<a href="#" url="'+url+'"  class="btn btn-danger btn-block btnReceipts"   onclick ="return popitup('+url+')">Sale-Receipt Print </a>'
        $("#payment_footer").html(btnReceipt+"<br>"+btnReceiptPrint)

    }




number_clear();
});
$(document).on('click', 'a.btnReceipts', function () {
 let url=$(this).attr("url")
popitup(url)
})

$(document).on('click', 'span.remove_receipt_payment_mode', function () { // <-- changes
    $(this).closest('tr').remove();
    $("#amount_paid_error").css('display', 'none');
    $("#payment_footer").html("")
    let payment_mode=$(this).attr("payment_mode");
    let amount=$(this).attr("amount");
    amount=parseInt(amount)
    if (payment_mode === 'mpesa') {
        mpesa_total -=  amount;
    } else {
        cash_total -= amount;
    }
  let  tr = "<tr><td>Total</td><td colspan='2'><strong>" + parseInt(cash_total+mpesa_total) + "</strong></td></tr>";
    $("#payment_totals").html(tr);
    return false;
});
