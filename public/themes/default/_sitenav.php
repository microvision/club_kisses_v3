<!--/sidebar-menu-->
<div class="sidebar-menu">
    <header class="logo1">
        <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
    </header>
    <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
    <div class="menu">
        <ul id="menu" >
            <li id="menu-academico" ><a href="#"><i class="fa fa-shopping-cart"></i> <span> Signal POS</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                <ul id="menu-academico-sub" >
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>">Make Sale</a></li>
                    <?php if(has_permission('Vision.Rooms.View')):?>
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>rooms/reception">Rooms Reception</a></li>
                    <?php endif; ?>
<!--                     <li id="menu-academico-avaliacoes" ><a href="<?php //echo base_url(); ?>home/manual_receipt">Manual Sale</a></li>-->
                    <?php if(has_permission('Vision.General.Sales_billing')):?>
                       <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>home/cashier">Receive Payment</a></li>
                    <?php endif; ?>
                    <?php if(has_permission('Vision.Events.View')):?>
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>promotions">Promotions/Events</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php if(has_permission('Vision.Catalog.View')):?>
                <li id="menu-academico" ><a href="#"><i class="fa fa-book"></i> <span> Inventory</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>catalog">Catalog</a></li>
                        <?php if(has_permission('Vision.Catalog.Create_product')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>catalog/new_product">New Product</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if(has_permission('Vision.Rooms.View')):?>
                <li id="menu-academico" ><a href="#"><i class="fa fa-building-o"></i> <span> Hotel Rooms</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>rooms">Hotel Rooms</a></li>
                        <?php if(has_permission('Vision.Rooms.Create_room')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>rooms/new_room">New Hotel Room</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Rooms.Manage_rooms')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>rooms/categories">Room Categories</a></li>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>rooms/levels">Room Levels</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if(has_permission('Vision.Stocks.View')):?>
                <li id="menu-academico" ><a href="#"><i class="fa fa-stack-overflow"></i> <span>Shop Stocks</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub" >
                        <?php if(has_permission('Vision.Stocks.Edit_stocks')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>stocks/shop_stock">Stocks in Shop</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Stocks.Store_orders')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>">Order From Store</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Stocks.Purchase_order')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>stocks/purchase_orders">Receive Stock</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if(has_permission('Vision.Stores.View')):?>

                <li id="menu-academico" ><a href="#"><i class="fa fa-bars"></i> <span>Inventory </span> <span class="fa fa-angle-right" style="float: right"></span></a>


                    <ul>

                        <li><a href="<?php echo base_url()?>stores/orders/store_orders"> Store Orders </a></li>
                        <?php if(has_permission('Vision.Finance.Petty_cash_view')): ?>
                            <?php if(has_permission('Vision.Finance.Petty_cash_approve')): ?>
                                <li><a href="<?php echo base_url()?>stores/petty_cash/petty_cash_approvals"> Petty Cash Requests </a></li>
                            <?php else: ?>
                                <li><a href="<?php echo base_url()?>stores/petty_cash"> Petty Cash Requests </a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Stores.Supervise')):?>
                            <li><a href="<?php echo base_url()?>stores/orders/approve_store_orders"> Approve Orders </a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Stores.Manage')):?>
                            <li><a href="<?php echo base_url()?>stores/orders/dispense_store_orders"> Dispense Orders </a></li>
                            <li><a href="<?php echo base_url()?>stores/requisitions/index/"> Purchase Requisitions </a></li>
<!--                            <li><a href="--><?php //echo base_url()?><!--stores/requisitions/approve_order/1"> Approve  Requisitions </a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url()?><!--stores/lpo/index"> Local Purchase Order </a></li>-->
                            <li><a href="<?php echo base_url()?>stores/store_stock"> Stores Stock</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Stores.Admin')):?>
                            <li><a href="<?php echo base_url()?>stores/manage"> Manage Stores</a></li>
                            <li><a href="<?php echo base_url()?>stores/inventory"> Stores Inventory</a></li>
                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>
            <!--										<li id="menu-academico" ><a href="#"><i class="fa fa-shopping-cart"></i> <span>Store Stocks</span> <span class="fa fa-angle-right" style="float: right"></span></a>-->
            <!--											<ul id="menu-academico-sub" >-->
            <!--												<li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--stores/purchase_orders">Purchase to Store</a></li>-->
            <!--												<li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--stores/shop_orders">Shop Orders</a></li>-->
            <!--												<li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--stores/store_stock">Stocks in Store</a></li>-->
            <!--											</ul>-->
            <!--										</li>-->
            <li id="menu-academico" ><a href="#"><i class="fa fa-sitemap"></i> <span>Distributors</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                <ul id="menu-academico-sub" >
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>distribution/distributors">Manage Distributors</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>distribution/distributors/distributor_statements">Distributors Statements</a></li>
                </ul>
            </li>
            <?php if(has_permission('Vision.Clients.View')):?>
                <li id="menu-academico" ><a href="#"><i class="fa fa-users"></i> <span>Clients</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>clients">Corporate Clients</a></li>
                        <?php if(has_permission('Vision.Clients.Reports')):?>
                            <!--                                                <li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--">General Aging Report</a></li>-->
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>clients/statement_of_account">Statement of Accounts</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if(has_permission('Vision.Suppliers.View')):?>
                <li id="menu-academico" ><a href="#"><i class="fa fa-truck"></i> <span>Suppliers</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub" >
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>suppliers">Manage Suppliers</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>suppliers/supplier_statements">Supplier Statements</a></li>
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>suppliers/supplier_history">Supplier History</a></li>
                        <!--												<li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--">Supply History</a></li>-->
                        <!--												<li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--">Supplies Summary</a></li>-->
                        <!--												<li id="menu-academico-avaliacoes" ><a href="--><?php //echo base_url(); ?><!--">Supplies Journal</a></li>-->
                    </ul>
                </li>
            <?php endif;?>


            <li id="menu-academico" ><a href="#"><i class="fa fa-tasks"></i> <span>Reports</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                <ul id="menu-academico-sub" >
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/product_sales">Product Sales</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/sales_report">Sales Report</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/manual_sales_report">Manual Receipt Sales</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/sales_categories">Sales Categories</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/user_sales">User Sales Report</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/purchases_report">Purchases Report</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/business_summary">Business Report</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/grouped_sales_report">Grouped Sales</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/expenses_accounts">Expenses Accounts</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>reports/rooms_report">Rooms Report</a></li>
                </ul>
            </li>
            <?php if(has_permission('Vision.Setting.View')):?>
                <li id="menu-academico" ><a href="#"><i class="fa fa-gears"></i> <span>General Settings</span> <span class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub" >
                        <?php if(has_permission('Vision.Setting.View_Users')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>setting/users/user_accounts">User Accounts</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Setting.Units_View')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>setting/units">Units of Sale</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Setting.View_categories')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>setting/categories">Product Categories</a></li>
                        <?php endif; ?>
                        <?php if(has_permission('Vision.Setting.Business_profile')):?>
                            <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>setting/business_profile">Business Profile</a></li>
                        <?php endif; ?>
                        <li id="menu-academico-avaliacoes" ><a href="<?php echo base_url(); ?>setting/backup">Backup Databases</a></li>
                    </ul>
                </li>
            <?php endif;?>
        </ul>
        </li>
        </ul>
    </div>
</div>

