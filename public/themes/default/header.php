<?php

Assets::add_css(array('bootstrap-responsive.min.css','style.css','font-awesome.css','icon-font.min.css','jPages.css','keypad.css','lib/uniform/themes/default/css/uniform.default.css','lib/chosen/chosen.min.css','bootstrap.min.css','dataTables.bootstrap.min.css',));

Assets::add_js(array('bootstrap.min.js','simpleCart.min.js','amcharts.js','serial.js','light.js','jquery-1.10.2.min.js','jPages.min.js','jquery.nicescroll.js','scripts.js','bootstrap.min.js','jquery.flot.js','menu_jquery.js','ajax_req.js','keypad.js',
'lib/uniform/jquery.uniform.min.js','lib/inputlimiter/jquery.inputlimiter.1.3.1.min.js','lib/chosen/chosen.jquery.min.js','lib/tagsinput/jquery.tagsinput.min.js','lib/switch/static/js/bootstrap-switch.min.js','lib/autosize/jquery.autosize.min.js','js/main.min.js','jquery.dataTables.min.js','dataTables.bootstrap.min.js'));

//$inline  = '$(".dropdown-toggle").dropdown();';
//$inline .= '$(".tooltips").tooltip();';
//Assets::add_js($inline, 'inline');
$today = date("Y-m-d");
$year=date('Y');
$month=date('m');
$month = $month+1;
$db = $this->load->database('default', TRUE);
$reorder_items = $db->query("SELECT bf_vision_products.id,product_name, Stocks, reorder_level, bf_vision_product_categories.name as category_name FROM bf_vision_products
										LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id=category_id
										RIGHT JOIN (SELECT SUM(current_qty) as Stocks,product_id FROM bf_vision_stock_shop GROUP BY product_id) as stocked ON stocked.product_id=bf_vision_products.id 										
										WHERE Stocks<reorder_level and bf_vision_products.status=1");
$reorder_count = $db->query("SELECT count(*) as numbers FROM bf_vision_products 
                                        RIGHT JOIN (SELECT SUM(`current_qty`) as Stocks,`product_id` FROM bf_vision_stock_shop GROUP BY product_id) as stocked ON stocked.product_id=bf_vision_products.id 
                                        WHERE Stocks<reorder_level and bf_vision_products.status=1");
$expiry_count = $db->query("SELECT COUNT(*) as numbers FROM bf_vision_stock_shop WHERE YEAR(expiry_date) <='".$year."' and MONTH(expiry_date)<='".$month."' and current_qty>0 GROUP BY product_id");
//$expiry_items = $db->query("SELECT bf_vision_products.id,product_name, bf_vision_product_categories.name as category_name, batch_no,expiry_date,current_qty,purchased_date FROM bf_vision_products
//										LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id=category_id
//										RIGHT JOIN (SELECT bf_vision_stock_shop.* FROM bf_vision_stock_shop WHERE expiry_date > date_sub(now(), interval 1 month) and current_qty>0) as expired ON expired.product_id=bf_vision_products.id
//										WHERE bf_vision_products.status=1");
$expiry_items = $db->query("SELECT  bf_vision_products.id,product_name, bf_vision_product_categories.name as category_name, batch_no,expiry_date,current_qty,purchased_date FROM bf_vision_stock_shop
										LEFT JOIN bf_vision_products ON bf_vision_stock_shop.product_id=bf_vision_products.id
										LEFT JOIN bf_vision_product_categories ON bf_vision_product_categories.id=category_id										 										
										WHERE YEAR(expiry_date) <='".$year."' and MONTH(expiry_date)<='".$month."' and current_qty>0 and bf_vision_products.status=1");

$reorder_items  = $reorder_items ->result();
$reorder_count = $reorder_count->row();
$expiry_count = $expiry_count->row();
$expiry_items = $expiry_items->result();
?>
<!doctype html>

<!--
Author: Microvision
Author URL: http://microvision.co.ke
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Signal POS | <?php echo $page_title;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Signal POS v3, Microvision Software Technologies, Point Of Sale, Inventory Management" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<?php
$receipt_url = base_url()."home/receipt";
    /* Modernizr is loaded before CSS so CSS can utilize its features */
    echo Assets::js('modernizr-2.5.3.js');
    echo Assets::js('jquery-3.3.1.js');
    ?>
    <?php echo Assets::css(); ?>
<!--    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen">-->
<!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" media="screen">-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
<!-- jQuery -->
    <script src="<?php echo Template::theme_url('js/jquery_excel.js');?>"></script>

    <script >
        $(document).ready(function() {
            $("#btnExport").click(function(e) {
                e.preventDefault();

                //getting data from our table
                var data_type = 'data:application/vnd.ms-excel';
                var table_div = document.getElementById('table_wrapper');
                var table_html = table_div.outerHTML.replace(/ /g, ' ');
                var csvData = new Blob([table_html], { type: 'application/vnd.ms-excel' });
                var csvUrl = URL.createObjectURL(csvData);

                var a = document.createElement('a');
                a.href = csvUrl;
                //a.href = data_type + ', ' + table_html;
                a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
                a.click();
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
        function popitup(url) {

            //creating a cookie that will store the current active tab and to be used by php
            var activetab = window.location.hash.substring(1);
            document.cookie = 'activetab=' + activetab;

            //alert(activetab);

            newwindow=window.open(url,'name','height=540,width=340,top=50,left=500');
            if (window.focus) {newwindow.focus()}
            return false;
        }
        function popitup2(url) {


            newwindow=window.open(url,'name','height=540,width=340,top=50,left=500');
            if (window.focus) {newwindow.focus()}
            return false;
        }
        let receipt_url='<?php echo $receipt_url ?>';
    </script>
    <script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-1.7.2.min.js"><\/script>');</script>
</head> 
<body>

<div class="page-container sidebar-collapsed"><!-- Start of Main Container -->
    <!--/content-inner-->
    <div class="left-content">
        <div class="inner-content">
            <!-- header-starts -->
            <div class="header-section">
                <!-- top_bg -->
                <div class="top_bg">

                    <div class="header_top">
                        <div class="top_right">
                            <ul>
                                <?php if(has_permission('Vision.Distribution.View')): ?>
                                <li><a href="<?php echo base_url(); ?>distribution">Distribution</a></li>|
                                <?php endif; ?>
                                <?php if(has_permission('Vision.Expenses.View')): ?>
                                <li><a href="<?php echo base_url(); ?>expenses/new_expense">Petty Cash</a></li>|
                                <?php endif; ?>
                                <?php if(has_permission('Vision.Setting.View')):?>
                                <li><a href="#receipt_search" data-toggle="modal">Printed Receipts</a></li> ::.
                                <?php if(ISSET($reorder_count->numbers)):?>
                                <li><a href="#reorder" data-toggle="modal" class="btn btn-small btn-danger"><i class="fa fa-warning"></i> Reorder Alert (<?php echo $reorder_count->numbers; ?> Products)</a></li>
                                <?php endif;?>
                                <?php if(ISSET($expiry_count->numbers)):?>
                                <li><a href="#expiry" data-toggle="modal" class="btn btn-small btn-warning"><i class="fa fa-warning"></i> Expiry Notification (<?php echo $expiry_count->numbers; ?> Products)</a></li>
                                <?php endif;?>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="top_left">
                            <h2><span></span> <?php echo $current_user->display_name; ?> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>logout"  class="text-danger "><i class="fa fa-power-off fa-lg"></i></a></h2>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <!-- /top_bg -->
            </div>


            <!-- #reorder level model -->
            <div id="reorder" class="modal fade" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Signal POS - Reorder Level Items</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                            <table id="dataTable" class="table table-bordered table-hover">
                                <thead>
                                <tr>

                                    <th>Product</th>
                                    <th>Category</th>
                                    <th>Stock</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $num=0; foreach($reorder_items as $row): $num++; ?>
                                    <tr>

                                        <td><?php echo $row->product_name; ?></td>
                                        <td><?php echo $row->category_name; ?></td>
                                        <td><?php echo $row->Stocks; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal --><!-- /#helpModal -->

            <!-- #expiry level model -->
            <div id="expiry" class="modal fade" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Signal POS - Expiry Notification Items</h4>
                        </div>
                        <div class="modal-body">
                            <p>
                            <table id="dataTable" class="table table-bordered table-hover">
                                <thead>
                                <tr>

                                    <th>Product</th>
                                    <th>Category</th>
                                    <th>Batch No:</th>
                                    <th>Purchase Date</th>
                                    <th>Expiry Date</th>
                                    <th>Current Stock</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $num=0; foreach($expiry_items as $row): $num++; ?>
                                    <tr>

                                        <td><?php echo $row->product_name; ?></td>
                                        <td><?php echo $row->category_name; ?></td>
                                        <td><?php echo $row->batch_no; ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($row->purchased_date)); ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($row->expiry_date)); ?></td>
                                        <td><?php echo $row->current_qty; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal --><!-- /#helpModal -->