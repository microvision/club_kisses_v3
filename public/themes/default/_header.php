<?php

Assets::add_css(array( 'bootstrap-responsive.min.css','keypad.css','lib/chosen/chosen.min.css','bootstrap.min.css','loginfonts.css','googleapilogin.css'));

Assets::add_js(array('jquery-3.3.1.js','simpleCart.min.js','bootstrap.min.js','keypad.js','lib/chosen/chosen.jquery.min.js','js/main.min.js','ajax_req.js'));


?>
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>-->
<!doctype html>

<!--
Author: Microvision
Author URL: http://microvision.co.ke
-->
<!DOCTYPE HTML>
<html>
<head>
    <?php echo Assets::js(); ?>
<title>Signal POS | Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Signal POS, Microvision Software Technologies, Point Of Sale, Inventory Management, Distribution" />
    <?php
    /* Modernizr is loaded before CSS so CSS can utilize its features */
    echo Assets::js('modernizr-2.5.3.js');
    ?>
<?php echo Assets::css(); ?>
    <script>
        $(".user").focusin(function(){
            $(".inputUserIcon").css("color", "#e74c3c");
        }).focusout(function(){
            $(".inputUserIcon").css("color", "white");
        });

        $(".pass").focusin(function(){
            $(".inputPassIcon").css("color", "#e74c3c");
        }).focusout(function(){
            $(".inputPassIcon").css("color", "white");
        });
    </script>
    <style type="text/css">

        /* zocial */
        [class*="entypo-"]:before {
            font-family: 'entypo', sans-serif;
        }

        *,
        *:before,
        *:after {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }


        h2 {
            color:rgba(255,255,255,.8);
            margin-left:12px;
        }
        h3 {
            color:rgba(249, 4, 4, 0.8);
            font-size: large;
            font-family: Arial;
            line-height: 1.1;
            font-weight: 500;
            width: 50%;
            position: relative;
            margin: 50px auto;
            background-color: rgba(255, 243, 233, 0.8);
            padding: 15px;
            padding-right: 35px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        h5 {
            color:rgba(255,255,255,.8);

            font-size: small;
            font-family: "courier new", courier, typewriter, monospace;
        }
        body {
            background: #000000;
            font-family: 'Roboto', sans-serif;
            background: url('<?php echo Template::theme_url('images/pos_bg3.jpg');?>')no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }


        input {
            padding: 16px;
            border-radius:7px;
            border:0px;
            background: rgba(255,255,255,.2);
            display: block;
            margin: 0px;
            width: 300px;
            color:white;
            font-size:18px;
            height: 54px;
        }

        input:focus {
            outline-color: rgba(0,0,0,0);
            background: rgba(255,255,255,.95);
            color: #e74c3c;
        }

        #butto {
            float:right;
            height: 121px;
            width: 50px;
            border: 0px;
            background: #e74c3c;
            border-radius:7px;
            padding: 10px;
            color:white;
            font-size:22px;
        }

        .inputUserIcon {
            position:absolute;
            top:68px;
            right: 80px;
            color:white;
        }

        .inputPassIcon {
            position:absolute;
            top:136px;
            right: 80px;
            color:white;
        }

        input::-webkit-input-placeholder {
            color: white;
        }

        input:focus::-webkit-input-placeholder {
            color: #e74c3c;
        }
        footer {
            bottom: 0%;
            position: fixed;
            width: 100%;
            align-content: center;
        }
        a {
            color: #e74c3c;;
        }
        alert{
            background-color: #fbfafb;
        }
        #loginform {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 0;
            top: 47%;
            left: 25%;
            margin: 0 auto;
        }
        button.close {
            -webkit-appearance: none;
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
        }
        .close {
            float: right;
            font-size: 21px;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            filter: alpha(opacity=20);
            opacity: .2;
        }
    </style>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
<!-- jQuery -->

    <!-- /.Java Script for th keypad -->
    <script type="text/javascript">
        function number_write(x)
        {
            var text_box = document.getElementById("number");
            if(x>=0 && x<=9)
            {
                if(isNaN(text_box.value))
                    text_box.value = 0;
                text_box.value = (text_box.value * 10)+x;
            }
        }
        function number_clear()
        {
            document.getElementById("number").value = 0;
        }
        function number_c()
        {
            var text_box = document.getElementById("number");
            var num = text_box.value;
            var num1 = num%10;
            num -= num1;
            num /= 10;
            text_box.value = num;
        }

    </script>
    <script type="text/javascript">
        function number_writes(y)
        {
            var text_box = document.getElementById("password");
            if(y>=0 && y<=9)
            {
                if(isNaN(text_box.value))
                    text_box.value = 0;
                text_box.value = (text_box.value * 10)+y;
            }
        }
        function number_clears()
        {
            document.getElementById("password").value = 0;
        }
        function number_cs()
        {
            var text_box = document.getElementById("password");
            var num = text_box.value;
            var num1 = num%10;
            num -= num1;
            num /= 10;
            text_box.value = num;
        }

    </script>

    <style type="text/css">
        .main_panel
        {
            width:290px;
            height:430px;
            background-color:#333333;
            border-top-right-radius:15px;
            border-top-left-radius:15px;
            border-bottom-right-radius:15px;
            border-bottom-left-radius:15px;
            padding:23px;

        }
        .number_button
        {
            width:60px;
            height:60px;
            margin:10px;
            float:left;
            background-color:#CCCCCC;
            border-top-right-radius:9px;
            border-top-left-radius:9px;
            border-bottom-right-radius:9px;
            border-bottom-left-radius:9px;
            font-size:32px;
            font-color:#000000;
            text-align:center;
        }
        .number_button:hover
        {
            background-color:#666666;
        }
        .text_box
        {
            width:250px;
            height:30px;
            font-size:24px;
            text-align:right;
        }
        .style4 {width: 70px; height: 70px; margin: 10px; float: left; background-color: #000000; border-top-right-radius: 20px; border-top-left-radius: 20px; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px; font-size: 36px; font-color: #FFFFFF; text-align: center; color: #FFFFFF; }
    </style>
    <script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-1.7.2.min.js"><\/script>');</script>
</head>

<body background="">

