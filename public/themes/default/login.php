<?php echo theme_view('_header'); ?>

<div class="container"><!-- Start of Main Container -->
    <?php
    echo isset($content) ? $content : Template::content();

    echo theme_view('_footer', array('show' => false));
?>