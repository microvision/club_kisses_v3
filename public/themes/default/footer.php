<!-- #helpModal -->
<div id="receipt_search" class="modal fade" data-backdrop="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" id="receipt_search" name="receipt_search" method="post" action="<?php echo base_url(); ?>home/receipt_search">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Search Receipt</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-lg-4">Enter Receipt Number</label>
                        <div class="col-lg-6">
                            <input type="number" class="form-control" name="receipt_number" id="receipt_number">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="Search Receipt" name="submit" class="btn btn-danger">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --><!-- /#helpModal -->


<div class="footer">
	<div class="clearfix"> <hr></div>
		<p>© <?php echo date('Y'); ?> Signal POS 3.0  All Rights Reserved | Developed by <a href="http://microvision.co.ke/">Microvision Software Technologies</a></p>
</div>
<?php echo theme_view('_sitenav'); ?>
<div class="clearfix"></div>
</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
<script>
    function mover(element_id){

        document.getElementById("w"+element_id).style.display=""
    }
    function mout(element_id) {
        document.getElementById("w"+element_id).style.display="none"
    }
    // document.getElementById('showhide').onmouseover=mover;
    // document.getElementById('showhide').onmouseout=mout;
</script>
<script>
	var toggle = true;

	$(".sidebar-icon").click(function() {
        if (toggle){
            $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
            $("#menu span").css({"position":"absolute"});
        }else{
            $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
            setTimeout(function() {
              $("#menu span").css({"position":"relative"});
            }, 400);
        }
	    toggle = !toggle;
	});

</script>

    <?php echo Assets::js(); ?>
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>-->
<script>
    $(function() {
        formGeneral();
    });
    $(document).ready(function() {
        $("input, textarea, select").not('.nostyle').uniform();
    });
</script>
</body>
</html>