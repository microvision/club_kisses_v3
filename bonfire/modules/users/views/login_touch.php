<?php
	$site_open = $this->settings_lib->item('auth.allow_register');
?>


<div >
<!--    <h3>--><?php //echo lang('us_login'); ?><!--</h3>-->
    <img src="<?php echo Template::theme_url('images/logo_light.png');?>" align="right" class="img-responsive" alt="">
        <?php //echo Template::message();
            $msg = Template::message();
            if($msg){
                echo "<h3>".$msg."</h3>";
            }
        ?>

    <?php
    if (validation_errors()) :

        ?>
        <h3>
        <div class="row-fluid" >
            <div class="span12">
                <div class="alert alert-danger fade in">
                    <a data-dismiss="alert" class="close">&times;</a>
                    <?php echo validation_errors(); ?>
                </div>
            </div>
        </div></h3>
    <?php endif; ?>
    <div id="loginform">



    <?php echo form_open(LOGIN_URL, array('autocomplete' => 'off')); ?>

    <h2><span class="entypo-login"><i class="fa fa-sign-in"></i></span> Login</h2>
    <button class="submit" id="butto"><span class="entypo-lock"><i class="fa fa-lock"></i></span></button>
    <input type="hidden" name="log-me-in" value="<?php e(lang('us_let_me_in')); ?>">
    <div class="control-group <?php echo iif( form_error('login') , 'error') ;?>">
        <span class="entypo-user inputUserIcon"> <i class="fa fa-user"></i> </span>
        <input type="text" class="user" name="login" id="login_value" value="<?php echo set_value('login'); ?>" tabindex="1" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>"/>
    </div>
    <div class="control-group <?php echo iif( form_error('password') , 'error') ;?>">
        <span class="entypo-key inputPassIcon"><i class="fa fa-key"></i> </span>
        <input type="password" class="pass" name="password" id="password" value="" tabindex="2" placeholder="<?php echo lang('bf_password'); ?><?php echo iif( form_error('password') , 'error') ;?>"/>
    </div>



    <?php echo form_close(); ?>
    </div>
    <?php // show for Email Activation (1) only
    if ($this->settings_lib->item('auth.user_activation_method') == 1) : ?>
        <!-- Activation Block -->
        <p style="text-align: left" class="well">
            <?php echo lang('bf_login_activate_title'); ?><br />
            <?php
            $activate_str = str_replace('[ACCOUNT_ACTIVATE_URL]',anchor('/activate', lang('bf_activate')),lang('bf_login_activate_email'));
            $activate_str = str_replace('[ACTIVATE_RESEND_URL]',anchor('/resend_activation', lang('bf_activate_resend')),$activate_str);
            echo $activate_str; ?>
        </p>
    <?php endif; ?>

    <p style="text-align: center">
        <?php if ( $site_open ) : ?>
            <?php //echo anchor(REGISTER_URL, lang('us_sign_up')); ?>
        <?php endif; ?>

        <br/><?php //echo anchor('/forgot_password', lang('us_forgot_your_password')); ?>
    </p>

</div>
<!-- #session start modal -->
<div id="txtResult3" class="modal fade in">  </div>