
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
$submit_url = base_url()."pos/sell_product";

$userName = $_GET['ch2'];

	$session_url = base_url()."login";
	$userimageLink = Template::theme_url('images/user.png');
	
	echo <<<eod
			
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">$userName Enter Password to Start</h4>
				  </div>
				  <form method="POST"  name="session" id="session" action="$session_url" autocomplete="off" accept-charset="utf-8">
				  <div class="modal-body">
				  	<div class="wizard-steps clearfix"><br></div>
				  	<div class="row">
						<div class="col-lg-3">
							<img class="media-object img-thumbnail user-img" alt="Start_Session"  src="$userimageLink">	
						</div>					
						<div class="col-lg-7">
							<div class="main_panel">
								<center>
								<input class="form-control" type="password" id="password"  required="" data-placement="top" name="password">
								<br />							
								<div class="number_button" onClick="number_writes(1);">1</div>
								<div class="number_button" onClick="number_writes(2);">2</div>
								<div class="number_button" onClick="number_writes(3);">3</div>
								<div class="number_button" onClick="number_writes(4);">4</div>
								<div class="number_button" onClick="number_writes(5);">5</div>
								<div class="number_button" onClick="number_writes(6);">6</div>
								<div class="number_button" onClick="number_writes(7);">7</div>
								<div class="number_button" onClick="number_writes(8);">8</div>
								<div class="number_button" onClick="number_writes(9);">9</div>
								<div class="number_button" onClick="number_clears();">Clre</div>
								<div class="number_button" onClick="number_writes(0);">0</div>
								<div class="number_button" onClick="number_cs();">C</div>
								<br >  
								</center>	
							</div>	
				  		</div>
				  	</div>
				  </div>
				  <div class="modal-footer">
					<input type="hidden" name="login" value="$userName">
					<input type="hidden" name="log-me-in" value="Sign In">
					<button class="btn btn-lg btn-warning btn-lg pull-left" type="submit" name="submit" value="submit"><span class="text-default">&nbsp;<i class="fa fa-unlock"></i>&nbsp;Open Session</span></button>
					<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>						  
				  </div>
				  </form>
				</div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			
eod;
	
	
?>
