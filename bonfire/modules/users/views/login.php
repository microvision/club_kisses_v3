<?php
	$site_open = $this->settings_lib->item('auth.allow_register');
?>


<div >

    <div class="row">
        <div class="col-lg-12">
            <!--    <h3>--><?php //echo lang('us_login'); ?><!--</h3>-->
            <img src="<?php echo Template::theme_url('images/logo_light.png');?>" align="right" class="img-responsive" alt="">
            <?php //echo Template::message();
            $msg = Template::message();
            if($msg){
                echo "<h3>".$msg."</h3>";
            }
            ?>

            <?php
            if (validation_errors()) :

                ?>
                <h3>
                    <div class="row-fluid" >
                        <div class="span12">
                            <div class="alert alert-danger fade in">
                                <a data-dismiss="alert" class="close">&times;</a>
                                <?php echo validation_errors(); ?>
                            </div>
                        </div>
                    </div></h3>
            <?php endif; ?>
            <br><br><br><hr><br><br>
        </div>
            <?php $num=0; foreach($active_sessions as $row): $num++;?>
                <div class="col-lg-3" align="center">
                    <p><a href="#txtResult3" class="text-muted"  data-original-title="session start" data-toggle="modal" title="Start Session"  >

                            <button type="button" class="btn btn-lg btn-danger btn-block" data-toggle="modal" href="#txtResult" onclick="htmlData('<?php echo base_url();?>users/session_login', 'ch=<?php echo $row->id; ?>&ch2=<?php echo $row->username; ?>')" >

                                <img src="<?php echo Template::theme_url('images/log_user.png'); ?>" width="45%" align="center" alt="login">
                                <h4 align="center"><?php echo $row->display_name; ?></h4>

                            </button>
                        </a>
                    </p>
                </div>
                <?php echo ($num==4)?"</div><div class=\"row\">" : ""; endforeach; ?>
        <div class="col-lg-12"><br><hr></div>

    </div>
    <?php // show for Email Activation (1) only
    if ($this->settings_lib->item('auth.user_activation_method') == 1) : ?>
        <!-- Activation Block -->
        <p style="text-align: left" class="well">
            <?php echo lang('bf_login_activate_title'); ?><br />
            <?php
            $activate_str = str_replace('[ACCOUNT_ACTIVATE_URL]',anchor('/activate', lang('bf_activate')),lang('bf_login_activate_email'));
            $activate_str = str_replace('[ACTIVATE_RESEND_URL]',anchor('/resend_activation', lang('bf_activate_resend')),$activate_str);
            echo $activate_str; ?>
        </p>
    <?php endif; ?>

    <p style="text-align: center">
        <?php if ( $site_open ) : ?>
            <?php //echo anchor(REGISTER_URL, lang('us_sign_up')); ?>
        <?php endif; ?>

        <br/><?php //echo anchor('/forgot_password', lang('us_forgot_your_password')); ?>
    </p>

</div>
<!-- #session start modal -->
<div id="txtResult" class="modal fade">  </div>

